setInterval(refreshToken, 3600000); // 1 hour 

function refreshToken()
{
	$.get('/recargar-csrf').done(function(data)
	{
		$('[name="_token"]').val(data); // the new token
	});

	setInterval(refreshToken, 3600000); // 1 hour 
}