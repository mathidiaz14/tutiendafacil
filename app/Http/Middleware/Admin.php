<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use View;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check())
        {
            if(Auth::user()->tipo == "admin")
            {
                if(Auth::user()->empresa->estado == "deshabilitado")
                    return response()->view('auth.empresa_deshabilitada');

                Auth::user()->update(['ultima_actividad' => now()]);

                return $next($request);

            }elseif(Auth::user()->tipo == "root")
            {
                return redirect('root');
            }
        }
        
        return redirect('login?redirect='.$request->url());
    }
}
