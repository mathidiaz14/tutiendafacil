<?php

namespace App\Http\Middleware\Plugins;

use Closure;
use Illuminate\Http\Request;

class Inmobiliaria
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $plugin = empresa()->plugins->where('carpeta', 'Inmobiliaria')->first();
        
        if((isset($plugin)) and ($plugin->pivot->estado == "activo"))
            return $next($request);

        return back();
    }
}
