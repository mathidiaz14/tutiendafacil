<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Empresa;
use App\Models\Configuracion;
use App\Models\Codigo;
use Carbon\Carbon;
use Auth;
use File;
use Session;
use MercadoPago;
use Hash;

class ControladorRegistroYPlanes extends Controller
{

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials, $request->remember)) 
        {
            registro(
                "login",
                usuario(),
                usuario('empresa'),
                "Login",
                "Login",
                "El usuario inicio sesión"
            );

            if($request->redirect != null)
                return redirect($request->redirect);
                
            return redirect('admin'); 
        
        }

        error('Usuario o contraseña incorrectos');

        return back()->withInput();
    }
    
    public function ver_reseteo_contrasena_envio()
    {
        return view('auth.forgot-password');
    }

    public function reseteo_contrasena_envio(Request $request)
    {
    	$user = User::where('email', $request->email)->first();

        if($user == null)
        {
            error("El email que ingreso no se encuentra en nuestra base de datos.");
            return back();
        }

    	$token = codigo_aleatorio(30);
    	
    	$user->reset_password          = $token;
    	$user->reset_password_expire   = Carbon::now()->addDay();
    	$user->save();

    	$contenido = [
            "titulo" => "Reseteo de contraseña",
            "url" => url('resetear/contrasena', $token),
        ];
    	
    	email("contrasena.reset", "Reseteo de contraseña - Mathias.uy", $contenido, $user->email);
    	
        registro(
            "info",
            $user->id,
            $user->empresa_id,
            "Login",
            "Contraseña",
            "Se solicito el reseteo de contraseña"
        );

    	exito("Se envió un email para el reseteo de la contraseña, el link será valido por 24 horas.");
    	return back();
    }
    
    public function ver_reseteo_contrasena($id)
    {
    	$user = User::where('reset_password', $id)->first();

    	if($user == null)
        {
    		error('Error al mostrar pagina');
            return redirect('login');
        }

    	if(Carbon::now()->diffInHours($user->reset_password_expire) <= 0)
    	{
    		error("El link que utilizo ya no está disponible, realice nuevamente el procedimiento para resetear la contraseña.");
    		return view('auth.forgot-password');
    	}

    	return view('auth.reset-password', compact('user'));
    }

    public function reseteo_contrasena(Request $request)
    {
    	$user = User::find($request->user);
    	
    	if($user == null)
        {
            error('Error al resetear contraseña');
            return view('auth.login');
        }

        if($request->password != $request->re_password)
        {
            error('Las contraseñas no coinciden, intente de nuevo por favor.');
            return back();
        }

        if(strlen($request->password) < 8)
        {
            error('La contraseña debe tener al menos 8 caracteres.');
            return back();   
        }

        $user->password                 = Hash::make($request->password);
        $user->reset_password           = null;
        $user->reset_password_expire    = null;
        $user->save();

        registro(
            "info",
            $user->id,
            $user->empresa_id,
            "Login",
            "Contraseña",
            "Se reseteo la contraseña"
        );

        exito('La contraseña se modifico correctamente.');
        return redirect('/admin'); 
    }

    public function ver_registrarse()
    {
        //return view('auth.register');
    }

    public function registrarse(Request $request)
    {
        //
    }

    private function comprobar_codigo($codigo, $plan)
    {
        if($codigo != null)
        {
            $codigo = Codigo::where('codigo', $codigo)->first();

            if(($codigo == null) or ($codigo->cantidad == 0))
            {
                return "error1";
            }else
            {
                if($codigo->tipo == $plan)
                {
                    if($codigo->email != null)
                    {
                        if($codigo->email != Auth::user()->email)
                        {
                            return "error2";
                        }
                    }

                    if($codigo->cantidad == 1)
                    {
                        $codigo->delete();
                    }else
                    {
                        $codigo->cantidad -= 1;
                        $codigo->save();
                    }

                    return "total";

                }elseif($codigo->tipo == "descuento")
                {
                    if($codigo->cantidad == 1)
                    {
                        $codigo->delete();
                    }else
                    {
                        $codigo->cantidad -= 1;
                        $codigo->save();
                    }
                    
                    return $codigo->descuento;
                }else
                {
                    return "error3";
                }
            }
        }

        return "no";
    }
}
