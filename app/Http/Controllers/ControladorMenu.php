<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;

class ControladorMenu extends Controller
{
    private $path = "admin.menu.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = empresa()->menus;

        return view($this->path."index", compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $numero = 0;

        if(empresa()->menus->sortBy('numero')->last() != null)
            $numero = empresa()->menus->sortBy('numero')->last()->numero + 1;

        $menu               = new Menu();
        $menu->titulo       = $request->titulo;
        $menu->numero       = $numero;
        $menu->empresa_id   = empresa()->id;
        
        if($request->tipo == "url")
        {
            $url = str_replace("http://", "", $request['contenido_url']);
            $url = str_replace("https://", "", $url);

            $menu->url = "http://".$url;
        }
        elseif($request->tipo == "inicio")
        {
            $menu->url = "/";
        }
        else
        {
            $menu->url = $request['contenido_'.$request->tipo];
        }

        $menu->save();

        exito('El registro se creo correctamente');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);

        if($menu == null)
        {
            error('Error al eliminar el registro');
            return back();
        }

        $menu->delete();

        exito('El registro se elimino correctamente');
        return back();
    }

    public function mover(Request $request)
    {
        foreach($request->array as $array)
        {
            $menu               = Menu::find($array[0]);
            $menu->numero       = $array[1];
            $menu->save();
        }

        return true;
    }
}
