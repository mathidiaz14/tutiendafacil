<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Atributo;

class ControladorAtributo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $atributo               = new Atributo();
        $atributo->nombre       = $request->nombre;
        $atributo->color        = $request->color;
        $atributo->producto_id  = $request->producto;
        $atributo->empresa_id   = empresa()->id;
        $atributo->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::find($id);

        if(($producto == null) or (!control_empresa($producto->empresa_id)))
        {
            error('Error al cargar las variantes del producto');
            return back();
        }
        
        return view('admin.producto.secciones.variantes.atributo_lista', compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $atributo = Atributo::find($id);

        if($atributo == null)
        {
            error('Error al eliminar el atributo');
            return false;
        }

        foreach($atributo->valores as $valor)
            $valor->variantes()->detach();

        $atributo->producto->variantes()->delete();
        $atributo->valores()->delete();
        $atributo->delete();

        return true;
    }
}
