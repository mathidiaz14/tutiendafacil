<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Venta;
use App\Models\Notificacion;
use App\Models\Cliente;
use App\Models\Stock;
use App\Models\Cupon;
use App\Models\Variante;
use App\Models\VentaTransferencia;
use App\Models\VentaEstado;
use Carbon\Carbon;
use Guzzlehttp\Guzzle;
use MercadoPago;
use Http;
use Storage;
use File;
use Session;

class ControladorPago extends Controller
{
    private $path = "pago.";

    public function ver_checkout($id)
    {
        $venta = Venta::where('codigo', $id)->first();

        if($venta == null)
            return redirect('/');

        if(($venta->estado == "aprobado") or ($venta->estado == "en_preparacion"))
            return redirect('eticket/'.$venta->codigo);

        if($venta->cliente_id != null)
            return view($this->path.'datos', compact('venta'));
        
        if(Session::get('cliente') != null)
        {
            $cliente            = Cliente::find(Session::get('cliente'))->first();
            $venta->cliente_id  = $cliente->id;
            $venta->save();

            return view($this->path.'datos', compact('venta'));
        }
        
        return view($this->path.'login', compact('venta'));
    }

    public function checkout_login($id, Request $request)
    {
       $venta  = Venta::where('codigo', $id)->first();

        if($venta == null)
        {
            error('Error al procesar los datos');
            return back();
        }

        $cliente = $venta->empresa->clientes->where('email', $request->email)->first();

        if($cliente == null) 
        {
            $cliente                = new Cliente();
            $cliente->email         = $request->email;
            $cliente->empresa_id    = $venta->empresa_id;
            $cliente->tmp           = "true";
            $cliente->save();
        }

        $venta->cliente_id      = $cliente->id;
        $venta->save();

        return redirect('checkout/datos/'.$venta->codigo);
    }

    public function ver_checkout_datos($id)
    {
        $venta = Venta::where('codigo', $id)->first();

        if($venta == null)
            return redirect('/');

        if(($venta->estado == "aprobado") or ($venta->estado == "en_proceso"))
            return redirect('eticket/'.$venta->codigo);

        return view($this->path.'datos', compact('venta'));
    }

    public function checkout_datos($id, Request $request)
    {
        $venta  = Venta::where('codigo', $id)->first();

        if($venta == null)
        {
            error('Error al procesar los datos');
            return back();
        }

        $cliente                    = $venta->cliente;
        $cliente->nombre            = $request->name;
        $cliente->apellido          = $request->surname;
        $cliente->telefono          = $request->telefono;
        $cliente->documento         = $request->documento;
        $cliente->documento_tipo    = $request->documento_tipo;
        $cliente->save();

        if($request->cupon != null)
        {
            $cupon = $venta->empresa->cupones->where('codigo', $request->cupon)->first();
            
            if(($cupon == null) or ($cupon->estado == "inactivo"))
            {
                error("El cupón no esta disponible");
                return back();
            }else
            {
                $venta->descuento = ($venta->precio * $cupon->descuento) / 100;
                $venta->save();

                if($cupon->cantidad == 1)
                    $cupon->estado = "inactivo";

                $cupon->cantidad -= 1;
                $cupon->save();
            }
        }

        return redirect('checkout/entrega/'.$venta->codigo);
    }

    public function ver_checkout_entrega($id)
    {
        $venta = Venta::where('codigo', $id)->first();

        if($venta == null)
            return redirect('/');

        if($venta->estado == "aprobado")
            return redirect('eticket/'.$venta->codigo);

        return view($this->path.'entrega', compact('venta'));
    }

    public function checkout_entrega($id, Request $request)
    {
        $venta  = Venta::where('codigo', $id)->first();

        if($venta == null)
        {
            error('Error al procesar los datos');
            return back();
        }

        $venta->entrega             = $request->entrega;
        $venta->local_id            = $request->local;
        $venta->cliente_observacion = $request->observacion;
        $venta->save();

        $cliente                    = $venta->cliente;
        $cliente->ciudad            = $request->ciudad;
        $cliente->direccion         = $request->direccion;
        $cliente->apartamento       = $request->apartamento;
        $cliente->save();

        return redirect('checkout/pago/'.$venta->codigo);
    }

    public function ver_checkout_pago($id)
    {
        $venta      = Venta::where('codigo', $id)->first();
        
        if($venta == null)
            abort(404);

        if(($venta->estado == "aprobado") or ($venta->estado == "en_proceso"))
            return redirect('eticket/'.$venta->codigo);

        return view($this->path.'pago', compact('venta'));
    }

    public function checkout_pago_tarjeta(Request $request)
    {
        $venta = Venta::where('codigo', $request->description)->first();
        
        if($venta == null)      
            return abort(404);
            
        $empresa    = $venta->empresa;
            
        if(empty($empresa->mercado_pago->access_token))
            return abort(404);

        if(!$this->comprobar_productos($venta))
            return back();

        MercadoPago\SDK::setAccessToken($empresa->mercado_pago->access_token);

        $payment                        = new MercadoPago\Payment();
        $payment->transaction_amount    = (float)$request->transaction_amount;
        $payment->token                 = $request->token;
        $payment->description           = $empresa->nombre." - #".$venta->codigo;
        $payment->installments          = (int)$request->installments;
        $payment->payment_method_id     = $request->payment_method_id;
        $payment->issuer_id             = (int)$request->issuer_id;
        $payment->notification_url      = url('mercadopago/webhooks', $venta->id);

        $payer                  = new MercadoPago\Payer();
        $payer->email           = $request->payer['email'];
        $payer->identification  = array(
            "type"      => $request->payer['identification']['type'],
            "number"    => $request->payer['identification']['number']
        );

        $payer->first_name  = $request->cardholderName;
        $payment->payer     = $payer;

        $payment->save();
        
        if($payment->status == "approved")
        {   
            $venta->mp_id           = $payment->id;

            $venta->estado          = "en_preparacion";
            $venta->pago_metodo     = "tarjeta";
            $venta->pago_estado     = "aprobado";
            $venta->save();

            estado_venta($venta->id, null, 'en_preparacion');
    
            $this->crear_notificacion($venta);
            $this->guardar_cliente($venta);
            $this->enviar_email($venta);
            $this->modificar_stock($venta);

            $response = array(
                'status'        => $payment->status,
                'status_detail' => $payment->status_detail,
                'id'            => $payment->id
            );

            registro(
                "Venta",
                $venta->id,
                'Guest',
                "Venta",
                "Pago",
                "Se realiza el pago por tarjeta"
            );

        }else
        {

            $response = array(
                'status'        => "rejected",
                'status_detail' => 'Error al procesar el pago: '.respuesta_mercadopago($payment->status_detail, $request->payment_method_id),
                'id'            => ""
            );

        }

        return $response;
    }

    public function checkout_pago_redes(Request $request)
    {
        $venta = Venta::where('codigo', $request->description)->first();
        
        if($venta == null)      
            return abort(404);
            
        $empresa    = $venta->empresa;
            
        if(empty($empresa->mercado_pago->access_token))
            return abort(404);

        if(!$this->comprobar_productos($venta))
            return back();

        MercadoPago\SDK::setAccessToken($empresa->mercado_pago->access_token);

        $payment                        = new MercadoPago\Payment();
        $payment->transaction_amount    = (float)$request->transactionAmount;
        $payment->description           = $empresa->nombre." - #".$venta->codigo;
        $payment->payment_method_id     = $request->payment_method_id;
        
        $payer                          = new MercadoPago\Payer();
        $payer->email                   = $request->email;
        $payer->identification          = array(
                                            "type"      => $request->identificationType,
                                            "number"    => $request->identificationNumber
                                        );
        
        $payment->payer                 = $payer;
        $payment->save();
        
        if($payment->status == "pending")
        {
            $venta->mp_id           = $payment->id;
            
            $venta->estado          = "en_preparacion";
            
            $venta->pago_metodo     = "redes";
            $venta->pago_estado     = "pendiente";
            $venta->save();

            estado_venta($venta->id, null, 'en_preparacion');

            $this->crear_notificacion($venta);
            $this->guardar_cliente($venta);
            $this->enviar_email($venta);
            $this->modificar_stock($venta);

            registro(
                "Venta",
                $venta->id,
                'Guest',
                "Venta",
                "Pago",
                "Se realiza el pago por redes"
            );

            return redirect('checkout/'.$venta->codigo);
        }

        error('No se pudo procesar el pago, intente con otro metodo.');
        return back();
        
    }

    public function checkout_pago_transferencia(Request $request)
    {
        $venta = Venta::find($request->id);
        
        if($venta == null)      
            return abort(404);

        if(!$this->comprobar_productos($venta))
            return back();

        $archivo           = $request->file('archivo');

        if($archivo == null)
        {
            error("Debe proporcionar el comprobante de transferencias");
            return back();
        }

        $nombre_archivo   = codigo_aleatorio(10).".".$archivo->getClientOriginalExtension();
        Storage::disk('transferencias')->put($nombre_archivo, File::get($archivo));

        $venta->estado          = "en_preparacion";

        $venta->pago_metodo     = "transferencia";
        $venta->pago_estado     = "pendiente";
        $venta->save();

        estado_venta($venta->id, null, 'en_preparacion');

        $transferencia              = new VentaTransferencia();
        $transferencia->venta_id    = $venta->id;
        $transferencia->archivo     = "storage/transferencias/".$nombre_archivo;
        $transferencia->save();

        $this->crear_notificacion($venta);
        $this->guardar_cliente($venta);
        $this->enviar_email($venta);
        $this->modificar_stock($venta);

        registro(
            "Venta",
            $venta->id,
            'Guest',
            "Venta",
            "Pago",
            "Se realiza el pago por transferencia"
        );

        return redirect('checkout/'.$venta->codigo);
    }

    public function checkout_pago_efectivo(Request $request)
    {
        $venta = Venta::find($request->id);
        
        if($venta == null)      
            return abort(404);

        if(!$this->comprobar_productos($venta))
            return back();

        $venta->estado          = "en_preparacion";
        
        $venta->pago_metodo     = "efectivo";
        $venta->pago_estado     = "pendiente";
        $venta->save();

        estado_venta($venta->id, null, 'en_preparacion');

        $this->crear_notificacion($venta);
        $this->guardar_cliente($venta);
        $this->enviar_email($venta);
        $this->modificar_stock($venta);

        registro(
            "Venta",
            $venta->id,
            'Guest',
            "Venta",
            "Pago",
            "Se realiza el pago por efectivo"
        );

        return redirect('checkout/'.$venta->codigo);
    }

    private function crear_notificacion($venta)
    {
        $titulo     = "Nuevo pedido";
        $contenido  = "Tienes un nuevo pedido, el numero es #".$venta->codigo;
        $url        = '/admin/venta/'.$venta->id;

        crear_notificacion($titulo, $contenido, $url,$venta->empresa_id);

        return true;
    }

    private function guardar_cliente($venta)
    {
        $cliente            = $venta->cliente;
        $cliente->tmp       = "false";
        $cliente->save();

        return true;
    }

    private function enviar_email($venta)
    {
        $contenido = [
            "titulo"    => "Comprobante de compra",
            "venta"     => $venta
        ];

        email('compra', "Comprobante de compra", $contenido, $venta->cliente->email);

        return true;
    }

    public function comprobar_productos($venta)
    {
        //recorro todos los productos de la venta, si uno da error devuelvo false.. si ninguno da error devuelvo true
        
        foreach($venta->productos as $producto)
        {
            //Controlo si la venta tiene variante
            if($producto->pivot->variante_id != null)
            {
                $variante = Variante::find($producto->pivot->variante_id);

                if($variante->cantidad == 0)
                {
                    //Si no hay stock de esta variante creo el error y devuelvo false
                    error('No hay stock de la variante seleccionada del producto '.$producto->nombre);
                    return false;
                
                }elseif($variante->cantidad - $producto->pivot->cantidad < 0)
                {
                    //Si quiere comprar mas productos de los que tengo devuelvo false
                    error('No hay suficiente stock de la variante seleccionada del producto '.$producto->nombre);
                    return false;
                }
            }else
            {
                if($producto->cantidad == 0)
                {
                    //Si no hay stock del producto creo el error y devuelvo false
                    error('No hay stock del producto '. $producto->nombre);
                    return false;
                
                }elseif($producto->cantidad - $producto->pivot->cantidad < 0)
                {
                    //Si quiere comprar mas productos de los que tengo devuelvo false
                    error('No hay suficiente stock del producto '. $producto->nombre);
                    return false;

                }
            }
        }

        return true;
    }

    public function checkout_cancelar($id)
    {
        $venta = Venta::find($id);

        if($venta == null)
            return back();

        if($venta->cliente->tmp == "true")
            $venta->cliente()->delete();

        $venta->productos()->delete();
        $venta->estados()->delete();
        $venta->transferencia()->delete();
        $venta->delete();

        return redirect('https://'.$venta->empresa->URL);
    }

    public function modificar_stock($venta)
    {
        //Modifico el stock de todos los productos de la compra

        foreach($venta->productos as $producto)
        {
            if($producto->pivot->variante_id != null)
            {
                $variante   = Variante::find($producto->pivot->variante_id);

                $cantidad   = $producto->variantes->sum('cantidad');
                $minimo     = $producto->minimo_producto == null ? 0 : $producto->minimo_producto;

                //Sumo todas las variantes y veo si llega al minimo de producto, si llega creo una notificacion
                if(($cantidad > $minimo) and ($cantidad - $producto->pivot->cantidad <= $minimo))
                {
                    crear_notificacion(
                                        "Un producto llego al minimo", 
                                        "El producto ".$producto->nombre." llego al minimo establecido", 
                                        "/admin/producto/".$producto->id."/edit", 
                                        $producto->empresa_id
                                    );
                }

                $variante->cantidad -= $producto->pivot->cantidad;
                $variante->save();

            }else
            {
                $cantidad   = $producto->cantidad;
                $minimo     = $producto->minimo_producto == null ? 0 : $producto->minimo_producto;

                //Veo si al efectuar la compra llego a la cantidad minima y creo una notificación si es asi
                if(($cantidad > $minimo) and ($cantidad - $producto->pivot->cantidad <= $minimo))
                {
                    crear_notificacion(
                                        "Un producto llego al minimo", 
                                        "El producto ".$producto->nombre." llego al minimo establecido", 
                                        "/admin/producto/".$producto->id."/edit", 
                                        $producto->empresa_id
                                    );
                }

                $producto->cantidad -= $producto->pivot->cantidad;
                $producto->save();

                if(($producto->mlu_id != null) and (plugin_estado('MercadoLibre')))
                {
                    \Http::timeout(60)->withHeaders(ml_headers_empresa($producto->empresa))
                                        ->put('https://api.mercadolibre.com/items/'.$producto->mlu_id,
                                                ['available_quantity' => $producto->cantidad]
                                            );

                    \Http::timeout(60)->withHeaders(ml_headers_empresa($producto->empresa))
                                        ->put('https://api.mercadolibre.com/items/'.$producto->mlu_id,
                                                ['available_quantity' => $producto->cantidad]
                                            );
                }
            }
        }

        return true;
    }
}
