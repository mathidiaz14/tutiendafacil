<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MultimediaCategoria;
use App\Models\Multimedia;
use App\Models\Producto;
use File;
use Storage;
use Auth;
use Image;

class ControladorMultimedia extends Controller
{
    protected $path = "admin.multimedia.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file       = $request->file('file');
        
        if($file == null)
            return false;
        
        $name       = $file->getClientOriginalName();
        $nameFile   = codigo_aleatorio(20).".".$file->getClientOriginalExtension();

        Storage::disk('public')->put($nameFile, File::get($file));

        reducir_imagen(public_path("storage/".$nameFile));

        $producto   = Producto::find($request->producto);

        $ultimo     = null;
        foreach($producto->imagenes as $imagen)
            $ultimo = $imagen->posicion;

        $media                  = new Multimedia();
        $media->url             = "storage/".$nameFile;
        $media->nombre          = $name;
        $media->tamaño          = Storage::disk('public')->size($nameFile);
        $media->tipo            = $file->getClientOriginalExtension();
        $media->empresa_id      = empresa()->id;
        $media->user_id         = usuario();
        $media->producto_id     = $request->producto;
        $media->posicion        = $ultimo + 1;
        $media->save();
        
        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Multimedia",
            "Agregar",
            "Se guardo imagen"
        );

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::find($id);

        $imagenes = $producto->imagenes;

        return view('admin.producto.secciones.imagenes.imagenes', compact('imagenes', 'producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $multimedia                 = Multimedia::find($id);
        $multimedia->nombre         = $request->nombre;
        $multimedia->alternativo    = $request->alternativo;
        $multimedia->descripcion    = $request->descripcion;
        $multimedia->save();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $multimedia = Multimedia::find($id);

        if(($multimedia == null) or (!control_empresa($multimedia->empresa_id)))
            return false;
        
        if(file_exists($multimedia->url))
            unlink($multimedia->url);

        $producto = $multimedia->producto;

        $variantes = $producto->variantes->where('imagen', $multimedia->url);

        foreach($variantes as $variante)
        {
            $variante->imagen = null;
            $variante->save();
        }
        
        $multimedia->delete();

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Multimedia",
            "Eliminar",
            "Se elimino imagen"
        );

        return true;
    }

    public function mover(Request $request)
    {   
        foreach($request->array as $array)
        {
            $media = Multimedia::find($array[0]);
            $media->numero = $array[1];
            $media->save();
        }
        
        return true;
    }
}
