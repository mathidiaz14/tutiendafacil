<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PagoTransferencia;
use App\Models\PagoEfectivo;

class ControladorMetodoPago extends Controller
{
    public function index()
    {
        return view('admin.metodo_pago.index');
    }

    public function set_transferencia()
    {
        $transferencia          = empresa()->pago_transferencia;

        if($transferencia == null)
        {
            $transferencia              = new PagoTransferencia();
            $transferencia->empresa_id  = empresa()->id;
        }

        if($transferencia->estado == "conectado")
            $transferencia->estado  = "desconectado";
        else
            $transferencia->estado  = "conectado";

        $transferencia->save();

        exito('El pago por transferencia se habilito de forma exitosa');

        return back();
    }

    public function save_transferencia(Request $request)
    {
        $transferencia          = empresa()->pago_transferencia;

        $transferencia->datos   = $request->datos;
        $transferencia->save();

        exito('Los datos se guardaron de forma exitosa');

        return back();
    }

    public function set_efectivo()
    {
        $efectivo          = empresa()->pago_efectivo;

        if($efectivo == null)
        {
            $efectivo              = new PagoEfectivo();
            $efectivo->empresa_id  = empresa()->id;
        }

        if($efectivo->estado == "conectado")
            $efectivo->estado  = "desconectado";
        else
            $efectivo->estado  = "conectado";

        $efectivo->save();

        exito('El pago en efectivo se habilito de forma exitosa');

        return back();
    }
}
