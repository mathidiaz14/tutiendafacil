<?php

namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ComandoSchedule;

class RootControladorComandoSchedule extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedule               = new ComandoSchedule();
        $schedule->signature    = $request->signature;
        $schedule->cron         = $request->cron;
        $schedule->save();

        exito("Se crea la tarea");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = ComandoSchedule::find($id);

        if($schedule == null)
        {
            error('Error al encontrar el schedule');
            return back();
        }

        $schedule->delete();

        exito('Se elimina la tarea');
        return back();
    }

    public function cambiar($id)
    {
        $schedule = ComandoSchedule::find($id);

        if($schedule == null)
        {
            error('Error al encontrar el schedule');
            return back();
        }

        if($schedule->estado == "activo")
            $schedule->estado = "bloqueado";
        else
            $schedule->estado = "activo";

        $schedule->save();

        exito('Se cambia el estado de la tarea');
        return back();
    }
}
