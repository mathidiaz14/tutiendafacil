<?php

namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use App\Models\EmailLog;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RootControladorEmail extends Controller
{
    private $path = "root.email.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emails = EmailLog::paginate(100);

        return view($this->path."index", compact('emails'));
    }

    public function show($id)
    {
        $email      = EmailLog::find($id);
        $contenido  = (array)json_decode($email->contenido);
        
        return view($email->plantilla, compact('contenido'));
    }

    public function destroy($id)
    {
        $emails = EmailLog::whereDate('created_at', '<', Carbon::now()->subDays(90));

        $emails->delete();
        
        exito('Se eliminaron los registros de emails');

        return back();
    }

    public function reenviar_email($id)
    {
        $email      = EmailLog::find($id);

        $contenido  = (array)json_decode($email->contenido);
        
        $return     = email(
                        str_replace('email.', '',$email->plantilla), 
                        $email->asunto, 
                        $contenido, 
                        $email->destinatario, 
                        "SI", 
                        $email->nombre,
                        $email->empresa_id == "root" ? null : $email->empresa
                    );

        if($return)
            exito("El email se reenvio correctamente");
        else
            error("Error al enviar el email");

        return back();
    }
}
