<?php

namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Opcion;
use Storage;
use ElasticEmail;
use GuzzleHttp;

class RootControladorAdministrar extends Controller
{
    public function index()
    {
        $git    = shell_exec('git show');
        $opcion = Opcion::first();

        return view('root.administrar.index', compact('git', 'opcion'));
    }

    public function email(Request $request)
    {
        $contenido = [
            "titulo" => "Prueba desde el ROOT",
            "texto" => "Prueba de envio de correo desde la cuenta ROOT",
        ];
        
        if(email("root.prueba", "Prueba de envio de correo", $contenido, $request->email))
            exito('Se envio el correo');
        else
            error('No se pudo enviar el correo');

        return back();
    }

    public function actualizar()
    {
        shell_exec('cd ~/apps/ttf');

        $respuesta  = shell_exec('git pull');

        exito('Respuesta desde el servidor: '.$respuesta);

        return back();
    }

    public function set_opcion(Request $request)
    {
        $opcion = Opcion::first();
        $opcion->plan1 =  $request->plan1;
        $opcion->plan2 =  $request->plan2;
        $opcion->plan3 =  $request->plan3;
        $opcion->save();

        exito('Las opciones se modificaron correctamente');
        return back();
    }
}
