<?php

namespace App\Http\Controllers\Root;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ComandoSchedule;
use App\Models\Comando;
use App\Models\RegistroComando;
use Artisan;

class RootControladorComandos extends Controller
{
    private $path = "root.comandos.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comandos   = Comando::all();
        $schedules  = ComandoSchedule::all();

        return view($this->path."index", compact('comandos', 'schedules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comando                = new Comando();
        $comando->signature     = $request->signature;
        $comando->descripcion   = $request->descripcion;
        $comando->save();

        exito('El comando se ejecuto correctamente');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comando    = Comando::find($id);

        $crons      = RegistroComando::where('comando', $comando->signature)
                                        ->OrderBy('created_at', 'desc')
                                        ->paginate(100);

        return view("root.cron.index", compact('crons'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comando = Comando::find($id);

        if($comando == null)
        {
            error('Error al obtener registro');
            return back();
        }

        $comando->delete();

        exito('Se elimino el comando');
        return back();
    }

    public function bloquear($id)
    {
        $comando = Comando::find($id);

        if($comando == null)
        {
            error('Error al obtener registro');
            return back();
        }

        if($comando->estado == "bloqueado")
            $comando->estado = "activo";
        else
            $comando->estado = "bloqueado";

        $comando->save();

        exito('El comando se modifico correctamente');
        return back();
    }

    public function ejecutar($id)
    {
        $comando = Comando::find($id);

        if($comando == null)
        {
            error('Error al obtener registro');
            return back();
        }

        Artisan::call($comando->signature);

        exito("El comando se ejecuto correctamente");

        return back();        
    }
}
