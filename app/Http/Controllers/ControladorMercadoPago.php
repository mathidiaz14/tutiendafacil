<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Empresa;
use App\Models\Venta;
use App\Models\MercadoPago;
use Http;
use Auth;

class ControladorMercadoPago extends Controller
{
    public function conexion(Request $request)
    {
        if($request->state == null)
            abort(500);

        $empresa                            = Empresa::find($request->state);
        $mercado_pago                       = $empresa->mercado_pago;
        
        $response = Http::post('https://api.mercadopago.com/oauth/token', [
            'grant_type'        => "authorization_code", 
            'client_secret'     => "tzpXWXJXwn2KBwtgkxI0NME5UgmUtYLN",
            'client_id'         => "7882233477752920", 
            'code'              => $request->code,
            'redirect_uri'      => "https://tutiendafacil.uy/mercadopago/conexion"
        ]);
        
        if($mercado_pago == null)
        {
            $mercado_pago               = new MercadoPago();
            $mercado_pago->empresa_id   = $empresa->id;
        }
        
        if(empty($response->json()['access_token']))
            abort(500);

        $mercado_pago->estado           = "conectado";
        $mercado_pago->access_token     = $response->json()['access_token'];
        $mercado_pago->public_key       = $response->json()['public_key'];
        $mercado_pago->refresh_token    = $response->json()['refresh_token'];
        $mercado_pago->user_id          = $response->json()['user_id'];
        $mercado_pago->expires_in       = $response->json()['expires_in'];
        $mercado_pago->expires_date     = \Carbon\Carbon::now();
        $mercado_pago->save();

        registro(
            "info",
            "sistema",
            $empresa->id,
            "MercadoPago",
            "Conexion",
            "Se conecto con MercadoPago"
        );

        return redirect("admin/metodoPago");
    }

    public function webhooks(Request $request)
    {
        registro(
            "Venta",
            "Guest",
            'Guest',
            "MercadoPago",
            "Webhooks_id",
            $request
        );

        return \Response::make('message', 200);  
    }

    public function webhooks_id(Request $request, $id)
    {
        $venta = Venta::find($id);

        if($venta == null)
            return \Response::make('Error', 404);              

        $venta->mp_id = $request->data["id"];
        $venta->save();

        registro(
            "Venta",
            $venta->empresa->id,
            'Guest',
            "MercadoPago",
            "Webhooks_id",
            $request
        );

        return \Response::make('Exito', 200);  
    }

    public function desconexion()
    {
        $mercado_pago = Auth::user()->empresa->mercado_pago;

        if($mercado_pago == null)
        {
            error('No se pudo realizar la desconexion');
            return back();
        }

        $mercado_pago->estado           = "desconectado";
        $mercado_pago->access_token     = null;
        $mercado_pago->public_key       = null;
        $mercado_pago->refresh_token    = null;
        $mercado_pago->user_id          = null;
        $mercado_pago->expires_in       = null;
        $mercado_pago->expires_date     = null;

        $mercado_pago->save();


        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "MercadoPago",
            "Desconexion",
            "Se desconecto con MercadoPago"
        );

        exito("La desconexion se realizo correctamente");

        return back();
    }
}
