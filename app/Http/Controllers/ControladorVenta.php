<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Venta;
use MercadoPago;
use Auth;

class ControladorVenta extends Controller
{
    private $path = "admin.venta.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->buscar == null)
        {
            $ventas = empresa()->ventas->where('estado', '!=', 'comenzado');
        }else
        {
            $ventas_cliente     = Venta::where('empresa_id', '=', Auth::user()->empresa_id)
                                        ->where('estado', '!=', 'comenzado')
                                        ->whereHas('cliente', function ($query) use ($request){
                                            $query->where('nombre', 'like', '%'.$request->buscar.'%')
                                            ->orWhere('apellido', 'LIKE', '%'.$request->buscar.'%')
                                            ->orWhere('telefono', 'LIKE', '%'.$request->buscar.'%')
                                            ->orWhere('email', 'LIKE', '%'.$request->buscar.'%');
                                        })->get();
            
            $ventas_opciones    = Venta::where('empresa_id', '=', Auth::user()->empresa_id)
                                        ->where('estado', '!=', 'comenzado')
                                        ->where(function ($query) use ($request) {
                                           $query->where('codigo', 'LIKE', '%'.$request->buscar.'%')
                                           ->orWhere('cliente_observacion', 'LIKE', '%'.$request->buscar.'%')
                                           ->orWhere('observacion', 'LIKE', '%'.$request->buscar.'%');
                                       })->get();

            $ventas = collect();

            foreach($ventas_cliente as $venta)
                $ventas->push($venta);

            foreach($ventas_opciones as $venta)
                $ventas->push($venta);
        }

        $ventas = paginate($ventas->unique(), 100);
        
        return view($this->path."index", compact('ventas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error("Error al mostrar venta");
            return redirect('admin/venta');
        }

        return view($this->path."show", compact('venta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $venta  = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error("Error al editar venta");
            return redirect('admin/venta');
        }

        $venta->observacion = $request->observacion;
        $venta->save();

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Venta",
            "Editar",
            "Se modifico venta"
        );

        exito('Se agrego una observacion');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error("Error al eliminar la venta");
            return redirect('admin/venta');
        }

        if($venta->cliente->tmp == true)
            $venta->cliente()->delete();

        $venta->productos()->delete();
        $venta->estados()->delete();
        $venta->transferencia()->delete();
        $venta->delete();

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Venta",
            "Eliminar",
            "Se elimino venta"
        );

        exito('El pedido se elimino correctamente');
        return back();
    }

    public function entregar($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error('Error al entregar producto');
            return redirect('admin/venta');
        }

        $venta->estado = "entregado";
        $venta->save();

        estado_venta($venta->id, usuario()->id, 'entregado');

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Venta",
            "Entrega",
            "Se cambio estado del pedido"
        );

        exito("Se cambio el estado del pedido.");
        return back();
    }

    public function devolver($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error('Error al cambiar el estado de la venta');
            return redirect('admin/venta');
        }

        if($venta->mp_id != null)
        {
            MercadoPago\SDK::configure(['ACCESS_TOKEN' => $venta->empresa->mercado_pago->access_token]);
            $payment = MercadoPago\Payment::find_by_id($venta->mp_id);
            
            if($payment == null)
            {
                error('Error al realizar devolución');
                return back();
            }

            $payment->refund();
        }    

        foreach($venta->productos as $producto)
        {
            if($producto->pivot->variante_id != null)
            {
                $variante           = Variante::find($producto->pivot->variante_id);
                $variante->cantidad += $producto->pivot->cantidad;
                $variante->save();

            }else
            {
                $producto->cantidad += $producto->pivot->cantidad;
                $producto->save();
            }
        }

        $venta->estado          = "cancelado";
        $venta->pago_estado     = "devuelto";
        $venta->save();

        estado_venta($venta->id, usuario()->id, 'devuelto');
        estado_venta($venta->id, usuario()->id, 'cancelado');

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Venta",
            "Devolver",
            "Se cambio estado del pedido"
        );

        exito("La devolución se realizo de forma exitosa.");
        return back();
    }

    public function cancelar($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error('Error al cambiar el estado de la venta');
            return redirect('admin/venta');
        }
        
        if($venta->mp_id != null)
        {
            MercadoPago\SDK::configure(['ACCESS_TOKEN' => $venta->empresa->mercado_pago->access_token]);
            
            $payment            = MercadoPago\Payment::find_by_id($venta->mp_id);
            $payment->status    = "cancelled";
            $payment->update();
        }

        foreach($venta->productos as $producto)
        {
            if($producto->pivot->variante_id != null)
            {
                $variante           = Variante::find($producto->pivot->variante_id);
                $variante->cantidad += $producto->pivot->cantidad;
                $variante->save();

            }else
            {
                $producto->cantidad += $producto->pivot->cantidad;
                $producto->save();
            }
        }

        $venta->estado = "cancelado";
        $venta->save();

        estado_venta($venta->id, usuario()->id, 'cancelado');

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Venta",
            "Cancelar",
            "Se cambio estado del pedido"
        );

        exito("Se cambio el estado del pedido.");
        return back();
    }

    public function descargar_transferencia($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or ($venta->pago_metodo != "transferencia"))
        {
            error("Error al abrir el archivo");
            return redirect('admin/venta');
        }

        $transferencia = $venta->transferencia;

        return redirect($transferencia->archivo);
    }

    public function pago($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error('Error al cambiar el estado de la venta');
            return redirect('admin/venta');
        }   

        if(($venta->pago_metodo != "transferencia") and ($venta->pago_metodo != "efectivo"))
        {
            error("Error al cambiar estado de la venta");
            return back();
        }

        $venta->pago_estado = "aprobado";
        $venta->save();

        estado_venta($venta->id, usuario()->id, 'pago_aprobado');

        exito('Se cambio el estado de la venta a Pago aprobado');
        return back();
    }

    public function entrega($id)
    {
        $venta = Venta::find($id);

        if(($venta == null) or (!control_empresa($venta->empresa_id)))
        {
            error('Error al cambiar el estado de la venta');
            return redirect('admin/venta');
        }   

        if($venta->estado != "en_preparacion")
        {
            error("Error al cambiar estado de la venta");
            return back();
        }

        $venta->estado = "entrega";
        $venta->save();

        estado_venta($venta->id, usuario()->id, 'entrega');

        $contenido = [
            "titulo"    => "Su pedido esta listo para retirar",
            "venta"     => $venta
        ];

        email('compra', "Listo para retirar", $contenido, $venta->cliente->email);

        exito('Se cambio el estado de la venta a "Pronto para entregar"');
        return back();
    }
}
