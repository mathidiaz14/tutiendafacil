<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Multimedia;
use App\Models\Tag;
use Auth;
use Storage;
use File;
use DB;

class ControladorProducto extends Controller
{

    private $path = "admin.producto.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->buscar == null)
        {
            $productos = Producto::where('empresa_id', '=', Auth::user()->empresa_id)
                                    ->paginate(50);
        }else
        {
            $productos = Producto::where('empresa_id', '=', Auth::user()->empresa_id)
                           ->where(function ($query) use ($request) {
                               $query->where('sku', 'LIKE', '%'.$request->buscar.'%')
                                    ->orWhere('nombre', 'LIKE', '%'.$request->buscar.'%')
                                    ->orWhere('precio', '=', $request->buscar);
                           })
                           ->paginate(50);
        }

        return view($this->path."index", compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $url        = str_replace(" ", "_", strtolower($request->nombre));
        
        $cantidad   = Auth::user()->empresa->productos->where('url', $url)->count();

        if($cantidad > 0)
            $url = $url.$cantidad;

        $producto               = new Producto();
        $producto->nombre       = $request->nombre;
        $producto->url          = $url;
        $producto->precio       = $request->precio;
        $producto->estado       = "activo";
        $producto->empresa_id   = Auth::user()->empresa->id;
        $producto->save();

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Producto",
            "Agregar",
            "Se agrega producto"
        );

        exito('El producto se creo exitosamente');
        return redirect('admin/producto/'.$producto->id.'/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $seccion = null)
    {
        $producto = Producto::find($id);

        if(($producto == null) or (!control_empresa($producto->empresa_id)))
        {
            error('No se puede editar el producto');
            return redirect('admin/producto');
        }
        
        return view($this->path."edit", compact('producto', 'seccion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto               = Producto::find($id);

        if(($producto == null) or (!control_empresa($producto->empresa_id)))
        {
            error('Error al actualizar el registro');
        }

        if($producto->url != $request->url)
        {
            $old = Auth::user()->empresa->productos->where('url', $request->url)->first();

            if($old != null)
            {
                error("Ya hay un producto con el mismo nombre");
                return back();
            }
        }

        if($producto->sku != $request->sku)
        {
            $old = Auth::user()->empresa->productos->where('sku', $request->sku)->first();

            if($old != null)
            {
                error("Ya hay un producto con el mismo codigo SKU");
                return back();
            }
        }

        $producto->sku                  = $request->sku;
        $producto->nombre               = $request->nombre;
        $producto->descripcion          = $request->descripcion;
        $producto->precio               = $request->precio;
        $producto->precio_promocion     = $request->precio_promocion;
        $producto->costo                = $request->costo;
        $producto->cantidad             = $request->cantidad;
        $producto->minimo_producto      = $request->minimo_producto;
        $producto->proveedor_id         = $request->proveedor;
        $producto->nuevo                = $request->nuevo;
        $producto->destacado            = $request->destacado;
        $producto->url                  = $request->url;
        $producto->talles               = $request->talles;
        $producto->condicion            = $request->condicion;
        $producto->save();

        $producto->categorias()->detach();

        foreach(empresa()->categorias as $categoria)
        {
            if($request['categoria_'.$categoria->id] == "on")
                $producto->categorias()->attach($categoria);
        }

        $tags  = explode(',', $request->tags);
        
        $producto->tags()->detach();

        foreach($tags as $tag)
        {
            $db = empresa()->tags->where('tag', $tag)->first();

            if($db == null)
            {
                $tag_db             = new Tag();
                $tag_db->empresa_id = empresa()->id;
                $tag_db->tag        = $tag;
                $tag_db->save();

                $producto->tags()->attach($tag_db);
            }else
            {
                $producto->tags()->attach($db);
            }
        }

        exito('El producto se modifico correctamente.');

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Producto",
            "Modificar",
            "Se modifico el producto"
        );

        if($request->continuar == "si")
            return back();

        return redirect('admin/producto');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);

        if(($producto == null) or (!control_empresa($producto->empresa_id)))
        {
            error('No se puede eliminar el registro');
            return back();
        }

        foreach($producto->imagenes as $multimedia)
        {
            if(file_exists($multimedia->url))
                unlink($multimedia->url);

            $multimedia->delete();
        }

        foreach($producto->variantes as $variante)
            $producto->variantes()->detach($variante);

        foreach($producto->atributos_ml as $atributo)
            $producto->atributos_ml()->detach($atributo);
        
        $producto->delete();

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Producto",
            "Eliminar",
            "Se elimina producto"
        );

        exito('El producto se elimino correctamente');
        return back();
    }

    public function cambiar_estado($id)
    {
        $producto = Producto::find($id);

        if($producto->estado == "borrador")
            $producto->estado = "activo";
        else
            $producto->estado = "borrador";

        if(($producto->mlu_id != null) and (plugin_estado("MercadoLibre")))
        {
            $estado         = $producto->estado == "activo" ? "active" : "paused";
        
            \Http::timeout(60)->withHeaders(ml_headers_empresa($producto->empresa))
                                ->put('https://api.mercadolibre.com/items/'.$producto->mlu_id, 
                                        ['status' => $estado]
                                    );

            \Http::timeout(60)->withHeaders(ml_headers_empresa($producto->empresa))
                                ->put('https://api.mercadolibre.com/items/'.$producto->mlu_id, 
                                        ['status' => $estado]
                                    );
        }

        $producto->save();

        return true;
    }
}
