<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Variante;

class ControladorVariante extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = Producto::find($request->producto);

        if(($producto == null) or (!control_empresa($producto->empresa_id)))
        {
            error('Error al cargar las variantes del producto');
            return back();
        }

        $producto->variantes()->delete();

        $atributos  = $producto->atributos;

        $array = array();

        foreach($atributos as $atributo)
        {
            $array2 = array();
            foreach($atributo->valores as $valor)
                array_push($array2, $valor);

            array_push($array, $array2);
        }

        $combinaciones = array();
        $this->obtenerCombinaciones($array, 0, array(), $combinaciones);
        
        foreach ($combinaciones as $combinacion) 
        {
            $variante               = new Variante();
            $variante->producto_id  = $producto->id;
            $variante->save();
            
            foreach($combinacion as $valor)
                $variante->valores()->attach($valor->id);
        }

        exito('Las variantes del producto se generaron correctamente');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $producto = Producto::find($id);

        if(($producto == null) or (!control_empresa($producto->empresa_id)))
            return false;

        return view('admin.producto.secciones.variantes.variantes_lista', compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $variante = Variante::find($id);

        $variante->sku      = $request->sku;
        $variante->cantidad = $request->cantidad;
        $variante->precio   = $request->precio;
        $variante->imagen   = $request->imagen;
        $variante->save();

        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $variante = Variante::find($id);

        $variante->delete();

        return true;
    }

    private function obtenerCombinaciones($matriz, $fila = 0, $combinacionActual = array(), &$combinaciones = array()) {
        // Verificar si hemos alcanzado la última fila de la matriz
        if ($fila == count($matriz)) {
            // Agregar la combinación actual al arreglo de combinaciones
            $combinaciones[] = $combinacionActual;
            return;
        }

        // Obtener la fila actual de la matriz
        $filaActual = $matriz[$fila];

        // Recorrer los elementos de la fila actual
        foreach ($filaActual as $elemento) {
            // Agregar el elemento a la combinación actual
            $combinacionActual[] = $elemento;

            // Recursivamente llamar a la función para la siguiente fila
            $this->obtenerCombinaciones($matriz, $fila + 1, $combinacionActual, $combinaciones);

            // Eliminar el último elemento agregado para probar la siguiente combinación
            array_pop($combinacionActual);
        }
    }
}
