<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;
use App\Models\Categoria;
use App\Models\Cliente;
use App\Models\Producto;
use App\Models\Venta;
use App\Models\Stock;
use App\Models\Configuracion;
use App\Models\Newsletter;
use App\Models\Empresa;
use App\Models\Mensaje;
use App\Models\Pagina;
use App\Models\Visita;
use App\Models\Error;
use App\Models\User;
use App\Models\Variante;
use App\Models\AtributoValor;
use Carbon\Carbon;
use MercadoPago;
use Storage;
use Session;
use Auth;
use File;
use Hash;

class ControladorPrincipal extends Controller
{
    private $empresa;
    private $carpeta;

    public function __construct()
    {
        $this->empresa = empresa();
        
        if($this->empresa != null)
            $this->carpeta = 'empresas.'.$this->empresa->id.".".$this->empresa->carpeta.".";
    }

    public function dashboard()
    {
        if(Auth::user()->tipo == "root")
            return view('root.index');

        return view('admin.index');
    }

    public function index()
    {
        if($this->comprobar() != null)
            return $this->comprobar();

        if(view()->exists("empresas.".$this->empresa->id.".".$this->empresa->carpeta.".index"))
            return view("empresas.".$this->empresa->id.".".$this->empresa->carpeta.".index");
        

        return $this->error404();
    }

    public function categoria($id, Request $request)
    {
        if($this->comprobar() != null)
            return $this->comprobar();
        
        if(is_numeric($id))
            $categoria = Categoria::find($id);
        else
            $categoria   = $this->empresa->categorias->where('url', $id)->first();
        
        if(($categoria != null) and ($categoria->empresa_id == $this->empresa->id))
        {
            switch ($request->orden) {
                case 'desc':
                    $productos = $categoria->productos->where('estado','activo')->sortByDesc('nombre');
                    break;
                
                case 'asc':
                    $productos = $categoria->productos->where('estado','activo')->sortBy('nombre');
                    break;

                case 'may':
                    $productos = $categoria->productos->where('estado','activo')->sortByDesc('precio');
                    break;

                case 'men':
                    $productos = $categoria->productos->where('estado','activo')->sortBy('precio');
                    break;
                
                default:
                    $productos = $categoria->productos->where('estado','activo')->sortByDesc('nombre');
                    break;
            }    
            

            $busqueda   = $request->busqueda != null ? $request->busqueda : "";
            $orden      = $request->orden != null ? $request->orden : "";

            if(view()->exists($this->carpeta."categoria-".$id))
                return view($this->carpeta."categoria-".$id, compact('categoria', 'productos', 'busqueda', 'orden'));
            
            if(view()->exists($this->carpeta."categoria"))
                return view($this->carpeta."categoria", compact('categoria', 'productos', 'busqueda', 'orden'));
        }

        return $this->error404();
    }

    public function productos(Request $request)
    {   
        if($this->comprobar() != null)
            return $this->comprobar();

        $busqueda           = $request->busqueda != null ? $request->busqueda : "";
        $orden              = $request->orden != null ? $request->orden : "";
        $categoria          = $request->categoria;
        $filtro_precio      = $request->filtro_precio;
        $filtro_talle       = $request->filtro_talle;
        $filtro_color       = $request->filtro_color;
        $filtro_tag         = $request->filtro_tag;

        if($busqueda != "")
        {
            $productos = Producto::where('empresa_id', $this->empresa->id)
                                ->where('nombre', 'LIKE', '%'.$busqueda.'%')
                                ->where('estado', 'activo')
                                ->get();
        }
        else
        {
            $productos = Producto::where('empresa_id', $this->empresa->id)
                                ->where('estado', 'activo')
                                ->get();
        }

        switch ($orden) 
        {
            case 'asc':
                $productos = $productos->sortBy('nombre');
                break;

            case 'desc':
                $productos = $productos->sortByDesc('nombre');
                break;

            case 'may':
                $productos_precio   = new collection();
                $productos2         = new collection();

                foreach($productos as $producto)
                {
                    $precio = 0;
                    
                    if($producto->precio_promocion != null)
                        $precio = $producto->precio_promocion;
                    else
                        $precio = $producto->precio;

                    $productos_precio->add(['precio' => $precio, 'producto' => $producto]);
                }

                foreach($productos_precio->sortByDesc('precio') as $producto_precio)
                    $productos2->add($producto_precio['producto']);

                $productos = $productos2;

                break;

            case 'men':
                
                $productos_precio   = new collection();
                $productos2         = new collection();

                foreach($productos as $producto)
                {
                    $precio = 0;
                    
                    if($producto->precio_promocion != null)
                        $precio = $producto->precio_promocion;
                    else
                        $precio = $producto->precio;

                    $productos_precio->add(['precio' => $precio, 'producto' => $producto]);
                }

                foreach($productos_precio->sortBy('precio') as $producto_precio)
                    $productos2->add($producto_precio['producto']);

                $productos = $productos2;

                break;
        }   

        if($categoria != null)
        {
            $productos = Categoria::find($categoria)->productos->where('estado', 'activo');
        }

        if($filtro_precio != null)
        {
            $explode    = explode('-', $filtro_precio);
            
            if(count($explode) == 2)
            {
                $inicio     = $explode[0] == '*' ? 0 : $explode[0];
                $fin        = $explode[1] == '*' ? 1000000 : $explode[1];
                
                $productos  = $productos->whereBetween('precio', [$inicio, $fin]);
            }
        }

        if($filtro_talle != null)
        {   
            $atributos  = empresa()->atributos->where('nombre', 'Talle');
            $productos2 = collect();
            
            foreach($atributos as $atributo)
            {
                foreach($atributo->valores as $valor)
                {
                    if($valor->nombre == $filtro_talle)
                    {
                        foreach($valor->variantes as $variante)
                            $productos2->push($productos->where('id', $variante->producto_id)->first());
                    }
                }
            }
            
            $productos = $productos2->unique()->whereNotNull();
        }

        if($filtro_color != null)
        {
            $explode    = explode('-', $filtro_color);

            if(count($explode) == 2)
            {
                $color1     = $explode[0];
                $color2     = $explode[1];

                $atributos  = empresa()->atributos->where('color', 'on');
                $productos2 = collect();
                
                foreach($atributos as $atributo)
                {
                    foreach($atributo->valores as $valor)
                    {
                        if(($valor->color1 == "#".$color1) and ($valor->color2 == "#".$color2))
                        {
                            foreach($valor->variantes as $variante)
                                $productos2->push($productos->where('id', $variante->producto_id)->first());
                        }
                    }
                }
                
                $productos = $productos2->unique()->whereNotNull();
            }
        }

        if($filtro_tag != null)
        {
            $tags       = empresa()->tags->where('tag', $filtro_tag);
            $productos2 = collect();
            
            foreach($tags as $tag)
            {
                foreach($tag->productos as $prod)
                    $productos2->push($productos->where('id', $prod->id)->first());
            }
            
            $productos = $productos2->unique()->whereNotNull();
        }

        if(view()->exists($this->carpeta."productos"))
            return view($this->carpeta."productos", compact('productos', 'busqueda', 'orden'));

        $this->error404();   
    }

    public function producto($id)
    {
        if($this->comprobar() != null)
            return $this->comprobar();

        if(is_numeric($id))
            $producto   = $this->empresa->productos->where('id', $id)->first();
        else
            $producto   = $this->empresa->productos->where('url', $id)->first();
        
        if(($producto != null) and ($producto->estado == "activo") and (view()->exists($this->carpeta."producto")))
            return view($this->carpeta."producto", compact('producto'));   

        return $this->error404();;
    }

    public function carrito()
    {
        if($this->comprobar() != null)
            return $this->comprobar();

        $productos  = Session::get('carrito');

        if(view()->exists($this->carpeta."carrito"))
            return view($this->carpeta."carrito", compact('productos'));   
        
        return $this->error404();
    }

    public function comprar_ahora(Request $request)
    {
        $producto_id    = $request->producto;
        $variante_id    = $request->variante;

        $producto = Producto::find($producto_id);

        do
        {
            $codigo = codigo_aleatorio_numerico(10);
            $old    = Venta::where('codigo', $codigo)->first();

        }while( $old != null);

        $venta              = new Venta();
        $venta->empresa_id  = $this->empresa->id;
        $venta->codigo      = $codigo;
        $venta->codigo_num  = $codigo;
        $venta->estado      = "comenzado";

        $precio = 0;

        if($producto->precio_promocion == null)
            $precio = $producto->precio;
        else
            $precio = $producto->precio_promocion;

        $venta->precio = $precio;
        $venta->save();

        estado_venta($venta->id, null, 'comenzado');

        $venta->productos()->attach(
                    $producto->id,
                    [
                        'cantidad'      => '1', 
                        'precio'        => $precio,
                        'variante_id'   => $variante_id,
                        'empresa_id'    => $this->empresa->id, 
                        'created_at'    => Carbon::now()
                    ]);
        
        return redirect("checkout/".$venta->codigo);
    }

    public function agregar_carrito(Request $request)
    {
        $producto = Producto::find($request->producto);

        if($producto == null)
            return false;
        
        $variante = null;

        if($request->variante != null)
            $variante = Variante::find($request->variante);

        $coleccion = collect([
            "id" => codigo_aleatorio(10),
            "producto" => $producto, 
            "variante" => $variante,
            "cantidad" => $request->cantidad
        ]);

        Session::push('carrito', $coleccion);   

        return true;
    }

    public function eliminar_carrito($id)
    {
        $productos = Session::get('carrito');

        if($productos != null)
        {
            Session::forget('carrito');

            foreach ($productos as $producto)
            {
                if($producto['id'] != $id)
                {
                    $coleccion = collect([
                            "id" => $producto['id'], 
                            "producto" => $producto['producto'], 
                            "variante" => $producto['variante']
                        ]);

                    Session::push('carrito', $coleccion);
                }
            }
        }
        return back();
    }

    public function vaciar_carrito()
    {
        Session::forget('carrito');

        return back();
    }

    public function comprar()
    {
        if(Session::get('carrito') != null)
        {
            do
            {
                $codigo = codigo_aleatorio_numerico(10);
                $old    = Venta::where('codigo', $codigo)->first();

            }while( $old != null);

            $venta              = new Venta();
            $venta->empresa_id  = $this->empresa->id;
            $venta->codigo      = $codigo;
            $venta->codigo_num  = $codigo;
            $venta->estado      = "comenzado";
            $venta->save();

            estado_venta($venta->id, null, 'comenzado');

            $precio_total = 0;

            foreach (Session::get('carrito') as $producto) 
            {
                
                $variante   = $producto['variante'] != null ? $producto['variante']->id : null;
                $cantidad   = $producto['cantidad'];
                $precio     = 0;

                if((isset($producto['variante'])) and ($producto['variante']->precio != null))
                {
                    $precio = $producto['variante']->precio * $cantidad;
                }else
                {
                    if($producto['producto']->precio_promocion == null)
                        $precio = $producto['producto']->precio * $cantidad;
                    else
                        $precio = $producto['producto']->precio_promocion * $cantidad;
                }

                $venta->productos()->attach(
                        $producto['producto']->id,
                            [
                                'cantidad'      => $cantidad, 
                                'precio'        => $precio,
                                'variante_id'   => $variante,
                                'empresa_id'    => $this->empresa->id,
                                'created_at'    => Carbon::now()
                            ]
                        );

                $precio_total += $precio;
            }
            
            $venta->precio = $precio_total;
            $venta->save();
            
            Session::forget('carrito');
            return redirect("checkout/".$venta->codigo);
        }
        
        return back();
    }

    public function pagina($id)
    {
        if($this->comprobar() != null)
            return $this->comprobar();

        $pagina     = $this->empresa->paginas->where('url', $id)->first();

        if($pagina != null)
        {
            if(view()->exists($this->carpeta."pagina-".$id))
                return view($this->carpeta."pagina-".$id);
            
            if(view()->exists($this->carpeta."pagina"))
                return view($this->carpeta."pagina", compact('pagina'));
        }

        return $this->error404();
    }

    public function contacto(Request $request)
    {
        $this->validate($request, [
            'g-recaptcha-response'  => 'required|recaptchav3:register,0.5',
            'nombre'                => 'required', 
            'email'                 => 'required|email', 
            'mensaje'               => 'required'
        ]);

        if((Session('resultado') != $request->math) or ($request->faxonly == "on"))
        {
            error('No pudimos enviar el mensaje por sospecha de SPAM');
            return redirect('/#contact');
        }

        $contenido = [
            "titulo"    => "Contacto desde TuTiendaFacil.uy",
            "nombre"    => $request->nombre,
            "email"     => $request->email,
            "mensaje"   => $request->mensaje,
        ];

        email('mensaje.nuevo', "Contacto desde TuTiendaFacil.uy", $contenido, "admin@tutiendafacil.uy");

        exito('El mensaje se envio correctamente, nos comunicaremos a la brevedad.');
        return redirect('/#contact');
    }

    public function contacto_empresa(Request $request)
    {
        $this->validate($request, [
            'g-recaptcha-response'  => 'required|recaptchav3:register,0.5',
            'nombre'                => 'required', 
            'email'                 => 'required|email', 
            'contenido'             => 'required'
        ]);

        if((Session('resultado') != $request->math) or ($request->faxonly == "on"))
        {
            error('No pudimos enviar el mensaje por sospecha de SPAM');
            return back();
        }

        $mensaje                = new Mensaje();
        $mensaje->nombre        = $request->nombre;
        $mensaje->email         = $request->email;
        $mensaje->contenido     = $request->contenido;
        $mensaje->estado        = "nuevo";
        $mensaje->empresa_id    = $this->empresa->id;
        $mensaje->save();

        $contenido = [
            "titulo"    => "Contacto desde la web",
            "nombre"    => $request->nombre,
            "email"     => $request->email,
            "mensaje"   => $request->mensaje,
        ];

        email('contacto', "Contacto desde la web", $contenido, $this->empresa->configuracion->email_admin);

        exito('El mensaje se envio correctamente, nos comunicaremos a la brevedad.');
        return back();
    }

    public function visita(Request $request)
    {
        $old = Visita::where('ip', $request->ip)
                        ->where('url', $request->url)
                        ->orderBy('created_at', 'desc')
                        ->first();

        if(($old == null) or (Carbon::now()->diffInMinutes($old->created_at) >= 30))
        {
            if($request->url != "/carrito")
            {
                $visita                 = new Visita();
                $visita->empresa_id     = $this->empresa->id;
                $visita->ip             = $request->ip;
                $visita->ciudad         = $request->ciudad;
                $visita->pais           = $request->pais;
                $visita->url            = $request->url;
                $visita->dispositivo    = $request->dispositivo;
                $visita->fecha          = date("Y-m-d");
                $visita->save();

                return true;
            }
        }

        return false;
    }

    public function newsletter(Request $request)
    {
        $old = $this->empresa->newsletters->where('email', $request->email)->first();
        
        if($old == null)
        {
            $suscriptor             = new Newsletter();
            $suscriptor->email      = $request->email;
            $suscriptor->empresa_id = $this->empresa->id;
            $suscriptor->save();

            exito("Su email fue guardado correctamente");

        }else
        {
            error("Su email ya esta registrado en la newsletter");
        }

        return back();
    }

    public function url($id)
    {
        if($this->comprobar() != null)
            return $this->comprobar();

        $producto   = $this->empresa->productos->where('url', $id)->first();

        if(($producto != null) and ($producto->estado == "activo"))
        {
            if(view()->exists($this->carpeta."producto"))
                return view($this->carpeta."producto", compact('producto'));   
        }

        $pagina     = $this->empresa->paginas->where('url', $id)->first();

        if($pagina != null)
        {
            if(view()->exists($this->carpeta."pagina-".$id))
                return view($this->carpeta."pagina-".$id);
            
            if(view()->exists($this->carpeta."pagina"))
                return view($this->carpeta."pagina", compact('pagina'));
        }
            
        return $this->error404();
    }

    public function eticket($id)
    {
        $venta = Venta::where('codigo', $id)->first();

        if($venta != null)
            return view('pago.resumen', compact('venta'));
        
        return abort(404);
    }

    public function generar_codigo()
    {
        $codigo = codigo_aleatorio_numerico(6);
        $hora   = now();

        $empresa = Auth::user()->empresa;

        $empresa->control_codigo    = $codigo;
        $empresa->control_hora      = $hora;
        $empresa->control_usuario   = usuario();
        $empresa->save();

        registro(
            "control",
            usuario(),
            usuario('empresa'),
            "Control",
            "Conceder control",
            "El usuario concedio control al ROOT"
        );

        return $codigo;
    }

    public function variante($id)
    {
        //Separo la consulta ya que la envio de la siguiente forma "18-19-29"
        $explode    = explode('-', $id);
        $array      = array();

        //Si es el primer atributo que consulto, lo busco y lo agrego al array que mas abajo devuelvo
        if(count($explode) == 1)
        {
            $valor = AtributoValor::find($id);
            $variantes = $valor->variantes->where('cantidad', '>', '0');
            
            if(empty($variantes->first()->valores[1]))
                return ['variante', $variantes];           
            

            foreach($variantes as $variante)
                array_push($array, $variante->valores[1]->id);
        }else
        {
            //Si mando mas de un atributo a consultar genero un array para guardar, reviso cuantos consulte y cuantos son en total

            $variantes  = array();
            $cantidad   = count($explode);
            $total      = AtributoValor::find($explode[0])->atributo->producto->atributos->count();

            //Recorro los id que llegan en la consulta, reviso el valor y guardo todas las variantes que estan asociadas a este atributo
            //De esta manera voy descartando las variantes que no tienen los atributos en comun
            //Guardo solo las ID para poder hacer el array_unique 

            for ($i=0; $i < $cantidad; $i++) 
            { 
                $valor = AtributoValor::find($explode[$i]);

                //ya que la consulta es un get, reviso que el valor si exista
                if($valor == null)
                    return false;

                //guardo las variantes asociadas al atributo solo si la cantidad es > 0
                
                foreach($valor->variantes->where('cantidad', '>', '0') as $variante)
                    array_push($variantes, $variante->id);
                
                //Si no es la primera vez que entra al for, agarro las variantes que se repiten, de esta manera voy a dejar solo las que se repiten en todos los tributos que recorro

                if($i > 0)
                    $variantes = array_diff_assoc($variantes, array_unique($variantes));
            }

            //Si estoy consultando por el ultimo valor de la variante, devuelvo la unica variante que debo tener y le aviso al front que estoy enviando una variante

            if($cantidad == $total)
            {
                //Ya que en vatiantes tengo un solo valor y es un ID, busco la variante asociada para enviarla

                $variante = Variante::find($variantes);

                return ['variante', $variante];
            
            }else
            {
                //Si no es la ultima, recorro la lista con los ID de las variantes y guardo los valores que estan en el nivel que consulto, es decir si envio una consulta con 2 atributos, reviso el valor[2] de las variantes que guarde mas arriba, esto nos dara los valores que estan conectados en la variante.

                foreach($variantes as $var)
                {
                    $variante = Variante::find($var);

                    array_push($array, $variante->valores[$cantidad]->id);
                }
            }
        }
        
        //Genero un nuevo array para devolver con los valores de los atributos que guarde mas arriba
        //Guardo solo los ID para poder hacer el array_unique, sino no funciona

        $array      = array_unique($array);
        $return     = array();
        
        foreach( $array as $arr)
        {
            $valor = AtributoValor::find($arr);
            array_push($return, $valor);
        }

        if(count($return) == 0)
            return null;

        return ['valores' ,$array];


    }

    private function comprobar()
    {
        if($this->empresa == null)
            return abort(404);

        elseif($this->empresa->configuracion->construccion == "on")
            return view('errors.construccion');

        elseif($this->empresa->estado == "deshabilitado")
            return view('errors.506');
        
    }

    private function error404()
    {
        if(view()->exists($this->carpeta."404"))
            return view($this->carpeta."404");
        
        return abort(404);
    }

    //*******************************************************************
    //*******************************************************************
    //*******************************************************************

    public function get_perfil()
    {
        $id = Session::get('cliente');
        
        if($id != null)
        {
            $cliente = Cliente::find($id)->first();
            
            return view("usuario.home", compact('cliente'));
        }

        return view("usuario.login");
    }

    public function set_perfil(Request $request)
    {
        $empresa = Empresa::find($request->empresa);
        $cliente = $empresa->clientes->where('email', $request->email)->first();

        if(($cliente == null) or ($cliente->contrasena == null))
        {
            error('El usuario no tiene una cuenta creada, coloca tu email y crearemos una.');
            return redirect('perfil/nuevo');
        }

        if(!Hash::check($request->pin, $cliente->contrasena))
        {
            error("Usuario o contraseña incorrectos.");
            return back()->withInput();
        }

        Session::push('cliente', $cliente->id);
        return view("usuario.home", compact('cliente'));
    }

    public function set_perfil_silencioso(Request $request)
    {
        $empresa = Empresa::find($request->empresa);
        $cliente = $empresa->clientes->where('email', $request->email)->first();

        if(($cliente == null) or ($cliente->contrasena == null))
            return [false, "El usuario no tiene una cuenta creada"];
        

        if(!Hash::check($request->pin, $cliente->contrasena))
            return [false, "Usuario o contraseña incorrectos"];
        
        Session::push('cliente', $cliente->id);
        
        return [true, ""];
    }

    public function get_crear_cuenta()
    {
        return view("usuario.crear_cuenta");
    }

    public function set_crear_cuenta(Request $request)
    {
        Session::forget('cliente');

        $empresa = Empresa::find($request->empresa);
        $cliente = $empresa->clientes->where('email', $request->email)->first();

        if(($cliente != null) and ($cliente->contrasena != null))
        {
            error("El cliente ya tiene una cuenta creada, si no recuerda el PIN solicitelo nuevamente.");
            return view('usuario.login');
        }

        $pin                    = codigo_aleatorio_numerico(4);

        if($cliente == null)
        {
            $cliente                = new Cliente();
            $cliente->email         = $request->email;
            $cliente->empresa_id    = $empresa->id;
        }
        
        $cliente->tmp           = "false";
        $cliente->contrasena    = Hash::make($pin);
        $cliente->save();

        Session::push('cliente', $cliente->id);

        $contenido = [
            "titulo"    => "Nuevo usuario creado",
            "texto"     => "Se creo un nuevo usuario para la web ".$cliente->empresa->nombre.", puedes ingresar cuando desees con el email y el PIN.",
            "email"     => $cliente->email,
            "pin"       => $pin
        ];

        email("nuevo_usuario", "Nuevo usuario creado", $contenido, $cliente->email);
        exito('Se creo el usuario y se envio un PIN por email');

        return redirect('perfil');
    }

    public function perfil_logout()
    {
        Session::forget('cliente');
        return redirect('/');
    }

    public function get_reenviar_pin()
    {
        return view("usuario.pin");
    }

    public function set_reenviar_pin(Request $request)
    {
        Session::forget('cliente');

        $empresa = Empresa::find($request->empresa);
        $cliente = $empresa->clientes->where('email', $request->email)->first();

        $pin                    = codigo_aleatorio_numerico(4);

        if($cliente == null)
        {
            $cliente                = new Cliente();
            $cliente->email         = $request->email;
            $cliente->empresa_id    = $empresa->id;

            $contenido = [
                "titulo"    => "Nuevo usuario creado",
                "texto"     => "Se creo un nuevo usuario para la web ".$cliente->empresa->nombre.", puedes ingresar cuando desees con el email y el PIN.",
                "email"     => $cliente->email,
                "pin"       => $pin
            ];

            email("nuevo_usuario", "Nuevo usuario creado", $contenido, $cliente->email);
            exito('Se creo el usuario y se envio un PIN por email');

            Session::push('cliente', $cliente->id);
        }else
        {
            $contenido = [
                "titulo"    => "Nuevo PIN generado",
                "texto"     => "Se genero un nuevo PIN para la cuenta de ".$cliente->empresa->nombre.", puedes ingresar cuando desees con el email y el PIN.",
                "email"     => $cliente->email,
                "pin"       => $pin
            ];

            email("nuevo_usuario", "Nuevo PIN generado", $contenido, $cliente->email);
            exito('Se genero un nuevo PIN y se envio por correo');
        }
        
        $cliente->tmp           = "false";
        $cliente->contrasena    = Hash::make($pin);
        $cliente->save();

        return redirect('perfil')->withInput();
    }

    public function get_compra($id)
    {
        $venta  = Venta::find($id);
        $id     = Session::get('cliente');
        
        if($id != null)
        {
            $cliente = Cliente::find($id)->first();

            if($cliente->id == $venta->cliente_id)
                return view("usuario.compra", compact('cliente', 'venta'));
        }

        return redirect('perfil');
    }

    public function get_datos()
    {
        $id = Session::get('cliente');
        
        if($id != null)
        {
            $cliente = Cliente::find($id)->first();
            
            return view("usuario.datos", compact('cliente'));
        }

        return view("usuario.login");
    }

    public function set_datos(Request $request)
    {
        $cliente = Cliente::find($request->cliente);

        if($cliente == null)
        {
            error('No se pueden modificar los datos');
            return back();
        }

        $cliente->nombre            = $request->nombre;
        $cliente->apellido          = $request->apellido;
        $cliente->documento         = $request->documento;
        $cliente->telefono          = $request->telefono;
        $cliente->fecha_nacimiento  = $request->fecha_nacimiento;
        $cliente->ciudad            = $request->ciudad;
        $cliente->direccion         = $request->direccion;
        $cliente->apartamento       = $request->apartamento;

        if($request->pin != null)
            $cliente->pin = Hash::make($request->pin);

        $cliente->save();

        exito('Los datos se modificaron correctamente');
        return back();
    }

    public function set_deseos(Request $request)
    {
        $id = Session::get('cliente');
        
        if($id != null)
        {
            $cliente    = Cliente::find($id)->first();
            $producto   = Producto::find($request->producto);
            
            if(!producto_es_deseo($producto->id))
                $producto->deseos()->attach($cliente);
            else
                $producto->deseos()->detach($cliente);

            return true;
        }

        return false;
    }

    public function get_deseos()
    {
        $id = Session::get('cliente');
        
        if($id != null)
        {
            $cliente = Cliente::find($id)->first();
            
            return view("usuario.deseos", compact('cliente'));
        }

        return view("usuario.login");
    }

    public function eliminar_deseos(Request $request)
    {
        $id = Session::get('cliente');

        if($id != null)
        {
            $cliente    = Cliente::find($id)->first();
            $producto   = Producto::find($request->producto);

            $cliente->deseos()->detach($producto);

            exito('El producto se elimino de la lista de deseos');
            
            return view("usuario.deseos", compact('cliente'));
        }

    }
}

