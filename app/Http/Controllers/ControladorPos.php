<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Cliente;
use App\Models\Variante;
use Session;

class ControladorPos extends Controller
{

    private $path = "admin.pos.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias     = empresa()->categorias;
        $sin_categoria  = Producto::where('empresa_id', empresa()->id)->doesntHave('categorias')->count();

        return view($this->path.'index', compact('categorias', 'sin_categoria'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = Producto::find($request->producto);
        $variante = $request->variante;
        
        if($variante != "NULL")
            $variante = Variante::find($request->variante);

        $coleccion = collect([
            "id" => codigo_aleatorio(10),
            "producto" => $producto, 
            "variante" => $variante,
            "cantidad" => $request->cantidad
        ]);

        Session::push('lista_productos', $coleccion);   

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $productos = null;

        if($id == 'sin_categoria')
        {
            $productos = Producto::where('empresa_id', empresa()->id)->doesntHave('categorias')->get();
            $categoria = "Sin Categoria";

            return view($this->path.'partes.productos', compact('productos', 'categoria'));
        }

        if(!is_numeric($id))
            return abort('404');
        
        $categoria = empresa()->categorias->where('id', $id)->first();
        
        if($categoria != null)
        {
            $productos = $categoria->productos;
            $categoria = $categoria->titulo;
        }
        
        return view($this->path.'partes.productos', compact('productos', 'categoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productos = Session::get('lista_productos');

        if($productos != null)
        {
            Session::forget('lista_productos');

            foreach ($productos as $producto)
            {
                if($producto['id'] != $id)
                {
                    $coleccion = collect([
                            "id" => $producto['id'], 
                            "producto" => $producto['producto'], 
                            "variante" => $producto['variante'],
                            "cantidad" => $producto['cantidad']
                        ]);

                    Session::push('lista_productos', $coleccion);
                }
            }
        }
        
        return true;
    }

    public function buscar($string)
    {
        $string = str_replace('-', ' ', $string);
        
        $productos = Producto::where('empresa_id', empresa()->id)
                                ->where(function ($query) use ($string) {
                                    $query->where('sku', 'LIKE', '%'.$string.'%')
                                        ->orWhere('nombre', 'LIKE', '%'.$string.'%')
                                        ->orWhere('descripcion', '=', $string);
                                })
                                ->get();
        
        return view($this->path.'partes.productos', compact('productos'));
    }

    public function buscar_cliente(Request $request)
    {
        $clientes = Cliente::where('empresa_id', empresa()->id)
                                ->where('tmp', 'false')
                                ->where(function ($query) use ($request) {
                                    $query->where('email', 'LIKE', '%'.$request->buscar.'%')
                                        ->orWhere('nombre', 'LIKE', '%'.$request->buscar.'%')
                                        ->orWhere('apellido', 'LIKE', $request->buscar)
                                        ->orWhere('telefono', 'LIKE', $request->buscar);
                                })
                                ->get();

        return view($this->path.'partes.clientes', compact('clientes'));
        
    }

    public function carrito()
    {
        return view($this->path.'partes.carrito');
    }
}
