<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ConfiguracionVisual as Visual;

class ControladorConfiguracionVisual extends Controller
{
    private $path = "admin.visual.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path."index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $array = [
            ['Dashboard tienda', 'tienda_dashboard'], 
            ['Menu tienda', 'tienda_menu']
        ];

        foreach(empresa()->plugins as $plugin)
        {
            array_push($array, [$plugin->nombre, $plugin->nombre.'_dashboard']);
            array_push($array, [$plugin->nombre, $plugin->nombre.'_menu']);
        }
        
        foreach($array as $arr)
        {
            $visual = visual($arr[1]);
            
            if($visual == null)
            {
                $visual             = new Visual();
                $visual->nombre     = $arr[0];
                $visual->key        = $arr[1];
                $visual->valor      = $request[$arr[1]];
                $visual->user_id    = usuario('id');
                $visual->save();
            }else
            {
                $visual->key        = $arr[1];
                $visual->valor      = $request[$arr[1]];
                $visual->save();
            }
        }

        exito("La configuración visual se modifico correctamente");
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
