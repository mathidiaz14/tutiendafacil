<?php

namespace App\Http\Controllers\Plugins\MercadoLibre;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Producto;
use App\Models\Multimedia;
use App\Models\Categoria;
use App\Models\Plugins\MercadoLibre\MercadoLibre;
use App\Models\Plugins\MercadoLibre\MercadoLibreAtributos;
use App\Models\Plugins\MercadoLibre\MercadoLibreCategoria;
use Http;

class MercadoLibreControladorConfiguracion extends Controller
{
    private $path = "Plugins.MercadoLibre.configuracion.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if((empty(empresa()->mercado_pago)) or (empresa()->mercado_pago->estado == null) or (empresa()->mercado_pago->estado == "desconectado"))
            return view($this->path.'conectar');

        $usuario_ml = Http::timeout(60)->withHeaders(ml_headers_empresa(empresa()))
                                ->get('https://api.mercadolibre.com/users/me')
                                ->json();
        
        if(empty($usuario_ml['nickname']))
            return view($this->path.'conectar');
        
        $cantidad     = Http::get('https://api.mercadolibre.com/sites/MLU/search?nickname='.$usuario_ml['nickname'])->json();
                
        return view($this->path.'index', compact('usuario_ml', 'cantidad'));
    }

    public function importar(Request $request)
    {
        guardar_comando_ejecutar('ttf:ml:productos:importar '.empresa()->id);
        info('Te avisaremos cuando finalice la importación');

        return back();
    }

    public function exportar(Request $request)
    {
        $categorias = Categoria::where('mlu_id', null)->count();

        if($categorias > 0)
        {
            error('Todas las categorias de la plataforma deben tener asociada una categoria de MercadoLibre para poder publicar');

            return redirect('admin/mercadolibre/configuracion/asociar/categoria');
        }

        guardar_comando_ejecutar('ttf:ml:productos:exportar '.empresa()->id);
        info('Te avisaremos cuando finalice la exportacion');

        return back();
    }

    public function asociar(Request $request)
    {
        
    }

    public function get_asociar_categoria()
    {
        $categorias_ml  = MercadoLibreCategoria::all();
        $categorias     = empresa()->categorias;

        return view($this->path."categorias", compact('categorias', 'categorias_ml'));

    }

    public function set_asociar_categoria(Request $request)
    {

    }

    public function conexion(Request $request)
    {
        if($request->state == null)
            abort(500);

        $empresa                            = Empresa::find($request->state);
        
        $response = Http::post('https://api.mercadopago.com/oauth/token', [
            'grant_type'        => "authorization_code", 
            'client_secret'     => env('ML_CLIENT_SECRET'),
            'client_id'         => env('ML_CLIENT_ID'), 
            'code'              => $request->code,
            'redirect_uri'      => url('admin/mercadolibre/conexion')
        ]);
        
        if(empty($response->json()['access_token']))
            abort(500);

        $mercado_lago                   = new MercadoLibre();
        $mercado_lago->empresa_id       = $empresa->id;

        $mercado_lago->estado           = "conectado";
        $mercado_lago->access_token     = $response->json()['access_token'];
        $mercado_lago->public_key       = $response->json()['public_key'];
        $mercado_lago->refresh_token    = $response->json()['refresh_token'];
        $mercado_lago->user_id          = $response->json()['user_id'];
        $mercado_lago->expires_in       = $response->json()['expires_in'];
        $mercado_lago->expires_date     = \Carbon\Carbon::now();
        $mercado_lago->save();

        registro(
            "info",
            "sistema",
            $empresa->id,
            "MercadoLibre",
            "Conexion",
            "Se conecto con MercadoLibre"
        );

        return redirect("admin");
    }
}

