<?php

namespace App\Http\Controllers\Plugins\Inmobiliaria;

use App\Http\Controllers\Controller;
use App\Models\Plugins\Inmobiliaria\InmobiliariaPlano;
use Illuminate\Http\Request;

class InmobiliariaControladorPlano extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPlano  $inmobiliariaPlano
     * @return \Illuminate\Http\Response
     */
    public function show(InmobiliariaPlano $inmobiliariaPlano)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPlano  $inmobiliariaPlano
     * @return \Illuminate\Http\Response
     */
    public function edit(InmobiliariaPlano $inmobiliariaPlano)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPlano  $inmobiliariaPlano
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InmobiliariaPlano $inmobiliariaPlano)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPlano  $inmobiliariaPlano
     * @return \Illuminate\Http\Response
     */
    public function destroy(InmobiliariaPlano $inmobiliariaPlano)
    {
        //
    }
}
