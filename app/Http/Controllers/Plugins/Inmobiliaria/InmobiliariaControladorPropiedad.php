<?php

namespace App\Http\Controllers\Plugins\Inmobiliaria;

use App\Http\Controllers\Controller;
use App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad;
use Illuminate\Http\Request;

class InmobiliariaControladorPropiedad extends Controller
{
    private $path = "Plugins.Inmobiliaria.propiedad.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path."create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $propiedad                  = new InmobiliariaPropiedad();
        $propiedad->empresa_id      = empresa()->id;
        $propiedad->user_id         = usuario()->id;
        $propiedad->titulo          = $request->titulo;
        $propiedad->descripcion     = $request->descripcion;
        $propiedad->tipo            = $request->tipo;
        $propiedad->precio          = $request->precio;
        $propiedad->estado          = $request->estado;
        $propiedad->direccion       = $request->direccion;
        $propiedad->ciudad          = $request->ciudad;
        $propiedad->localidad       = $request->localidad;
        $propiedad->pais            = "Uruguay";
        $propiedad->codigo_postal   = $request->codigo_postal;
        $propiedad->dormitorios     = $request->dormitorios;
        $propiedad->banos           = $request->banos;
        $propiedad->area            = $request->area;
        $propiedad->garaje          = $request->garaje;
        $propiedad->destacado       = $request->destacado;
        $propiedad->venta           = $request->venta;
        $propiedad->save();

        exito('La propiedad se creo correctamente, ya puede modificarla');
        
        return redirect('admin/inmobiliaria/propiedad/'.$propiedad->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function show(InmobiliariaPropiedad $propiedad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function edit(InmobiliariaPropiedad $propiedad)
    {
        return view($this->path."edit", compact('propiedad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InmobiliariaPropiedad $propiedad)
    {
        $propiedad->titulo          = $request->titulo;
        $propiedad->descripcion     = $request->descripcion;
        $propiedad->tipo            = $request->tipo;
        $propiedad->precio          = $request->precio;
        $propiedad->estado          = $request->estado;
        $propiedad->direccion       = $request->direccion;
        $propiedad->ciudad          = $request->ciudad;
        $propiedad->localidad       = $request->localidad;
        $propiedad->codigo_postal   = $request->codigo_postal;
        $propiedad->dormitorios     = $request->dormitorios;
        $propiedad->banos           = $request->banos;
        $propiedad->area            = $request->area;
        $propiedad->garaje          = $request->garaje;
        $propiedad->destacado       = $request->destacado;
        $propiedad->venta           = $request->venta;
        $propiedad->save();

        exito('La propiedad se edito correctamente');
        
        return redirect('admin/inmobiliaria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad  $propiedad
     * @return \Illuminate\Http\Response
     */
    public function destroy(InmobiliariaPropiedad $propiedad)
    {
        $propiedad->caracteristicas()->detach();
        $propiedad->imagenes()->delete();
        $propiedad->planos()->delete();

        $propiedad->delete();

        exito('La propiedad se elimino correctamente');
        return back();
    }
}
