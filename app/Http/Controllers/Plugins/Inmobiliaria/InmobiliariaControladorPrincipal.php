<?php

namespace App\Http\Controllers\Plugins\Inmobiliaria;

use App\Http\Controllers\Controller;
use App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad as Propiedad;
use Illuminate\Http\Request;

class InmobiliariaControladorPrincipal extends Controller
{
    private $empresa;
    private $carpeta;
    private $path = 'Plugins.Inmobiliaria.';

    public function __construct()
    {
        $this->empresa = empresa();
        
        if($this->empresa != null)
            $this->carpeta = 'empresas.'.$this->empresa->id.".".$this->empresa->carpeta.".";

        if($this->comprobar() != null)
            return $this->comprobar();
    }

    public function index()
    {
        $propiedades = empresa()->inmobiliaria_propiedades;
        
        return view($this->path.'index', compact('propiedades'));
    }

    public function ver_propiedades($tipo = "todos")
    {
        if($tipo == "todos")
            $propiedades = empresa()->inmobiliaria_propiedades;
        else if ($tipo == "venta")
            $propiedades = empresa()->inmobiliaria_propiedades->where('venta', 'on');
        else if ($tipo == "alquiler")
            $propiedades = empresa()->inmobiliaria_propiedades->where('venta', '!=', 'on');
        else if($tipo == "terreno")
            $propiedades = empresa()->inmobiliaria_propiedades->where('tipo', 'terreno');
        
        if(view()->exists($this->carpeta.".Inmobiliaria.propiedades"))
            return view($this->carpeta.".Inmobiliaria.propiedades", compact('propiedades'));   

        return $this->error404();
    }

    public function ver_propiedad($id)
    {
        $propiedad = Propiedad::find($id);

        if(($propiedad == null) or ($propiedad->empresa_id != empresa()->id) or ($propiedad->estado == "inactivo"))
            return $this->error404();
        
        if(view()->exists($this->carpeta.".Inmobiliaria.propiedad"))
            return view($this->carpeta.".Inmobiliaria.propiedad", compact('propiedad'));   

        return $this->error404();
    }

    public function buscar_propiedades(Request $request)
    {   
        $propiedades = Propiedad::where('empresa_id', '=', empresa()->id)
                                ->where('venta', $request->operacion == "venta" ? "on" : "off")
                                ->where(function ($query) use ($request) {
                                    $query->where('tipo', $request->tipo)
                                        ->orWhere('estado', $request->estado)
                                        ->orWhere('localidad', 'LIKE', '%'.$request->departamento.'%')
                                        ->orWhere('dormitorios', $request->dormitorios)
                                        ->orWhere('banos', $request->banos)
                                        ->orWhere('garaje', $request->garaje);
                                })
                                ->get();

        if(view()->exists($this->carpeta.".Inmobiliaria.propiedades"))
            return view($this->carpeta.".Inmobiliaria.propiedades", compact('propiedades'));   

        return $this->error404();
    }

    public function contacto(Request $request)
    {
        
    }

    private function comprobar()
    {
        if($this->empresa == null)
            return abort(404);

        elseif($this->empresa->configuracion->construccion == "on")
            return view('errors.construccion');

        elseif($this->empresa->estado == "deshabilitado")
            return view('errors.506');
        
    }

    private function error404()
    {
        if(view()->exists($this->carpeta."404"))
            return view($this->carpeta."404");
        
        return abort(404);
    }
}
