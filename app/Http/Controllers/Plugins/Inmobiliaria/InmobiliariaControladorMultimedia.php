<?php

namespace App\Http\Controllers\Plugins\Inmobiliaria;

use App\Http\Controllers\Controller;
use App\Models\Plugins\Inmobiliaria\InmobiliariaMultimedia as Multimedia;
use App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad as Propiedad;
use Illuminate\Http\Request;
use File;
use Storage;

class InmobiliariaControladorMultimedia extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file       = $request->file('file');
        
        if($file == null)
            return false;
    
        $name       = $file->getClientOriginalName();
        $nameFile   = codigo_aleatorio(20).".".$file->getClientOriginalExtension();
        
        Storage::disk('inmobiliaria')->put($nameFile, File::get($file));

        if($file->getClientOriginalExtension() != 'mp4')
            reducir_imagen(public_path("storage/inmobiliaria/".$nameFile));

        $propiedad   = Propiedad::find($request->propiedad);

        $ultimo     = null;
        
        foreach($propiedad->imagenes as $imagen)
            $ultimo = $imagen->numero;

        $media                  = new Multimedia();
        $media->url             = "storage/inmobiliaria/".$nameFile;
        $media->nombre          = $name;    
        $media->empresa_id      = empresa()->id;
        $media->propiedad_id    = $request->propiedad;
        $media->numero          = $ultimo + 1;
        $media->save();

        return true;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaMultimedia  $inmobiliariaMultimedia
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $propiedad = Propiedad::find($id);

        return view('Plugins.Inmobiliaria.propiedad.partes.imagenes', compact('propiedad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaMultimedia  $inmobiliariaMultimedia
     * @return \Illuminate\Http\Response
     */
    public function edit(InmobiliariaMultimedia $inmobiliariaMultimedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaMultimedia  $inmobiliariaMultimedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        foreach($request->array as $array)
        {
            $media = Multimedia::find($array[0]);
            $media->numero = $array[1];
            $media->save();
        }
        
        return true;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaMultimedia  $inmobiliariaMultimedia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Multimedia::find($id);

        if(file_exists($media->url))
            unlink($media->url);

        $media->delete();

        exito('La imagen se elimino correctamente');

        return back();
    }
}
