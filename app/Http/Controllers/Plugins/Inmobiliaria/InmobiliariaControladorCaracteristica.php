<?php

namespace App\Http\Controllers\Plugins\Inmobiliaria;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plugins\Inmobiliaria\InmobiliariaCaracteristica as Caracteristica;
use App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad as Propiedad;

class InmobiliariaControladorCaracteristica extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $caracteristica             = new Caracteristica();
        $caracteristica->empresa_id = empresa()->id;
        $caracteristica->nombre     = $request->nombre;
        $caracteristica->tipo       = "";
        $caracteristica->save();


        $propiedad = Propiedad::find($request->propiedad);
        $propiedad->caracteristicas()->attach($caracteristica, ['valor' => $request->valor]);

        exito('La caracteristica se agrego correctamente');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaCaracteristica  $inmobiliariaCaracteristica
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaCaracteristica  $inmobiliariaCaracteristica
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaCaracteristica  $inmobiliariaCaracteristica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $propiedad = Propiedad::find($id);
        
        foreach($propiedad->caracteristicas as $caracteristica)
        {
            $propiedad->caracteristicas()->detach($caracteristica);
            $propiedad->caracteristicas()->attach($caracteristica, ['valor' => $request['caracteristica_valor_'.$caracteristica->id]]);
        }

        exito('Las caracteristicas se modificaron correctamente');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plugins\Inmobiliaria\InmobiliariaCaracteristica  $inmobiliariaCaracteristica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
    }

    public function attach(Request $request)
    {
        $propiedad      = Propiedad::find($request->propiedad);
        $caracteristica = Caracteristica::find($request->caracteristica);

        $propiedad->caracteristicas()->attach($caracteristica, ['valor' => $request->valor]);

        exito("La caracteristica se agrego correctamente");
        return back();
    }

    public function detach(Request $request)
    {
        $propiedad      = Propiedad::find($request->propiedad);
        $caracteristica = Caracteristica::find($request->caracteristica);

        $propiedad->caracteristicas()->detach($caracteristica);

        exito("La caracteristica se elimino correctamente");
        return true;
    }
}
