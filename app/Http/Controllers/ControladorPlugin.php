<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Plugin;
use App\Models\ConfiguracionVisual as Visual;
use Auth;
use Http;

class ControladorPlugin extends Controller
{
    private $path = "admin.plugin.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $misPlugins = Auth::user()->empresa->plugins;
        $plugins    = Plugin::all();

        return view($this->path."index", compact('misPlugins', 'plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plugin = Plugin::find($id);

        if($plugin == null)
        {
            error('Error al eliminar plugin');
            return back();
        }

        Auth::user()->empresa->plugins()->detach($plugin);

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Plugin",
            "Eliminar",
            "Se elimina plugin"
        );

        exito("El plugin se desinstalo correctamente.");
        return back();
    }

    public function instalar($id)
    {
        $plugin     = Plugin::find($id);
        
        if($plugin == null)
        {
            error('No se pudo instalar el plugin');
            return back();
        }

        $empresa    = Auth::user()->empresa;

        $empresa->plugins()->attach($plugin, ['estado' => 'instalado']);
        
        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Plugin",
            "Instalar",
            "Se instalo plugin"
        );

        exito("El plugin se instalo correctamente");        
        return back();
    }

    public function activar($id)
    {
        $plugin     = Plugin::find($id);

        if($plugin == null)
        {
            error('No se pudo activar el plugin');
            return back();
        }

        $empresa    = Auth::user()->empresa;
        
        if(!$this->comprobar_plugin($plugin->id))
        {
            error('La empresa no tiene instalado este plugin');
            return redirect('admin/plugin');
        }

        $empresa->plugins()->detach($plugin);
        $empresa->plugins()->attach($plugin, ['estado' => 'activo']);

        //esta linea ejecuta el archivo install de cada plugin ya que cada uno realiza acciones diferentes al instalarse.
        if(file_exists(resource_path('views/plugins/'.$plugin->carpeta.'/install.php')))
            include(resource_path('views/plugins/'.$plugin->carpeta.'/install.php'));

        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Plugin",
            "Activar",
            "Se activa plugin"
        );

        exito("El plugin ".$plugin->nombre." se activo correctamente");
        return back();    
    }

    public function desactivar($id)
    {
        $plugin     = Plugin::find($id);

        if($plugin == null)
        {
            error('Error al desactivar plugin');
            return back();
        }

        $empresa    = Auth::user()->empresa;

        if(!$this->comprobar_plugin($plugin->id))
        {
            error('La empresa no tiene instalado este plugin');
            return redirect('admin/plugin');
        }

        $empresa->plugins()->detach($plugin);
        $empresa->plugins()->attach($plugin, ['estado' => 'instalado']);
        
        registro(
            "info",
            usuario(),
            usuario('empresa'),
            "Plugin",
            "Desactivar",
            "Se desacriva plugin"
        );

        exito("El plugin se desactivo correctamente");
        return back();    
    }

    public function pago(Request $request, $id)
    {
        $plugin     = Plugin::find($id);
        
        $body = [
            'preapproval_plan_id'           => $plugin->suscripcion,
            'reason'                        => $plugin->suscripcion_nombre,
            'external_reference'            => $plugin->suscripcion_nombre,
            'payer_email'                   => $request->payer['email'],
            'card_token_id'                 => $request->token,
            'back_url'                      => url('admin/plugin'),
            'status'                        => "authorized",
        ];
        
        //Guardo el plan
        $response = Http::withHeaders(['Authorization' => 'Bearer '.env("MP_ACCESS_TOKEN")])
                            ->post("https://api.mercadopago.com/preapproval", $body);
        
        //Si el pago es aprobado entro al if
        if($response->json()['status'] === "authorized")
        {
            //Guardo los datos del plan
            empresa()->plugins()->attach($plugin, ['mp_id' => $response->json()['id'], 'estado' => 'instalado']);

            exito('Su suscripción se aprobo correctamente');

            return [
                "status"    => true,
                "message"   => "",
            ];
        }
        
        //Si el pago no fue aprobado muestro el error que devuelve mercadopago
        return [
            "status"    => false,
            "message"   => 'Pago rechazado, error: '.$response->json()['message'],
        ];
    }

    private function comprobar_plugin($id)
    {
        $empresa    = Auth::user()->empresa;
        $flag       = false;

        foreach($empresa->plugins as $plugin)
        {
            if($plugin->id == $id)
                $flag = true;
        }

        return $flag;
    }
}
