<?php

namespace App\Console\Commands\Sistema;

use Illuminate\Console\Command;
use App\Models\ComandoEjecutar;

class EjecutarComando extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:ejecutar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ejecuta comandos de la tabla EjecutarComando';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');

        $comando = ComandoEjecutar::where('estado', 'pendiente')->first();

        if($comando == null)
        {
            $this->info('No hay ningun comando para ejecutar');
            return false;
        }
        
        $comando->estado    = 'ejecutando';
        $comando->save();
        
        $this->info('################################################');
        $this->info('-> Ejecutando comando: '.$comando->cmd);
        $this->info('################################################');
        $this->newLine(2);

        Artisan::call($comando->cmd, [], $this->getOutput());

        $comando->delete();
        
        $this->newLine(2);
        $this->info('########################################################');
        $this->info('################### Comando completo ###################');
        $this->info('########################################################');
        $this->newLine();

        comando_hora($this->signature, 'fin');
    }
}
