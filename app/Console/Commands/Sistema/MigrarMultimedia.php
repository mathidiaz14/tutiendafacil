<?php

namespace App\Console\Commands\Sistema;

use Illuminate\Console\Command;
use App\Models\Multimedia;
use App\Models\Producto;

class MigrarMultimedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:migrar_multimedia_producto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migra las imagenes de los productos de la versión anterior a la nueva';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('***************************');
        $this->info('**** Inicia el proceso ****');
        $this->info('***************************');
        $this->newLine(2);

        $multimedia = Multimedia::all();

        $bar        = $this->output->createProgressBar(count($multimedia));
        $bar->start();

        foreach($multimedia as $multimedia) 
        {
            $producto = Producto::find($multimedia->producto_id);

            $producto->imagenes()->attach($multimedia,['posicion' => $multimedia->numero]);

            $bar->advance();
        }

        $bar->finish();

        $this->newLine(2);
        $this->info('------------------------');
        $this->info('-> Termina el proceso');
        $this->info('------------------------');
        $this->newLine(2);
    }
}
