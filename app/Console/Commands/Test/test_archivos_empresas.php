<?php

namespace App\Console\Commands\Test;

use Illuminate\Console\Command;

class test_archivos_empresas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:test:archivos_empresas';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Controla los archivos de los temas que tienen las empresas';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');

        $this->info('********************************************');
        $this->info('*** Inicia testeo de archivos de empresa ***');
        $this->info('********************************************');

        $empresas   = \App\Models\Empresa::all();
        
        $archivos   = collect([
            "info.xml",
            "install.xml",
            "index.blade.php",
            "master.blade.php",
            "carrito.blade.php",
            "categoria.blade.php",
            "pagina.blade.php",
            "producto.blade.php",
            "productos.blade.php",
            "screenshot.png",
        ]);

        $empresas_error = null;
        $flag           = true;


        foreach($empresas as $empresa)
        {
            $ruta       = resource_path("views/empresas/".$empresa->id."/".$empresa->carpeta);

            for($i = 0; $i < count($archivos); $i++)
            {             
                if(!file_exists($ruta."/".$archivos[$i]))
                {
                    $flag = false;
                    $this->error("-> ".$empresa->nombre." - ".$archivos[$i]);
                    $empresas_error .= "-   ".$empresa->nombre." - ".$archivos[$i]."<br>";
                }
            }
        }
        
        if($flag)
        {
            $mensaje = "El comando se ejecuto correctamente";
        }
        else
        {
            $mensaje = "Hay algunas empresas con archivos corruptos: <br>".$empresas_error;

            $data = [
                "comando" => "test_archivos_empresas",
                "mensaje" => $mensaje
            ];

            email('root.testeo', "Error en test", $data, 'admin@tutiendafacil.uy');
        }

        $this->newLine();
        $this->info('*********************************');
        $this->info('Finalizo la ejecución del comando');
        $this->info('*********************************');

        comando_registro($this->signature, $mensaje);
        comando_hora($this->signature, 'fin');
    }
}
