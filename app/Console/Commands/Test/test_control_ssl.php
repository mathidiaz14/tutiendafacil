<?php

namespace App\Console\Commands\Test;

use Illuminate\Console\Command;
use App\Models\Ssl;

class test_control_ssl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:test:control_ssl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checkeo el certificado SSL de las paginas guardadas';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');

        $this->info('***************************');
        $this->info('Iniciando comando');
        $this->info('***************************');
        $this->newLine(2);

        $urls       = Ssl::all();
        $flag       = true;
        $empresas   = null;

        foreach($urls as $url)
        {
            $this->newLine();
            $this->info('-----');
            $this->info('Comprobando url: '.$url->url);

            $dias = control_ssl($url->url);

            if(!$dias)
            {
                $flag       = false;
                $empresas   .= "- ".$url->url."<br>";
                $this->error('- Certificado vencido o URL inaccesible');
            }
            elseif($dias < 15)
            {
                $flag       = false;
                $empresas   .= "- ".$url->url." Vence: ".\Carbon\Carbon::now()->addDays($dias)->format('d/m/Y')."<br>";

                $this->error("- ".$url->url." Vence: ".\Carbon\Carbon::now()->addDays($dias)->format('d/m/Y'));
            }else
            {
                $this->info("- Correcto, vence el ".\Carbon\Carbon::now()->addDays($dias)->format('d/m/Y'));
            }
        }

        if($flag)
        {
            $mensaje = "Se ejecuto el comando y los certificados estan correctos.";
        }
        else
        {
            $mensaje = "Hay algunas empresas con certificados vencidos: <br>". $empresas;

            $data = [
                    "comando" => $this->signature,
                    "mensaje" => $mensaje
                ];

                email('root.testeo', "Error en testeo control de SSL", $data, 'admin@tutiendafacil.uy');
        }

        $this->newLine();
        $this->info('*********************************');
        $this->info('Finalizo la ejecución del comando');
        $this->info('*********************************');
        
        comando_registro($this->signature, $mensaje);
        comando_hora($this->signature, 'fin');
    }
}

