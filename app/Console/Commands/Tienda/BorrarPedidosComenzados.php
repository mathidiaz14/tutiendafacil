<?php

namespace App\Console\Commands\Tienda;

use Illuminate\Console\Command;
use App\Models\Venta;
use Carbon\Carbon;

class BorrarPedidosComenzados extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:pedidos_comenzados';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Borra los pedidos comenzados con mas de 24 horas de diferencia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');

        $this->info("************************************************");
        $this->info("*** Comenzando borrado de pedidos comenzados ***");
        $this->info("************************************************");
     
        $ventas     = Venta::where('estado', 'comenzado')->get();
        $now        = Carbon::now();

        $count      = 0;
        $recordado  = 0;
        
        $bar = $this->output->createProgressBar(count($ventas));
        $bar->start();

        foreach($ventas as $venta)
        {
            if($now->diffInHours($venta->created_at) >= 24)
            {
                if(($venta->cliente_email == null) or ($now->diffInHours($venta->created_at) >= 47))
                {
                    $venta->delete();
                    $venta->estados()->delete();

                    $count ++;
                }else
                {
                    $contenido = [
                        "pagina"    => $venta->empresa->url,
                        "url"       => url('checkout/pago', $venta->codigo),
                    ];
                    
                    email("recordatorio_compra", "Finaliza tu compra", $contenido, $venta->cliente_email);

                    $recordado ++;
                }
            }
            $bar->advance();
        }

        $bar->finish();

        if($count > 0)
        {
            $this->newLine();
            $this->info('-----------------------------------');
            $this->info('Se eliminaron '.$count.' ventas');
            $this->info('-----------------------------------');
        }
        else
        {
            $this->newLine();
            $this->info('------------------------');
            $this->info('No se eliminaron ventas');
            $this->info('------------------------');
        }

        if($recordado > 0)
        {
            $this->newLine();
            $this->info('------------------------------------');
            $this->info('Se recordaron '.$recordado.' ventas');
            $this->info('------------------------------------');
        }

        $this->info("***************************");
        $this->info("*** Finaliza el proceso ***");
        $this->info("***************************");

        $mensaje = "Se eliminaron ".$count." ventas y se enviaron ".$recordado." recordatorios de ventas pendientes";
        
        comando_registro($this->signature, $mensaje);
        comando_hora($this->signature, $mensaje);
    }
}
