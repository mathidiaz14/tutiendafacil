<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;
use App\Models\Plugins\MercadoLibre\MercadoLibreCategoria;
use Http;

class CargarCategorias extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:ml:cargar_categorias';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que carga todas las categorias de MercadoLibre';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('*************************************************');
        $this->info('Inicio la carga de las categorias de MercadoLibre');
        $this->info('*************************************************');
        $this->newLine(2);

        $this->info('--------------------------------------');
        $this->info('-> Eliminando las categorias guardadas');
        $this->info('--------------------------------------');

        //Elimino las categorias que estan creadas
        $categorias_eliminar    = MercadoLibreCategoria::all();
        
        $bar                    = $this->output->createProgressBar($categorias_eliminar->count());
        $bar->start();

        foreach($categorias_eliminar as $categoria_eliminar)
        {
            $categoria_eliminar->delete();
            $bar->advance();
        }

        $bar->finish();

        $this->newLine(2);
        $this->info('---------------------------------');
        $this->info('-> Cargando las nuevas categorias');
        $this->info('---------------------------------');
        $this->newLine(2);

        //Cargo las categorias nuevas
        $categorias_ml          = Http::timeout(60)->withHeaders(ml_headers_ttf())
                                        ->get('https://api.mercadolibre.com/sites/MLU/categories/all')
                                        ->json();

        $bar                    = $this->output->createProgressBar(count($categorias_ml));
        $bar->start();

        foreach($categorias_ml as $categoria_ml)
        {
            if($categoria_ml['settings']['listing_allowed'])
            {
                $categoria              = new MercadoLibreCategoria();
                $categoria->mlu_id      = $categoria_ml['id'];
                $categoria->nombre      = $categoria_ml['name'];
                $categoria->save();
            }

            $bar->advance();
        }

        $bar->finish();

        $this->newLine(2);
        $this->info('********************************************');
        $this->info('********************************************');
        $this->info("-> Se cargaron ".count($categorias_ml)." categorias ");
    }
}
