<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;
use App\Models\Empresa;
use Carbon\Carbon;
use Http;

class RefreshMercadoLibre extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:ml:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refrescar el token de MercadoLibre';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');

        $this->info('*****************************************************');
        $this->info('Comienza el proceso de refresh_token de MercadoLibre');
        $this->info('*****************************************************');

        $empresas   = Empresa::all();
        $count      = 0;

        foreach ($empresas as $empresa) 
        {
            if(isset($empresa->mercado_libre) and ($empresa->mercado_libre->estado == "conectado"))
            {
                $minutos = $empresa->mercado_libre->expires_date->diffInMinutes(Carbon::now());

                if($minutos >= 360)
                {
                    $response = \Http::post('https://api.mercadopago.com/oauth/token', [
                        'access_token'      => $empresa->mercado_libre->access_token,
                        'grant_type'        => "refresh_token", 
                        'client_secret'     => env('MP_CLIENT_SECRET'),
                        'client_id'         => env('MP_CLIENT_ID'), 
                        'refresh_token'     => $empresa->mercado_libre->refresh_token
                    ]);

                    if(isset($response->json()['access_token']))
                    {
                        $empresa->mercado_libre->access_token         = $response->json()['access_token'];
                        $empresa->mercado_libre->refresh_token        = $response->json()['refresh_token'];
                        $empresa->mercado_libre->user_id              = $response->json()['user_id'];
                        $empresa->mercado_libre->expires_in           = $response->json()['expires_in'];
                        $empresa->mercado_libre->expires_date         = \Carbon\Carbon::now();
                        $empresa->mercado_libre->save();       

                        $count ++;
                    }
                }
            }
        }

        $this->info('**********************************************************');
        $this->info('*** Finaliza el proceso, se refrescaron '.$count.' Tokens ***');
        $this->info('**********************************************************');
        
        comando_hora($this->signature, 'fin');

        if($count > 0)
            comando_registro($this->signature, 'Finaliza el proceso, se refrescaron '.$count.' Tokens');
    }
}
