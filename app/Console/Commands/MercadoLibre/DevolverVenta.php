<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;
use App\Models\Venta;
use MercadoPago;


class DevolverVenta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:ml:devolver_venta {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Devolver venta de mercadopago';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('*****************************************');
        $this->info('*** Comienza el proceso de devolución ***');
        $this->info('*****************************************');

        $id     = $this->argument("id");
        $venta  = Venta::find($id);
        
        if ($venta == null)
        {
            $this->error('La venta no existe');
            return false;
        }

        MercadoPago\SDK::configure(['ACCESS_TOKEN' => $venta->empresa->mercado_pago->access_token]);
        $payment = MercadoPago\Payment::find_by_id($venta->mp_id);
        $payment->refund();
        
        $venta->estado = "devuelto";
        $venta->save();
       
        $payment = MercadoPago\Payment::find_by_id($venta->mp_id);
        
        $this->info('********************************');
        $this->info('*** Se realizo la devolucion ***');
        $this->info('********************************');
    }
}
