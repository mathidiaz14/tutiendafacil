<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;
use App\Models\Producto;
use App\Models\Multimedia;
use App\Models\Empresa;
use App\Models\Plugins\MercadoLibre\MercadoLibre;
use App\Models\Plugins\MercadoLibre\MercadoLibreAtributos;
use Http;

class ImportarProductos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:ml:productos:importar {empresa}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que importa las publicaciones desde MercadoLibre';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $empresa_id         = $this->argument('empresa');
        $empresa            = Empresa::find($empresa_id);
        $user_id            = $empresa->mercado_pago->user_id;
        $publicaciones      = collect();

        $cuenta             = \Http::timeout(60)->withHeaders(ml_headers_empresa($empresa))
                                        ->get('https://api.mercadolibre.com/users/'.$user_id.'/items/search?search_type=scan&limit=100')
                                        ->json();
        
        $scroll_id          = $cuenta['scroll_id'];

        $this->newLine();
        $this->info('-----------------------------------------');
        $this->info('-> Importar publicaciones de MercadoLibre');
        $this->info('-----------------------------------------');
        $this->newLine(2);

        $bar = $this->output->createProgressBar($cuenta['paging']['total']);
        $bar->start();

        if(isset($cuenta['results']))
        {
            foreach($cuenta['results'] as $publicacion_id)
            {
                $publicaciones->push($publicacion_id);
                $bar->advance();
            }
        } 

        do
        {
            $cuenta   = \Http::timeout(60)->withHeaders(ml_headers_empresa($empresa))
                                    ->get('https://api.mercadolibre.com/users/'.$user_id.'/items/search?search_type=scan&limit=100&scroll_id='.$scroll_id)
                                    ->json();
            
            if(isset($cuenta['results']))
            {
                foreach($cuenta['results'] as $publicacion_id)
                {
                    $publicaciones->push($publicacion_id);
                    $bar->advance();
                }
            } 
        
        }while((isset($cuenta['results'])) and (count($cuenta['results']) > 0));

        $bar->finish();

        $this->newLine(2);
        $this->info('--------------------------------------');
        $this->info('-> Cargando contenido de los productos');
        $this->info('--------------------------------------');
        $this->newLine();

        $bar = $this->output->createProgressBar(count($publicaciones));
        $bar->start();

        foreach($publicaciones as $publicacion_id)
        {
            $old = Producto::where('mlu_id', $publicacion_id)->first();

            if(($old == null) and ($publicacion['status'] == "active"))
            {
                $publicacion        = \Http::withHeaders(ml_headers_ttf())
                                            ->get("https://api.mercadolibre.com/items/".$publicacion_id)
                                            ->json();

                $descripcion        = \Http::withHeaders(ml_headers_ttf())
                                            ->get("https://api.mercadolibre.com/items/".$publicacion_id."/description")
                                            ->json();

                $numero                 = "";
                
                do{
                    $url    = str_replace(" ", "_", strtolower($publicacion['title'])).$numero;
                    $existe = $empresa->productos->where('url', $url)->first();

                    if($numero == "")
                        $numero = 1;
                    else
                        $numero += 1;

                }while($existe != null);

                $producto               = new Producto();
                $producto->empresa_id   = $empresa->id;
                $producto->nombre       = $publicacion['title'];
                $producto->precio       = $publicacion['price'];
                $producto->cantidad     = $publicacion['available_quantity'];
                $producto->descripcion  = $descripcion['plain_text'];
                $producto->estado       = "activo";
                $producto->url          = $url;
                $producto->mlu_id       = $publicacion['id'];
                $producto->condicion    = $publicacion['condition'] == "new" ? "nuevo" : "usado";
                $producto->save();

                foreach($publicacion['attributes'] as $atributo)
                {
                    $atributo_ml               = new MercadoLibreAtributos();
                    $atributo_ml->mlu_id       = $atributo['id'];
                    $atributo_ml->nombre       = $atributo['name'];
                    $atributo_ml->valor_id     = $atributo['value_id'];
                    $atributo_ml->valor_nombre = $atributo['value_name'];
                    $atributo_ml->valor_tipo   = $atributo['value_type'];
                    $atributo_ml->producto_id  = $producto->id;
                    $atributo_ml->save();

                    if($atributo['id'] == "GTIN")
                    {
                        $producto->sku = $atributo['value_name'];
                        $producto->save();
                    }
                }

                foreach($publicacion['pictures'] as $imagen)
                {
                    $ultimo     = 0;
                    foreach($producto->imagenes as $imagen)
                        $ultimo = $imagen->posicion;
                    
                    $media                  = new Multimedia();
                    $media->url_ml          = $imagen['url'];
                    $media->nombre          = $imagen['id'];
                    $media->tamaño          = $imagen['size'];
                    $media->tipo            = "URL";
                    $media->producto_id     = $producto->id;
                    $media->empresa_id      = $empresa->id;
                    $media->user_id         = usuario();
                    $media->posicion        = $ultimo + 1;
                    $media->save();
                }
            }

            $bar->advance();
        }

        $bar->finish();

        $this->newLine(2);
        $this->info('--------------------');
        $this->info('Se cargaron '.count($publicaciones).' productos desde MercadoLibre');
        $this->info('--------------------');
        $this->newLine(1);

        crear_notificacion("Importación MercadoLibre", 
                            "Se importaron ".$publicaciones." productos de MercadoLibre", 
                            url('admin/productos'), 
                            $empresa->id);

        return back();
    }
}
