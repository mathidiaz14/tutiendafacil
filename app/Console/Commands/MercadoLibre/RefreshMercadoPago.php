<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;
use App\Models\Empresa;
use Carbon\Carbon;
use Http;

class RefreshMercadoPago extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:mp:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refrescar el token de MercadoPago';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');
        
        $this->info('*****************************');
        $this->info('**** Comienza el proceso ****');
        $this->info('*****************************');
        $this->newLine(2);

        $empresas   = Empresa::all();
        $count      = 0;

        foreach ($empresas as $empresa) 
        {   
            if(isset($empresa->mercado_pago) and ($empresa->mercado_pago->estado == "conectado"))
            {
                $minutos = $empresa->mercado_pago->expires_date->diffInMinutes(Carbon::now());

                if($minutos >= 360)
                {
                    $response = \Http::post('https://api.mercadopago.com/oauth/token', [
                        'access_token'      => $empresa->mercado_pago->access_token,
                        'grant_type'        => "refresh_token", 
                        'client_secret'     => env('MP_CLIENT_SECRET'),
                        'client_id'         => env('MP_CLIENT_ID'), 
                        'refresh_token'     => $empresa->mercado_pago->refresh_token
                    ]);

                    if(isset($response->json()['access_token']))
                    {

                        $empresa->mercado_pago->access_token         = $response->json()['access_token'];
                        $empresa->mercado_pago->refresh_token        = $response->json()['refresh_token'];
                        $empresa->mercado_pago->user_id              = $response->json()['user_id'];
                        $empresa->mercado_pago->expires_in           = $response->json()['expires_in'];
                        $empresa->mercado_pago->expires_date         = \Carbon\Carbon::now();
                        $empresa->mercado_pago->save();       

                        $count ++;
                    }
                }
            }
        }

        $this->info('*************************************************');
        $this->info('Finaliza el proceso, se refrescaron '.$count.' Tokens');
        $this->info('*************************************************');

        comando_registro($this->signature, "Se ejecuto el comando para refrescar el token de MercadoPago");
        comando_hora($this->signature, 'fin');
    }
}
