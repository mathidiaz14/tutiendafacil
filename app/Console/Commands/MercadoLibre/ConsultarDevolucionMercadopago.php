<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;
use App\Models\Venta;
use MercadoPago;

class ConsultarDevolucionMercadopago extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:mp:devolucion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que consulta todas las ventas para saber si se devolvio desde MercadoPago';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');

        $this->info("******************************************************");
        $this->info("Comienza comando que comprueba el estado de las ventas");
        $this->info("******************************************************");
        $this->newLine(2);

        $ventas = Venta::where('mp_id', '!=', null)->where('estado', '!=', 'devuelto')->get();
                        
        $count  = 0;

        $bar    = $this->output->createProgressBar(count($ventas));
        $bar->start();

        foreach($ventas as $venta)
        {
            if((isset($venta->empresa->mercado_pago->estado)) && ($venta->empresa->mercado_pago->estado == "conectado"))
            {
                MercadoPago\SDK::configure(['ACCESS_TOKEN' => $venta->empresa->mercado_pago->access_token]);
                
                $payment = MercadoPago\Payment::find_by_id($venta->mp_id);
                
                if((isset($payment->status)) and ($payment->status == "refunded"))
                {
                    $venta->estado = "devuelto";
                    $venta->save();
                    
                    crear_notificacion(
                                    "Devolución de venta", 
                                    "Se realizo una devolución desde MercadoPago", 
                                    url('admin/venta', $venta->id), 
                                    $venta->empresa_id
                                );

                    $count ++;
                }

                $bar->advance();
            }
        }

        $bar->finish();

        $this->newLine(2);
        $this->info("******************************************************************************");
        $this->info("*** Se cambio el estado de ".$count." ventas de un total de ".count($ventas)." ***");
        $this->info("******************************************************************************");

        comando_registro($this->signature, "Se cambio el estado de ".$count." ventas de un total de ".count($ventas));
        comando_hora($this->signature, 'fin');
    }
}
