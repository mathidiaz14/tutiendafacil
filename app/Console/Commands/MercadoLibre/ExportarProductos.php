<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;

class ExportarProductos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:productos:exportar {empresa}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que exporta publicaciones hacia MercadoLibre';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $empresa_id         = $this->argument('empresa');
        $empresa            = Empresa::find($empresa_id);
        $user_id            = $empresa->mercado_pago->user_id;
     
        $this->newLine();
        $this->info('----------------------------------------');
        $this->info('-> Exportar publicaciones a MercadoLibre');
        $this->info('----------------------------------------');
        $this->newLine();

        $publicaciones = $empresa->productos->where('mlu_id', null)->get();

        $bar = $this->output->createProgressBar(count($publicaciones));
        $bar->start();

        foreach($publicaciones as $publicacion)
        {
            //Reviso cual es la categoria en la que se va a publicar el producto
            $categoria      = \Http::withHeaders(drop_headers_ttf())
                                ->get('https://api.mercadolibre.com/categories/'.$publicacion->categoria->mlu_id)
                                ->json();

            //Reviso el largo del titulo por limitaciones en la categoria, normalmente 60 caracteres
            $largo_titulo   = $categoria['settings']['max_title_length'];
            $titulo         = substr($publicacion->titulo, 0, $largo_titulo);

            //Cargo las imagenes a mercadolibre y en un array
            $array_imagenes = array();
            $atributos      = array();

            foreach($publicacion->imagenes as $imagen)
            {
                if(count($array_imagenes) >= $categoria['settings']['max_pictures_per_item'])
                    break;

                $nombre     = str_replace('storage/', '', $imagen->url);

                $imagen     = \Http::timeout(60)->withHeaders([
                                    'Authorization' => 'Bearer '.env('ML_ACCESS_TOKEN')
                                ])
                                ->attach('file', Storage::disk('public')->get($nombre), $nombre)
                                ->post('https://api.mercadolibre.com/pictures/items/upload')
                                ->json();

                array_push($array_imagenes, ["source" => $imagen['variations'][0]['url']]);
            }

            array_push($atributos, ['id' => "EMPTY_GTIN_REASON", 'value_name' => "17055161"]);

            $catalog_product_id = null;
            $catalog_listing    = false;
            $free_shipping      = $empresa->MercadoLibreConfiguracion->envio_gratis == "on" ? true : false;
            $local_pick_up      = $empresa->MercadoLibreConfiguracion->retiro_local == "on" ? true : false;

            $body   =   [
                "title"                 => $titulo,
                "category_id"           => $publicacion->categoria->mlu_id,
                "price"                 => round($publicacion->precio),
                "currency_id"           => "UYU",
                "available_quantity"    => producto_stock_cantidad($publicacion->id),
                "buying_mode"           => "buy_it_now",
                "condition"             => "new",
                "listing_type_id"       => "gold_special",
                "description"           => $publicacion->descripcion,
                "pictures"              => $imagenes,
                "attributes"            => $atributos,
                "catalog_product_id"    => $catalog_product_id,
                "catalog_listing"       => $catalog_listing,
                "shipping"              => [
                    "mode"              => "me2",
                    "local_pick_up"     => $local_pick_up,
                    "free_shipping"     => $free_shipping
                ]
            ];

            $publicado = \Http::withHeaders(ml_headers_empresa($empresa))
                                ->post('https://api.mercadolibre.com/items/validate', $body)
                                ->json(); 
  
            dd($publicado);
        }
    }
}
