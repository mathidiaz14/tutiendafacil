<?php

namespace App\Console\Commands\MercadoLibre;

use Illuminate\Console\Command;
use App\Models\Venta;
use MercadoPago;
use DB;

class ConsultarPagosPendientes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ttf:mp:pendientes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consultar pagos pendientes para saber si se acreditaron en MercadoPago';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!comando_estado($this->signature))
        {
            $this->error('El comando esta bloqueado desde el dashboard');
            return false; 
        }

        comando_hora($this->signature, 'inicio');

        $this->info('***********************************************');
        $this->info('*** Comienza el proceso de pagos pendientes ***');
        $this->info('***********************************************');

        $ventas     = Venta::where('pago_metodo', 'redes')->where('pago_estado', 'pendiente')->get();
        $plugins    = DB::table('empresa_plugin')->where('estado', 'pendiente')->get();
        $temas      = DB::table('empresa_tema')->where('estado', 'pendiente')->get();
        $empresas   = DB::table('empresas')->where('pago', 'pendiente')->get();

        $contador_ventas    = 0;
        $contador_plugins   = 0;
        $contador_temas     = 0;
        $contador_empresas  = 0;

        $this->info("------------------------");
        $this->info("Comienzo a comprobar las ventas de un total de ".count($ventas));
        $this->info("------------------------");

        //Controlo el estado del pago de las ventas que quedaron en pendiente
        foreach ($ventas as $venta)
        {
            if($venta->empresa->mercado_pago->access_token != null)
            {
                MercadoPago\SDK::configure(['ACCESS_TOKEN' => $venta->empresa->mercado_pago->access_token]);
                $payment = MercadoPago\Payment::find_by_id($venta->mp_id);
                
                if($payment->status == "approved")
                {
                    $venta->estado = "aprobado";
                    $venta->save();

                    $titulo     = "Pago aprobado";
                    $contenido  = "Se aprobo el pago de la venta #".$venta->codigo;
                    $url        = '/admin/venta/'.$venta->id;

                    crear_notificacion($titulo, $contenido, $url,$venta->empresa_id);

                    $contador_ventas += 1;

                }elseif($payment->status == "rejected")
                {
                    $venta->estado = "rechazado";
                    $venta->save();

                    $titulo     = "Pago rechazado";
                    $contenido  = "Se rechazo el pago de la venta #".$venta->codigo;
                    $url        = '/admin/venta/'.$venta->id;

                    crear_notificacion($titulo, $contenido, $url,$venta->empresa_id);
                }
            }
        }

        $this->info("------------------------");
        $this->info("Finaliza comprobación de ventas");
        $this->info("------------------------");
        $this->newLine();

        $this->info("------------------------");
        $this->info("Comienzo a comprobar los plugins de un total de ".count($plugins));
        $this->info("------------------------");

        //Controlo el estado del pago de los plugins que se hicieron por MP
        \MercadoPago\SDK::setAccessToken(env('MP_ACCESS_TOKEN'));

        foreach ($plugins as $plugin) 
        {    
            $payment = MercadoPago\Payment::find_by_id($plugin->mp_id);

            if($payment->status == "approved")
            {
                $plugin->estado = "instalado";
                $plugin->save();

                $titulo     = "Pago aprobado";
                $contenido  = "Se aprobo el pago de un plugin";
                $url        = url('/admin/plugin');

                crear_notificacion($titulo, $contenido, $url, $plugin->empresa_id);   

                $contador_plugins += 1;

            }elseif($payment->status == "rejected")
            {
                $plugin->delete();

                $titulo     = "Pago rechazado";
                $contenido  = "Se rechazo el pago de un plugin";
                $url        = url('/admin/plugin');

                crear_notificacion($titulo, $contenido, $url, $plugin->empresa_id);   
                
            } 
        }

        $this->info("------------------------");
        $this->info("Finaliza comprobación de plugins");
        $this->info("------------------------");
        $this->newLine();

        $this->info("------------------------");
        $this->info("Comienzo a comprobar los temas de un total de ".count($temas));
        $this->info("------------------------");

        //Controlo el estado del pago de los temas que se hicieron por MP
        foreach ($temas as $tema) 
        {    
            $payment = MercadoPago\Payment::find_by_id($tema->mp_id);

            if($payment->status == "approved")
            {
                $tema->estado = "instalado";
                $tema->save();

                $titulo     = "Pago aprobado";
                $contenido  = "Se aprobo el pago de un tema";
                $url        = url('/admin/tema');

                crear_notificacion($titulo, $contenido, $url, $tema->empresa_id);   

                $contador_temas ++;

            }elseif($payment->status == "rejected")
            {
                $tema->delete();

                $titulo     = "Pago rechazado";
                $contenido  = "Se rechazo el pago de un tema";
                $url        = url('/admin/tema');

                crear_notificacion($titulo, $contenido, $url, $tema->empresa_id);   
                
            } 
        }

        $this->info("------------------------");
        $this->info("Finaliza comprobación de temas");
        $this->info("------------------------");
        $this->newLine();

        $this->info("------------------------");
        $this->info("Comienzo a comprobar las empresas de un total de ".count($empresas));
        $this->info("------------------------");

        //Controlo el pago de las empresas
        foreach ($empresas as $empresa) 
        {    
            $payment = MercadoPago\Payment::find_by_id($empresa->mp_id);

            if($payment->status == "approved")
            {
                $empresa->pago  = "aprobado";
                $empresa->mp_id = null;
                $tema->save();

                $contenido = [
                    "nombre" => $empresa->nombre,
                ];
                
                email("nueva_empresa.pago_aprobado", "Pago aprobado - TuTiendaFacil.uy", $contenido, $empresa->configuracion->email_admin);

                $contador_empresas ++;

            }elseif($payment->status == "rejected")
            {
                $empresa->pago = "rechazado";
                $empresa->mp_id = null;
                $empresa->save();

                $contenido = [
                    "nombre" => $empresa->nombre,
                ];
                
                email("nueva_empresa.pago_rechazado", "Pago rechazado - TuTiendaFacil.uy", $contenido, $empresa->configuracion->email_admin);
            } 
        }

        $this->info("------------------------");
        $this->info("Finaliza comprobación de empresas");
        $this->info("------------------------");
        $this->newLine();

        $this->info("------------------------");
        $this->info('Se aprobaron '.$contador_ventas.' ventas de un total de '.$ventas->count());
        $this->info('Se aprobaron '.$contador_plugins.' plugins de un total de '.$plugins->count());
        $this->info('Se aprobaron '.$contador_temas.' temas de un total de '.$temas->count());
        $this->info('Se aprobaron '.$contador_empresas.' empresas de un total de '.$empresas->count());
        $this->info("------------------------");


        $mensaje = 'Se aprobaron '.$contador_ventas.' ventas de un total de '.$ventas->count().'<br>'.
        'Se aprobaron '.$contador_plugins.' plugins de un total de '.$plugins->count().'<br>'.
        'Se aprobaron '.$contador_temas.' temas de un total de '.$temas->count().'<br>'.
        'Se aprobaron '.$contador_empresas.' empresas de un total de '.$empresas->count();

        comando_registro($this->signature, $mensaje);
        comando_hora($this->signature, 'fin');

    }
}
