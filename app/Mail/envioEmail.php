<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class envioEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $contenido;
    public $template;
    public $asunto;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contenido, $template, $asunto, $email, $empresa)
    {
        $this->contenido    = $contenido;
        $this->template     = $template;
        $this->asunto       = $asunto;
        $this->email        = $email;
        $this->empresa      = $empresa;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->asunto)
                    ->from($this->email, $this->empresa)
                    ->view($this->template);
    }
}
