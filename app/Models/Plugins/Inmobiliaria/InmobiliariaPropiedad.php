<?php

namespace App\Models\Plugins\Inmobiliaria;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmobiliariaPropiedad extends Model
{
    use HasFactory;

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function caracteristicas()
    {
        return $this->belongsToMany('App\Models\Plugins\Inmobiliaria\InmobiliariaCaracteristica', "inmobiliaria_propiedad_inmobiliaria_caracteristica", "inmobiliaria_propiedad_id", "inmobiliaria_caracteristica_id")->withPivot('valor', 'id');
    }

    public function comodidades()
    {
        return $this->belongsToMany('App\Models\Plugins\Inmobiliaria\InmobiliariaComodidad', "inmobiliaria_propiedad_inmobiliaria_comodidad", "inmobiliaria_propiedad_id", "inmobiliaria_comodidad_id");
    }

    public function planos()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaPlano', 'propiedad_id');
    }

    public function imagenes()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaMultimedia', 'propiedad_id');
    }
}
