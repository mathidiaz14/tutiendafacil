<?php

namespace App\Models\Plugins\Inmobiliaria;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmobiliariaComodidad extends Model
{
    use HasFactory;

    public function propiedades()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad', "inmobiliaria_propiedad_inmobiliaria_comodidad");
    }

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }
}
