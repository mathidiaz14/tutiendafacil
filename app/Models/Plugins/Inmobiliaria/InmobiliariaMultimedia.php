<?php

namespace App\Models\Plugins\Inmobiliaria;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InmobiliariaMultimedia extends Model
{
    use HasFactory;

    public function propiedades()
    {
        return $this->belongsTo('App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }
}
