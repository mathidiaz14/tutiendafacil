<?php

namespace App\Models\Plugins\MercadoLibre;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MercadoLibreCategoria extends Model
{
    use HasFactory;
}
