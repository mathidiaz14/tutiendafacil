<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Atributo extends Model
{
    use HasFactory;

    public function producto()
    {
        return $this->belongsTo('App\Models\Producto');
    }

    public function valores()
    {
        return $this->hasMany('App\Models\AtributoValor');
    }
}
