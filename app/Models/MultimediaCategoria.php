<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MultimediaCategoria extends Model
{
    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }

    public function imagenes()
    {
        return $this->hasMany('App\Models\Multimedia', 'categoria_id');
    }

    public function hijos()
    {
        return $this->hasMany('App\Models\MultimediaCategoria', 'parent_id');   
    }

    public function padre()
    {
        return $this->belongsTo('App\Models\MultimediaCategoria', 'parent_id');   
    }
}
