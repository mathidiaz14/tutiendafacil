<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    public function productos()
    {
        return $this->belongsToMany('App\Models\Producto');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }
}
