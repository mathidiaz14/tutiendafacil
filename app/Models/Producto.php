<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }

    public function categorias()
    {
        return $this->belongsToMany('App\Models\Categoria', 'categoria_producto');
    }

    public function proveedor()
    {
        return $this->belongsTo('App\Models\Proveedor');
    }

    public function imagenes()
    {
        return $this->hasMany('App\Models\Multimedia');
    }

    public function ventas()
    {
        return $this->belongToMany('App\Models\Venta')->withPivot('cantidad', 'precio', 'variante_id');
    }

    public function atributos()
    {
        return $this->hasMany('App\Models\Atributo');
    }
    
    public function variantes()
    {
        return $this->hasMany('App\Models\Variante');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function deseos()
    {
        return $this->belongsToMany('App\Models\Cliente', 'deseos');
    }

    public function atributos_ml()
    {
        return $this->hasMany('App\Models\Plugins\MercadoLibre\MercadoLibreAtributos');
    }
}
