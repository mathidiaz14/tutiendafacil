<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VentaTransferencia extends Model
{
    use HasFactory;

    public function venta()
    {
        return $this->belongsTo('App\Models\Venta');
    }
}
