<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AtributoValor extends Model
{
    use HasFactory;

    public function atributo()
    {
        return $this->belongsTo('App\Models\Atributo');
    }

    public function variantes()
    {
        return $this->belongsToMany('App\Models\Variante', 'atributo_valors_variantes');
    }
}
