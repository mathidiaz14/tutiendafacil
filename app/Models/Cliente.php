<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;

    public function ventas()
    {
        return $this->hasMany('App\Models\Venta');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }

    public function deseos()
    {
        return $this->belongsToMany('App\Models\Producto', 'deseos');
    }
}
