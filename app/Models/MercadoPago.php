<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MercadoPago extends Model
{
    use HasFactory;

    protected $dates = ["expires_date"];

    public function empresa()
    {
        return $this->belongsTo('App\Models\Empresa');
    }
}
