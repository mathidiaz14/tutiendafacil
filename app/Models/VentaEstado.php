<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VentaEstado extends Model
{
    use HasFactory;

    public function venta()
    {
        return $this->belongsTo('App\Models\Venta');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Models\User');
    }
}
