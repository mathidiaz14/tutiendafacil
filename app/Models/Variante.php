<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variante extends Model
{
    use HasFactory;

    public function producto()
    {
        return $this->belongsTo('App\Models\Producto');
    }    

    public function valores()
    {
        return $this->belongsToMany('App\Models\AtributoValor', 'atributo_valors_variantes');
    }
}
