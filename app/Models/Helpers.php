<?php

/*****************************************************
 Archivo con funciones que se reutilizan varias veces 
******************************************************/


//Funciones del sistema

if (! function_exists('registro')) 
{
	function registro($estado, $usuario, $empresa, $objeto, $accion, $mensaje)
	{
		$log 				= new \App\Models\Log();
		$log->estado 		= $estado;
		$log->usuario_id 	= $usuario;
		$log->empresa_id 	= $empresa;
		$log->objeto 		= $objeto;
		$log->accion 		= $accion;
		$log->mensaje 		= $mensaje;
		$log->save();

		return true;
	}
}

if (! function_exists('usuario')) 
{
	function usuario($dato = null)
	{
		if(Auth::check())
		{
			if($dato == null)
				return Auth::user();

			switch ($dato) {
				case 'id':
					return Auth::user()->id;
					break;
				
				case 'nombre':
					return Auth::user()->nombre;
					break;
				
				case 'email':
					return Auth::user()->email;
					break;
				
				case 'ci':
					return Auth::user()->ci;
					break;
				
				case 'empresa':
					return Auth::user()->empresa_id;
					break;
				
				case 'tipo': 
					return Auth::user()->tipo; 
					break;
			}
		}
	}
}

if (! function_exists('control_empresa')) 
{
	function control_empresa($id)
	{
		if($id == Auth::user()->empresa_id)
			return true;

		return false;
	}
}

if(! function_exists('control_ssl'))
{
	function control_ssl($url)
	{
		try 
		{
			$orignal_parse  = parse_url($url, PHP_URL_HOST);
	        $get            = stream_context_create(array("ssl" => array("capture_peer_cert" => TRUE)));
	        $read           = stream_socket_client(
	                                                "ssl://".$orignal_parse.":443", 
	                                                $errno, 
	                                                $errstr, 
	                                                30, 
	                                                STREAM_CLIENT_CONNECT, 
	                                                $get
	                                            );

	        $cert           = stream_context_get_params($read);
	        $certinfo       = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);
	        
	        return \Carbon\Carbon::createFromTimestamp($certinfo['validTo_time_t'])->diffInDays(\Carbon\Carbon::now());
		} catch (Exception $e) 
		{
			return false;		
		}
	}
}

if (! function_exists('exito')) 
{
	function exito($mensaje)
	{
		session(['exito' => $mensaje]);
	}
}

if (! function_exists('error')) 
{
	function error($mensaje)
	{
		session(['error' => $mensaje]);
	}
}

if (! function_exists('warning')) 
{
	function warning($mensaje)
	{
		session(['warning' => $mensaje]);
	}
}

if (! function_exists('info_alert')) 
{
	function info_alert($mensaje)
	{
		session(['info' => $mensaje]);
	}
}

if (! function_exists('visual')) 
{
	function visual($key)
	{
		return usuario()->visual->where('key', $key)->first();
	}
}

if (! function_exists('codigo_unico')) 
{
	function codigo_unico($id, $digitos = 6)
	{
		return str_pad($id, $digitos, "0", STR_PAD_LEFT);
	}
}

if (! function_exists('codigo_aleatorio')) 
{
	function codigo_aleatorio($cantidad = 10)
	{
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $cantidad); 
	}
}

if (! function_exists('codigo_aleatorio_numerico')) 
{
	function codigo_aleatorio_numerico($cantidad = 10)
	{
		return substr(str_shuffle("0123456789"), 0, $cantidad); 
	}
}

if (! function_exists('mes')) 
{
	function mes($id, $corto = null)
	{
		$id 	= $id - 1;

		$meses 	= ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		$cortos = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Set', 'Oct', 'Nov', 'Dic'];

		if($corto)
			return $cortos[$id];

		return $meses[$id];
	}
}

if(! function_exists('copiar'))
{
	function copiar( $source, $target ) {
		if ( is_dir( $source ) ) {
			@mkdir( $target );
			$d = dir( $source );
			while ( FALSE !== ( $entry = $d->read() ) ) {
				if ( $entry == '.' || $entry == '..' ) {
					continue;
				}
				$Entry = $source . '/' . $entry; 
				if ( is_dir( $Entry ) ) {
					copiar( $Entry, $target . '/' . $entry );
					continue;
				}
				copy( $Entry, $target . '/' . $entry );
			}
	 
			$d->close();
		}else {
			copy( $source, $target );
		}
	}
}

if(! function_exists('eliminar_carpeta'))
{
	function eliminar_carpeta($carpeta)
	{
		foreach(glob($carpeta . "/*") as $archivos_carpeta)
		{             
			if (is_dir($archivos_carpeta))
				eliminar_carpeta($archivos_carpeta);
			else
				unlink($archivos_carpeta);
		}
		 
		rmdir($carpeta);
	}
}

if(! function_exists('opcion'))
{
	function opcion($key) 
	{
		$opcion = App\Models\Opcion::first();
		
		if($opcion[$key] != null)
			return $opcion[$key];

		return null;   
	}
}

if(! function_exists('configuracion'))
{
	function configuracion()
	{
		if(Auth::check())
			return Auth::user()->empresa->configuracion;
	}
}

if(! function_exists('crear_notificacion'))
{
	function crear_notificacion($titulo, $contenido, $url, $empresa)
	{
		$notificacion 				= new \App\Models\Notificacion();
		$notificacion->titulo 		= $titulo;
		$notificacion->contenido 	= $contenido;
		$notificacion->estado		= "nuevo";
		$notificacion->url 			= $url;
		$notificacion->empresa_id 	= $empresa;
		$notificacion->save();

		return true;
	}
}

if(! function_exists('email'))
{
	function email($plant, $asunto, $contenido, $destinatario, $reenvio = null, $nombre = "TuTiendaFacil.uy", $empresa = null)
	{
		$plantilla 	= 'email.'.$plant;
		
		if($reenvio == null )
		{
			$empresa 	= empresa();

			if($empresa != null)
			{
				if(view()->exists("empresas.".$empresa->id.".".$empresa->carpeta.".email.".$plant))
					$plantilla = "empresas.".$empresa->id.".".$empresa->carpeta.".email.".$plant;

				$nombre = $empresa->configuracion->titulo;
			}
		}

		$mj = new \Mailjet\Client(
								'23a68841ab44393a370bc1fab8c34653', 
								'4793266e3e7e30f8fde8c2b8e90c1700', 
								true, 
								['version' => 'v3.1']
							);
    
	    $body = [
	        'Messages' => [
	            [
	                'From' => [
	                    'Email' => env('MAIL_FROM_ADDRESS'),
	                    'Name' => env('MAIL_FROM_NAME'),
	                ],
	                'To' => [
	                    [
	                        'Email' => $destinatario
	                    ]
	                ],
	                'Subject' => $asunto,
	                'HTMLPart' => view($plantilla)->with('contenido', $contenido)->render()
	            ]
	        ]
	    ];

        $log 				= new \App\Models\EmailLog();

		if($empresa == null)
			$log->empresa_id = "root";
		else
			$log->empresa_id = $empresa->id;

		$log->asunto 		= $asunto;
		$log->nombre 		= $nombre;
		$log->plantilla 	= $plantilla;
		$log->contenido 	= json_encode($contenido);
		$log->destinatario 	= $destinatario;
		$log->estado 		= "pendiente";
		$log->save();

	    $response = $mj->post(\Mailjet\Resources::$Email, ['body' => $body]);

		if($response->success())
		{
			$log->estado 	= "enviado";
			$log->detalle 	= "Enviado correctamente";
			$log->save();

			return true;
		}
	
		$log->estado 	= "error";
		$log->detalle 	= $response->getMessage()['body']['ErrorMessage'];
		$log->save();
		
		return false;
	}
}

if(! function_exists('myasset'))
{
	function myasset($path)
	{
		return asset("empresas/".empresa()->id."/".empresa()->carpeta."/".$path);
	}
}

if(! function_exists('instalar_tema'))
{
	function instalar_tema($id)
	{
		try {
			$tema       = \App\Models\Tema::find($id);
			$empresa    = \Auth::user()->empresa;
			$nombre 	= codigo_aleatorio(10);
			$ruta 		= 'views/empresas/'.Auth::user()->empresa->id."/".$nombre;
			
			$empresa->temas()->attach($tema, ['estado' => 'instalado', 'carpeta' => $nombre]);

			mkdir(resource_path($ruta, 0777));

			copiar(
				resource_path('views/temas/'.$tema->carpeta), 
				resource_path($ruta)
			);

			return true;
		} catch (Exception $e) {
			return false;
		}
	}
}

if (! function_exists('instalar_tema_default'))
{
	function instalar_tema_default($empresa)
	{
		try {
			$tema       = \App\Models\Tema::where('nombre', 'default')->first();
			$nombre 	= codigo_aleatorio(10);

			$empresa->temas()->attach($tema, ['estado' => 'instalado', 'carpeta' => $nombre]);
			$empresa->carpeta = $nombre;
			$empresa->save();
			
			mkdir(resource_path('views/empresas/'.$empresa->id, 0777));
			mkdir(resource_path('views/empresas/'.$empresa->id."/".$nombre, 0777));

			copiar(
				resource_path('views/temas/'.$tema->carpeta), 
				resource_path('views/empresas/'.$empresa->id."/".$nombre)
			);

			$contenido = simplexml_load_file(
							resource_path('views/empresas/'.$empresa->id."/".$nombre."/install.xml")
						);

			foreach($contenido as $elemento)
			{
				\App\Models\Landing::create([
					'titulo'        => $elemento->titulo,
					'llave'         => $elemento->llave,
					'valor'         => $elemento->valor,
					'tipo'          => $elemento->tipo,
					'empresa_id'    => $empresa->id
				]);
			}

			return true;

		} catch (Exception $e) {
			return false;	
		}
	}
}

if (! function_exists('respuesta_mercadopago'))
{
	function respuesta_mercadopago($error, $payment_method_id)
	{
		$respuesta = "";

		switch ($error) 
		{
			case 'cc_rejected_bad_filled_card_number':
				$respuesta = "Revisa el número de tarjeta.";
				break;

			case 'cc_rejected_bad_filled_date':
				$respuesta = "Revisa la fecha de vencimiento.";
				break;

			case 'cc_rejected_bad_filled_other':
				$respuesta = "Revisa los datos.";
				break;

			case 'cc_rejected_bad_filled_security_code':
				$respuesta = "Revisa el código de seguridad de la tarjeta.";
				break;

			case 'cc_rejected_blacklist':
				$respuesta = "No pudimos procesar tu pago.";
				break;

			case 'cc_rejected_call_for_authorize':
				$respuesta = "Debes autorizar ante ".$payment_method_id." el pago de amount.";
				break;

			case 'cc_rejected_card_disabled':
				$respuesta = "Llama a ".$payment_method_id." para activar tu tarjeta o usa otro medio de pago.";
				break;

			case 'cc_rejected_card_error':
				$respuesta = "No pudimos procesar tu pago.";
				break;

			case 'cc_rejected_duplicated_payment':
				$respuesta = "Ya hiciste un pago por ese valor.";
				break;

			case 'cc_rejected_high_risk':
				$respuesta = "Tu pago fue rechazado.";
				break;

			case 'cc_rejected_insufficient_amount':
				$respuesta = "Tu ".$payment_method_id." no tiene fondos suficientes.";
				break;

			case 'cc_rejected_invalid_installments':
				$respuesta = $payment_method_id." no procesa pagos en installments cuotas.";
				break;

			case 'cc_rejected_max_attempts':
				$respuesta = "Llegaste al límite de intentos permitidos.";
				break;

			case 'cc_rejected_other_reason':
				$respuesta = $payment_method_id." no procesó el pago.";
				break;

			default:
				$respuesta = "Tu pago fue rechazado.";
				break;
		}

		return $respuesta;
	}
}

if(! function_exists('reducir_imagen'))
{
	function reducir_imagen($ruta)
	{
		if((file_exists($ruta)) and (is_file($ruta)))
		{
			$img    = Image::make($ruta);
		
			if($img->filesize() > "1000000")
			{   
				$img->orientate();

				$img->resize(1024, null, function($constraint){ 
					$constraint->upsize();
					$constraint->aspectRatio();
				});

				$img->save($ruta);
			}
		}
	}
}

if(! function_exists('registro_comando'))
{
	function comando_registro($comando, $mensaje)
	{
		$registro 			= new \App\Models\RegistroComando();
		$registro->comando 	= $comando;
		$registro->mensaje 	= $mensaje;
		$registro->save();

		return true;
	}
}

if(! function_exists('comando_estado'))
{
	function comando_estado($firma)
	{
		$comando = App\Models\Comando::where('signature', $firma)->first();

		if ($comando == null) 
			return true;
		elseif($comando->estado == "bloqueado")
			return false;

		return true;
	}
}

if(! function_exists('comando_hora'))
{
	function comando_hora($firma, $tipo)
	{
		$comando = App\Models\Comando::where('signature', $firma)->first();

		if ($comando == null) 
			return false;
		
		$hora = \Carbon\Carbon::now();

		if($tipo == "inicio")
		{
			$comando->estado 	= "ejecutando";
			$comando->inicio 	= $hora;
		}
		else
		{
			$comando->estado 	= "activo";
			$comando->fin 		= $hora; 
		}

		$comando->save();

		return true;
	}
}

if(! function_exists('empresa'))
{
	function empresa()
	{
		if(Session()->has('empresa'))
			return Session('empresa');

		if((Auth::check()) and Auth::user()->tipo != "root")
	        return Auth::user()->empresa;

		$string = str_replace("http://", "", url('/'));
		$string = str_replace("https://", "", $string);
		
		$empresa = App\Models\Empresa::where('URL', 'LIKE', $string)->first();

		if($empresa == null)
			$empresa = App\Models\Empresa::where('URL1', 'LIKE', $string)->first();

		if($empresa == null)
			$empresa = App\Models\Empresa::where('URL2', 'LIKE', $string)->first();

		if($empresa == null)
			$empresa = App\Models\Empresa::where('URL3', 'LIKE', $string)->first();
		
		if($empresa != null)
			return $empresa;

		return null;
	}
}

if (! function_exists('estado_venta')) 
{
	function estado_venta($venta, $usuario, $estado)
	{
		$estado_venta 				= new App\Models\VentaEstado();
		$estado_venta->venta_id 	= $venta;
		$estado_venta->usuario_id 	= $usuario;
		$estado_venta->estado 		= $estado;
		$estado_venta->save();

		return true;
	}
}

if(! function_exists('paginate'))
{
	function paginate($items, $perPage = 15, $page = null, $options = [])
	{
	    $page = $page ?: (Illuminate\Pagination\Paginator::resolveCurrentPage() ?: 1);
	    $items = $items instanceof Illuminate\Support\Collection ? $items : Illuminate\Support\Collection::make($items);
	    return new Illuminate\Pagination\LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
	}
}

if(! function_exists('cuenta_contacto'))
{
	function cuenta_contacto()
	{
		$a      = rand(1,9);
	    $b      = rand(1,9);
	    $signo  = rand(0,2);
	    
	    if($signo == 0)
	    {
	        $resultado = $a + $b;
	        $operacion = $a." + ".$b." = ?";
	    }
	    elseif($signo == 1)
	    {
	        $resultado = $a - $b;
	        $operacion = $a." - ".$b." = ?";
	    }
	    else{
	        $resultado = $a * $b;
	        $operacion = $a." * ".$b." = ?";
	    }

	    Session(['resultado' => $resultado, 'operacion' => $operacion]);
	}
}

if(! function_exists('plugin_estado'))
{
	function plugin_estado($carpeta, $estado = "activo")
	{
		$plugin = empresa()->plugins->where('carpeta', $carpeta)->first();
        
        if((isset($plugin)) and ($plugin->pivot->estado == $estado))
            return true;

        return false;
	}
}

if (! function_exists('guardar_comando_ejecutar')) 
{
	function guardar_comando_ejecutar($comando)
	{
		$comando 		= new \App\Models\ComandoEjecutar();
		$log->cmd 		= $comando;
		$log->save();

		return true;
	}
}


//Funciones de Temas

if(! function_exists('i'))
{
	function i($clave)
	{
		$info = empresa()->configuracion[$clave];

		if($info != null)
			return $info;

		return "null";
	}
}

if(! function_exists('info'))
{
	function info($clave)
	{
		$info = empresa()->configuracion[$clave];

		if($info != null)
			return $info;

		return "null";
	}
}

if(! function_exists('ver_menus'))
{
	function ver_menus()
	{
		return empresa()->menus->sortBy('numero');
	}
}

if(! function_exists('destacados'))
{
	function destacados($cantidad = 4)
	{
		return empresa()->productos->where('estado', 'activo')->where('destacado', 'on')->take($cantidad);
	}
}

if(! function_exists('nuevos'))
{
	function nuevos($cantidad = 4)
	{
		return empresa()->productos->where('estado', 'activo')->where('nuevo', 'on')->take($cantidad);
	}
}

if(! function_exists('precio_formato'))
{
	function precio_formato($precio)
	{
		return number_format($precio, 0, ',', '.');
	}
}

if(! function_exists('comprar'))
{
	function comprar()
	{
		return url('comprar');
	}
}

if(! function_exists('comprar_ahora'))
{
	function comprar_ahora($id)
	{
		return url('comprar_ahora', $id);
	}
}

if(! function_exists('agregar_carrito'))
{
	function agregar_carrito($id)
	{
		return url('agregar_carrito', $id);
	}
}

if(! function_exists('cantidad_productos_carrito'))
{
	function cantidad_productos_carrito()
	{
		$i = 0;
		
		if(Session::has('carrito'))
		{
			foreach (Session::get('carrito') as $producto)
				$i++;
		}

		return $i;    
	}
}

if(! function_exists('eliminar_producto_carrito'))
{
	function eliminar_producto_carrito($id)
	{
		return url('eliminar_carrito', $id);
	}
}

if(! function_exists('logo_url'))
{
	function logo_url()
	{
		if(empresa()->configuracion->logo != null)
			return asset(empresa()->configuracion->logo);
		else
			return asset("img/default.jpg");
	}
}

if(! function_exists('login_url'))
{
	function login_url()
	{
		return env('APP_URL')."/login";
	}
}

if(! function_exists('landing'))
{
	function landing($key) 
	{
		$landing = App\Models\Landing::where('empresa_id', empresa()->id)
										->where('llave', $key)
										->first();
		if($landing != null)
			return $landing->valor;

		return null;   
	}
}

//Funciones categorias

if(! function_exists('categoria'))
{
	function categoria($id)
	{
		$categoria = \App\Models\Categoria::find($id);

		if($categoria != null)
			return $categoria;

		return "";
	}
}

if(! function_exists('categorias'))
{
	function categorias($cantidad = 100)
	{
		return empresa()->categorias->where('online', 'on')->take($cantidad);
	}
}

if(! function_exists('categoria_url_imagen'))
{
	function categoria_url_imagen($id)
	{
		$categoria = App\Models\Categoria::find($id);

		if($categoria != null)
		{
			if($categoria->imagen != null)
				return asset($categoria->imagen);
		}

		return asset("img/default.jpg");
	}
}

if(! function_exists('categoria_url'))
{
	function categoria_url($id)
	{
		$categoria = App\Models\Categoria::find($id);

		if(($categoria != null) and ($categoria->empresa_id == empresa()->id))
			return url('categoria', $categoria->url);

		return false;
	}
}

//Funciones productos


if(! function_exists('productos'))
{
	function productos($cantidad = 1000)
	{
		return empresa()->productos->where('estado', 'activo')->sortByDesc('created_at')->take($cantidad);
	}
}

if(! function_exists('producto_url_imagen'))
{
	function producto_url_imagen($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if($producto->imagenes->count() >= 1)
			{
				if($producto->mlu_id == null)
					return asset($producto->imagenes->sortBy('numero')->first()->url);

				return $producto->imagenes->sortBy('numero')->first()->url_ml;
			}
		}

		return asset("img/default.jpg");
	}
}

if(! function_exists('producto_url_imagen2'))
{
	function producto_url_imagen2($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if($producto->imagenes->count() > 1)
				return asset($producto->imagenes[1]->url);
			
		}

		return asset("img/default.jpg");
	}
}

if(! function_exists('producto_stock_bool'))
{
	function producto_stock_bool($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if((($producto->cantidad != null) and ($producto->cantidad > 0)) or ($producto->variantes->count('cantidad') > 0))
				return true;
			elseif((($producto->cantidad != null) and ($producto->cantidad == 0)) or (($producto->variantes->first() != null) and ($producto->variantes->count('cantidad') == 0)))
				return false;
		}

		return false;
	}
}

if(! function_exists('producto_stock_cantidad'))
{
	function producto_stock_cantidad($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if($producto->variantes->sum('cantidad') > 0)
				return $producto->variantes->sum('cantidad');
			elseif($producto->cantidad != null)
				return $producto->cantidad;
		}

		return null;
	}
}

if(! function_exists('producto_nuevo'))
{
	function producto_nuevo($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if($producto->nuevo == 'on')
				return true;
			else
				return false;
		}

		return false;
	}
}

if(! function_exists('producto_url'))
{
	function producto_url($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
			return url('producto', $producto->url);

		return false;
	}
}

if(! function_exists('producto_precio'))
{
	function producto_precio($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if($producto->precio_promocion == null)
				return $producto->precio;
			else
				return $producto->precio_promocion;
		}
		
		return false;
	}
}

if(! function_exists('precio_producto'))
{
	function precio_producto($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if($producto->precio_promocion == null)
				return $producto->precio;
			else
				return $producto->precio_promocion;
		}
		
		return false;
	}
}

if(! function_exists('producto_precio_promocion'))
{
	function producto_precio_promocion($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto != null)
		{
			if($producto->precio_promocion != null)
				return $producto->precio_promocion;
		}
		
		return false;
	}
}

if(! function_exists('productos_precio_total'))
{
	function productos_precio_total($productos)
	{
		$suma = 0;

		foreach($productos as $producto)
		{

			if(isset($producto['variante']) and ($producto['variante']->precio != null))
			{
				$suma += $producto['variante']->precio * $producto['cantidad'];
			}else
			{
				if($producto['producto']->precio_promocion == null)
					$suma += $producto['producto']->precio * $producto['cantidad'];
				else
					$suma += $producto['producto']->precio_promocion * $producto['cantidad'];
			}
		}

		return $suma;
	}
}

if(! function_exists('productos_descontar_porcentaje'))
{
	function productos_descontar_porcentaje($productos, $porcentaje)
	{
		$total = productos_precio_total($productos);

		return ($total - ($total * ($porcentaje / 100)));
	}
}

if(! function_exists('producto_variante'))
{
	function producto_variante($id)
	{
		$variante = App\Models\Variante::find($id);
		
		if($variante != null)
			return $variante;
		
		return null;
	}
}

if(! function_exists('producto_categoria'))
{
	function producto_categoria($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto->categorias->first() != null)
			return $producto->categorias->first()->titulo;

		return null;
	}
}

if(! function_exists('producto_categoria_url'))
{
	function producto_categoria_url($id)
	{
		$producto = App\Models\Producto::find($id);

		if($producto->categorias->first() != null)
			return $producto->categorias->first()->url;

		return null;
	}
}

if(! function_exists('producto_colores'))
{
	function producto_colores($id)
	{
		$producto 	= App\Models\Producto::find($id);
        $atributos  = $producto->atributos->where('color', 'on');
        $valores    = collect();
        
        foreach($atributos as $atributo)
        {
            foreach($atributo->valores as $valor)
                $valores->push([$valor->color1, $valor->color2]);
        }

        return $valores->unique();
                
	}
}

if(! function_exists('producto_es_deseo'))
{
	function producto_es_deseo($id)
	{
		$producto 	= App\Models\Producto::find($id);
        $cliente 	= App\Models\Cliente::find(Session::get('cliente'))->first();

        foreach($cliente->deseos as $deseo)
        {
        	if($producto->id == $deseo->id)
        		return true;
        }    

        return false;
	}
}

if(! function_exists('suma_productos'))
{
	function suma_productos($productos)
	{
		$suma = 0;
		
		foreach($productos as $producto)
		{
			if($producto->precio_promocion == null)
				$suma += $producto->precio;
			else
				$suma += $producto->precio_promocion;
		}

		return $suma;
	}
}

if(! function_exists('tags_unicos'))
{
	function tags_unicos()
	{
		$tags 		= empresa()->tags;
		$collect 	= collect();
		
		foreach($tags as $tag)
			$collect->push(strtoupper($tag->tag));

		return $collect->unique();
	}
}

//Funciones de pagina

if(! function_exists('paginas'))
{
	function paginas($cantidad = 10)
	{	
		return empresa()->paginas->take($cantidad);
	}
}

if(! function_exists('pagina_url'))
{
	function pagina_url($id)
	{
		$pagina = App\Models\Pagina::find($id);

		if(($pagina != null) and ($pagina->empresa_id == empresa()->id))
			return url('pagina', $pagina->url);

		return false;
	}
}


//Funciones blog

if(! function_exists('plugin_blog_instalado'))
{
	function plugin_blog_instalado()
	{
		return false;
	}
}

if(! function_exists('blog_extracto'))
{
	function blog_extracto($entrada)
	{
		if($entrada->extracto != null)
			return substr($entrada->extracto, 0, 200);

		return substr($entrada->contenido, 0, 200);
	}
}

if(! function_exists('blog_categoria_titulo'))
{
	function blog_categoria_titulo($entrada)
	{
		if($entrada->categoria_id != null)
			return $entrada->categoria->titulo;

		return null;
	}
}

if(! function_exists('blog_entrada_imagen'))
{
	function blog_entrada_imagen($entrada)
	{
		if($entrada->imagen != null)
			return asset($entrada->imagen);

		return asset("img/default_blog.jpg");
	}
}

if(! function_exists('blog_entrada_url'))
{
	function blog_entrada_url($entrada)
	{
		if($entrada != null)
			return url('blog', $entrada->url);
	}
}

if(! function_exists('blog_categoria_url'))
{
	function blog_categoria_url($categoria)
	{
		if($categoria != null)
			return url('blog/categoria', $categoria->url);
		
		return "false";
	}
}

if(! function_exists('blog_entradas_categoria'))
{
	function blog_entradas_categoria($titulo)
	{
		$categoria = empresa()->blogCategorias->where('titulo', $titulo)->first();
		
		if($categoria != null)
			return $categoria->entradas;

		return [];
	}
}

if(! function_exists('blog_categorias'))
{
	function blog_categorias()
	{
		return empresa()->blogCategorias;
	}
}

if(! function_exists('blog_fecha_entrada'))
{
	function blog_fecha_entrada($entrada, $formato = "d/m/y")
	{
		$fecha = $entrada->created_at->format($formato);

		$fecha = str_replace("Jan", "Ene", $fecha);
		$fecha = str_replace("Feb", "Feb", $fecha);
		$fecha = str_replace("Apr", "Abr", $fecha);
		$fecha = str_replace("Aug", "Ago", $fecha);
		$fecha = str_replace("Sep", "Set", $fecha);
		$fecha = str_replace("Dec", "Dic", $fecha);
		
		return $fecha;
	}
}

if(! function_exists('blog_ultimas_entradas'))
{
	function blog_ultimas_entradas($cantidad = 5)
	{
		return empresa()->blogEntradas->take($cantidad);
	}
}

if(! function_exists('blog_entrada_autor'))
{
	function blog_entrada_autor($entrada)
	{
		return $entrada->usuario->nombre;
	}
}

if(! function_exists('blog_entrada_autor_url'))
{
	function blog_entrada_autor_url($entrada)
	{
		return url('blog/autor', $entrada->usuario->id);
	}
}

if(! function_exists('blog_comentarios'))
{
	function blog_comentarios($entrada)
	{
		if($entrada->comentario_activo)
			return $entrada->comentarios->where('estado', 'aprobado')->where('parent_id', null);

		return false;
	}
}

if(! function_exists('blog_comentarios_activos'))
{
	function blog_comentarios_activos($entrada)
	{
		if($entrada->comentario_activo)
			return true;

		return false;
	}
}

// Funciones del plugin MercadoLibre

if(! function_exists('ml_headers_ttf'))
{
	function ml_headers_ttf()
	{
		return [ 'Authorization' => 'Bearer '.env('ML_ACCESS_TOKEN')];
	}
}

if(! function_exists('ml_headers_empresa'))
{
	function ml_headers_empresa($empresa)
	{
		return [ 'Authorization' => 'Bearer '.$empresa->mercado_pago->access_token];
	}
}

// Funciones que llaman a views

if(! function_exists('ttf_carpeta'))
{
	function ttf_carpeta()
	{
		return "empresas.".empresa()->id.".".empresa()->carpeta;
	}
}

if(! function_exists('ttf_footer'))
{
	function ttf_footer()
	{
		return view('ayuda.tema.footer');
	}
}

if(! function_exists('ttf_busqueda'))
{
	function ttf_busqueda() 
	{
		return view('ayuda.tema.busqueda');  
	}
}

if(! function_exists('ttf_extends'))
{
	function ttf_extends($path)
	{
		return "empresas.".empresa()->id.".".empresa()->carpeta.".".$path;
	}
}

//Funcion de ayuda

if(! function_exists('random_style'))
{
	function random_style()
	{
		$estilo = collect([
			"style1",
			"style2",
			"style3",
			"style4",
			"style5",
			"style6",
		]);

		return $estilo[rand(0,5)];
	}
}

