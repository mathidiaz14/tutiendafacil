<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $dates = ["expira"];

    public function configuracion()
    {
        return $this->hasOne('App\Models\Configuracion');
    }

    public function mensajes()
    {
        return $this->hasMany('App\Models\Mensaje');
    }

    public function notificaciones()
    {
        return $this->hasMany('App\Models\Notificacion');
    }

    public function paginas()
    {
        return $this->hasMany('App\Models\Pagina');
    }

    public function menus()
    {
        return $this->hasMany('App\Models\Menu');
    }

    public function usuarios()
    {
        return $this->hasMany('App\Models\User');
    }

    public function visitas()
    {
        return $this->hasMany('App\Models\Visita');
    }

    public function landings()
    {
        return $this->hasMany('App\Models\Landing');
    }

    public function plugins()
    {
        return $this->belongsToMany('App\Models\Plugin')->withPivot('estado', 'created_at', 'mp_id');
    }

    public function temas()
    {
        return $this->belongsToMany('App\Models\Tema')->withPivot('estado', 'carpeta');
    }

    public function newsletters()
    {
        return $this->hasMany('App\Models\Newsletter');
    }

    public function errores()
    {
        return $this->hasMany('App\Models\Error');
    }

    public function registros()
    {
        return $this->hasMany('App\Models\Log');
    }

    /***************************************/

    public function productos()
    {
        return $this->hasMany('App\Models\Producto');
    }

    public function atributos()
    {
        return $this->hasMany('App\Models\Atributo');
    }

    public function clientes()
    {
        return $this->hasMany('App\Models\Cliente');
    }

    public function categorias()
    {
        return $this->hasMany('App\Models\Categoria');
    }

    public function proveedores()
    {
        return $this->hasMany('App\Models\Proveedor');
    }

    public function ventas()
    {
        return $this->hasMany('App\Models\Venta');
    }

    public function imagenes()
    {
        return $this->hasMany('App\Models\Multimedia');
    }

    public function carpetas()
    {
        return $this->hasMany('App\Models\MultimediaCategoria');
    }

    public function locales()
    {
        return $this->hasMany('App\Models\Local');
    }

    public function cupones()
    {
        return $this->hasMany('App\Models\Cupon');
    }

    public function mercado_pago()
    {
        return $this->hasOne('App\Models\MercadoPago');
    }

    public function pago_transferencia()
    {
        return $this->hasOne('App\Models\PagoTransferencia');
    }

    public function pago_efectivo()
    {
        return $this->hasOne('App\Models\PagoEfectivo');
    }

    public function tags()
    {
        return $this->hasMany('App\Models\Tag');
    }

    /***************************************/

    public function blogCategorias()
    {
        return $this->hasMany('App\Models\Plugins\Blog\BlogCategoria');
    }

    public function blogComentarios()
    {
        return $this->hasMany('App\Models\Plugins\Blog\BlogComentario');
    }

    public function blogEntradas()
    {
        return $this->hasMany('App\Models\Plugins\Blog\BlogEntrada');
    }

    /***************************************/

    public function mercado_libre()
    {
        return $this->hasMany('App\Models\Plugins\MercadoLibre\MercadoLibre');
    }

    public function mercadoLibreConfiguracion()
    {
        return $this->hasMany('App\Models\Plugins\MercadoLibre\MercadoLibreConfiguracion');
    }

    /***************************************/

    public function inmobiliaria_propiedades()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaPropiedad');
    }

    public function inmobiliaria_caracteristicas()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaCaracteristica');
    }

    public function inmobiliaria_comodidades()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaComodidad');
    }

    public function inmobiliaria_multimedia()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaMultimedia');
    }

    public function inmobiliaria_planos()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaPlano');
    }

    public function inmobiliaria_contactos()
    {
        return $this->hasMany('App\Models\Plugins\Inmobiliaria\InmobiliariaContacto');
    }

}
