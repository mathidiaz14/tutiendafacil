<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mercado_libre_configuracions', function (Blueprint $table) {
            $table->id();
            $table->string('envio_gratis')->nullable();
            $table->string('retiro_local')->nullable();
            $table->string('dias_garantia')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mercado_libre_configuracions');
    }
};
