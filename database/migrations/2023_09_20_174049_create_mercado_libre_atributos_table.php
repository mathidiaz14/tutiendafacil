<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mercado_libre_atributos', function (Blueprint $table) {
            $table->id();
            $table->string('mlu_id');
            $table->string('nombre')->nullable();
            $table->string('valor_id')->nullable();
            $table->string('valor_nombre')->nullable();
            $table->string('valor_tipo')->nullable();
            $table->string('producto_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mercado_libre_atributos');
    }
};
