<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmobiliaria_propiedad_inmobiliaria_caracteristica', function (Blueprint $table) {
            $table->id();
            $table->string("inmobiliaria_propiedad_id");
            $table->string("inmobiliaria_caracteristica_id");
            $table->string("valor");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmobiliaria_propiedad_inmobiliaria_caracteristica');
    }
};
