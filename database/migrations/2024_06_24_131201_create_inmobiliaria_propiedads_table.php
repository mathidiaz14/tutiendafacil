<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inmobiliaria_propiedads', function (Blueprint $table) {
            $table->id();
            $table->string("empresa_id");
            $table->string("user_id");
            $table->string("titulo");
            $table->text("descripcion");
            $table->string("tipo");
            $table->string("precio");
            $table->string("estado");
            $table->string("direccion")->nullable();
            $table->string("ciudad")->nullable();
            $table->string("localidad")->nullable();
            $table->string("pais")->nullable();
            $table->string("codigo_postal")->nullable();
            $table->string("dormitorios")->nullable();
            $table->string("banos")->nullable();
            $table->string("area")->nullable();
            $table->string("garaje")->nullable();
            $table->string("destacado")->nullable();
            $table->string("venta")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inmobiliaria_propiedads');
    }
};
