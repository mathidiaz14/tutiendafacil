<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ventas', function (Blueprint $table) {
            $table->dropColumn('cliente_email');
            $table->dropColumn('cliente_nombre');
            $table->dropColumn('cliente_apellido');
            $table->dropColumn('cliente_telefono');
            $table->dropColumn('cliente_ciudad');
            $table->dropColumn('cliente_direccion');
            $table->dropColumn('cliente_apartamento');
            $table->dropColumn('cliente_documento');
            $table->dropColumn('cliente_documento_tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venta', function (Blueprint $table) {
            //
        });
    }
};
