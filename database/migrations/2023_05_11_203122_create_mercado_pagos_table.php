<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mercado_pagos', function (Blueprint $table) {
            $table->id();
            $table->string('estado')->nullable();
            $table->string('access_token')->nullable();
            $table->string('public_key')->nullable();
            $table->string('refresh_token')->nullable();
            $table->string('user_id')->nullable();
            $table->string('expires_in')->nullable();
            $table->string('expires_date')->nullable();
            $table->string('empresa_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mercado_pagos');
    }
};
