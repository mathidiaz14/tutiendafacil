<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('opcions', function (Blueprint $table) {
            $table->string('ARStoUYU')->default(0.23);
            $table->string('USDtoUYU')->default(39.12);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('opcions', function (Blueprint $table) {
            $table->dropColumn('ARStoUYU');
            $table->dropColumn('USDtoUYU');
        });
    }
};
