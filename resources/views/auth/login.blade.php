@extends('layouts.auth')

@section('contenido')
    <form action="{{url('login')}}" method="post" class="login100-form validate-form">
        @csrf

        <input type="hidden" name="redirect" value="{{$request->redirect}}">
        <span class="login100-form-title p-b-43">
            <img src="{{asset('img/favicon.png')}}" alt="" width="50%">
            <br>
            <br>
            ¡Que bueno verte otra vez!
            <br>
        </span>

        @if(session('error'))
            <div style=" color: red; text-align: center; margin: 0 0 1em 0; background: #ffe1e1; padding: 1em; border-radius: 6px;">
                {{session('error')}}
            </div>
            {{session()->forget('error')}}
        @endif
        
        <div class="wrap-input100 validate-input" data-validate = "Escribe un correo valido: ex@abc.xyz">
            <input class="input100" type="text" name="email" value="{{old('email')}}" autofocus="">
            <span class="focus-input100"></span>
            <span class="label-input100">Dirección de email</span>
        </div>
        
        
        <div class="wrap-input100 validate-input" data-validate="La contraseña es requerida">
            <input class="input100" type="password" name="password">
            <span class="focus-input100"></span>
            <span class="label-input100">Contraseña</span>
        </div>

        <div class="flex-sb-m w-full p-t-3 p-b-32">
            <div class="contact100-form-checkbox">
                <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                <label class="label-checkbox100" for="ckb1">
                    Recuerdame
                </label>
            </div>

            <div>
                <a href="{{url('envio_contrasena')}}" class="txt1">
                    ¿Olvidaste tu contraseña?
                </a>
            </div>
        </div>
        <br>
        <div class="container-login100-form-btn">
            <button class="login100-form-btn" style="background: #00B36F;">
                Iniciar Sesión
            </button>
        </div>
        
    </form>
@endsection