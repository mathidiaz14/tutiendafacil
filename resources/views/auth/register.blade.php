@extends('layouts.auth')

@section('contenido')
    <form action="{{ url('registrarse') }}" method="post" class="login100-form" style="padding-top: 100px;">
        @csrf
        <span class="login100-form-title p-b-43">
            <img src="{{asset('img/favicon.png')}}" alt="" width="50">
            <br><br>
            Completa tus datos y nos comunicaremos contigo
        </span>
        
        
        <div class="wrap-input100 validate-input">
            <input class="input100" type="text" name="name" value="{{old('name')}}" required>
            <span class="focus-input100"></span>
            <span class="label-input100">Tu nombre</span>
        </div>

        <div class="wrap-input100 validate-input">
            <input class="input100" type="text" name="telefono" value="{{old('phone')}}" required>
            <span class="focus-input100"></span>
            <span class="label-input100">Tu teléfono</span>
        </div>

        <div class="wrap-input100 validate-input">
            <input class="input100" type="email" name="email" value="{{old('email')}}" required>
            <span class="focus-input100"></span>
            <span class="label-input100">Tu email</span>
        </div>
        <hr>
        <div class="wrap-input100 validate-input">
            <select name="plan" class="input100 plan" required="" style="border:none;" required>
                <option value="plan1">Emprendedor - ${{round(opcion('plan1') / 12)}} / mes</option>
                <option value="plan2">Pequeña empresa - ${{round(opcion('plan2') / 12)}} / mes</option>
                <option value="plan3" selected>Profesional - ${{round(opcion('plan3') / 12)}} / mes</option>
            </select>
            <span class="focus-input100"></span>
            <span class="label-input100">Elije un sistema</span>
        </div>
        <hr>
        <div class="container-login100-form-btn">
            <button class="login100-form-btn" style="background: #00B36F;">
                Enviar solicitud
            </button>
        </div>
        <div class="container-login100-form-btn mt-2">
            <a href="{{url('login')}}" class="login100-form-btn" style="border: 2px solid #00b36f;background: none;color: #00b36f;">
                Volver
            </a>
        </div>
    </form>
@endsection

