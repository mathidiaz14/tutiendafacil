@extends('layouts.dashboard', ['menu_activo' => 'multimedia', 'menu_superior' => 'tienda'])

@section('css')
	<link rel="stylesheet" href="{{asset('css/baguetteBox.css')}}">
	<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
	
	<script src="{{asset('js/baguetteBox.js')}}"></script>
@endsection

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Multimedia</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">Dashboard</li>
						<li class="breadcrumb-item active">Multimedia</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col">
							<small >
								Click derecho en las imagenes para ver las opciones
							</small>
						</div>
						<div class="col text-right">
							<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarImagenes">
								<i class="fa fa-upload mr-3"></i>
								Cargar imágenes
							</button>
						</div>
					</div>
				</div>
				<div class="card-body">
					<div id="imagenes" class="context-menu-two">
						@include('admin.multimedia.imagenes', ['imagenes' => $imagenes])
					</div>
				</div>
			</div>
		</div>
	</section>
</div>


<div class="modal fade" id="crearCarpeta" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gradient-secondary">
				<div class="col-10 text-left">
					<i class="fa fa-upload mr-3"></i>
					Crear carpeta
				</div>
				<div class="col-2">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="modal-body text-left">
				<input type="hidden" name="parent_id" id="parent_id" value="home">
				<div class="form-group">
					<label for="">Nombre</label>
					<input type="text" name="nombre" class="form-control" placeholder="Nombre..." id="crearCarpetaNombre" required>
				</div>
				<hr>
				<div class="form-group text-right">
					<button class="btn btn-secondary btn_agregar_carpeta">
						<i class="fa fa-save mr-3"></i>
						Guardar
					</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="agregarImagenes" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gradient-info">
				<div class="col-10 text-left">
					<i class="fa fa-upload mr-3"></i>
					Cargar imágenes
				</div>
				<div class="col-2">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="modal-body text-left">
				<form action="{{url('admin/multimedia')}}" method="post" class="dropzone" id="cargaDeImagenes" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="categoria" id="categoria" value="home">
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>	

	<script>
		Dropzone.autoDiscover = false;

		baguetteBox.run('.tz-gallery');

		$('#crearCarpeta').on('shown.bs.modal', function () 
		{
		  	$('#crearCarpetaNombre').trigger('focus')
		});

		$('#cargaDeImagenes').dropzone({ 
		    dictDefaultMessage: "Haz click aquí o arrastra las imágenes para cargarlas",
		    maxFilesize: 30, 
		    acceptedFiles: ".jpeg,.jpg,.png,.gif",
		    success: function () 
		    {
		    	$('#imagenes').load("{{url('admin/multimedia/cargar/imagen')}}", function()
        		{
	            	$('.carpetas').hide();
	            	
	            	var carpeta = $('#categoria').val();
					$('#carpeta_'+carpeta).fadeIn();

        		});	
		    }
		});

		$('.btn_agregar_carpeta').click(function()
		{
			$.ajax({
	            url: "{{url('admin/multimedia/carpeta/crear')}}",
				type: 'post',
	            dataType: "JSON",
	            data: {
	                "_token": $('meta[name="csrf-token"]').attr('content'),
	                "parent_id": $('#parent_id').val(),
	                "nombre": $('#crearCarpetaNombre').val(),
	            },success: function (data) 
	            {
	            	$('#imagenes').load("{{url('admin/multimedia/cargar/imagen')}}", function()
	        		{
		            	$('.carpetas').hide();
		            	
		            	var carpeta = $('#parent_id').val();
						$('#carpeta_'+carpeta).fadeIn();
	        		});	

	        		$('#crearCarpeta').modal('toggle');
	            }
	        });
		});

		$.contextMenu({
            selector: '.context-menu-one', 
            callback: function(key, options) 
            {	
                switch (key)
                {
                	case 'view':
                		$(".contenedor_imagenes").selectable();
            		break;

                	case 'edit':
                		var id = $(this).attr("attr-id");
                		$('#modalImagen_'+id).modal('toggle');
            		break;

                	case 'delete':
                		if($('.ui-selected').length == 0)
                		{
                			var id = $(this).attr("attr-id");

	                		$.ajax({
					            url: "{{url('admin/multimedia')}}/"+id,
								type: 'DELETE',
					            dataType: "JSON",
					            data: {
					                "id": id,
					                "_method": 'DELETE',
					                "_token": $('meta[name="csrf-token"]').attr('content'),
					            },success: function (data) 
					            {
					            	$('#imagen_'+id).fadeOut();
					            }
					        });
                		}else
                		{
                			$('.ui-selected').each(function(index, elemento)
                			{
                				var id = $(this).attr('attr-id');

                				$.ajax({
						            url: "{{url('admin/multimedia')}}/"+id,
									type: 'DELETE',
						            dataType: "JSON",
						            data: {
						                "id": id,
						                "_method": 'DELETE',
						                "_token": $('meta[name="csrf-token"]').attr('content'),
						            },success: function (data) 
						            {
						            	$('#imagen_'+id).fadeOut();
						            }
						        });
                			});
                		}

            		break;

            		@foreach(empresa()->carpetas as $carpeta)
                		
                		case 'carpeta-{{$carpeta->id}}':

	                		if($('.ui-selected').length == 0)
	                		{
	                			var id = $(this).attr("attr-id");

		                		$.ajax({
						            url: "{{url('admin/multimedia/mover/imagen')}}",
									type: 'post',
						            dataType: "JSON",
						            data: {
						                "_token": $('meta[name="csrf-token"]').attr('content'),
						                "id": id,
						                "carpeta": "{{$carpeta->id}}"
						            },success: function (data) 
						            {
						            	$('#imagenes').load("{{url('admin/multimedia/cargar/imagen')}}", function()
					            		{
							            	$('.carpetas').hide();
							            	
							            	var carpeta = $('#categoria').val();
											$('#carpeta_'+carpeta).fadeIn();
					            		});	
						            }
						        });
	                		}else
	                		{
	                			$('.ui-selected').each(function(index, elemento)
	                			{
	                				var id = $(this).attr("attr-id");

			                		$.ajax({
							            url: "{{url('admin/multimedia/mover/imagen')}}",
										type: 'post',
							            dataType: "JSON",
							            data: {
							                "_token": $('meta[name="csrf-token"]').attr('content'),
							                "id": id,
							                "carpeta": "{{$carpeta->id}}"
							            },success: function (data) 
							            {
							            	$('#imagenes').load("{{url('admin/multimedia/cargar/imagen')}}", function()
						            		{
								            	$('.carpetas').hide();
							            	
								            	var carpeta = $('#categoria').val();
												$('#carpeta_'+carpeta).fadeIn();
						            		});	
							            }
							        });
	                			});
	                		}

                		break;

                	@endforeach

               	 	case 'carpeta-home':

                		if($('.ui-selected').length == 0)
                		{
                			var id = $(this).attr("attr-id");

	                		$.ajax({
					            url: "{{url('admin/multimedia/mover/imagen')}}",
								type: 'post',
					            dataType: "JSON",
					            data: {
					                "_token": $('meta[name="csrf-token"]').attr('content'),
					                "id": id,
					                "carpeta": "home"
					            },success: function (data) 
					            {
					            	$('#imagenes').load("{{url('admin/multimedia/cargar/imagen')}}", function()
				            		{
						            	$('.carpetas').hide();
					            	
						            	var carpeta = $('#categoria').val();
										$('#carpeta_'+carpeta).fadeIn();
				            		});	
					            }
					        });
                		}else
                		{
                			$('.ui-selected').each(function(index, elemento)
                			{
                				var id = $(this).attr("attr-id");

		                		$.ajax({
						            url: "{{url('admin/multimedia/mover/imagen')}}",
									type: 'post',
						            dataType: "JSON",
						            data: {
						                "_token": $('meta[name="csrf-token"]').attr('content'),
						                "id": id,
						                "carpeta": "home"
						            },success: function (data) 
						            {
						            	$('#imagenes').load("{{url('admin/multimedia/cargar/imagen')}}", function()
					            		{
							            	$('.carpetas').hide();
						            	
							            	var carpeta = $('#categoria').val();
											$('#carpeta_'+carpeta).fadeIn();
					            		});	
						            }
						        });
                			});
                		}

            		break;
                }
            },
            items: {
                "view": {name: "Seleccionar", icon: "fas fa-object-group"},
                "edit": {name: "Editar", icon: "edit"},
                "delete": {name: "Eliminar", icon: "delete"},
                
                @if(empresa()->carpetas->count() > 0)
	                "move": {
		                name: "Mover a carpeta", 
		                icon: "fas fa-folder",
		                items: {
			                	"carpeta-home": {
									"name": "Inicio",
									"icon": "fas fa-folder",
								},

		                	@foreach(empresa()->carpetas as $carpeta)
								"carpeta-{{$carpeta->id}}": {
									"name": "{{$carpeta->nombre}}",
									"icon": "fas fa-folder",
								},
		                    @endforeach
		                }
		            },
		        @endif

                "sep2": "---------",
                "quit": {name: "Salir", icon: function(){
                    return 'context-menu-icon context-menu-icon-quit';
                }}
            }
        });

		$.contextMenu({
            selector: '.context-menu-two', 
            callback: function(key, options) 
            {	
                switch (key)
                {
                	case 'view':
                		$("#contenedor_imagenes").selectable();
            		break;

                	case 'newFolder':
                		$('#crearCarpeta').modal('toggle');
            		break;

                	case 'upload':
                		$('#agregarImagenes').modal('toggle');
            		break;
                }
            },
            items: {
            	"view": {name: "Seleccionar", icon: "fas fa-object-group"},
            	"newFolder": {name: "Nueva carpeta", icon: "fas fa-folder-plus"},
            	"upload": {name: "Cargar imagenes", icon: "fas fa-upload"},
                "sep2": "---------",
                "quit": {name: "Salir", icon: function(){
                    return 'context-menu-icon context-menu-icon-quit';
                }}
            }
        });

        $.contextMenu({
            selector: '.context-menu-carpeta', 
            callback: function(key, options) 
            {	
                switch (key)
                {
                	case 'rename':
                		
            		break;

                	case 'delete':
                		
            		break;
                }
            },
            items: {
            	"rename": {name: "Renombrar", icon: "fas fa-font"},
            	"delete": {name: "Eliminar", icon: "fas fa-trash"},
                "sep2": "---------",
                "quit": {name: "Salir", icon: function(){
                    return 'context-menu-icon context-menu-icon-quit';
                }}
            }
        });
	
	</script>
@endsection