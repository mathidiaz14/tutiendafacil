
<ol class="row" style="list-style:none;">
	@foreach($carpetas as $hijo)
		<li class="col-4 col-md-3 col-lg-2 mb-2 text-center carpeta context-menu-carpeta" style="cursor:pointer;" attr-id="{{$hijo->id}}">
			<i class="fa fa-folder text-secondary fa-4x"></i>
			<br>
			<small>{{$hijo->nombre}}</small>
		</li>
	@endforeach
</ol>

<div class="tz-gallery p-0">
	<ul class="row contenedor_imagenes" style="list-style:none;">
		@foreach($imagenes->sortBy('nombre') as $imagen)
			<li class="col-6 col-md-3 col-lg-2 mb-2 li_producto text-center context-menu-one p-1" id="imagen_{{$imagen->id}}" attr-id="{{$imagen->id}}">

				<a class="lightbox" href="{{asset($imagen->url)}}" >
					<img src="{{asset($imagen->url)}}" alt="{{$imagen->alternativo}}" width="90%" class="imagen_producto mb-0" >
				</a>

				<small>{{$imagen->nombre}}</small>
			</li>

			<div class="modal fade" id="modalImagen_{{$imagen->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-dialog-centered text-left" role="document">
			  		<div class="modal-content">
						<div class="modal-header">
							<div class="col">
								<h5 class="modal-title">Detalles de imagen</h5>
							</div>
							<div class="col">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times</span>
								</button>
							</div>
			    		</div>
			    		<div class="modal-body">
			    			<div class="row">
			    				<div class="col-12">
			    					<div class="row">
			    						<div class="col-12 mt-2">
			    							<p>
			    								URL: <small><a href="{{asset($imagen->url)}}" target="_blank">{{$imagen->url}}</a></small> <br>
					    						Subido el: <b>{{$imagen->created_at->format('d/m/Y H:i')}}</b> <br>
						    					Subido por: <b>{{$imagen->user_id != null ? $imagen->usuario->nombre : "--"}}</b> <br>
						    					Tipo: <b>{{$imagen->tipo != null ? $imagen->tipo : "--"}}</b> <br>
						    					Tamaño: <b>{{$imagen->tamaño != null ? round(($imagen->tamaño / 1024), 2)." KB" : "--"}}</b> <br>
					    					</p>
			    						</div>
			    						<div class="col-12">
			    							<hr>		
			    						</div>
			    						<div class="col-12">
			    							<form action="{{url('admin/multimedia', $imagen->id)}}" method="post" class="form-horizontal">
			    								@csrf
			    								@method('PATCH')
			    								<div class="form-group">
						    						<label for="">Nombre del archivo</label>
				    								<input type="text" class="form-control" value="{{$imagen->nombre}}" name="nombre">
						    					</div>	
						    					<div class="form-grouop">
						    						<label for="">Texto alternativo</label>
						    						<input type="text" class="form-control" name="alternativo" value="{{$imagen->alternativo}}">
						    					</div>	
						    					<div class="form-group mt-2">
						    						<label for="">Descripción</label>
						    						<textarea name="descripcion" id="" cols="30" rows="3" class="form-control">{{$imagen->descripcion}}</textarea>
						    					</div>
						    					<div class="form-group text-right">
						    						<button class="btn btn-info">
						    							<i class="fa fa-save mr-3"></i>
						    							Guardar
						    						</button>
						    					</div>
			    							</form>
			    						</div>
			    					</div>
			    				</div>
			    			</div>
			    		</div>
			  		</div>
				</div>
			</div>
		@endforeach	
	</ul>
</div>

<script>
	baguetteBox.run('.tz-gallery');
</script>