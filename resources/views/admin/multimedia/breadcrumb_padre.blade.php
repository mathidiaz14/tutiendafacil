
@if($carpeta->parent_id != null)
	@include('admin.multimedia.breadcrumb_padre', ['carpeta' => $carpeta->padre])

	<li class="breadcrumb-item" aria-current="page">
		<a class="carpeta" attr-id="{{$carpeta->padre->id}}" style="cursor:pointer;">
			{{$carpeta->padre->nombre}}
		</a>
	</li>
@endif