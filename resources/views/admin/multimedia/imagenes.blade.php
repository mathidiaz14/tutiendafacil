@if((empresa()->imagenes->count() == 0) and (empresa()->carpetas->count() == 0))
	@include('ayuda.sin_registros')
@else
	
	<div id="carpeta_home" class="row carpetas">
		<div class="col-12">
			@include('admin.multimedia.hijo', [
											'carpetas' => empresa()->carpetas->where('parent_id', null), 
											'imagenes' => empresa()->imagenes->where('categoria_id', null)
											])
		</div>
	</div>

	@foreach(empresa()->carpetas as $carpeta)
		<div style="display: none;" id="carpeta_{{$carpeta->id}}" class="row carpetas">
			<div class="col-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a class="carpeta" attr-id="home" style="cursor:pointer;">
								Inicio
							</a>
						</li>

						@include('admin.multimedia.breadcrumb_padre', ['carpeta' => $carpeta])

						<li class="breadcrumb-item active" aria-current="page">{{$carpeta->nombre}}</li>

					</ol>
				</nav>
			</div>
			<div class="col-12">
				@include('admin.multimedia.hijo', ['carpetas' => $carpeta->hijos, 'imagenes' => $carpeta->imagenes->where('categoria_id', $carpeta->id)])
			</div>
		</div>
	@endforeach

@endif

<script>
	$('.carpeta').click(function()
	{
		$('.carpetas').hide();

		var id = $(this).attr('attr-id');

		$('#carpeta_'+id).fadeIn();
		$('#categoria').val(id);
		$('#parent_id').val(id);
	});
</script>