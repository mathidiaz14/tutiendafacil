@extends('layouts.dashboard', ['menu_activo' => 'visual', 'menu_superior' => 'administrar'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-6">
					<h1 class="m-0 text-dark">Configuración Visual</h1>
				</div><!-- /.col -->
				<div class="col-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Configuración Visual</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<section class="content">
		<div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5>
                                Configuración Visual
                            </h5>
                        </div>
                    </div>
                </div>
				<div class="card-body">
					<form action="{{url('admin/visual')}}" class="form-horizontal" method="post" >
						@csrf
                        
                        <div class="row">
                            <div class="col">
                                <div class="row">
                                    <div class="col-6">
                                        <label for="">Dashboard tienda</label>
                                    </div>
                                    <div class="col-6">
                                        @include('ayuda.switch', ['nombre' => 'tienda_dashboard', 'estado' => empty(visual('tienda_dashboard')) ? "on" : visual('tienda_dashboard')->valor])
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col-6">
                                        <label for="">Menu tienda</label>
                                    </div>
                                    <div class="col-6">
                                    @include('ayuda.switch', ['nombre' => 'tienda_menu', 'estado' => empty(visual('tienda_menu')) ? "on" : visual('tienda_menu')->valor])
                                    </div>
                                </div>
                            </div>
                        </div>
						
                        @foreach(empresa()->plugins as $plugin)
                            @if($plugin->pivot->estado == "activo")
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="">Dashboard {{$plugin->nombre}}</label>
                                            </div>
                                            <div class="col-6">
                                                @include('ayuda.switch', ['nombre' => $plugin->nombre.'_dashboard', 'estado' => empty(visual($plugin->nombre.'_dashboard')) ? "on" : visual($plugin->nombre.'_dashboard')->valor])
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="">Menu {{$plugin->nombre}}</label>
                                            </div>
                                            <div class="col-6">
                                            @include('ayuda.switch', ['nombre' => $plugin->nombre.'_menu', 'estado' => empty(visual($plugin->nombre.'_menu')) ? "on" : visual($plugin->nombre.'_menu')->valor])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        
						<hr>
						<div class="row text-right">
							<div class="col">
								<button class="btn btn-primary">
									<i class="fa fa-save"></i>
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			
		</div>
	</section>	
</div>
@endsection

@section('scripts')

@endsection