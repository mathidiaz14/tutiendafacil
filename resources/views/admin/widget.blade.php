<div class="row">
  <div class="col-lg-3 col-6">
    
    <div class="small-box bg-secondary bg-gradient">
      <div class="inner">
        <h3>
          @php
            $pedidos_nuevos = empresa()
                                  ->ventas
                                  ->where('estado', 'en_preparacion')
                                  ->whereBetween('created_at', [\Carbon\Carbon::now()->firstOfMonth(), \Carbon\Carbon::now()])
                                  ->count();

            
          @endphp
          {{$pedidos_nuevos}}
        </h3>

        <p>Pedidos nuevos</p>
      </div>
    </div>
  </div>
  
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-secondary bg-gradient">
      <div class="inner">
        <h3>
          @php
            $pendiente_pago = empresa()
                                  ->ventas
                                  ->where('estado', 'en_preparacion')
                                  ->where('pago_estado', 'pendiente')
                                  ->whereBetween('created_at', [\Carbon\Carbon::now()->firstOfMonth(), \Carbon\Carbon::now()])
                                  ->count();

            
          @endphp
          {{$pendiente_pago}}
        </h3>

        <p>Pendiente pago</p>
      </div>
    </div>
  </div>
  
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-secondary bg-gradient">
      <div class="inner">
        <h3>
          @php
            $pendiente_entrega = empresa()
                                  ->ventas
                                  ->where('estado', 'entrega')
                                  ->whereBetween('created_at', [\Carbon\Carbon::now()->firstOfMonth(), \Carbon\Carbon::now()])
                                  ->count();

            
          @endphp
          {{$pendiente_entrega}}
        </h3>

        <p>Pendientes entrega</p>
      </div>
    </div>
  </div>
  
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-secondary bg-gradient">
      <div class="inner">
        <h3>
          @php
            $total_pedidos = empresa()
                                  ->ventas
                                  ->where('estado', '!=', 'comenzado')
                                  ->whereBetween('created_at', [\Carbon\Carbon::now()->firstOfMonth(), \Carbon\Carbon::now()])
                                  ->count();

            
          @endphp
          {{$total_pedidos}}
        </h3>

        <p>Total</p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-3 col-6">   
    <div class="small-box bg-white">
      <div class="inner">
        <h3>
          @php
            $ventas_hoy = empresa()
                            ->ventas
                            ->where('estado', '!=','comenzado')
                            ->whereBetween('created_at', [\Carbon\Carbon::now()->startOfDay(), \Carbon\Carbon::now()->endOfDay()])
                            ->sum('precio');

            $ventas_hoy_descuento = empresa()
                                    ->ventas
                                    ->where('estado', '!=','comenzado')
                                    ->whereBetween('created_at', [\Carbon\Carbon::now()->startOfDay(), \Carbon\Carbon::now()->endOfDay()])
                                    ->sum('descuento');

            
          @endphp
          $ {{$ventas_hoy - $ventas_hoy_descuento}}
        </h3>

        <p>Ventas hoy</p>
      </div>
    </div>
  </div>
  
  <div class="col-lg-3 col-6">
    <div class="small-box bg-white">
      <div class="inner">
        <h3>
          @php
            $ventas_siete = empresa()
                            ->ventas
                            ->where('estado', '!=','comenzado')
                            ->whereBetween('created_at', [\Carbon\Carbon::now()->subDays(7)->startOfDay(), \Carbon\Carbon::now()->endOfDay()])
                            ->sum('precio');

            $ventas_siete_descuento = empresa()
                                    ->ventas
                                    ->where('estado', '!=','comenzado')
                                    ->whereBetween('created_at', [\Carbon\Carbon::now()->subDays(7)->startOfDay(), \Carbon\Carbon::now()->endOfDay()])
                                    ->sum('descuento');

            
          @endphp
          $ {{$ventas_siete - $ventas_siete_descuento}}
        </h3>

        <p>Ultimos 7 días</p>
      </div>
    </div>
  </div>
  
  <div class="col-lg-3 col-6">
   <div class="small-box bg-white">
      <div class="inner">
        <h3>
          @php
            $ventas_mes_actual = empresa()
                                    ->ventas
                                    ->where('estado','entregado')
                                    ->whereBetween('created_at', [\Carbon\Carbon::now()->subDays(30), \Carbon\Carbon::now()->endOfDay()])
                                    ->sum('precio');

            $ventas_mes_actual_descuento = empresa()
                                    ->ventas
                                    ->where('estado','entregado')
                                    ->whereBetween('created_at', [\Carbon\Carbon::now()->subDays(30), \Carbon\Carbon::now()->endOfDay()])
                                    ->sum('descuento');

            
          @endphp
          $ {{$ventas_mes_actual - $ventas_mes_actual_descuento}}
        </h3>

        <p>Ultimos 30 días ({{\Carbon\Carbon::now()->subDays(30)->format('d/m/Y')}})</p>
      </div>
    </div>
  </div>

  <div class="col-lg-3 col-6">
    <div class="small-box bg-white">
      <div class="inner">
        <h3>
          @php
            $ventas_mes_actual = empresa()
                                    ->ventas
                                    ->where('estado','entregado')
                                    ->whereBetween('created_at', [\Carbon\Carbon::now()->subDays(180), \Carbon\Carbon::now()->endOfDay()])
                                    ->sum('precio');

            $ventas_mes_actual_descuento = empresa()
                                    ->ventas
                                    ->where('estado','entregado')
                                    ->whereBetween('created_at', [\Carbon\Carbon::now()->subDays(180), \Carbon\Carbon::now()->endOfDay()])
                                    ->sum('descuento');

            
          @endphp
          $ {{$ventas_mes_actual - $ventas_mes_actual_descuento}}
        </h3>

        <p>
          Ultimos 180 días ({{\Carbon\Carbon::now()->subDays(180)->format('d/m/Y')}})
        </p>
      </div>
    </div>
  </div>
</div>