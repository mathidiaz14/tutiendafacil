@extends('layouts.dashboard', ['menu_activo' => 'producto', 'menu_superior' => 'tienda'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Productos de la categoria</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item"><a href="{{url('admin/categoria')}}">Categoría</a></li>
						<li class="breadcrumb-item active">Productos</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">

					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col">
									<h3 class="card-title">
										<i class="fas fa-boxes"></i>
										{{$categoria->titulo}}
									</h3>
								</div>
								<div class="col text-right">
									<a href="{{url('admin/categoria')}}" class="btn btn-secondary">
										<i class="fa fa-chevron-left"></i>
										Atras
									</a>
								</div>
							</div>
						</div>
						<div class="card-body">
							@include('admin.producto.secciones.lista_productos', ['productos' => $productos])		
						</div>
					</section>
				</div>
			</div>
		</section>
	</div>
	@endsection

	@section('scripts')
	<script>
		$('#agregarProducto').on('shown.bs.modal', function () {
			$('#nombre').trigger('focus');
		});
	</script>
	@endsection