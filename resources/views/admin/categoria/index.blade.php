@extends('layouts.dashboard', ['menu_activo' => 'categoria', 'menu_superior' => 'tienda'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Categorías</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Categorías</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">

					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col">
									<h3 class="card-title">
										<i class="fas fa-boxes"></i>
										Categorías
									</h3>
								</div>
								<div class="col text-right">
									@include('admin.categoria.partes.agregar')
								</div>
							</div>
						</div>
						<div class="card-body">
							@if($categorias->count() == 0)
								@include('ayuda.sin_registros')
							@else
							<div class="table table-responsive">
								<table class="table table-striped">
									<thead>
										<tr>
											<th></th>
											<th>Imagen</th>
											<th>URL</th>
											<th>Nombre</th>
											<th>Observación</th>
											<th>Online</th>
											<th class="text-center">Productos</th>
											<th class="text-center">Editar</th>
											<th class="text-center">Eliminiar</th>
										</tr>
									</thead>
									<tbody class="sortable">
										@foreach($categorias->sortBy('posicion') as $categoria)
										<tr class="tr_categoria" attr-id="{{$categoria->id}}">
											<td>
												<i class="fa fa-bars text-secondary" style="cursor:move;"></i>
											</td>
											<td>
												<img src="{{categoria_url_imagen($categoria->id)}}" alt="" width="50px" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'">
											</td>
											<td>
												<a href="http://{{$categoria->empresa->URL}}/categoria/{{$categoria->url}}" target="_blank">
													<small>{{$categoria->url}}</small>
												</a>
											</td>
											<td>{{$categoria->titulo}}</td>
											<td>{{$categoria->observacion == null ? "--" : $categoria->observacion}}</td>
											<td>
												@include('ayuda.switch', ['nombre' => $categoria->id, 'estado' => $categoria->online])
											</td>
											<td class="text-center">
												<a href="{{url('admin/categoria', $categoria->id)}}" class="btn btn-dark">
													<i class="fa fa-eye"></i>
												</a>
											</td>
											<td class="text-center">
												@include('admin.categoria.partes.editar', ['categoria' => $categoria])
											</td>
											<td class="text-center">
												@include('ayuda.eliminar', ['id' => $categoria->id, 'ruta' => url('admin/categoria', $categoria->id)])
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							@endif
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script>
	$('#agregarCategoria').on('shown.bs.modal', function () {
		$('#titulo').trigger('focus');
	});

	$('.onoffswitch-checkbox').change(function()
	{
		var id = $(this).attr('attr-id');

		$.get('{{url("admin/categoria/estado")}}/'+id);
	})

	$( ".sortable" ).sortable({
		opacity: 0.5,
		stop: function()
		{
			var array = [];

			$('.tr_categoria').each(function(index, elemento)
			{
				array.push([$(this).attr('attr-id'), index]);
			})

			$.ajax({
				url: "{{url('admin/categoria/mover')}}",
				type: 'POST',
				dataType: "JSON",
				data: {
					"_token": $('meta[name="csrf-token"]').attr('content'),
					"array": array,
				}
			});
		}
	});

</script>
@endsection