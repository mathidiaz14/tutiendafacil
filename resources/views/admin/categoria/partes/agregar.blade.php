
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarCategoria">
	<i class="fa fa-plus"></i>
	Agregar
</button>

<!-- Modal -->
<div class="modal fade" id="agregarCategoria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content text-left">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Agregar categoría</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{url('admin/categoria')}}" class="form-horizontal" method="post" enctype="multipart/form-data">
					@csrf
					<div class="form-group">
						<label for="">Titulo</label>
						<input type="text" name="titulo" id="titulo" class="form-control" placeholder="Nombre de la categoria" required="">
					</div>
					<div class="form-group">
						<label for="">Imagen</label>
						<input type="file" name="imagen" class="form-control-file">
					</div>
					<div class="form-group">
						<label for="">Observación</label>
						<textarea name="observacion" id="" class="form-control" placeholder="Observación de la categoria"></textarea>
					</div>
					<hr>
					<div class="row">
						<div class="col">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-chevron-left"></i>
								Atras
							</button>
						</div>
						<div class="col text-right">
							<button class="btn btn-primary">
								<i class="fa fa-save"></i>
								Guardar
							</button>
						</div>
					</div>	
				</form>  
			</div>
		</div>
	</div>
</div>