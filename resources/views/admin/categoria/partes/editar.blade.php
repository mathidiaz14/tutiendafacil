
<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#editarCategoria_{{$categoria->id}}">
	<i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="editarCategoria_{{$categoria->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content text-left">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar categoria</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{url('admin/categoria', $categoria->id)}}" class="form-horizontal" method="post" enctype="multipart/form-data">
					@csrf
					@method('PATCH')
					<div class="form-group">
						<label for="">Nombre</label>
						<input type="text" name="titulo" class="form-control" value="{{$categoria->titulo}}">
					</div>
					@if($categoria->imagen != null)
					<div class="form-group">
						<img src="{{asset($categoria->imagen)}}" alt="" width="100px" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'">
						<br><br>
						<a href="{{url('admin/categoria/borrar/imagen', $categoria->id)}}" class="btn btn-danger">
							<i class="fa fa-trash"></i>
							Borrar imagen
						</a>
					</div>
					@else
					<div class="form-group">
						<img src="{{categoria_url_imagen($categoria->id)}}" alt="" width="100px">
					</div>
					@endif
					<div class="form-group">
						<label for="">Imagen</label>
						<input type="file" name="imagen" class="form-control-file">
					</div>
					<div class="form-group">
						<label for="">Observación</label>
						<textarea name="observacion" id="" class="form-control">{{$categoria->observacion}}</textarea>
					</div>
					<hr>
					<div class="row">
						<div class="col">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-chevron-left"></i>
								Atras
							</button>
						</div>
						<div class="col text-right">
							<button class="btn btn-primary">
								<i class="fa fa-save"></i>
								Guardar
							</button>
						</div>
					</div>	
				</form>  
			</div>
		</div>
	</div>
</div>