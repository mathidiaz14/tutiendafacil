<button type="button" class="btn btn-info" data-toggle="modal" data-target="#comprarPlugin_{{$plugin->id}}">
	<i class="fa fa-money-bill mr-3"></i>
	Suscribirme
</button>

<div class="modal fade deleteModal" id="comprarPlugin_{{$plugin->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gradient-info">
				<div class="col-10 text-left">
					<h5>Suscripción</h5>
				</div>
				<div class="col-2">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="modal-body text-left">
				<div class="alert alert-danger hide"></div>
				<form id="form-checkout" class="form-horizontal">
					<progress value="0" class="progress-bar" style="display:none;">Cargando...</progress>
					<div class="row">
						<div class="form-group col-12">
							<label for="">Numero de tarjeta</label>
							<input class="form-control number_card" type="text" name="cardNumber" id="form-checkout__cardNumber" maxlength="16" required autofocus />
						</div>
						<div class="form-group col-6">
							<label for="">Vencimiento</label>
							<input class="form-control" type="text" name="cardExpirationDate" id="form-checkout__cardExpirationDate" maxlength="5" required />
						</div>
						<div class="form-group col-6">
							<label for="">CCV</label>
							<input class="form-control" type="text" name="securityCode" id="form-checkout__securityCode" maxlength="3" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required />
						</div>
						
						<div class="form-group col-12">
							<label for="">Titular de la tarjeta</label>
							<input class="form-control name_card" type="text" name="cardholderName" id="form-checkout__cardholderName" required/>
						</div>
						
						<div class="form-group col-12">
							<div class="row">
								<div class="col-12">
									<label for="">Documento</label>
								</div>
								<div class="col-4">
									<select class="form-control" name="identificationType" id="form-checkout__identificationType" required ></select>
								</div>
								<div class="col-8">
									<input class="form-control" type="text" name="identificationNumber" id="form-checkout__identificationNumber" value="" required />
								</div>
							</div>
						</div>

						<div class="col-12">
							<label for="">Email</label>
							<input class="form-control" type="email" name="cardholderEmail" id="form-checkout__cardholderEmail" value="" required />	
						</div>
						
						<select class="form-control issuer" name="issuer" id="form-checkout__issuer" required style="display:none;"></select>
						<select class="form-control" name="installments" id="form-checkout__installments" required style="display:none;"></select>

						<div class="col-12">
							<hr>
						</div>

						<div class="form-group col-12">
							<button type="submit" id="form-checkout__submit" class="btn btn-secondary btn-block py-3">
								<i class="fa fa-lock mr-3"></i>
								Pagar
							</button>
						</div>
					</div>
				</form>

				<script>
					$('.number_card').bind('keyup paste', function(){
						this.value = this.value.replace(/[^0-9]/g, '');
					});

					const mp = new MercadoPago('{{env("MP_PUBLIC_KEY")}}');
					const cardForm = mp.cardForm({
						amount: "{{(int)$plugin->precio}}",
						autoMount: true,
						form: {
							id: "form-checkout",
							cardholderName: {
								id: "form-checkout__cardholderName",
								placeholder: "Juan Perez",
							},
							cardholderEmail: {
								id: "form-checkout__cardholderEmail",
								placeholder: "juan@gmail.com",
							},
							cardNumber: {
								id: "form-checkout__cardNumber",
								placeholder: "0000 0000 0000 0000",
							},
							cardExpirationDate: {
								id: "form-checkout__cardExpirationDate",
								placeholder: "MM/YY",
							},
							securityCode: {
								id: "form-checkout__securityCode",
								placeholder: "CCV",
							},
							installments: {
								id: "form-checkout__installments",
								placeholder: "Cuotas",
							},
							identificationType: {
								id: "form-checkout__identificationType",
								placeholder: "Tipo de documento",
							},
							identificationNumber: {
								id: "form-checkout__identificationNumber",
								placeholder: "12345678",
							},
							issuer: {
								id: "form-checkout__issuer",
								placeholder: "Banco emisor",
							},
						},
						callbacks: {
							onFormMounted: error => {
								if (error) return console.warn("Form Mounted handling error: ", error);
							},
							onSubmit: event => {
								$('.cargando').fadeIn();
								event.preventDefault();

								const {
									cardholderName,
									paymentMethodId: payment_method_id,
									issuerId: issuer_id,
									cardholderEmail: email,
									amount,
									token,
									installments,
									identificationNumber,
									identificationType,
								} = cardForm.getCardFormData();

								$.ajax({
									type:'POST',
									url:"{{url('admin/plugin/pago')}}",
									headers: {
										"Content-Type": "application/json",
									},
									data:JSON.stringify({
										_token:"{{csrf_token()}}",
										token,
										issuer_id,
										payment_method_id,
										cardholderName,
										transaction_amount: Number(amount),
										installments: Number(installments),
										description: "{{$plugin->id}}",
										payer: {
											email,
											identification: {
												type: identificationType,
												number: identificationNumber,
											},
										},
									}),
									success: function (result) 
									{
										if(result.status == "approved")
										{
											window.location.replace("{{url('admin/plugin')}}");
										}
										else
										{
											$('.alert').html(result.message);
											$('.alert').fadeIn();

											//$('input').val("");
											//$("select option:selected").prop("selected", false);
										}

										$('.cargando').fadeOut();
									},
								});
							},
							onFetching: (resource) => {
								
							}
						},
					});
				</script>
			</div>
		</div>
	</div>
</div>