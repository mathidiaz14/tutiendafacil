@if($misPlugins->count() == 0)
	@include('ayuda.sin_registros')
@else
	<div class="row">
		@foreach($misPlugins as $plugin)
			<div class="col-6 col-md-4 col-lg-2 p-3">
				<div class="row modal_plugins">
					<div class="col-12 p-0">
						<img src="{{asset($plugin->imagen)}}" alt="" width="100%" >
						<div class="modal_plugins_link" type="button" data-toggle="modal" data-target="#pluginModal_{{$plugin->id}}">
							<i class="fa fa-link"></i>
						</div>
					</div>
					<div class="col-12 text-center">
						@if($plugin->pivot->estado == "instalado")
							<span class="badge bg-info" style="width:100%;">Instalado</span>
						@elseif($plugin->pivot->estado == "activo")
							<span class="badge bg-success" style="width:100%;">Activo</span>
						@elseif($plugin->pivot->estado == "pendiente")
							<span class="badge bg-warning" style="width:100%;">Pago pendiente</span>
						@endif
					</div>
					<div class="col-12 text-center py-2">
						<h5>
							{{$plugin->nombre}}
						</h5>
					</div>
				</div>
			</div>

			<div class="modal fade" id="pluginModal_{{$plugin->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			  		<div class="modal-content">
						<div class="modal-header">
							<div class="col">
								<h5>Detalles del plugin</h5>
							</div>
							<div class="col">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
			    		</div>
			    		<div class="modal-body">
			    			<div class="row">
			    				<div class="col-12 col-md-6">
			    					<img src="{{asset($plugin->imagen)}}" alt="" width="100%" >
			    				</div>
			    				<div class="col-12 col-md-6">
			    					<div class="row">
			    						<div class="col-12">
			    							<h5>
					    						{{$plugin->nombre}}
					    					</h5>
					    					<hr>
					    					<p>{{$plugin->descripcion}}</p>
			    						</div>
			    					</div>	
			    				</div>
			    			</div>
			    		</div>
			    		<div class="modal-footer">
			    			<div class="col">
		      					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal_{{$plugin->id}}">
									<i class="fa fa-trash"></i>
									Desinstalar
								</button>

								<!-- Modal -->
								<div class="modal fade" id="deleteModal_{{$plugin->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
									<div class="modal-dialog modal-dialog-centered" role="document">
								  		<div class="modal-content">
											<div class="modal-header bg-gradient-danger">
												<button type="button" class="close" onClick="$('#deleteModal_{{$plugin->id}}').modal('hide');" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
								    		</div>
								    		<div class="modal-body text-center">
								    			<br>
								    			<p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
								      			<h4>¿Desea eliminar el plugin?</h4>
								      			<br>
								            	<hr>
								      			<div class="row">
								      				<div class="col">
							    						<button type="button" class="btn btn-secondary btn-block" 
							    							onClick="$('#deleteModal_{{$plugin->id}}').modal('hide');">
							    							NO
							    						</button>
								      				</div>
								      				<div class="col">
								      					<form action="{{url('admin/plugin',$plugin->id)}}" method="POST">
										                    @csrf
										                    <input type='hidden' name='_method' value='DELETE'>
										                    <button class="btn btn-danger btn-block">
										                        SI
										                    </button>
										                </form>
								      				</div>
								      			</div>
								    		</div>
								  		</div>
									</div>
								</div>
			    			</div>
			    			<div class="col text-right">
			    				@if($plugin->pivot->estado == "instalado")
			    					<a href="{{url('admin/plugin/activar', $plugin->id)}}" class="btn btn-info">
				    					<i class="fa fa-check"></i>
				    					Activar
				    				</a>
			    				@elseif($plugin->pivot->estado == "activo")
			    					<a href="{{url('admin/plugin/desactivar', $plugin->id)}}" class="btn btn-primary">
				    					<i class="fa fa-times"></i>
				    					Desactivar
				    				</a>
			    				@endif
			    			</div>
			    		</div>
			  		</div>
				</div>
			</div>
		@endforeach
	</div>
@endif