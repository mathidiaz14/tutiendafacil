@extends('layouts.dashboard', ['menu_activo' => 'plugin', 'menu_superior' => 'administrar'])

@section('contenido')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">
            	<i class="fa fa-plug"></i>
            	Plugins
            </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Plugins</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    	
      	<div class="container-fluid">
	        <div class="row">
	        	<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">
										Plugins instalados
									</h5>
								</div>
								<div class="card-body">
									@include('admin.plugin.instalados')
								</div>
							</div>
						</div>
						<div class="col-12">
							<div class="card">
								<div class="card-header">
									<h5 class="card-title">
										Tienda de Plugins
									</h5>
								</div>
								<div class="card-body">
									@include('admin.plugin.tienda')
								</div>
							</div>
						</div>
	        </div>
      	</div>
    </section>
  </div>
@endsection

@section('scripts')
	
@endsection