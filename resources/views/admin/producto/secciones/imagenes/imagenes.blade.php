@if($imagenes->count() == 0)
	@include('ayuda.sin_registros')
@else
	<link rel="stylesheet" href="{{asset('css/baguetteBox.css')}}">
	<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">
	<div class="row tz-gallery p-0">
		<div class="col-12">
			<div class="table table-responsive">
				<table class="table table-striped">
					<tbody id="sortable">
						@foreach($imagenes->sortBy('numero') as $imagen)
							<tr class="li_producto imagen_producto_biblioteca" id="imagen_producto_{{$imagen->id}}" attr-id="{{$imagen->id}}">	
								<td class="align-middle">
									<i class="fa fa-bars text-secondary" style="cursor:pointer;"></i>
								</td>
								<td class="align-middle">
									<input type="checkbox" class="imagen_check" attr-id="{{$imagen->id}}">
								</td>
								<td width="100px">
									@if($imagen->url_ml == null)
										<a href="{{asset($imagen->url)}}" class="lightbox mb-0">
											<img 	src="{{asset($imagen->url)}}" 
													alt="{{$imagen->alternativo}}" 
													class="imagen_producto" 
													onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'"  	 
											>
										</a>
									@else
										<a href="{{$imagen->url_ml}}" class="lightbox mb-0">
											<img 	src="{{$imagen->url_ml}}" 
												alt="{{$imagen->alternativo}}" 
												class="imagen_producto" 
												onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'"  	 
											>
										</a>
									@endif
								</td>
								<td>
									<small>{{$imagen->nombre}}</small>
								</td>
								<td>
									<a class="btn btn-info" href="#" data-toggle="modal" data-target="#editModal_{{$imagen->id}}">
										<i class="fa fa-edit"></i>
									</a>
								</td>
								<td>
							      	<a class="btn btn-danger" href="#" data-toggle="modal" data-target="#deleteModal_{{$imagen->id}}">
							      		<i class="fa fa-trash"></i>
							      	</a>
								</td>
							</tr>

							<div class="modal fade" id="editModal_{{$imagen->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
								<div class="modal-dialog modal-dialog-centered text-left" role="document">
							  		<div class="modal-content">
										<div class="modal-header">
											<div class="col">
												<h5 class="modal-title">Detalles de imagen</h5>
											</div>
											<div class="col">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times</span>
												</button>
											</div>
							    		</div>
							    		<div class="modal-body">
							    			<div class="row">
							    				<div class="col-12">
							    					<div class="row">
							    						<div class="col-12 mt-2">
							    							<p>
							    								URL: <small><a href="{{asset($imagen->url)}}" target="_blank">{{$imagen->url}}</a></small> <br>
									    						Subido el: <b>{{$imagen->created_at->format('d/m/Y H:i')}}</b> <br>
										    					Subido por: <b>{{$imagen->user_id != null ? $imagen->usuario->nombre : "--"}}</b> <br>
										    					Tipo: <b>{{$imagen->tipo != null ? $imagen->tipo : "--"}}</b> <br>
										    					Tamaño: <b>{{$imagen->tamaño != null ? $imagen->tamaño : "--"}}</b> <br>
									    					</p>
							    						</div>
							    						<div class="col-12">
							    							<hr>		
							    						</div>
							    						<div class="col-12">
							    							<div class="form-group">
									    						<label for="">Nombre del archivo</label>
							    								<input type="text" class="form-control" value="{{$imagen->nombre}}" id="nombre_{{$imagen->id}}">
									    					</div>	
									    					<div class="form-grouop">
									    						<label for="">Texto alternativo</label>
									    						<input type="text" class="form-control" id="alternativo_{{$imagen->id}}" value="{{$imagen->alternativo}}">
									    					</div>	
									    					<div class="form-group mt-2">
									    						<label for="">Descripción</label>
									    						<textarea id="descripcion_{{$imagen->id}}" id="" cols="30" rows="3" class="form-control">{{$imagen->descripcion}}</textarea>
									    					</div>
									    					<div class="form-group text-right">
									    						<button class="btn btn-info btn_editar_imagen" attr-id="{{$imagen->id}}">
									    							<i class="fa fa-save mr-3"></i>
									    							Guardar
									    						</button>
									    					</div>
							    						</div>
							    					</div>
							    				</div>
							    			</div>
							    		</div>
							  		</div>
								</div>
							</div>

							<div class="modal fade deleteModal" id="deleteModal_{{$imagen->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
								<div class="modal-dialog modal-dialog-centered" role="document">
									<div class="modal-content">
										<div class="modal-header bg-gradient-danger">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body text-center">
											<i class="fa fa-exclamation-triangle fa-4x text-secondary"></i>
											<br>
											<br>
											<h4>¿Desea eliminar el item?</h4>
											<hr>
											<div class="row">
												<div class="col">
													<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
														NO
													</button>
												</div>
												<div class="col">
													<a class="btn btn-danger btn-block text-white btn_eliminar_imagen"  attr-id="{{$imagen->id}}">
														SI
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach	
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-12 text-right">
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarVarios">
				<i class="fa fa-trash mr-3"></i>
				Eliminar varios
			</button>

			<div class="modal fade deleteModal" id="eliminarVarios" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header bg-gradient-danger">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body text-center">
							<i class="fa fa-exclamation-triangle fa-4x text-secondary"></i>
							<br>
							<br>
							<h4>¿Desea eliminar el item?</h4>
							<hr>
							<div class="row">
								<div class="col">
									<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
										NO
									</button>
								</div>
								<div class="col">
									<a class="btn btn-danger btn-block text-white btn_eliminar_imagen_varios">
										SI
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>

		var $ = jQuery.noConflict();
		$(document).ready(function()
		{
			baguetteBox.run('.tz-gallery');
			
			$( "#sortable" ).sortable({
				opacity: 0.5,
				stop: function()
				{
					var array = [];

					$('.li_producto').each(function(index, elemento)
					{
						array.push([$(this).attr('attr-id'), index]);
					})

					$.ajax({
			            url: "{{url('admin/multimedia/mover/imagen')}}",
						type: 'POST',
			            dataType: "JSON",
			            data: {
			                "_token": $('meta[name="csrf-token"]').attr('content'),
			                "array": array,
			                "producto": "{{$producto->id}}",
			            }
			        });
				}
			});

			$('.btn_eliminar_imagen').click(function()
			{	
				var id 			= $(this).attr("attr-id");

				$.ajax({
		            url: "{{url('admin/multimedia')}}/"+id,
					type: 'post',
		            dataType: "JSON",
		            data: {
		                "_token": $('meta[name="csrf-token"]').attr('content'),
		            	"_method": 'DELETE',
		            },success: function (data) 
		            {
		            	 jQuery.noConflict();
		            	$('#deleteModal_'+id).modal('toggle');
			    		$('#imagenes_producto').load("{{url('admin/multimedia')}}/{{$producto->id}}");
			    		$('#variantes_lista').load("{{url('admin/variante')}}/{{$producto->id}}");
			    		ttf_notificacion('La imagen se elimino correctamente', 'exito');
		            }
		        });
			});

			$('.btn_editar_imagen').click(function()
			{
				var id 			= $(this).attr("attr-id");
				var nombre 		= $('#nombre_'+id).val();
				var alternativo = $('#alternativo_'+id).val();
				var descripcion = $('#descripcion_'+id).val();

				$.ajax({
		            url: "{{url('admin/multimedia')}}/"+id,
					type: 'post',
		            dataType: "JSON",
		            data: {
		                "_token": $('meta[name="csrf-token"]').attr('content'),
		            	"_method": 'PATCH',
		                "nombre": nombre,
		                "alternativo": alternativo,
		                "descripcion": descripcion,
		            },success: function (data) 
		            {
		            	$('#editModal_'+id).modal('toggle');
			    		$('#imagenes_producto').load("{{url('admin/multimedia')}}/{{$producto->id}}");
			    		ttf_notificacion('La imagen se edito correctamente', 'exito');
		            }
		        }); 
			});

			$('.btn_eliminar_imagen_varios').click(function()
			{
				$('.imagen_check').each(function(index, elemento)
				{
					if($(this).is(':checked'))
					{
						var id 			= $(this).attr("attr-id");

						$.ajax({
				            url: "{{url('admin/multimedia')}}/"+id,
							type: 'post',
				            dataType: "JSON",
				            data: {
				                "_token": $('meta[name="csrf-token"]').attr('content'),
				            	"_method": 'DELETE',
				            },success: function (data) 
				            {
				            	$('#eliminarVarios').modal('toggle');
					    		$('#imagenes_producto').load("{{url('admin/multimedia')}}/{{$producto->id}}");
					    		$('#variantes_lista').load("{{url('admin/variante')}}/{{$producto->id}}");
					    		ttf_notificacion('Las imagenes se eliminaron correctamente', 'exito');
				            }
				        });
					}
				});
			});
		})
	</script>
@endif