<div class="modal fade" id="agregarImagenes" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form action="{{url('admin/imagen/producto')}}" method="post" class="dropzone" id="cargaDeImagenes"  enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="producto" value="{{$producto->id}}">
			</form>
		</div>
	</div>
</div>