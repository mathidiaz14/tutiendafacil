<link rel="stylesheet" href="{{asset('css/baguetteBox.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">

<script src="{{asset('js/baguetteBox.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>

<div class="row">
	<div class="col-12 text-right">
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarImagenes">
			<i class="fa fa-image mr-3"></i>
			Agregar imágenes
		</button>

		<div class="modal fade deleteModal" id="agregarImagenes" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header bg-gradient-info">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body text-center">
						<form action="{{url('admin/multimedia')}}" method="post" class="dropzone" id="cargaDeImagenes"  enctype="multipart/form-data">
							@csrf
							<input type="hidden" name="producto" value="{{$producto->id}}">
						</form>
					</div>
				</div>
			</div>
		</div>
	<hr>
	</div>
	<div class="col-12">
		<div id="imagenes_producto">
			@include('admin.producto.secciones.imagenes.imagenes', ['imagenes' => $producto->imagenes])
		</div>
	</div>
</div>

<script>

	$(document).ready(function()
	{
		$('#cargaDeImagenes').dropzone({ 
		    dictDefaultMessage: "Haz click aquí o arrastra las imágenes para cargarlas",
		    maxFilesize: 30, 
		    acceptedFiles: ".jpeg,.jpg,.png,.gif",

		    success: function () 
		    {
		    	$('#imagenes_producto').load("{{url('admin/multimedia')}}/{{$producto->id}}");
		    }
		});
	});
</script>