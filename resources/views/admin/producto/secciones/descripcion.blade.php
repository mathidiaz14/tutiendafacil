<div class="row">
	<div class="col-12">
		<form action="{{url('admin/producto', $producto->id)}}" class="form-horizontal" method="post" id="formProducto">
			@csrf
			@method('PATCH')
			<input type="hidden" name="continuar" id="continuar">

			<div class="row">
				<div class="col-6 col-md-2">
					<div class="form-group">
						<label for="">SKU</label>
						<input type="text" name="sku" id="sku" class="form-control" value="{{$producto->sku}}" placeholder="Codigo SKU">
					</div>
				</div>
				<div class="col-6 col-md-5">
					<div class="form-group">
						<label for="">Nombre</label>
						<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre del producto" value="{{$producto->nombre}}" required="">
					</div>		
				</div>
				<div class="col-10 col-md-4">
					<div class="form-group">
						<label for="">URL</label>
						<input type="text" class="form-control" id="url2" value="{{Auth::user()->empresa->URL}}/{{$producto->url}}" readonly="">
						<input type="hidden" name="url" id="url" value="{{$producto->url}}">
					</div>		
				</div>
				<div class="col-2 col-md-1">
					<label for="">&nbsp; </label>
					<a href="http://{{empresa()->URL}}/{{$producto->url}}" class="btn btn-info btn-block" target="_blank">
						<i class="fa fa-link"></i>
					</a>
				</div>
				<div class="col-12">
					<div class="form-group">
						<label for="">Cuéntanos mas sobre el producto</label>
						<textarea name="descripcion" id="editor" rows="10" class="summernote form-control" placeholder="Escribe aquí una descripción">{{$producto->descripcion}}</textarea>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Categoría</label>
					<br>
					<button type="button" class="btn btn-dark btn-block" data-toggle="modal" data-target="#agregarCategoria">
						<i class="fa fa-boxes mr-3"></i>
						Agregar categoría
					</button>

					<div class="modal fade deleteModal" id="agregarCategoria" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header bg-gradient-dark">
									<div class="col-10">
										<h5>Categorias del producto</h5>
									</div>
									<div class="col-2">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-12">
											@foreach(empresa()->categorias as $categoria)
												<div class="row">
													<div class="col-4 text-right">
														@if($producto->categorias()->get()->contains($categoria->id))
															@include('ayuda.switch', ['nombre' => 'categoria_'.$categoria->id, 'estado' => 'on'])
														@else
															@include('ayuda.switch', ['nombre' => 'categoria_'.$categoria->id, 'estado' => null])
														@endif
													</div>
													<div class="col-8">
														<label for="categoria_{{$categoria->id}}">
															{{$categoria->titulo}}
														</label>
														
													</div>
												</div>
											@endforeach
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<div class="row">
										<div class="col">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">
												<i class="fa fa-times mr-3"></i>
												Cerrar
											</button>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Etiquetas</label>
					<input type="text" value="@foreach($producto->tags as $tag){{$tag->tag}},@endforeach" name="tags" id="tags" data-role="tagsinput">
				</div>	
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Guía de talles</label>
					<br>
					<button type="button" class="btn btn-dark btn-block" data-toggle="modal" data-target="#tablaTalles">
						<i class="fas fa-shoe-prints mr-3"></i>
						Guía de talles
					</button>

					<div class="modal fade deleteModal" id="tablaTalles" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
						<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
							<div class="modal-content">
								<div class="modal-header bg-gradient-dark">
									<div class="col-10">
										<h5>Guía de talles</h5>
									</div>
									<div class="col-2">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-12">
											<textarea name="talles" id="" cols="30" rows="10" class="summernote">{{$producto->talles}}</textarea>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<div class="row">
										<div class="col">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">
												<i class="fa fa-times mr-3"></i>
												Cerrar
											</button>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Proveedor</label>
					<select name="proveedor" class="form-control">
						@if($producto->proveedor_id == null)
						<option value="" selected="">Ninguno</option>
						@else
						<option value="">Ninguno</option>
						@endif

						@foreach(Auth::user()->empresa->proveedores as $proveedor)
						@if($proveedor->id == $producto->proveedor_id)
						<option value="{{$proveedor->id}}" selected="">{{$proveedor->nombre}}</option>
						@else
						<option value="{{$proveedor->id}}">{{$proveedor->nombre}}</option>
						@endif
						@endforeach
					</select>	
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Precio general</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text">$</div>
						</div>
						<input type="number" class="form-control" name="precio" placeholder="Precio de venta" value="{{$producto->precio}}" required="">
					</div>
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Precio descuento</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text">$</div>
						</div>
						<input type="number" class="form-control" name="precio_promocion" placeholder="Precio de venta" value="{{$producto->precio_promocion}}">
					</div>
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Costo de fabricación</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<div class="input-group-text">$</div>
						</div>
						<input type="number" class="form-control" id="costo" name="costo" placeholder="Costo del producto" value="{{$producto->costo}}">
					</div>
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Stock del producto</label>
					<button type="button" class="btn btn-xs btn-secondary" data-container="body" data-toggle="popover" data-placement="right" data-content="Utiliza esta opción solamente si no vas a agregar variantes de stock, ya que si tienes variantes este campo no se utilizara.">
						?
					</button>
					<div class="input-group">
						<input type="number" class="form-control" name="cantidad" placeholder="Cantidad" value="{{$producto->cantidad}}">
					</div>
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Stock mínimo del producto</label>
					<div class="input-group">
						<input type="number" class="form-control" name="minimo_producto" placeholder="Cantidad" value="{{$producto->minimo_producto}}">
					</div>
				</div>	
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">¿Marcar producto como nuevo?</label>
					<div class="onoffswitch">
						<input type="checkbox" name="nuevo" class="onoffswitch-checkbox" id="producto_nuevo" tabindex="0" @if($producto->nuevo == "on") checked @endif>
						<label class="onoffswitch-label" for="producto_nuevo"></label>
					</div>
				</div>	
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">¿Marcar producto como destacado?</label>
					<div class="onoffswitch">
						<input type="checkbox" name="destacado" class="onoffswitch-checkbox" id="producto_destacado" tabindex="0" @if($producto->destacado == "on") checked @endif>
						<label class="onoffswitch-label" for="producto_destacado"></label>
					</div>
				</div>
				<div class="form-group col-6 col-md-4 col-lg-3">
					<label for="">Condición del producto</label>
					<select name="condicion" id="" class="form-control">
						@if(($producto->condition == "new") or ($producto->condition == null))
							<option value="new" selected>Nuevo</option>
							<option value="used">Usado</option>
						@else
							<option value="new">Nuevo</option>
							<option value="used" selected>Usado</option>
						@endif
					</select>
				</div>
				@if($producto->mlu_id != null)
					<div class="form-group col-6 col-md-4 col-lg-3">
						<label for="">Atributos MercadoLibre</label>
						<br>
						<button type="button" class="btn btn-dark btn-block" data-toggle="modal" data-target="#atributosML">
							<i class="fa fa-list"></i>
							Atributos
						</button>

						<div class="modal fade deleteModal" id="atributosML" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
							<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-header bg-gradient-dark">
										<div class="col-10">
											<h5>Atributos MercadoLibre</h5>
										</div>
										<div class="col-2">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-12">
												<div class="table table-responsive">
													<table class="table table-striped">
														<tr>
															<th>ID</th>
															<th>Nombre</th>
															<th>Valor ID</th>
															<th>Valor Nombre</th>
															<th>Valor Tipo</th>
														</tr>
														@foreach($producto->atributos_ml as $atributo)
															<tr>
																<td>{{$atributo->mlu_id}}</td>
																<td>{{$atributo->nombre}}</td>
																<td>{{$atributo->valor_id}}</td>
																<td>{{$atributo->valor_nombre}}</td>
																<td>{{$atributo->valor_tipo}}</td>
															</tr>
														@endforeach
													</table>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<div class="row">
											<div class="col">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">
													<i class="fa fa-times mr-3"></i>
													Cerrar
												</button>	
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endif
			</div>
			<hr>
			<div class="row">
				<div class="col-12 text-right">
					<div class="form-group">
						<button class="btn btn-info">
							<i class="fa fa-chevron-left"></i>
							Guardar y salir
						</button>
						<a class="btn btn-secondary btn_continuar text-white">
							<i class="fa fa-save"></i>
							Guardar
						</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>