<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarValorAtributo_{{$atributo->id}}">
	<i class="fa fa-plus"></i>
</button>

<!-- Modal -->
<div class="modal fade agregarValorAtributo" id="agregarValorAtributo_{{$atributo->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content text-dark">
			<div class="modal-header bg-gradient-info">
				<div class="col-10 text-left">
					<i class="fa fa-plus mr-3"></i>
					Agregar valor
				</div>
				<div class="col-2">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="modal-body text-left" id="guardar_valor_{{$atributo->id}}">
				@if($atributo->color != "on")
					<div class="row">
						<div class="col-4">
							<label for="">
								Valor
							</label>
						</div>
						<div class="col-8">
							<input type="text" class="form-control" id="valor_nombre_{{$atributo->id}}" placeholder="Valor">
						</div>
					</div>
				@else
					<div class="row mt-2">
						<div class="col-6">
							<label for="">
								Color 1
							</label>
							<input type="color" class="form-control" id="valor_color1_{{$atributo->id}}">
						</div>
						<div class="col-6">
							<label for="">
								Color 2
							</label>
							<input type="color" class="form-control" id="valor_color2_{{$atributo->id}}">
						</div>
					</div>	
				@endif
				<div class="row mt-2">
					<div class="col-12">
						<hr>
						<div class="form-group text-right">
							<a class="btn btn-info text-white btn_guardar_valor_{{$atributo->id}}">
								<i class="fa fa-save mr-3"></i>
								Guardar
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function()
	{
		$('.btn_guardar_valor_{{$atributo->id}}').click(function()
		{
			var nombre 				= $('#valor_nombre_{{$atributo->id}}').val();
			var color1 				= null;
			var color2 				= null;
			var es_color 			= null;
			
			@if($atributo->color == "on")
				es_color = true;
			@endif

			if((nombre == "") && (!es_color))
			{
				ttf_notificacion('El campo nombre no puede estar vacio', 'error');
			}else
			{
				@if($atributo->color == "on")
					color1 				= $('#valor_color1_{{$atributo->id}}').val();
					color2 				= $('#valor_color2_{{$atributo->id}}').val();
				@endif
				
				$('.cargando').fadeIn();
				
				$.ajax({
		            url: "{{url('admin/atributoValor')}}",
		            dataType: "JSON",
		            method: "POST",
		            data: {
		                "_token"			: $('meta[name="csrf-token"]').attr('content'),
		                "atributo"			: '{{$atributo->id}}',
		                "nombre"			: nombre,	
		                "color1"			: color1,	
		                "color2"			: color2,
		            },success: function (data) 
		            {
		            	$('#valor_atributo_{{$atributo->id}}').load("{{url('admin/atributoValor', $atributo->id)}}");
		            }
		        }).done(function()
		        {
		        	$('#agregarValorAtributo_{{$atributo->id}}').modal('toggle');
		        	$('#valor_nombre_{{$atributo->id}}').val("");
		        	$('#valor_color1_{{$atributo->id}}').val("");
		        	$('#valor_color2_{{$atributo->id}}').val("");

		        	$('.cargando').fadeOut();
		        });
			}
		});

		$('#agregarValorAtributo_{{$atributo->id}}').on('shown.bs.modal', function () {
		  	$('#valor_nombre_{{$atributo->id}}').trigger('focus');
		});
	})
</script>	