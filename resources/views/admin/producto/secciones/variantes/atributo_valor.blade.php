@if($atributo->valores->count() == 0)
	<small class="text-secondary">Aún no hay ningun valor</small>
@endif

@foreach($atributo->valores as $valor)
	<h2 class="badge badge-dark" style="background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%);; min-width: 4em;">
		{{$valor->nombre != null ? $valor->nombre : "-"}}
	</h2>
	<i class="fa fa-times fa-sm text-danger mr-2 btn_eliminar_valor" attr-id="{{$valor->id}}" attr-atributo="{{$atributo->id}}" style="cursor: pointer;"></i>
@endforeach

<script>
	$('.btn_eliminar_valor').on('click', function()
	{
		var id 			= $(this).attr('attr-id');
		var atributo 	= $(this).attr('attr-atributo');

		$('.cargando').fadeIn();

		$.ajax({
            url: "{{url('admin/atributoValor')}}/"+id,
            dataType: "JSON",
            method: "POST",
            data: {
                "_token"			: $('meta[name="csrf-token"]').attr('content'),
                "_method"			: "DELETE",
            },success: function (data) 
            {
            	$('#valor_atributo_'+atributo).load("{{url('admin/atributoValor')}}/"+atributo);
            	$('#variantes_lista').load("{{url('admin/variante')}}/{{$atributo->producto->id}}");
            }
        }).done(function()
        {
        	$('.cargando').fadeOut();
        });
	})
</script>