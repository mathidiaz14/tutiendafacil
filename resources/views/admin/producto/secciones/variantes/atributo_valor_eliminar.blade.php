<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal_{{$atributo->id}}">
	<i class="fa fa-trash"></i>
</button>

<div class="modal fade deleteModal" id="deleteModal_{{$atributo->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gradient-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<i class="fa fa-exclamation-triangle fa-4x text-secondary"></i>
				<br>
				<br>
				<h4>¿Desea eliminar el atributo y los valores?</h4>
				<hr>
				<div class="row">
					<div class="col">
						<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
							NO
						</button>
					</div>
					<div class="col">
						<a class="btn btn-danger btn-block text-white btn_eliminar_atributo_{{$atributo->id}}" style="cursor:pointer;" >
							SI
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function()
	{
		$('.btn_eliminar_atributo_{{$atributo->id}}').click(function()
		{	
			$('.cargando').fadeIn();

			$.ajax({
	            url: "{{url('admin/atributo', $atributo->id)}}",
	            dataType: "JSON",
	            method: "POST",
	            data: {
	                "_token"			: $('meta[name="csrf-token"]').attr('content'),
	                "_method"			: "DELETE",
	            },success: function (data) 
	            {
	            	$('#deleteModal_{{$atributo->id}}').modal('toggle');
	            	$('#atributos_productos').load("{{url('admin/atributo')}}/{{$producto->id}}");
	            	$('#variantes_lista').load("{{url('admin/variante')}}/{{$producto->id}}");
	            }
	        }).done(function()
	        {
	        	$('.cargando').fadeOut();
	        });	
		});
	});
</script>