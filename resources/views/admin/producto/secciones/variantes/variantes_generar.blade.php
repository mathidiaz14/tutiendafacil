<button type="button" class="btn btn-dark" data-toggle="modal" data-target="#limpiarVariantes">
	<i class="fa fa-sitemap mr-3"></i>
	Generar variantes
</button>

<!-- Modal -->
<div class="modal fade deleteModal" id="limpiarVariantes" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-dark">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<i class="fa fa-exclamation-triangle fa-4x text-secondary"></i>
				<br>
				<br>
				<h4>¿Desea generar las variantes?</h4>
				<hr>
				<div class="row">
					<div class="col">
						<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
							NO
						</button>
					</div>
					<div class="col">
						<form action="{{url('admin/variante')}}" method="post">
							@csrf
							<input type="hidden" name="producto" value="{{$producto->id}}">
							<button class="btn btn-dark btn-block">
								SI
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>