@if($producto->variantes->count() > 0)
<div class="card card-dark">
	<div class="card-header">
		<div class="row">
			<div class="col-6">
				<h5 class="modal-title">Lista de variantes</h5>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12">
				<div class="table table-responsive">
					<table class="table">
						<tr>
							<th></th>
							<th>Variante</th>
							<th>SKU</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th></th>
							<th>Imagen</th>
							<th>Guardar</th>
							<th>Eliminar</th>
						</tr>
						@foreach($producto->variantes->where('estado', '!=', 'no') as $variante)
						<tr id="tr_variante_{{$variante->id}}" class="tr_variante" attr-id="{{$variante->id}}">
							<td>
								#{{$variante->id}}
							</td>
							<td>
								@foreach($variante->valores as $valor)
								<h2 class="badge badge-dark mb-0" style="width:100%; background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%);">
									{{$valor->nombre != null ? $valor->nombre : "-"}}
								</h2>
								@endforeach
							</td>
							<td>
								<input type="text" class="form-control" id="variante_sku_{{$variante->id}}" placeholder="SKU" value="{{$variante->sku}}">
							</td>
							<td>
								<input type="text" class="form-control" id="variante_cantidad_{{$variante->id}}" placeholder="Cantidad" value="{{$variante->cantidad}}">
							</td>
							<td>
								<input type="text" class="form-control" id="variante_precio_{{$variante->id}}" placeholder="Precio" value="{{$variante->precio}}">
							</td>
							<td>
								<img src="{{asset($variante->imagen)}}" id="variante_imagen_muestra_{{$variante->id}}" alt="" width="80px">
							</td>
							<td>
								@include('admin.producto.secciones.variantes.variante_seleccionar_imagen', ['id' => $variante->id])
							</td>
							<td>
								<a class="btn btn-info btn-block text-white btn_guardar_variante" attr-id="{{$variante->id}}">
									<i class="fa fa-save"></i>
								</a>
							</td>
							<td>
								<button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#deleteModalVariante_{{$variante->id}}">
									<i class="fa fa-trash"></i>
								</button>

								<div class="modal fade deleteModal" id="deleteModalVariante_{{$variante->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header bg-gradient-danger">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body text-center">
												<i class="fa fa-exclamation-triangle fa-4x text-secondary"></i>
												<br>
												<br>
												<h4>¿Desea eliminar la variante?</h4>
												<hr>
												<div class="row">
													<div class="col">
														<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
															NO
														</button>
													</div>
													<div class="col">
														<a class="btn btn-block btn-danger text-white btn_eliminar_variante" attr-id="{{$variante->id}}">
															SI
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>		
						@endforeach
					</table>
				</div>
			</div>
		</div>	
	</div>
	<div class="card-footer">
		<div class="row">
			<div class="col-12 text-right">
				<a class="btn btn-info btn_guardar_todos text-white">
					<i class="fa fa-save"></i>
					Guardar todos
				</a>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function()
	{
		$('.btn_guardar_todos').click(function()
		{
			$('.cargando').fadeIn();

			$('.tr_variante').each(function(index, elemento)
			{
				var id 			= $(this).attr('attr-id');
				var sku 		= $('#variante_sku_'+id).val();
				var cantidad 	= $('#variante_cantidad_'+id).val();
				var precio 		= $('#variante_precio_'+id).val();
				var imagen 		= $('#variante_imagen_'+id).val();

				$.ajax({
					url: "{{url('admin/variante')}}/"+id,
					dataType: "JSON",
					method: "POST",
					data: {
						"_token"			: $('meta[name="csrf-token"]').attr('content'),
						"_method"			: 'PATCH',
						"sku"				: sku,
						"cantidad"			: cantidad,
						"precio"			: precio,
						"imagen"			: imagen
					}
				});
			});

			ttf_notificacion('Se guardaron todas las variantes', 'exito');
			$('.cargando').fadeOut();
		});

		$('.btn_guardar_variante').click(function()
		{
			var id 			= $(this).attr('attr-id');
			var sku 		= $('#variante_sku_'+id).val();
			var cantidad 	= $('#variante_cantidad_'+id).val();
			var precio 		= $('#variante_precio_'+id).val();
			var imagen 		= $('#variante_imagen_'+id).val();

			$('.cargando').fadeIn();

			$.ajax({
				url: "{{url('admin/variante')}}/"+id,
				dataType: "JSON",
				method: "POST",
				data: {
					"_token"			: $('meta[name="csrf-token"]').attr('content'),
					"_method"			: 'PATCH',
					"sku"				: sku,
					"cantidad"			: cantidad,
					"precio"			: precio,
					"imagen"			: imagen
				}
			}).done(function()
			{
				ttf_notificacion('Se guardaron los datos de la variante #'+id, 'exito');
				$('.cargando').fadeOut();
			});
		});

		$('.btn_eliminar_variante').click(function()
		{
			var id 			= $(this).attr('attr-id');

			$('.cargando').fadeIn();

			$.ajax({
				url: "{{url('admin/variante')}}/"+id,
				dataType: "JSON",
				method: "POST",
				data: {
					"_token"			: $('meta[name="csrf-token"]').attr('content'),
					"_method"			: 'DELETE',
				},success: function (data) 
				{
					$('#deleteModalVariante_'+id).modal('toggle');
					$('.modal-backdrop').remove();
					$('#tr_variante_'+id).remove();
				}
			}).done(function()
			{
				ttf_notificacion('Se elimino la variante #'+id, 'exito');
				$('.cargando').fadeOut();
			});
		});
	});
</script>
@endif