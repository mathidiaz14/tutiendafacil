<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#seleccionar_imagen_variante{{$id}}">
	<i class="fa fa-image"></i>
</button>

<div class="modal fade" id="seleccionar_imagen_variante{{$id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="col-10">
					<h5 class="modal-title">Seleccionar imagen a la variante</h5>
				</div>
				<div class="col-2">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="modal-body">
				<div class="row">
					@foreach($producto->imagenes as $imagen)
						<div class="col-xs-6 col-md-3 col-lg-2">
							<img src="{{asset($imagen->url)}}" alt="" width="100%" class="imagen_biblioteca_variante m-1" id="imagen_{{$imagen->id}}_variante_{{$id}}" attr-id="{{$id}}" attr-imagen="{{$imagen->id}}" attr-url="{{$imagen->url}}" onclick="imagen_biblioteca_variante('#imagen_{{$imagen->id}}_variante_{{$id}}');">
						</div>
					@endforeach
				</div>
			</div>
			<div class="modal-footer">
				<div class="row">
					<div class="col-12 text-right">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							<i class="fa fa-times mr-3"></i>
							Cerrar
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="variante_imagen_{{$id}}">

<script>
	$('#seleccionar_imagen_variante{{$id}}').on('shown.bs.modal', function () {
		$('#seleccionar_imagen_variante{{$id}} > .imagen_biblioteca_variante').height($('.imagen_biblioteca_variante').width());
	});

	function imagen_biblioteca_variante(nombre)
	{

		var id 	= $(nombre).attr('attr-id');
		var url = $(nombre).attr('attr-url');
		var imagen = $(nombre).attr('attr-imagen');

		if($(nombre).hasClass('seleccionado'))
		{
			$(nombre).removeClass('seleccionado');

			$('#variante_imagen_'+id).val('');
			$('#variante_imagen_muestra_'+id).attr('src', '');
		}
		else
		{
			$('#seleccionar_imagen_variante'+id+' .imagen_biblioteca_variante').removeClass('seleccionado');
			
			$(nombre).addClass('seleccionado');

			$('#variante_imagen_'+id).val(url);
			$('#variante_imagen_muestra_'+id).attr('src', "{{asset('')}}/"+url);
		}
	}
</script>