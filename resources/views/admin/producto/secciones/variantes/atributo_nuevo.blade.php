<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarAtributo">
	<i class="fa fa-plus mr-3"></i>
	Agregar Atributo
</button>

<!-- Modal -->
<div class="modal fade" id="agregarAtributo" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content text-dark">
			<div class="modal-header bg-gradient-info">
				<div class="col-10 text-left">
					<i class="fa fa-plus mr-3"></i>
					Agregar Atributo
				</div>
				<div class="col-2">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
			<div class="modal-body text-left">
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<label for="">
								Nombre <small>(Talle, color, etc.)</small>
							</label>
							<input type="text" class="form-control" id="variante_nombre" placeholder="Nombre">
						</div>
						<div class="form-group">
							<div class="row mt-4">
								<div class="col-4">
									<label for="color">
										¿Es un color?
									</label>
								</div>
								<div class="col-8">
									@include('ayuda.switch', ['nombre' => 'color', 'estado' => null])
								</div>
							</div>
						</div>	
						<hr>
						<div class="form-group text-right">
							<a class="btn btn-info text-white btn_guardar_atributo">
								<i class="fa fa-save mr-3"></i>
								Guardar
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function()
	{
		$('#agregarAtributo').on('shown.bs.modal', function () {
		  	$('#variante_nombre').trigger('focus')
		})

		$('.btn_guardar_atributo').click(function()
		{
			var nombre 		= $('#variante_nombre').val();
			var producto 	= "{{$producto->id}}";
			
			var color 		= null;
			
			if($('#color').is(':checked'))
				var color 		= "on";

			if(nombre == "")
			{
				ttf_notificacion('El nombre del atributo es requerido', 'error');
			}else
			{
				$('.cargando').fadeIn();
				
				$.ajax({
		            url: "{{url('admin/atributo/')}}",
		            dataType: "JSON",
		            method: "POST",
		            data: {
		                "_token"	: $('meta[name="csrf-token"]').attr('content'),
		                "nombre"	: nombre,	
		                "producto"	: producto,
		                "color"		: color,
		            },success: function (data) 
		            {
		            	$('#atributos_productos').load("{{url('admin/atributo')}}/{{$producto->id}}");
		            }
		        }).done(function()
		        {
		        	$('#agregarAtributo').modal('toggle');
		        	$('#variante_nombre').val("");
		        	$('#variante_depende option[value="null"]').attr('selected', true);
		        	
		        	$('.cargando').fadeOut();
		        });
	       	}
		});
	});
</script>