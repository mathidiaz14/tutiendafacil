@if($producto->atributos->count() > 0)
	<div class="card card-dark">
		<div class="card-header">
			<div class="row">
				<div class="col">
					<h5 class="modal-title">Lista de atributos</h5>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					<div class="table table-responsive">
						<table class="table">
							@foreach($producto->atributos as $atributo)
								<tr>
									<td>
										<b>{{$atributo->nombre}}</b>
									</td>
									<td id="valor_atributo_{{$atributo->id}}">
										@include('admin.producto.secciones.variantes.atributo_valor', ['atributo' => $atributo])
									</td>
									<td>
										@include('admin.producto.secciones.variantes.atributo_valor_nuevo', ['atributo' => $atributo])
									</td>
									<td>
										@include('admin.producto.secciones.variantes.atributo_valor_eliminar', ['atributo' => $atributo])
									</td>
								</tr>			
							@endforeach
						</table>
					</div>	
				</div>
			</div>		
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-12 text-right">
					@include('admin.producto.secciones.variantes.variantes_generar')
				</div>
			</div>
		</div>
	</div>	
@else
	@include('ayuda.sin_registros')
@endif