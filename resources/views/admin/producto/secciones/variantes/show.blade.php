<div class="row">
	<div class="col-12 text-right">
		@include('admin.producto.secciones.variantes.atributo_nuevo')
		<hr>
	</div>
	<div class="col-12">
		<div id="atributos_productos">
			@include('admin.producto.secciones.variantes.atributo_lista')
		</div>
	</div>
	<div class="col-12">
		<div id="variantes_lista">
			@include('admin.producto.secciones.variantes.variantes_lista')
		</div>
	</div>
</div>