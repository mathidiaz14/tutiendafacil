@if($productos->count() == 0)
	@include('ayuda.sin_registros')
@else
	<div class="table table-responsive">
		<table class="table table-striped">
			<tr>
				<th>SKU</th>
				<th>Imagen</th>
				<th>Nombre</th>
				<th>Precio</th>
				
				@if(plugin_estado('MercadoLibre', 'activo'))
					<th>MercadoLibre</th>
				@endif
				
				<th>Stock</th>
				<th>Estado</th>
				<th>Editar</th>
				<th>Eliminiar</th>
			</tr>
			@foreach($productos as $producto)
				<tr>
					<td class="align-middle">
						{{$producto->sku != null ? $producto->sku : "--" }}
					</td>
					<td class="align-middle">
						<img src="{{producto_url_imagen($producto->id)}}" width="100px" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'">
					</td>
					<td class="align-middle">
						<a href="http://{{$producto->empresa->URL}}/{{$producto->url}}" target="_blank">
							<b>{{$producto->nombre}}</b>
						</a>
					</td>
					<td class="align-middle">
						${{producto_precio($producto->id)}}
					</td>

					@if(plugin_estado('MercadoLibre', 'activo'))
						<td class="align-middle">
							@if($producto->mlu_id != null)
								<a href="https://articulo.mercadolibre.com.uy/MLU-{{str_replace('MLU', '', $producto->mlu_id)}}" target="_blank">
									{{$producto->mlu_id}}
								</a>
							@else
								--
							@endif
						</td>
					@endif
					
					<td class="align-middle">
						{{producto_stock_cantidad($producto->id) != null ? producto_stock_cantidad($producto->id) : "--"}}
					</td>
					<td class="align-middle">
						@include('ayuda.switch', ['nombre' => $producto->id, 'estado' => $producto->estado == "borrador" ? "off" : "on"])
					</td>
					<td class="align-middle">
						<a href="{{route('producto.edit', $producto->id)}}" class="btn btn-dark">
							<i class="fa fa-edit"></i>
						</a>
					</td>
					<td class="align-middle">
						@include('ayuda.eliminar', ['id' => $producto->id, 'ruta' => url('admin/producto', $producto->id)])
					</td>
				</tr>
			@endforeach
		</table>
	</div>
	@include('ayuda.links', ['link' => $productos])
	
	<script>
		$('.onoffswitch-checkbox').change(function()
			{
				var id = $(this).attr('attr-id');

				$.get('{{url("admin/producto/estado")}}/'+id);
			})
	</script>
@endif