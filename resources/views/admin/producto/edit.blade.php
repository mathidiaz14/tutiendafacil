@extends('layouts.dashboard', ['menu_activo' => 'producto', 'menu_superior' => 'tienda'])

@section('css')
<link rel="stylesheet" href="{{asset('dashboard/bootstrap-tagsinput.css')}}">
@endsection

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">
						Producto #{{$producto->id}}
					</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item"><a href="{{url('/admin/producto')}}">Productos</a></li>
						<li class="breadcrumb-item active">Editar</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>

	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<ul class="nav nav-tabs" id="tab">
						<li class="nav-item">
							<a class="nav-link @if(($seccion == null) or ($seccion == 'detalles')) active @endif" href="#detalles">
								<b> <i class="fa fa-edit"></i> Contenido</b>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @if($seccion == 'imagenes') active @endif" href="#imagenes">
								<b><i class="fa fa-images"></i> Imágenes</b>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link @if($seccion == 'variantes') active @endif" href="#variantes">
								<b><i class="fa fa-sitemap"></i> Variantes</b>
							</a>
						</li>
					</ul>
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-12">
									<div class="tab-content" id="myTabContent">
										<div class="tab-pane fade @if(($seccion == null) or ($seccion == 'detalles')) show active @endif" id="detalles" role="tabpanel" aria-labelledby="home-tab">
											@include('admin.producto.secciones.descripcion')
										</div>
										<div class="tab-pane fade @if($seccion == 'imagenes') show active @endif" id="imagenes" role="tabpanel" aria-labelledby="profile-tab">
											@include('admin.producto.secciones.imagenes.show')
										</div>
										<div class="tab-pane fade @if($seccion == 'variantes') show active @endif" id="variantes" role="tabpanel" aria-labelledby="profile-tab">
											@include('admin.producto.secciones.variantes.show')
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')

<script src="{{asset('js/baguetteBox.js')}}"></script>
<script src="{{asset('dashboard/bootstrap-tagsinput.js')}}"></script>
<script>
	Dropzone.autoDiscover = false;
  	
	$(document).ready(function()
	{
		$('#tab a').on('click', function (e) {
			e.preventDefault();
			jQuery.noConflict();
			$(this).tab('show');
		});

		$(function () {
			jQuery.noConflict();
			$('[data-toggle="popover"]').popover();
		});
		$('.summernote').summernote({
		  	height: 200,   //set editable area's height
		  	codemirror: { // codemirror options
		  		theme: 'monokai'
		  	}
		  });

		$('#sku').change(function()
		{
			$('.sku_variable').html($(this).val()+" - ");
		});

		$('#nombre').keyup(function()
		{
			$('#url').val($(this).val().replace(/\s/g, "-").replace(/[^ a-z0-9áéíóúüñ]+/ig,"_"));
			$('#url2').val("{{Auth::user()->empresa->URL}}/"+$(this).val().replace(/\s/g, "-").replace(/[^ a-z0-9áéíóúüñ]+/ig,"_"));
		});

		$('.btn_continuar').click(function()
		{
			$('#continuar').val('si');
			$('#formProducto').submit();
		});
	});

</script>
@endsection