@extends('layouts.dashboard', ['menu_activo' => 'configuracion', 'menu_superior' => 'administrar'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-6">
					<h1 class="m-0 text-dark">Configuración</h1>
				</div><!-- /.col -->
				<div class="col-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Configuración</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<section class="content">
		<div class="container-fluid">
			
			<div class="card">
				<div class="card-body">
					<form action="{{url('admin/configuracion', configuracion()->id)}}" class="form-horizontal" method="post"  enctype="multipart/form-data">
						@csrf
						@method('PATCH')
						<div class="row">
							<div class="col-12">
								
								<div class="form-group">
									<label for="">Logo del sitio</label>
									<div class="row">
										<div class="col-12 col-md-2">
											@if($configuracion->logo != null)
												<img src="{{asset($configuracion->logo)}}" alt="" width="100px" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'">
											@else
												<img src="{{asset('img/default.jpg')}}" alt="" width="100px">
											@endif
										</div>
										<div class="col-12 col-md-10">
											<input type="file" name="logo" class="form-control-file">
											
											@if($configuracion->logo != null)
											<a href="{{url('admin/configuracion/eliminar/logo')}}" class="btn btn-danger mt-3 px-4">
												<i class="fa fa-trash"></i>
												Eliminar logo
											</a>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="">Título de la web</label>
									<input type="text" name="titulo" class="form-control" value="{{$configuracion->titulo}}" placeholder="Título de la web">
								</div>
								<div class="form-group">
									<label for="">Descripción corta</label>
									<input type="text" name="descripcion" class="form-control" value="{{$configuracion->descripcion}}" placeholder="Descripción">
								</div>
								<div class="row">
									<div class="col-12 col-md-6">
										<div class="form-group">
											<label for="">Email </label>
											<input type="text" name="email_admin" class="form-control" value="{{$configuracion->email_admin}}" required="">
										</div>
									</div>
									<div class="col-12 col-md-6">
										<div class="form-group">
											<label for="">Telefono </label>
											<input type="text" name="telefono" class="form-control" value="{{$configuracion->telefono}}" placeholder="Telefono">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label for="">¿El sitio web esta en construcción?</label>
									@include('ayuda.switch', ['nombre' => 'construccion', 'estado' => $configuracion->construccion])
								</div>
							</div>	
						</div>
						<hr>
						<div class="row text-right">
							<div class="col">
								<button class="btn btn-primary">
									<i class="fa fa-save"></i>
									Guardar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>	
</div>
@endsection

@section('scripts')

@endsection