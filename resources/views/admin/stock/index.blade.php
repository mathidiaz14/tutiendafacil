<div class="form-group">
	@if($variantes->count() == 0)
		<div class="col-12 text-center text-secondary">
			<i class="fa fa-random fa-3x"></i>
			<p>No hay ninguna variante</p>
		</div>
	@else
		<div class="table table-responsive">
			<table class="table table-striped">	
				<tr>
					<th>SKU</th>
					<th>Nombre</th>
					<th>Canidad</th>
					<th>Editar</th>
					<th>Eliminar</th>
				</tr>

				@foreach($variantes as $variante)
					<tr id="variante_{{$variante->id}}">
						<td id="sku">
							{{$variante->producto->sku}} - {{$variante->sku}}
						</td>
						<td id="nombre">
							{{$variante->nombre}}
						</td>
						<td id="cantidad">
							{{$variante->cantidad}}
						</td>
						<td>
							<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal_{{$variante->id}}">
								<i class="fa fa-edit"></i>
							</button>

							<!-- Modal -->
							<div class="modal fade" id="editModal_{{$variante->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
								<div class="modal-dialog modal-dialog-centered" role="document">
							  		<div class="modal-content">
										<div class="modal-header bg-gradient-info">
											<div class="col">
												<h5>Editar variante</h5>
											</div>
											<div class="col text-right">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
							    		</div>
							    		<div class="modal-body">
							    			<form action="{{url('admin/stock', $variante->id)}}"	 class="form-horizontal" method="post">
							    				@csrf
							    				@method('PATCH')
								    				<div class="form-group text-center" id="errorVariante_{{$variante->id}}" style="display:none;">
							    					<span class="alert alert-danger">
								    					Complete los campos obligatorios
								    				</span>
							    				</div>
							    				<div class="form-group">
							    					<label for="">SKU</label>
							    					<div class="row">
														<div class="col-2 text-right">
															<b class="sku_variable">{{$variante->producto->sku}} - </b>
														</div>
														<div class="col-10">
															<input class="form-control" name="sku" placeholder="Codigo unico" value="{{$variante->sku}}"/>
														</div>
													</div>		
							    				</div>
							    				<div class="form-group">
							    					<label for="">Nombre *</label>
							    					<input class="form-control" name="nombre" placeholder="Nombre variante" value="{{$variante->nombre}}" />
							    				</div>
							    				<div class="form-group">
							    					<label for="">Cantidad *</label>
							    					<input type="number" class="form-control" name="cantidad" placeholder="Cantidad" value="{{$variante->cantidad}}"/>
							    				</div>
							    				<hr>
							    				<div class="form-group text-right">
							    					<button class="btn btn-info">
							    						<i class="fa fa-save"></i>
														Guardar
							    					</button>
							    				</div>	
							    			</form>
							    		</div>
							  		</div>
								</div>
							</div>
						</td>
						<td>
							@include('ayuda.eliminar', ['id' => $variante->id, 'ruta' => url('admin/stock', $variante->id)])
						</td>
					</tr>
				@endforeach
			</table>
		</div>
	@endif
</div>
