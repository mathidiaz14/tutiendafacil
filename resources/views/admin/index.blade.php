@extends('layouts.dashboard', ['menu_activo' => 'inicio'])

@section('contenido')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      
      <div class="container-fluid">

        @if(empty(visual('tienda_dashboard')) or (visual('tienda_dashboard')->valor == "on"))
          @include('admin.widget')
          @include('admin.dashboard')
        @endif

        @foreach(empresa()->plugins as $plugin)
          @if($plugin->pivot->estado == "activo")
            @if(empty(visual($plugin->nombre.'_dashboard')) or (visual($plugin->nombre.'_dashboard')->valor == "on"))
              @include('Plugins.'.$plugin->carpeta.".dashboard")
            @endif
          @endif
        @endforeach

      </div>
    </section>
  </div>

@endsection
