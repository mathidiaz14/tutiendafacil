  @extends('layouts.dashboard', ['menu_activo' => 'pos'])

  @section('contenido')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">POS</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">POS</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-6">
                    <h5 class="card-title">
                      Punto de venta
                    </h5>
                  </div>
                  <div class="col-6 text-right">
                    
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-12 col-md-4 pr-2">
                    <div class="row carrito">
                      @include('admin.pos.partes.carrito')
                    </div>
                    <hr>
                    @include('admin.pos.partes.cliente')
                    @include('admin.pos.partes.limpiar_compra')
                    @include('admin.pos.partes.finalizar_compra')
                    <!-- @include('admin.pos.partes.calculadora') -->
                  </div>
                  <div class="col-12 col-md-8">
                    @include('admin.pos.partes.buscar_producto')
                    <hr>
                    @include('admin.pos.partes.categorias')
                    <hr>
                    <div class="row productos">
                      @include('admin.pos.partes.productos', ['productos' => empresa()->categorias->first()->productos, 'categoria' => empresa()->categorias->first()->titulo])
                    </div>
                  </div>
                </div>
            </div>
            <div class="card-footer"></div>
          </div>
        </div>
      </div>
    </section>
  </div>
  @endsection

  @section('scripts')
    <script>
      $(document).ready(function()
      {
        $('.productos_categorias img').height(200);

        $('#modal_agregar_cliente').on('shown.bs.modal', function () {
          $('#buscar_cliente').trigger('focus');
        });
        
        $('.btn_categoria').click(function()
        {
          var id = $(this).attr('attr-id');
          $('.productos').load("{{url('admin/pos')}}/"+id);
        });

        $('.btn_busqueda').click(function()
        {
          $('.productos').load("{{url('admin/pos/buscar')}}/"+$('#busqueda').val().replace(/\s/g, '-'));
        });

        $('#busqueda').keyup(function()
        {
          if($('#busqueda').val().length > 3)
            $('.productos').load("{{url('admin/pos/buscar')}}/"+$('#busqueda').val().replace(/\s/g, '-'));
          
        });

        $('.btn_agregar_producto').click(function()
        {
          $('.cargando').show();
          var producto = $(this).attr('attr-producto');
          var variante = $(this).attr('attr-variante');
          
          if(variante == "NULL")
            var cantidad = $('#cantidad_producto_'+producto).val();
          else
            var cantidad = $('#cantidad_variante_'+variante).val();

          $.ajax({
              url:      '{{url("admin/pos")}}',
              type:     'POST',
              dataType: 'JSON',
              data: {
                  "_token"    : "{{csrf_token()}}",
                  "producto"    : producto,
                  "variante"    : variante,
                  "cantidad"    : cantidad,
              },success: function (data) 
              {
                $('.carrito').load("{{url('admin/pos/carrito')}}");
                $('.producto_modal').modal('hide');
              }
          }).done(function(){
            $('.cargando').hide();
          });
        });

        $(document).on('click', '.btn_producto_menos', function()
        {
          var id        = $(this).attr('attr-id');
          var cantidad  = parseInt($('#tr_'+id+' > .td_cantidad').html());

          if (cantidad > 1)
            $('#tr_'+id+' > .td_cantidad').html(cantidad - 1);
        });

        $(document).on('click', '.btn_producto_mas', function()
        {
          var id        = $(this).attr('attr-id');
          var cantidad  = parseInt($('#tr_'+id+' > .td_cantidad').html());

          $('#tr_'+id+' > .td_cantidad').html(cantidad + 1);
        });

        $(document).on('click', '.btn_eliminar_producto_carrito', function()
        {
          var id        = $(this).attr('attr-id');
          $('.cargando').show();

          $.ajax({
              url:      '{{url("admin/pos")}}/'+id,
              type:     'POST',
              dataType: 'JSON',
              data: {
                  "_token"      : "{{csrf_token()}}",
                  "_method"     : "DELETE",
              },success: function (data) 
              {
                $('.carrito').load("{{url('admin/pos/carrito')}}");
              }
          }).done(function(){
            $('.cargando').hide();
          });
        });

        $('.btn_buscar_cliente').click(function()
        {
          var busqueda = $('#buscar_cliente').val();

          $.ajax({
              url:      '{{url("admin/pos/cliente")}}',
              type:     'POST',
              dataType: 'JSON',
              data: {
                  "_token"    : "{{csrf_token()}}",
                  "buscar"    : busqueda
              },success: function (data) 
              {
                $('#tabla_cliente').html(data);
              }
            });
          });

          $('#buscar_cliente').on('keypress', function(e)
          {
            if(e.which == 13)
                $('.btn_buscar_cliente').click();

          })

          $(document).on('click', '.btn_seleccionar_cliente', function()
          {
            var id        = $(this).attr('attr-id');
            
            console.log(id);

          });

          $('.btn_nuevo_cliente').click(function()
          {
            $('#modal_agregar_cliente').modal('hide');
            $('#modal_nuevo_cliente').modal('show');
          })
      });
    </script>
  @endsection