
<div class="col-12">
	<div class="row">
		@if($productos->count() == 0)
			<div class="col-12 text-center">
				@include('ayuda.sin_registros')
			</div>
		@else
			<div class="col-12 my-2">
				@if(isset($categoria))
					<b class="badge badge-dark">
						{{$categoria}}
					</b>
				@endif
			</div>
			@foreach($productos as $producto)
				<div class="col-4 col-md-2 mt-1">
					<a class="btn btn-secondary btn-block text-white text-left p-0 text-center btn_producto" onClick="$('#producto_{{$producto->id}}').modal('show')">
						@if($producto->imagenes->count() > 0)
							<img src="{{asset($producto->imagenes->first()->url)}}" alt="{{asset($producto->imagenes->first()->alternativo)}}" width="100%" height="150" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'" style="object-fit: cover;object-position: center;">
						@else
							<img src="{{asset("img/default.jpg")}}" width="100%" style="object-fit: cover;object-position: center;">
						@endif
						
						<small class="pt-2">
							{{$producto->nombre}}
						</small>
						
						<b class="badge badge-info" style="position: absolute;top: -5px;right: 0px;padding: 5px;">
							${{$producto->precio}}
						</b>
					</a>
					<div class="modal fade producto_modal" id="producto_{{$producto->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
						<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header bg-gradient-info">
									<div class="col-9">
										<h5 class="title-modal">{{$producto->nombre}}</h5>
									</div>
									<div class="col-3">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
								</div>
								<div class="modal-body text-center">
									@if($producto->variantes->count() > 0)	
										<div class="row">
											<div class="col-12">
												<label for="">Variantes</label>
											</div>
											<div class="col-12">
												<div class="table table-responsive">
													<table class="table table-striped">
														<tr>
															<th>SKU</th>
															<th>Variante</th>
															<th>Precio</th>
															<th>Disponible</th>
															<th>Cantidad</th>
															<th></th>
														</tr>
														@foreach($producto->variantes as $variante)
															<tr>
																<td>{{$variante->sku != null ? $variante->sku : "--"}}</td>
																<td>
																	@foreach($variante->valores as $valor)
																		<h2 id      = "valor_{{$valor->id}}"
																			class   = "badge badge-dark mr-2" 
																			attr-id = "{{$valor->id}}"
																			style   = "background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%); 
																						border:1px solid grey;
																						min-width: 4em;
																						cursor: pointer;">
																			{{$valor->nombre != null ? $valor->nombre : "-"}}
																		</h2>
																	@endforeach
																</td>
																<td>${{$variante->precio == null ? $producto->precio : $variante->precio}}</td>
																<td>{{$variante->cantidad}}</td>
																<td>
																	<div class="row">
																		<div class="col-2">
																			<a class="btn btn-info btn-block text-white" 
																				onClick="if($('#cantidad_variante_{{$variante->id}}').val() > $('#cantidad_variante_{{$variante->id}}').attr('min')) $('#cantidad_variante_{{$variante->id}}').val($('#cantidad_variante_{{$variante->id}}').val()-1)"
																				>
																				<i class="fa fa-minus"></i>
																			</a>
																		</div>
																		<div class="col-4">
																			<input type="number" class="form-control" id="cantidad_variante_{{$variante->id}}" value="1" max="{{$variante->cantidad}}" min="1"
																			@if(($variante->cantidad == 0) or ($variante->cantidad == null))
																				disabled
																			@endif
																			>
																		</div>
																		<div class="col-2">
																			<a class="btn btn-info btn-block text-white" 
																				onClick="if(parseInt($('#cantidad_variante_{{$variante->id}}').val()) < $('#cantidad_variante_{{$variante->id}}').attr('max')) $('#cantidad_variante_{{$variante->id}}').val(parseInt($('#cantidad_variante_{{$variante->id}}').val()) +1)"
																				>
																				<i class="fa fa-plus"></i>
																			</a>
																		</div>
																	</div>
																</td>
																<td>
																	@if(($variante->cantidad == 0) or ($variante->cantidad == null))
																		<a class="btn btn-info btn-block text-white disabled">
																			<i class="fa fa-plus"></i>
																		</a>
																	@else
																		<a class="btn btn-info btn-block text-white btn_agregar_producto" attr-producto="{{$producto->id}}" attr-variante="{{$variante->id}}">
																			<i class="fa fa-plus"></i>
																		</a>
																	@endif
																</td>
															</tr>
														@endforeach
													</table>
												</div>
											</div>
										</div>	
									@else
										<div class="row text-left">
											<div class="col-12">
												<div class="table table-responsive">
													<table class="table table-striped">
														<tr>
															<th>SKU</th>
															<th>Precio</th>
															<th>Disponible</th>
															<th>Cantidad</th>
															<th></th>
														</tr>
														<tr>
															<td>{{$producto->sku}}</td>
															<td>${{$producto->precio}}</td>
															<td>{{$producto->cantidad}}</td>
															<td>
																<div class="row">
																	<div class="col-2">
																		<a class="btn btn-info btn-block text-white" 
																			onClick="if($('#cantidad_producto_{{$producto->id}}').val() > $('#cantidad_producto_{{$producto->id}}').attr('min')) $('#cantidad_producto_{{$producto->id}}').val($('#cantidad_producto_{{$producto->id}}').val()-1)"
																			>
																			<i class="fa fa-minus"></i>
																		</a>
																	</div>
																	<div class="col-4">
																		<input type="number" class="form-control" id="cantidad_producto_{{$producto->id}}" value="1" max="{{$producto->cantidad}}" min="1"
																		@if(($producto->cantidad == 0) or ($producto->cantidad == null))
																			disabled
																		@endif
																		>
																	</div>
																	<div class="col-2">
																		<a class="btn btn-info btn-block text-white" 
																			onClick="if(parseInt($('#cantidad_producto_{{$producto->id}}').val()) < $('#cantidad_producto_{{$producto->id}}').attr('max')) $('#cantidad_producto_{{$producto->id}}').val(parseInt($('#cantidad_producto_{{$producto->id}}').val()) +1)"
																			>
																			<i class="fa fa-plus"></i>
																		</a>
																	</div>
																</div>
															</td>
															<td>
																@if(($producto->cantidad == 0) or ($producto->cantidad == null))
																	<a class="btn btn-info btn-block text-white disabled">
																		<i class="fa fa-plus"></i>
																	</a>
																@else
																	<a class="btn btn-info btn-block text-white btn_agregar_producto" attr-producto="{{$producto->id}}" attr-variante="NULL">
																		<i class="fa fa-plus"></i>
																	</a>
																@endif
															</td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		@endif
	</div>
</div>