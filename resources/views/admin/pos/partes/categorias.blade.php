<div class="row">
    @if($sin_categoria > 0)
        <div class="col-12 col-md-4 my-2">
            <a class="btn btn-secondary btn-block text-white text-left btn_categoria" attr-id="sin_categoria">
                <div class="row mt-2">
                    <div class="col-6">                    
                        <p>Sin categoria</p>
                    </div>
                    <div class="col-6 text-right">
                        <p class="badge badge-light">{{$sin_categoria}}</p>
                    </div>
                </div>    
            </a>
        </div>
    @endif

    @foreach($categorias as $categoria)
        <div class="col-12 col-md-4 my-2">
            <a class="btn btn-secondary btn-block text-white text-left btn_categoria" attr-id="{{$categoria->id}}">
                <div class="row mt-2">
                    <div class="col-6">                    
                        <p>{{$categoria->titulo}}</p>
                    </div>
                    <div class="col-6 text-right">
                        <p class="badge badge-light">{{$categoria->productos->count()}}</p>
                    </div>
                </div>    
            </a>
        </div>
    @endforeach
</div>