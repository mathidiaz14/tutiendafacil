<hr>
<div class="row">
    <div class="col-12 py-2">
        <input type="text" class="form-control text-center" readonly id="cantidad_productos">
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="7"><b>7</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="8"><b>8</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="9"><b>9</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="4"><b>4</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="5"><b>5</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="6"><b>6</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="1"><b>1</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="2"><b>2</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="3"><b>3</b></a>
    </div>
    <div class="col-4 p-1 text-white">
        <a class="btn btn-info btn-block btn-calculadora" attr-numero="0"><b>0</b></a>
    </div>
    <div class="col-8 p-1 text-white">
        <a class="btn btn-info btn-block btn-borrar-calculadora"><b>Limpiar</b></a>
    </div>
</div>

<script>
    $(document).ready(function()
    {
        $('.btn-calculadora').click(function()
        {
            var numero = $(this).attr('attr-numero');

            var cantidad = $('#cantidad_productos').val();

            $('#cantidad_productos').val(cantidad+numero);
        });

        $('.btn-borrar-calculadora').click(function()
        {
            $('#cantidad_productos').val("");
        });

    })
</script>