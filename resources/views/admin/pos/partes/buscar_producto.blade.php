<div class="row">
	<div class="col-10">
		<input type="text" class="form-control" placeholder="Buscar producto..." id="busqueda">
	</div>
	<div class="col-2">
		<a class="btn btn-info btn-block text-white btn_busqueda">
			<i class="fa fa-search"></i>
		</a>
	</div>
</div>