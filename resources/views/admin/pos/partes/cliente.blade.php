<div class="row">
	<div class="col-12">
		<button type="button" class="btn btn-info btn-block text-left" data-toggle="modal" data-target="#modal_agregar_cliente">
			<i class="fa fa-user mr-2"></i>
			Agregar Cliente
		</button>

		<!-- Modal -->
		<div class="modal fade" id="modal_agregar_cliente" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
			<div class="modal-dialog modal-lg modal-dialog-centered text-left" role="document">
				<div class="modal-content">
					<div class="modal-header bg-gradient-info">
						<div class="col-9">
							<h5 class="title-modal">Agregar cliente</h5>
						</div>
						<div class="col-3">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-10">
								<input type="text" class="form-control" id="buscar_cliente" placeholder="Busca un cliente...">
							</div>
							<div class="col-2">
								<a class="btn btn-info btn_buscar_cliente btn-block">
									<i class="fa fa-search"></i>
								</a>
							</div>
						</div>
						<div class="row mt-4" id="tabla_cliente">
							
						</div>
					</div>
					<div class="modal-footer">
						<div class="row">
							<div class="col-12 text-rigth">
								<a class="btn btn-info btn_nuevo_cliente">
									<i class="fa fa-plus"></i>
									Nuevo cliente
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('admin.pos.partes.nuevo_cliente')