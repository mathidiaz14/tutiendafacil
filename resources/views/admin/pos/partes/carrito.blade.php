
@if(Session::get('lista_productos') == 0)
	<div class="col-12">
	@include('ayuda.sin_registros')
	</div>
@else
	<div class="col-12">
		<div class="table table-resposinve">
			<table class="table table-striped">
				<tbody class="tabla_productos">
					<tr>
						<th>SKU</th>
						<th>Producto</th>
						<th>Cantidad</th>
						<th>Eliminar</th>
					</tr>
					@foreach(Session::get('lista_productos') as $carrito)
						<tr>
							<td>
								@if($carrito['variante'] != null)
									{{$carrito['variante']->sku != null ? $carrito['variante']->sku : "-" }}
								@else
									{{$carrito['producto']->sku != null ? $carrito['producto']->sku : "-" }}
								@endif
							</td>
							<td>
								<b>{{$carrito['producto']->nombre}}</b>
								@if($carrito['variante'] != null)
									<br>
									@foreach($carrito['variante']->valores as $valor)
										<h2 id      = "valor_{{$valor->id}}"
											class   = "badge badge-dark mr-2" 
											attr-id = "{{$valor->id}}"
											style   = "background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%); 
														border:1px solid grey;
														min-width: 3em;">
											{{$valor->nombre != null ? $valor->nombre : "-"}}
										</h2>
									@endforeach
								@endif
							</td>
							<td class="text-center">{{$carrito['cantidad']}}</td>
							<td><a class="btn btn-danger btn_eliminar_producto_carrito text-white btn-block" attr-id="{{$carrito['id']}}"><i class="fa fa-times"></i></a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endif