@if((isset($clientes)) and ($clientes->count() > 0))
	<div class="col-12">
		<div class="table table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<td>Nombre</td>
						<td>Email</td>
						<td>Teléfono</td>
						<td></td>
					</tr>
				</thead>
				<tbody">
					@foreach($clientes as $cliente)
						<tr>
							<td>{{$cliente->nombre}} {{$cliente->apellido}}</td>
							<td>{{$cliente->email}}</td>
							<td>{{$cliente->telefono}}</td>
							<td>
								<a class='btn btn-info btn_seleccionar_cliente btn-block text-white' attr-id='{{$cliente->id}}'><i class='fa fa-plus'></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endif