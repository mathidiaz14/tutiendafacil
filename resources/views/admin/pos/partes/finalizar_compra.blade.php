<div class="row">
	<div class="col-12 mt-2">
		<button type="button" class="btn btn-info btn-block text-left" data-toggle="modal" data-target="#boton_finalizar">
			<i class="fa fa-money-bill mr-2"></i>
			Finalizar compra
		</button>

		<!-- Modal -->
		<div class="modal fade" id="boton_finalizar" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header bg-gradient-info">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body text-center">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>