@extends('layouts.dashboard', ['menu_activo' => 'local', 'menu_superior' => 'tienda'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Envío y retiro</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Envío y retiro</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">
					<ul class="nav nav-tabs" id="tab">
						<li class="nav-item">
							<a class="nav-link active" href="#configuracion">
								<b>Configuración</b>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#locales">
								<b>Locales</b>
							</a>
						</li>
					</ul>
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-12">
									<div class="tab-content" id="myTabContent">
										<div class="tab-pane fade show active" id="configuracion" role="tabpanel" aria-labelledby="home-tab">
											@include('admin.local.partes.configuracion')
										</div>
										<div class="tab-pane fade" id="locales" role="tabpanel" aria-labelledby="profile-tab">
											@include('admin.local.partes.locales')
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
	<script>
		$('#tab a').on('click', function (e) {
			e.preventDefault()
			$(this).tab('show')
		});
	</script>
@endsection