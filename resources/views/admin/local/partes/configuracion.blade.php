<div class="row mt-5">
	<div class="col-12">
		<form action="{{url('admin/local/guardar')}}" method="post" class="form-horizontal col-12">
			@csrf
				<div class="form-group">
					<div class="row">
						<div class="col-12 col-md-9">
							<label for="">¿Realizas envíos a Domicilio?</label>
						</div>
						<div class="col-12 col-md-3">
							<div class="onoffswitch">
								<input type="checkbox" name="envio" class="onoffswitch-checkbox" id="envio" tabindex="0" @if(configuracion()->envio == "on") checked @endif>
								<label class="onoffswitch-label" for="envio"></label>
							</div>	
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-12 col-md-9">
							<label for="">¿Realizas entrega en local?</label>	
						</div>
						<div class="col-12 col-md-3">
							<div class="onoffswitch">
								<input type="checkbox" name="retiro" class="onoffswitch-checkbox" id="entrega_retiro" tabindex="0" @if(configuracion()->retiro == "on") checked @endif>
								<label class="onoffswitch-label" for="entrega_retiro"></label>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group text-right">
					<hr>
					<button class="btn btn-primary">
						<i class="fa fa-save"></i>
						Guardar
					</button>
				</div>
		</form>
	</div>
</div>