@section('css')
    <script src="{{asset('dashboard/plugins/chart.js/Chart.min.js')}}"></script>    
@endsection

<div class="row">
    <div class="col-12 col-md-6">
    <section class="col-lg-12 connectedSortable">
        <div class="card bg-gradient-secundary">
        <div class="card-header border-0">
            <h5 class="card-title">
            Ordenes (Ultimos 6 meses)
            </h5>
        </div>
        <div class="card-body">

            @php
                use App\Models\Venta;
                use Carbon\Carbon;

                $empresaId = empresa()->id;
                $fechaInicio = Carbon::now()->subMonths(5)->firstOfMonth();
                $fechaFin = Carbon::now()->lastOfMonth();

                $ventasPorMes = Venta::where('empresa_id', $empresaId)
                    ->where('estado', '!=', 'comenzado')
                    ->whereBetween('created_at', [$fechaInicio, $fechaFin])
                    ->selectRaw('YEAR(created_at) as year, MONTH(created_at) as month, 
                                 COUNT(*) as total, 
                                 SUM(CASE WHEN estado = "entregado" THEN 1 ELSE 0 END) as entregado,
                                 SUM(CASE WHEN estado = "cancelado" THEN 1 ELSE 0 END) as cancelado,
                                 SUM(CASE WHEN estado = "devuelto" THEN 1 ELSE 0 END) as devuelto,
                                 SUM(CASE WHEN estado = "entregado" THEN precio ELSE 0 END) as facturado')
                    ->groupBy('year', 'month')
                    ->orderBy('year', 'asc')
                    ->orderBy('month', 'asc')
                    ->get();


            @endphp
                
            @if($ventasPorMes->first() == null)
                @include('ayuda.sin_registros')
            @else
                <canvas id="graficaVentas" width="100%"></canvas>

                <script>
                    var ctx = document.getElementById('graficaVentas').getContext('2d');
                    var taskChart = new Chart(ctx, {
                    type: 'bar',
                        data: {
                            labels: [
                                @for($i = 5 ; $i >= 0; $i--)
                                    "{{mes(\Carbon\Carbon::now()->subMonth($i)->format('n'))}}",
                                @endfor
                            ],
                            datasets: [{
                                label: "Totales",
                                backgroundColor: "#17A2B8",
                                borderColor: "#108da0",
                                pointBorderColor: "#108da0",
                                pointBackgroundColor: "#108da0",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "#108da0",
                                pointBorderWidth: 1,
                                data: [
                                    @for($i = 5 ; $i >= 0; $i--)
                                        "{{$ventasPorMes->first(function ($venta) use ($i) {return $venta->year == \Carbon\Carbon::now()->subMonth($i)->format('Y') && $venta->month == \Carbon\Carbon::now()->subMonth($i)->format('n');})->total ?? 0}}",
                                    @endfor
                                ]
                            },{
                                label: "Entregadas",
                                backgroundColor: "#2D8153",
                                borderColor: "#56a579",
                                pointBorderColor: "#56a579",
                                pointBackgroundColor: "#56a579",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "#56a579",
                                pointBorderWidth: 1,
                                data: [
                                    @for($i = 5 ; $i >= 0; $i--)
                                        "{{$ventasPorMes->first(function ($venta) use ($i) {return $venta->year == \Carbon\Carbon::now()->subMonth($i)->format('Y') && $venta->month == \Carbon\Carbon::now()->subMonth($i)->format('n');})->entregado ?? 0}}",
                                    @endfor
                                ]
                            },{
                                label: "Devueltas",
                                backgroundColor: "#6C757D",
                                borderColor: "#52585e",
                                pointBorderColor: "#52585e",
                                pointBackgroundColor: "#52585e",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "#52585e",
                                pointBorderWidth: 1,
                                data: [
                                    @for($i = 5 ; $i >= 0; $i--)
                                        "{{$ventasPorMes->first(function ($venta) use ($i) {return $venta->year == \Carbon\Carbon::now()->subMonth($i)->format('Y') && $venta->month == \Carbon\Carbon::now()->subMonth($i)->format('n');})->devuelto ?? 0}}",
                                    @endfor
                                ]
                            },{
                                label: "Canceladas",
                                backgroundColor: "#f44b4b",
                                borderColor: "#f44b4b",
                                pointBorderColor: "#f44b4b",
                                pointBackgroundColor: "#f44b4b",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "#f44b4b",
                                pointBorderWidth: 1,
                                data: [
                                    @for($i = 5 ; $i >= 0; $i--)
                                        "{{$ventasPorMes->first(function ($venta) use ($i) {return $venta->year == \Carbon\Carbon::now()->subMonth($i)->format('Y') && $venta->month == \Carbon\Carbon::now()->subMonth($i)->format('n');})->cancelado ?? 0}}",
                                    @endfor
                                ]
                            }]
                        },
                    }); 
                </script>
            @endif
        </div>
        </div>
    </section>
    </div>       

    <div class="col-12 col-md-6">
        <section class="col-lg-12 connectedSortable">
            <div class="card bg-gradient-secundary">
            <div class="card-header border-0">
                <h5 class="card-title">
                Monto facturado (Ultimos 6 meses)
                </h5>
            </div>
            <div class="card-body">

                <canvas id="graficaMonto" width="100%"></canvas>

                <script>
                    var ctx = document.getElementById('graficaMonto').getContext('2d');
                    var taskChart = new Chart(ctx, {
                    type: 'line',
                        data: {
                            labels: [
                            @for($i = 5 ; $i >= 0; $i--)
                                "{{mes(\Carbon\Carbon::now()->subMonth($i)->format('n'))}}",
                            @endfor
                                ],
                            datasets: [{
                                label: "Monto",
                                backgroundColor: "#17A2B8",
                                borderColor: "#108da0",
                                pointBorderColor: "#108da0",
                                pointBackgroundColor: "#108da0",
                                pointHoverBackgroundColor: "#fff",
                                pointHoverBorderColor: "#108da0",
                                pointBorderWidth: 1,
                                data: [
                                    @for($i = 6 ; $i >= 0; $i--)
                                        "{{$ventasPorMes->first(function ($venta) use ($i) {return $venta->year == \Carbon\Carbon::now()->subMonth($i)->format('Y') && $venta->month == \Carbon\Carbon::now()->subMonth($i)->format('n');})->facturado ?? 0}}",
                                    @endfor
                                ]
                            }]
                        },
                    });
                </script>
            </div>
            </div>
        </section>
    </div> 

    <div class="col-12 col-md-6">
    <section class="col-lg-12 connectedSortable">
        <div class="card bg-gradient-secundary">
        <div class="card-header border-0">
            <h5 class="card-title">
            Medio de pago
            </h5>
        </div>
        <div class="card-body">
            @php
                $total    = empresa()
                                ->ventas
                                ->where('estado', '!=', 'comenzado');

                $total_contador = $total->count() == 0 ? 1 : $total->count();

                $tarjeta        = $total->where('pago_metodo', 'tarjeta')->count();
                $redes          = $total->where('pago_metodo', 'redes')->count();
                $transferencia  = $total->where('pago_metodo', 'transferencia')->count();
                $efectivo       = $total->where('pago_metodo', 'efectivo')->count();
            @endphp

            @if($total->count() == 0)
                @include('ayuda.sin_registros')
            @else
                <canvas id="graficaMedioDePago" width="100%"></canvas>
                <script>
                    var ctx = document.getElementById('graficaMedioDePago').getContext('2d');
                    var taskChart = new Chart(ctx, {
                    type: 'doughnut',
                    responsive: true,
                    data: {
                        labels: [
                            "Tarjeta %{{round(($tarjeta * 100) / $total_contador, 2)}}",
                            "Redes %{{round(($redes * 100) / $total_contador, 2)}}",
                            "Transferencia %{{round(($transferencia * 100) / $total_contador, 2)}}",
                            "Efectivo %{{round(($efectivo * 100) / $total_contador, 2)}}"
                            ],
                        datasets: [{
                        data: [
                            {{$tarjeta}},
                            {{$redes}},
                            {{$transferencia}},
                            {{$efectivo}}
                            ],
                        backgroundColor: [
                            '#17A2B8',
                            '#6C757D',
                            '#23272B',
                            '#007BFF'
                        ],
                        }]
                    },
                    });

                </script>
            @endif
        </div>
        </div>
    </section>
    </div> 

    <div class="col-12 col-md-6">
    <section class="col-lg-12 connectedSortable">
        <div class="card bg-gradient-secundary">
        <div class="card-header border-0">
            <h5 class="card-title">
            Metodos de entrega
            </h5>
        </div>
        <div class="card-body">
            @if($total->count() == 0)
                @include('ayuda.sin_registros')
            @else
                <canvas id="graficaMetodosDeEntrega" width="100%"></canvas>
                <script>
                    @php
                        $envio          = $total->where('entrega', 'envio')->count();
                        $retiro         = $total->where('entrega', 'retiro')->count();
                    @endphp

                    var ctx = document.getElementById('graficaMetodosDeEntrega').getContext('2d');
                    var taskChart = new Chart(ctx, {
                        type: 'doughnut',
                        responsive: true,
                        data: {
                            labels: [
                                "Envio %{{round(($envio * 100) / $total_contador, 2)}}",
                                "Retiro %{{round(($retiro * 100) / $total_contador, 2)}}",
                                ],
                            datasets: [{
                            data: [
                                {{$envio}},
                                {{$retiro}}
                                ],
                            backgroundColor: [
                                '#17A2B8',
                                '#6C757D',
                            ],
                            }]
                        },
                    });    
                </script>        
            @endif
        </div>
        </div>
    </section>
    </div> 

</div>