@extends('layouts.dashboard', ['menu_activo' => 'venta', 'menu_superior' => 'tienda'])

@section('contenido')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pedido #{{$venta->codigo}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
              <li class="breadcrumb-item"><a href="{{url('admin/venta')}}">Ventas</a></li>
              <li class="breadcrumb-item active">#{{$venta->codigo}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <section class="col-lg-12">
      			<div class="row">
              <div class="col-12 col-md-8">
                @include('admin.venta.partes.datos_pedido')
  
                @include('admin.venta.partes.datos_cliente')
              </div>
              <div class="col-12 col-md-4">
                @include('admin.venta.partes.controles')
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                @include('admin.venta.partes.productos')
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('scripts')

@endsection