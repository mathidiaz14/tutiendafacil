@extends('layouts.dashboard', ['menu_activo' => 'venta', 'menu_superior' => 'tienda'])

@section('contenido')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Ventas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
              <li class="breadcrumb-item active">Venta</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

   <section class="content">
    	<div class="row container-fluid">
	    	<div class="col-12 col-md-4 offset-md-8">
	    		<form action="{{url('admin/venta')}}" method="get" class="form-horizontal pb-2">
	    			<div class="row">
	    				<div class="col-8">
		    				<div class="form-group">
		    					<input type="text" class="form-control" name="buscar" placeholder="Buscar Venta">
		    				</div>
	    				</div>
	    				<div class="col-4">
		    				<div class="form-group">
		    					<button class="btn btn-info mx-2 px-4 btn-block">
				    				<i class="fa fa-search"></i>
				    			</button>
		    				</div>
	    				</div>
	    			</div>
	    		</form>
	    	</div>
	    </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <section class="col-lg-12">
          	
            <div class="card">
              <div class="card-header">
                <div class="row">
                	<div class="col">
                		<h3 class="card-title">
		                  <i class="fas fa-shopping-cart"></i>
		                  Ventas
		                </h3>
                	</div>
                	<div class="col text-right">
                	</div>
                </div>
              	</div>
							<div class="card-body">
								@if($ventas->count() == 0)
									@include('ayuda.sin_registros')
								@else
									<div class="table table-responsive">
										<table class="table table-striped">
											<tr>
												<th>#</th>
												<th>Estado</th>
												<th>Cliente</th>
												<th>Tipo Entrega</th>
												<th>Pago</th>
												<th></th>
												<th>Precio</th>
												<th>Fecha</th>
												<th>Ver</th>
											</tr>
											@foreach($ventas->sortByDesc('created_at') as $venta)
												<tr>
													<td>{{$venta->codigo}}</td>
													<td>
														@include('ayuda.venta_estado')
													</td>
													<td>
														<a href="{{url('admin/cliente', $venta->cliente_id)}}">
															@if(isset($venta->cliente->nombre))
																{{$venta->cliente->nombre}}
															@else
																--
															@endif
														</a>
													</td>
													<td>
														@if($venta->entrega == "retiro")
															Retiro en local
														@else
															Envio a domicilio
														@endif
													</td>
													<td>
														@include('ayuda.pago_estado')
													</td>
													<td>
														@include('ayuda.pago_metodo')
													</td>
													<td>
														${{$venta->precio - $venta->descuento}}
													</td>
													<td>
														{{$venta->created_at->format('d/m/Y - H:i')}}
													</td>
													<td>
														<a href="{{url('admin/venta', $venta->id)}}" class="btn btn-dark">
															<i class="fa fa-eye"></i>
														</a>
													</td>
												</tr>
											@endforeach
										</table>
									</div>
                  @include('ayuda.links', ['link' => $ventas])
								@endif
							</div>
            </div>
          </section>
        </div>
      </div>
    </section>
  </div>
@endsection

@section('scripts')
@endsection