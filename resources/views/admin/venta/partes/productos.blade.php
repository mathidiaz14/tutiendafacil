<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col">
				<h5 class="card-title">
					Lista de productos
				</h5>
			</div>
			<div class="col text-right">

			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="table table-responsive">
			<table class="table table-striped">
				<tbody>
					@foreach($venta->productos as $producto)
					<tr>
						<td>
							@if(isset($producto->pivot->variante_id))
				    			<img 	src="{{asset(producto_variante($producto->pivot->variante_id)->imagen)}}" alt="" width="50px" 
				    					style="	-webkit-box-shadow: 	1px 1px 2px 0px rgba(0,0,0,0.75);
												-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												object-fit: cover;
											    object-position: center;
											    height: 50px;"	>
				    		@else
				    			<img 	src="{{producto_url_imagen($producto->id)}}" alt="" width="50px" 
				    					style="	-webkit-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												object-fit: cover;
											    object-position: center;
											    height: 50px;"	>
				    		@endif
						</td>
						<td>
							<b>{{$producto->nombre}}</b>
							@if($producto->pivot->variante_id != null)
								<br>
								<small>
									@foreach(producto_variante($producto->pivot->variante_id)->valores as $valor)
										<h2 id      = "valor_{{$valor->id}}"
			                                class   = "badge badge-dark mr-2" 
			                                attr-id = "{{$valor->id}}"
			                                style   = "background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%);
			                                            min-width: 4em;
			                                            opacity: .8;"
			                            >
			                                {{$valor->nombre != null ? $valor->nombre : "-"}}
			                            </h2>
									@endforeach
								</small>	
							@endif
						</td>
						<td>x {{$producto->pivot->cantidad}}</td>
						<td>${{$producto->pivot->precio}}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					@if($venta->descuento != null)
					<tr>
						<td></td>
						<td></td>
						<td>Descuento:</td>
						<td>$ -{{$venta->descuento}}</td>
					</tr>
					@endif
					<tr>
						<td></td>
						<td></td>
						<td>Total:</td>
						<td><b>${{$venta->precio - $venta->descuento}}</b></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>