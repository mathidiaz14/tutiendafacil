<div class="card">
	<div class="card-header">
		<div class="row">
			<div class="col-12 text-right d-sm-none d-md-block">
				<a href="{{url('admin/venta')}}" class="btn btn-default">
					<i class="fa fa-chevron-left mr-3"></i>
					Atras
				</a>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-12 mt-2">
				@if((($venta->pago_metodo == "efectivo") or ($venta->pago_metodo == "transferencia")) and ($venta->pago_estado == "pendiente"))
				<button type="button" class="btn btn-dark btn-block text-left" data-toggle="modal" data-target="#pedidoPago">
					<i class="fa fa-money-bill mr-3"></i>
					Confirmar pago
				</button>

				<!-- Modal -->
				<div class="modal fade deleteModal" id="pedidoPago" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body text-center">
								<h4>¿Desea confirmar el pago?</h4>
								<br>
								<hr>
								<div class="row">
									<div class="col">
										<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
											NO
										</button>
									</div>
									<div class="col">
										<a href="{{url('admin/venta/pago', $venta->id)}}" class="btn btn-dark btn-block">
											SI
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@else
				<a href="#" class="btn btn-dark btn-block disabled text-left">
					<i class="fa fa-money-bill mr-3"></i>
					Confirmar pago
				</a>
				@endif
			</div>
			<div class="col-12 mt-2">
				@if(($venta->estado == "en_preparacion") and ($venta->pago_estado == "aprobado") and ($venta->entrega == "retiro"))
				<button type="button" class="btn btn-dark btn-block text-left" data-toggle="modal" data-target="#pedidoPago">
					<i class="fa fa-box mr-3"></i>
					Marcar "Para entregar"
				</button>

				<!-- Modal -->
				<div class="modal fade deleteModal" id="pedidoPago" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body text-center">
								<h4>¿Desea marcar el producto para entregar?</h4>
								<br>
								<hr>
								<div class="row">
									<div class="col">
										<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
											NO
										</button>
									</div>
									<div class="col">
										<a href="{{url('admin/venta/entrega', $venta->id)}}" class="btn btn-dark btn-block">
											SI
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@else
				<a href="#" class="btn btn-dark btn-block disabled text-left">
					<i class="fa fa-box mr-3"></i>
					Marcar "Para entregar"
				</a>
				@endif
			</div>
			<div class="col-12 mt-2">
				@if((($venta->pago_estado == "aprobado") and ($venta->estado == "en_preparacion")) or (($venta->pago_estado == "aprobado") and ($venta->estado == "entrega")))
				<button type="button" class="btn btn-dark btn-block text-left" data-toggle="modal" data-target="#pedidoAprobado">
					<i class="fa fa-dolly mr-3"></i>
					Marcar "Entregado"
				</button>

				<!-- Modal -->
				<div class="modal fade deleteModal" id="pedidoAprobado" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body text-center">
								<h4>¿Desea marcar la venta como entregada?</h4>
								<br>
								<hr>
								<div class="row">
									<div class="col">
										<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
											NO
										</button>
									</div>
									<div class="col">
										<a href="{{url('admin/venta/entregar', $venta->id)}}" class="btn btn-dark btn-block">
											SI
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@else
				<a href="" class="btn btn-dark btn-block disabled text-left">
					<i class="fa fa-dolly mr-3"></i>
					Marcar "Entregado"
				</a>
				@endif
			</div>
			
			<div class="col-12 mt-2">
				@if((($venta->pago_estado == "aprobado") and ($venta->estado == "en_preparacion")) or (($venta->pago_estado == "aprobado") and ($venta->estado == "entrega")))
				<button type="button" class="btn btn-dark btn-block text-left" data-toggle="modal" data-target="#pedidoDevolucion">
					<i class="fa fa-undo mr-3"></i>
					Devolver dinero
				</button>

				<!-- Modal -->
				<div class="modal fade deleteModal" id="pedidoDevolucion" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body text-center">
								<h4>¿Desea devolver el monto de la venta?</h4>
								<br>
								<hr>
								<div class="row">
									<div class="col">
										<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
											NO
										</button>
									</div>
									<div class="col">
										<a href="{{url('admin/venta/devolver', $venta->id)}}" class="btn btn-dark btn-block">
											SI
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@else
				<a href="" class="btn btn-dark btn-block disabled text-left">
					<i class="fa fa-undo mr-3"></i>
					Devolver dinero
				</a>
				@endif
			</div>
			<div class="col-12 mt-2">
				@if(($venta->estado == "en_preparacion") or ($venta->estado == "entrega"))
				<button type="button" class="btn btn-dark btn-block text-left" data-toggle="modal" data-target="#pedidoCancelado">
					<i class="fa fa-times mr-3"></i>
					Cancelar pedido
				</button>

				<!-- Modal -->
				<div class="modal fade deleteModal" id="pedidoCancelado" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body text-center">
								<h4>¿Desea cancelar el pedido?</h4>
								<br>
								<hr>
								<div class="row">
									<div class="col">
										<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
											NO
										</button>
									</div>
									<div class="col">
										<a href="{{url('admin/venta/cancelar', $venta->id)}}" class="btn btn-dark btn-block">
											SI
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@else
				<a href="" class="btn btn-dark btn-block disabled text-left">
					<i class="fa fa-times mr-3"></i>
					Cancelar pedido
				</a>
				@endif
			</div>
			<div class="col-12 mt-2">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-dark btn-block text-left" data-toggle="modal" data-target="#editarPedido">
					<i class="fa fa-edit mr-3"></i>
					Agregar observación
				</button>

				<!-- Modal -->
				<div class="modal fade" id="editarPedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content text-left">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Observación del pedido</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="{{url('admin/venta', $venta->id)}}" class="form-horizontal" method="post">
									@csrf
									@method('PATCH')
									<div class="row">
										<div class="col">
											<div class="form-group">
												<textarea name="observacion" id="" cols="30" rows="3" class="form-control" placeholder="Observación del pedido">{{$venta->observacion}}</textarea>
											</div>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col">
											<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
												<i class="fa fa-chevron-left mr-3"></i>
												Atras
											</button>
										</div>
										<div class="col text-right">
											<button class="btn btn-dark btn-block">
												<i class="fa fa-save mr-3"></i>
												Guardar
											</button>
										</div>
									</div>  
								</form>  
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 mt-2">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-dark btn-block text-left" data-toggle="modal" data-target="#historialPedido">
					<i class="fa fa-list mr-3"></i>
					Historial del pedido
				</button>

				<!-- Modal -->
				<div class="modal fade" id="historialPedido" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
						<div class="modal-content text-left">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Historial de estados del pedido
								</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								  <div class="row">
								  	<div class="col-12">
								  		<div class="table table-resposnive">
								  			<table class="table table-striped">
								  				<tr>
								  					<th>
								  						Estado
								  					</th>
								  					<th>
								  						Usuario
								  					</th>
								  					<th>
								  						Fecha
								  					</th>
								  				</tr>
								  				@foreach($venta->estados as $estado)
								  					<tr>
								  						<td>
								  							@include('ayuda.venta_estado', ['venta' => $estado])
								  						</td>
								  						<td>
								  							@if($estado->usuario_id == null)
								  								Sistema
								  							@else
								  								{{$estado->usuario->nombre}}
								  							@endif
								  						</td>
								  						<td>
								  							{{$estado->created_at->format('d/m/Y H:i')}}	
								  						</td>
								  					</tr>
								  				@endforeach
								  			</table>
								  		</div>
								  	</div>
								  </div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>