<div class="row">
	<div class="col-12">
		<div class="card bg-default">
			<div class="card-header">
				<div class="row">
					<div class="col">
						<h5 class="card-title">Datos del pedido</h5>
					</div>
					<div class="col text-right d-none d-sm-block d-md-none">
						<a href="{{url('admin/venta')}}" class="btn btn-default">
							<i class="fa fa-chevron-left mr-3"></i>
							Atras
						</a>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-4">
						Codigo venta:
					</div>
					<div class="col-8">
						<b>{{$venta->codigo}}</b>
					</div>
				</div>
				<div class="row">
					<div class="col-4">
						Fecha:
					</div>
					<div class="col-8">
						<b>{{$venta->created_at->format('d/m/Y - H:i')}}</b>
					</div>
				</div>
				<div class="row">
					<div class="col-4">
						Estado:
					</div>
					<div class="col-8">
						@include('ayuda.venta_estado')
					</div>
				</div>
				<div class="row">
					<div class="col-4">
						Metodo Pago:
					</div>
					<div class="col-8">
						@include('ayuda.pago_metodo')
					</div>
				</div>
				<div class="row">
					<div class="col-4">
						Estado Pago:
					</div>
					<div class="col-8">
						@include('ayuda.pago_estado')
					</div>
				</div>

				@if($venta->mp_id != null)
					<div class="row">
						<div class="col-4">
							Codigo MercadoPago:
						</div>
						<div class="col-8">
							<b>{{$venta->mp_id}}</b>
						</div>
					</div>
				@endif
				
				@if($venta->pago_metodo == "transferencia")
					<div class="row">
						<div class="col-4">
							Archivo:
						</div>
						<div class="col-8">
							<a href="{{url('admin/venta/transferencia', $venta->id)}}" target="_blank">
								<i class="fa fa-download"></i>
								Ver archivo
							</a>
						</div>
					</div>
				@endif

				<div class="row">
					<div class="col-4">
						Entrega:
					</div>
					<div class="col-8">
						@include('ayuda.metodo_envio')
					</div>
				</div>
				@if($venta->observacion != null)
					<div class="row">
						<div class="col-4">
							Observación:
						</div>
						<div class="col-8">
							<b>{{$venta->observacion}}</b>		
						</div>
					</div>
				@endif
			</div>
		</div>
	</div>
</div>
