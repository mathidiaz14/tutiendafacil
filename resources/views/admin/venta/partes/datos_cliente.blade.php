<div class="card">
	<div class="card-header">
		<h5 class="card-title">
			Datos del cliente
		</h5>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-4">
				Nombre:
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente->nombre) ? $venta->cliente->nombre : "--" }}
				</b>
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				Apellido:
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente->apellido) ? $venta->cliente->apellido : "--" }}
				</b>
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				Email:
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente->email) ? $venta->cliente->email : "--" }}
				</b>
			</div>
		</div>
		
		<div class="row">
			<div class="col-4">
				Telefono:			
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente->telefono) ? $venta->cliente->telefono : "--" }}
				</b>	
			</div>
		</div>
		
		<div class="row">
			<div class="col-4">
				Ciudad:			
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente->ciudad) ? $venta->cliente->ciudad : "--" }}
				</b>
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				Dirección:			
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente->direccion) ? $venta->cliente->direccion : "--" }}
				</b>
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				Apartamento:			
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente->apartamento) ? $venta->cliente->apartamento : "--" }}
				</b>
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				Observación:		
			</div>
			<div class="col-8">
				<b>
					{{isset($venta->cliente_observacion) ? $venta->cliente_observacion : "--" }}
				</b>
			</div>
		</div>
	</div>
</div>