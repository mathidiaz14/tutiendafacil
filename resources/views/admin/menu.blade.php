@if(env('APP_ENV') == 'local')

<li class="nav-item ">
  <a href="{{url('admin/pos')}}" class="nav-link @if($menu_activo == 'pos') active @endif">
    <i class="nav-icon fas fa-cash-register"></i>
    <p>
      POS
    </p>
  </a>
</li>

@endif

<li class="nav-item ">
  <a href="{{url('admin/venta')}}" class="nav-link @if($menu_activo == 'venta') active @endif">
    <i class="nav-icon fas fa-shopping-cart"></i>
    <p>
      Ventas
    </p>
  </a>
</li>
<li class="nav-item ">
  <a href="{{url('admin/producto')}}" class="nav-link @if($menu_activo == 'producto') active @endif">
    <i class="nav-icon fas fa-archive"></i>
    <p>
      Productos
    </p>
  </a>
</li>

<li class="nav-item ">
  <a href="{{url('admin/categoria')}}" class="nav-link @if($menu_activo == 'categoria') active @endif">
    <i class="nav-icon fas fa-boxes"></i>
    <p>
      Categorías
    </p>
  </a>
</li>
<li class="nav-item ">
  <a href="{{url('admin/cliente')}}" class="nav-link @if($menu_activo == 'cliente') active @endif">
    <i class="nav-icon fas fa-users"></i>
    <p>
      Clientes
    </p>
  </a>
</li>
<li class="nav-item ">
  <a href="{{url('admin/proveedor')}}" class="nav-link @if($menu_activo == 'proveedor') active @endif">
    <i class="nav-icon fas fa-truck"></i>
    <p>
      Proveedores
    </p>
  </a>
</li>
<li class="nav-item ">
  <a href="{{url('admin/cupon')}}" class="nav-link @if($menu_activo == 'cupon') active @endif">
    <i class="nav-icon fas fa-ticket-alt"></i>
    <p>
      Cupones
    </p>
  </a>
</li>

<li class="nav-item">
  <a href="{{url('admin/local')}}" class="nav-link @if($menu_activo == 'local') active @endif">
    <i class="fa fa-truck-loading nav-icon"></i>
    <p>
      Envío y retiro
    </p>
  </a>
</li>
<li class="nav-item">
  <a href="{{url('admin/metodoPago')}}" class="nav-link @if($menu_activo == 'metodoPago') active @endif">
    <i class="fa fa-money-bill nav-icon"></i>
    <p>
      Metodos de pago
    </p>
  </a>
</li>