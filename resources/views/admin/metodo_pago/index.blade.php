@extends('layouts.dashboard', ['menu_activo' => 'metodoPago', 'menu_superior' => 'tienda'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Metodos de pago</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Administrar</li>
						<li class="breadcrumb-item active">Metodos de pago</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<div class="col-12">
					<ul class="nav nav-tabs" id="tab">
						<li class="nav-item">
							<a class="nav-link active" href="#mercadopago">
								<b>MercadoPago</b>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#transferencia">
								<b>Transferencia</b>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#efectivo">
								<b>Efectivo</b>
							</a>
						</li>
					</ul>
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-12">
									<div class="tab-content py-5" id="myTabContent">
										<div class="tab-pane fade show active" id="mercadopago" role="tabpanel" aria-labelledby="home-tab">
											@include('admin.metodo_pago.partes.mercadopago')
										</div>
										<div class="tab-pane fade" id="transferencia" role="tabpanel" aria-labelledby="profile-tab">
											@include('admin.metodo_pago.partes.transferencia')
										</div>
										<div class="tab-pane fade" id="efectivo" role="tabpanel" aria-labelledby="profile-tab">
											@include('admin.metodo_pago.partes.efectivo')
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
<script>
	$('#tab a').on('click', function (e) {
		e.preventDefault()
		$(this).tab('show')
	});

	$('.summernote').summernote({
	  	height: 100,
	  	toolbar: false,
	  	codemirror: {
	    	theme: 'monokai'
	  	}
	});
</script>
@endsection