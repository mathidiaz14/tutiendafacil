<div class="row text-center">
	<div class="col-10 offset-1">
		<h5>¿Desea recibir pagos en efectivo? </h5>
		<br>
		@if((isset(empresa()->pago_efectivo)) and (empresa()->pago_efectivo->estado == "conectado"))
			<a href="{{url('admin/metodoPago/set/efectivo')}}" class="btn btn-lg btn-danger px-5">
				<i class="fa fa-times"></i>
				Deshabilitar
			</a>
		@else
			<a href="{{url('admin/metodoPago/set/efectivo')}}" class="btn btn-lg btn-info px-5">
				<i class="fa fa-check"></i>
				Habilitar
			</a>
		@endif
	</div>
</div>
