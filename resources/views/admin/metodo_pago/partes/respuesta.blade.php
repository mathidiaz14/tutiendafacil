@extends('layouts.dashboard', ['menu_activo' => 'inicio'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Conexión </h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Administrar</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-body text-center">
							<div class="row mt-5">
								<div class="col-12">
									<img src="{{asset('img/mercadopago.png')}}" alt="" width="200px">
									<img src="{{asset('img/mercadolibre.png')}}" alt="" width="200px">
								</div>
								<div class="col-12 my-2">
									<hr>
								</div>
								<div class="col-12 my-4">
									@if($estado == "exitosa")
										<h2>La conexión se realizo de forma exitosa, ya puede continuar utilizando la plataforma</h2>
									@else
										<h1>Conexión fallida, intentelo nuevamente</h1>
										<br><br>
										<a 
										href="https://auth.mercadopago.com.uy/authorization?
											client_id={{env('MP_CLIENT_ID')}}
											&response_type=code
											&platform_id=mp
											&state={{Auth::user()->empresa_id}}
											&redirect_uri=https://tutiendafacil.uy/mercadopago/conexion" class="btn btn-info btn-lg px-5">
												<i class="fa fa-plug"></i>
												Conectar
										</a>
									@endif
								</div>
							</div>
						</div>
					</div>			
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')

@endsection