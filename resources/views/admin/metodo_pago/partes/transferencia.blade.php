<div class="row text-center">
	<div class="col-10 offset-1">
		<h5>¿Desea recibir pagos por transferencia bancaria? </h5>
		<br>
		@if((isset(empresa()->pago_transferencia)) and (empresa()->pago_transferencia->estado == "conectado"))
			<a href="{{url('admin/metodoPago/set/transferencia')}}" class="btn btn-lg btn-danger px-5">
				<i class="fa fa-times"></i>
				Deshabilitar
			</a>
		@else
			<a href="{{url('admin/metodoPago/set/transferencia')}}" class="btn btn-lg btn-info px-5">
				<i class="fa fa-check"></i>
				Habilitar
			</a>
		@endif
	</div>
</div>

@if((isset(empresa()->pago_transferencia)) and (empresa()->pago_transferencia->estado == "conectado"))
	<div class="row mt-4">
		<div class="col-10 offset-1">
			<form action="{{url('admin/metodoPago/save/transferencia')}}" method="post" class="form-horizontal">
				@csrf
				<div class="form-group">
					<label for="">Datos para recibir la transferencia:</label>
					<textarea name="datos" id="" cols="30" rows="4" class="form-control summernote">{{empresa()->pago_transferencia->datos}}</textarea>
				</div>
				<div class="form-group text-right">
					<button class="btn btn-info">
						<i class="fa fa-save"></i>
						Guardar
					</button>
				</div>
			</form>	
		</div>
	</div>
@endif