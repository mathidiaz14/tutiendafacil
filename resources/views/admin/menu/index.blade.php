@extends('layouts.dashboard', ['menu_activo' => 'menu', 'menu_superior' => "apariencia"])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Menús</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Menús</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">
					
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col">
									<h3 class="card-title">
										<i class="fas fa-bars"></i>
										Menús
									</h3>
								</div>
								<div class="col text-right">
									<!-- Button trigger modal -->
									<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#agregarMenu">
										<i class="fa fa-plus"></i>
										Agregar item
									</button>

									<!-- Modal -->
									<div class="modal fade deleteModal" id="agregarMenu" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header bg-gradient-info">
													<h5 class="modal-title" id="exampleModalLabel">Agregar menú</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body text-left">
													<form action="{{url('admin/menu')}}" method="post" class="form-horizontal">
														@csrf
														<div class="form-group">
															<label for="">Titulo</label>
															<input type="text" class="form-control" name="titulo" id="agregarMenuTitulo" placeholder="Ingrese el titulo aquí" required>
														</div>
														<div class="form-group">
															<label for="">Tipo de contenido</label>
															<select name="tipo" id="tipo" class="form-control">
																<option value="inicio">Inicio</option>
																<option value="url">URL</option>
																<option value="pagina">Pagina</option>
																<option value="producto">Producto</option>
																<option value="categoria_tienda">Categoria tienda</option>
																<option value="entrada">Entrada</option>
																<option value="categoria_blog">Categoria blog</option>
															</select>
														</div>	

														<div class="form-group menu url hide">
															<label for="">URL</label>
															<input type="text" name="contenido_url" class="form-control" placeholder="Ingrese la URL aquí">
														</div>
														
														<div class="form-group menu pagina hide">
															<label for="">Pagina</label>
															<select name="contenido_pagina" id="" class="form-control">
																@foreach(empresa()->paginas as $pagina)
																	<option value="pagina/{{$pagina->url}}">{{$pagina->titulo}}</option>
																@endforeach
															</select>
														</div>

														<div class="form-group menu producto hide">
															<label for="">Producto</label>
															<select name="contenido_producto" id="" class="form-control">
																@foreach(empresa()->productos as $producto)
																	<option value="producto/{{$producto->url}}">{{$producto->nombre}}</option>
																@endforeach
															</select>
														</div>
														
														<div class="form-group menu categoria_tienda hide">
															<label for="">Categoria</label>
															<select name="contenido_categoria_tienda" id="" class="form-control">
																@foreach(empresa()->categorias as $categoria)
																	<option value="categoria/{{$categoria->url}}">{{$categoria->titulo}}</option>
																@endforeach
															</select>
														</div>
														
														<div class="form-group menu entrada hide">
															<label for="">Entradas</label>
															<select name="contenido_entrada" id="" class="form-control">
																@foreach(empresa()->blogEntradas as $entrada)
																	<option value="blog/{{$entrada->url}}">{{$entrada->titulo}}</option>
																@endforeach
															</select>
														</div>

														<div class="form-group menu categoria_blog hide">
															<label for="">Categorias</label>
															<select name="contenido_categoria_blog" id="" class="form-control">
																@foreach(empresa()->blogCategorias as $categoria)
																	<option value="blog/catgoria/{{$categoria->url}}">{{$categoria->titulo}}</option>
																@endforeach
															</select>
														</div>
														<hr>
														<div class="form-group text-right">
															<button class="btn btn-info">
																<i class="fa fa-save"></i>
																Guardar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							@if($menus->count() == 0)
								@include('ayuda.sin_registros')
							@else
								<div class="table table-responsive">
									<table class="table table-striped">
										<thead>
											<tr>
												<th></th>
												<th>Titulo</th>
												<th>URL</th>
												<th>Eliminar</th>
											</tr>
										</thead>
										<tbody id="sortable">
											@foreach($menus->sortBy('numero') as $menu)
												<tr class="tr_menu" attr-id="{{$menu->id}}">
													<td>
														<i class="fa fa-bars text-secondary"></i>
													</td>
													<td>{{$menu->titulo}}</td>
													<td>
														<a href="{{url($menu->url)}}" target="_blank">
															{{$menu->url}}
														</a>
													</td>
													<td>
														@include('ayuda.eliminar', ['id' => $menu->id, 'ruta' => url('admin/menu', $menu->id)])
													</td>
												</tr>	
											@endforeach
										</tbody>
									</table>
								</div>
							@endif
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$( "#sortable" ).sortable({
				opacity: 0.5,
				stop: function()
				{
					var array = [];

					$('.tr_menu').each(function(index, elemento)
					{
						array.push([$(this).attr('attr-id'), index]);
					})

					$.ajax({
						url: "{{url('admin/menu/mover')}}",
						type: 'POST',
						dataType: "JSON",
						data: {
							"_token": $('meta[name="csrf-token"]').attr('content'),
							"array": array,
						}
					});
				}
			});

			$('#agregarMenu').on('shown.bs.modal', function () {
			  $('#agregarMenuTitulo').trigger('focus')
			})

			$('#tipo').on('change', function()
			{
				var seleccion = $('#tipo option:selected').val();

				$('.menu').hide();
				$('.menu.'+seleccion).show();
			});
		});
	</script>
@endsection