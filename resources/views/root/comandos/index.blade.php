@extends('layouts.root', ['menu_activo' => 'comandos'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Comandos</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item">Home</li>
						<li class="breadcrumb-item active">Comandos</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<div class="col">
						<h5 class="card-title">
							Lista de comandos
						</h5>
					</div>
					<div class="col text-right">
						<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarComando">
							<i class="fa fa-plus"></i>
							Agregar comando
						</button>

						<div class="modal fade deleteModal" id="agregarComando" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header bg-gradient-info">
										<div class="col">
											<h5 class="modal-title">
												Agregar comando
											</h5>
										</div>
										<div class="col text-right">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
									<div class="modal-body text-left">
										<div class="row">
											<div class="col-12">
												<form action="{{url('root/comandos')}}" class="form-horizontal" method="post">
													@csrf
													<div class="form-group">
														<label for="">Comando</label>
														<input type="text" class="form-control" name="signature" placeholder="Comando">
													</div>
													<div class="form-group">
														<label for="">Descripción</label>
														<input type="text" class="form-control" name="descripcion" placeholder="Descripción">
													</div>
													<hr>
													<div class="row">
														<div class="col">
															<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
																Atras
															</button>
														</div>
														<div class="col text-right">
															<button class="btn btn-info btn-block">
																<i class="fa fa-save"></i>
																Guardar
															</button>	
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					@if($comandos->count() == 0)
						@include('ayuda.sin_registros')
					@else
					<div class="row">
						<div class="col">
							<div class="table table-responsive">
								<table class="table table-striped">
									<tr>
										<th>Comando</th>
										<th>Descripción</th>
										<th>Estado</th>
										<th>Inicio ejecución</th>
										<th>Fin ejecución</th>
										<th class="text-center">Ejecutar</th>
										<th class="text-center">Cambiar estado</th>
										<th class="text-center">Historial</th>
										<th class="text-center">Eliminar</th>
									</tr>
									@foreach($comandos as $comando)
										<tr>
											<td>{{$comando->signature}}</td>
											<td>{{$comando->descripcion}}</td>
											<td>
												@switch($comando->estado)
													@case('activo')
														<span class="badge badge-success">
															Activo
														</span>
													@break
													@case('bloqueado')
														<span class="badge badge-danger">
															Bloqueado
														</span>
													@break
													@case('ejecutando')
														<span class="badge badge-info">
															Ejecutando
														</span>
													@break
												@endswitch
											</td>
											<td>{{$comando->inicio == null ? "--" : $comando->inicio->format('d/m/Y H:i')}}</td>
											<td>{{$comando->fin == null ? "--" : $comando->fin->format('d/m/Y H:i')}}</td>
											<td class="text-center">
												<button type="button" class="btn btn-success" data-toggle="modal" data-target="#ejecutarComando_{{$comando->id}}">
													<i class="fa fa-play"></i>
												</button>

												<div class="modal fade deleteModal" id="ejecutarComando_{{$comando->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
													<div class="modal-dialog modal-dialog-centered" role="document">
														<div class="modal-content">
															<div class="modal-header bg-gradient-success">
																<div class="col">
																	<h5 class="modal-title">
																		Ejecutar comando
																	</h5>
																</div>
																<div class="col text-right">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
															</div>
															<div class="modal-body text-center">
																<div class="row">
																	<div class="col-12">
																		<i class="fa fa-4x fa-exclamation-circle text-secondary"></i>
																		<br>
																		<br>
																		<h4>¿Quiéres ejecutar el comando?</h4>
																		<hr>
																		<div class="row">
																			<div class="col">
																				<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
																					NO
																				</button>
																			</div>
																			<div class="col text-right">
																				<a href="{{url('root/comandos/ejecutar', $comando->id)}}" class="btn btn-success btn-block">
																					SI
																				</a>	
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</td>
											<td class="text-center">
												<button type="button" class="btn btn-info" data-toggle="modal" data-target="#bloquearComando_{{$comando->id}}">
													<i class="fa fa-random"></i>
												</button>

												<div class="modal fade deleteModal" id="bloquearComando_{{$comando->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
													<div class="modal-dialog modal-dialog-centered" role="document">
														<div class="modal-content">
															<div class="modal-header bg-gradient-info">
																<div class="col">
																	<h5 class="modal-title">
																		Cambiar estado
																	</h5>
																</div>
																<div class="col text-right">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
															</div>
															<div class="modal-body text-center">
																<div class="row">
																	<div class="col-12">
																		<i class="fa fa-4x fa-exclamation-circle text-secondary"></i>
																		<br>
																		<br>
																		<h4>¿Quiéres cambiar el estado del comando?</h4>
																		<hr>
																		<div class="row">
																			<div class="col">
																				<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
																					NO
																				</button>
																			</div>
																			<div class="col text-right">
																				<a href="{{url('root/comandos/bloquear', $comando->id)}}" class="btn btn-info btn-block">
																					SI
																				</a>	
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</td>
											<td class="text-center">
												<a href="{{url('root/comandos', $comando->id)}}" class="btn btn-dark">
													<i class="fa fa-list"></i>
												</a>
											</td>
											<td class="text-center">
												@include('ayuda.eliminar', ['id' => $comando->id, 'ruta' => url('root/comandos', $comando->id)])
											</td>
										</tr>
									@endforeach	
								</table>
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>

			<div class="card">
				<div class="card-header">
					<div class="col">
						<h5 class="card-title">
							Schedules
						</h5>
					</div>
					<div class="col text-right">
						<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarSchedule">
							<i class="fa fa-plus"></i>
							Agregar Schedule
						</button>

						<div class="modal fade deleteModal" id="agregarSchedule" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header bg-gradient-info">
										<div class="col">
											<h5 class="modal-title">
												Agregar Schedule
											</h5>
										</div>
										<div class="col text-right">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
									</div>
									<div class="modal-body text-left">
										<div class="row">
											<div class="col-12">
												<form action="{{url('root/comandosSchedules')}}" class="form-horizontal" method="post">
													@csrf
													<div class="form-group">
														<label for="">Comando</label>
														<input type="text" class="form-control" name="signature" placeholder="Comando">
													</div>
													<div class="form-group">
														<label for="">CRON</label>
														<input type="text" class="form-control" name="cron" placeholder="* * * * *">
													</div>
													<hr>
													<div class="row">
														<div class="col">
															<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
																Atras
															</button>
														</div>
														<div class="col text-right">
															<button class="btn btn-info btn-block">
																<i class="fa fa-save"></i>
																Guardar
															</button>	
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					@if($schedules->count() == 0)
						@include('ayuda.sin_registros')
					@else
						<div class="table table-responsive">
							<table class="table table-striped">
								<tr>
									<th>Comando</th>
									<th>Estado</th>
									<th>CRON</th>
									<th class="text-center">Cambiar</th>
									<th class="text-center">Eliminar</th>
								</tr>
								@foreach($schedules as $schedule)
									<tr>
										<td>
											{{$schedule->signature}}
										</td>
										<td>
											@switch($schedule->estado)
												@case('activo')
													<span class="badge badge-success">
														Activo
													</span>
												@break
												@case('bloqueado')
													<span class="badge badge-danger">
														Bloqueado
													</span>
												@break
											@endswitch
										</td>
										<td>
											{{$schedule->cron}}
										</td>
										<td class="text-center">
											<button type="button" class="btn btn-info" data-toggle="modal" data-target="#bloquearComando_{{$schedule->id}}">
												<i class="fa fa-random"></i>
											</button>

											<div class="modal fade deleteModal" id="bloquearComando_{{$schedule->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
												<div class="modal-dialog modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-header bg-gradient-info">
															<div class="col">
																<h5 class="modal-title">
																	Cambiar estado
																</h5>
															</div>
															<div class="col text-right">
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
														</div>
														<div class="modal-body text-center">
															<div class="row">
																<div class="col-12">
																	<i class="fa fa-4x fa-exclamation-circle text-secondary"></i>
																	<br>
																	<br>
																	<h4>¿Quiéres cambiar el estado de la tarea?</h4>
																	<hr>
																	<div class="row">
																		<div class="col">
																			<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
																				NO
																			</button>
																		</div>
																		<div class="col text-right">
																			<a href="{{url('root/comandosSchedules/cambiar', $schedule->id)}}" class="btn btn-info btn-block">
																				SI
																			</a>	
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</td>
										<td class="text-center">
											@include('ayuda.eliminar', ['id' => 'schedule_'.$schedule->id, 'ruta' => url('root/comandosSchedules', $schedule->id)])
										</td>
									</tr>
								@endforeach
							</table>
						</div>
					@endif
				</div>
			</div>
		</div>
	</section>
</div>
@endsection