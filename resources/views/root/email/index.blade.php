@extends('layouts.root', ['menu_activo' => 'email'])

@section('contenido')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Registro de emails</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
              <li class="breadcrumb-item active">Registro de emails</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
    	
      <div class="container-fluid">

        <!-- Main row -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header border-1">
                  <div class="row">
                    <div class="col">
                      <h3 class="card-title">
                        Registro de emails
                      </h3>
                    </div>
                    <div class="col text-right">
                      @include('ayuda.eliminar', ['id' => 1, 'ruta' => url('root/email', 1)])
                      <b class="text-danger">Dejar los ultimos 3 meses</b>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="table table-responsive">
                      <table class="table table-striped">
                         <tbody>
                          <tr>
                            <th>Fecha</th>
                            <th>Empresa</th>
                            <th>Estado</th>
                            <th>Nombre</th>
                            <th>Destinatario</th>
                            <th>Ver</th>
                            <th>Reenviar</th>
                          </tr>
                          @foreach($emails->sortByDesc('id') as $email)
                          	<tr>
                          		<td>{{$email->created_at->format('d/m/Y H:i')}}</td>
	                          	<td>
                                @if($email->empresa_id == "root")
                                  <b class="badge badge-dark">
                                    Sistema
                                  </b>  
                                @elseif(isset($email->empresa->nombre))
                                  <b class="badge badge-info">
                                    {{$email->empresa->nombre}}
                                  </b>
                                @else
                                  --
                                @endif
                              </td>
	                          	<td>
	                          		@switch($email->estado)
	                          			@case('pendiente')
	                          				<b class="badge badge-warning">
	                          					Pendiente
	                          				</b>
	                          			@break
	                          			@case('enviado')
	                          				<b class="badge badge-success" data-toggle="tooltip" data-placement="top" title="{{$email->detalle}}">
	                          					Enviado
	                          				</b>
	                          			@break
	                          			@case('error')
	                          				<b class="badge badge-danger" data-toggle="tooltip" data-placement="top" title="{{$email->detalle}}"> 
	                          					Error
	                          				</b>
	                          			@break
	                          		@endswitch
	                          	</td>
	                          	<td>{{$email->nombre}}</td>
	                          	<td>{{$email->destinatario}}</td>
	                          	<td>
	                          		<a href="{{url('root/email', $email->id)}}" class="btn btn-success" target="_blank">
	                          			<i class="fa fa-eye"></i>
	                          		</a>
	                          	</td>
	                          	<td>
	                          		<a href="{{url('root/email/reenviar', $email->id)}}" class="btn btn-info">
	                          			<i class="fa fa-undo"></i>
	                          		</a>
	                          	</td>
                          	</tr>
                          @endforeach
                        </tbody>
                      </table>  
                    </div>
                  </div>
                  @include('ayuda.links', ['link' => $emails])
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
  </div>
@endsection

@section('scripts')
  <script>
    $('.badge').tooltip()
  </script>
@endsection