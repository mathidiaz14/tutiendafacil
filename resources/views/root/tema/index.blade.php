@extends('layouts.root', ['menu_activo' => 'tema'])

@section('contenido')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Temas cargados</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
              <li class="breadcrumb-item active">Temas cargados</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      
      <div class="container-fluid">

        <!-- Main row -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header border-1">
                  <div class="row">
                    <div class="col">
                      <h3 class="card-title">
                        Temas cargados
                      </h3>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="table table-responsive">
                      <table class="table table-striped">
                         <tbody>
                          <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Carpeta</th>
                            <th>Tipo</th>
                            <th>Precio</th>
                            <th>Eliminar</th>
                          </tr>
                          @foreach($temas->sortBy('id') as $tema)
                            <tr>
                              <td>{{$tema->id}}</td>
                              <td>{{$tema->nombre}}</td>
                              <td>{{$tema->carpeta}}</td>
                              <td>{{$tema->tipo}}</td>
                              <td>{{$tema->precio == 0 ? "Gratis" : $tema->precio}}</td>
                              <td>
                                @include('ayuda.eliminar', ['id' => $tema->id, 'ruta' => url('root/tema', $tema->id)])
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
  </div>
@endsection
