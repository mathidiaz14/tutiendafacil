@extends('layouts.root', ['menu_activo' => 'respaldo'])

@section('css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
@endsection

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Archivos respaldados en AWS S3</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
            <li class="breadcrumb-item active">Respaldo</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <!-- Main content -->
  <section class="content">
    
    <div class="container-fluid">

      <!-- Main row -->
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header border-1">
              <div class="row">
                <div class="col">
                  <h3 class="card-title">
                    Archivos respaldados en AWS S3
                  </h3>
                </div>
                <div class="col text-right">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#agregarArchivo">
                    <i class="fa fa-upload"></i>
                    Cargar archivo
                  </button>

                  <!-- Modal -->
                  <div class="modal fade" id="agregarArchivo" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header bg-gradient-info">
                          <div class="col-10 text-left">
                            <i class="fa fa-upload"></i>
                            Cargar archivo
                          </div>
                          <div class="col-2">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>
                        <div class="modal-body text-left">
                          <form action="{{url('root/respaldo')}}" method="post" class="dropzone" id="cargaDeArchivos"  enctype="multipart/form-data">
                            @csrf
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="table table-responsive">
                  <table class="table table-striped">
                   <tbody>
                    <tr>
                      <th>Nombre</th>
                      <th>Tamaño</th>
                      <th>Fecha ultima modificación</th>
                      <th>Descargar</th>
                      <th>Eliminar</th>
                    </tr>
                    @foreach($archivos as $archivo)
                    <tr>
                      <td>{{$archivo}}</td>
                      <td>
                        @php
                          $tamaño = Storage::disk('s3')->size($archivo) ;

                          if($tamaño < 1024)
                            $tamaño = round($tamaño / 1024, 1)." KB"; 
                          elseif($tamaño > 1024)
                            $tamaño = round($tamaño / 1048576, 1)." MB";

                        @endphp
                        {{$tamaño}}
                      </td>
                      <td>{{Carbon\Carbon::createFromTimestamp(Storage::disk('s3')->lastModified($archivo))->format('d/m/Y H:i')}}</td>
                      <td>
                        <a href="{{url('root/respaldo', $archivo)}}" class="btn btn-success">
                          <i class="fa fa-download"></i>
                        </a>
                      </td>
                      <td>
                        @include('ayuda.eliminar', ['id' => $loop->iteration, 'ruta' => url('root/respaldo', $archivo)])
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>  
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
@endsection

@section('scripts')
  <script>
    $('#cargaDeArchivos').dropzone({ 
        dictDefaultMessage: "Haz click aquí o arrastra las imágenes para cargarlas",
    });
  </script>
@endsection
