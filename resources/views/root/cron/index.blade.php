@extends('layouts.root', ['menu_activo' => 'cron'])

@section('contenido')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Trabajos CRON</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
              <li class="breadcrumb-item active">Trabajos CRON</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
    	
      <div class="container-fluid">

        <!-- Main row -->
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header border-1">
                  <div class="row">
                    <div class="col">
                      <h3 class="card-title">
                        Trabajos CRON
                      </h3>
                    </div>
                    <div class="col text-right">
                      @include('ayuda.eliminar', ['id' => 1, 'ruta' => url('root/cron', 1)])
                      <b class="text-danger">Dejar los ultimos 3 meses</b>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="table table-responsive">
                      <table class="table table-striped">
                         <tbody>
                          <tr>
                            <th>Fecha</th>
                            <th>Comando</th>
                            <th>Mensaje</th>
                          </tr>
                          @foreach($crons as $registro)
                            <tr>
                              <td>
                                {{$registro->created_at->format('d/m/Y H:i')}}
                              </td>
                              <td>
                                {{$registro->comando}}
                              </td>
                              <td>
                                {!!$registro->mensaje!!}
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>  
                    </div>
                  </div>
                  @include('ayuda.links', ['link' => $crons])
                </div>
              </div>
            </div>
          </div>
      </div>
    </section>
  </div>
@endsection
