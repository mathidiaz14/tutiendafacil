@extends('layouts.root', ['menu_activo' => 'empresa'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Empresas</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb-item active">Empresas</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		
		<div class="container-fluid">

			<!-- Main row -->
			<div class="row">
				<!-- right col (We are only adding the ID to make the widgets sortable)-->
				<section class="col-12">
					<!-- Map card -->
					<div class="card">
						<div class="card-header border-1">
							<div class="row">
								<div class="col">
									<h3 class="card-title">
										Empresas
									</h3>
								</div>
								<div class="col text-right">
									<a href="{{url('root/empresa/create')}}" class="btn btn-info">
										<i class="fa fa-plus"></i>
										Crear empresa
									</a>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="table table-responsive">
									<table class="table table-striped">
										<tbody>
											<tr>
												<th>#</th>
												<th>Nombre</th>
												<th>URL</th>
												<th>Estado</th>
												<th>Expira</th>
												<th>Carpeta</th>
												<th></th>
											</tr>
											@foreach($empresas->sortBy('id') as $empresa)
											<tr>
												<td>{{$empresa->id}}</td>
												<td>{{$empresa->nombre}}</td>
												<td><a href="http://{{$empresa->URL}}" target="_blank">{{$empresa->URL}}</a></td>
												<td>
													@switch($empresa->estado)
													@case('pendiente')
													<label for="" class="badge badge-warning">
														Pendiente
													</label>
													@break

													@case('creando')
													<label for="" class="badge badge-info">
														Creando
													</label>
													@break

													@case('completo')
													<label for="" class="badge badge-success">
														Completo
													</label>
													@break

													@case('deshabilitado')
													<label for="" class="badge badge-danger">
														Deshabilitado
													</label>
													@break

													@endswitch
												</td>
												<td>
													@if($empresa->expira != null) 
													{{$empresa->expira->format('d/m/Y')}} 
													@endif
												</td>
												<td>
													{{$empresa->carpeta}}
												</td>

												<td class="text-center">
													<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
														<div class="btn-group" role="group">
															<button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																Opciones
															</button>
															<div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
																<a href="{{url('root/empresa',$empresa->id)}}" class="dropdown-item">Ver</a>
																
																<a class="dropdown-item" href="#" data-toggle="modal" data-target="#concederControl_{{$empresa->id}}">Controlar</a>
																
																<a class="dropdown-item" href="#" data-toggle="modal" data-target="#editarEmpresa_{{$empresa->id}}">Editar</a>

																@if($empresa->estado != "deshabilitado")
																<a href="{{url('root/empresa/deshabilitar', $empresa->id)}}" class="dropdown-item">
																	Deshabilitar
																</a>
																@else
																<a href="{{url('root/empresa/deshabilitar', $empresa->id)}}" class="dropdown-item">
																	Habilitar
																</a>
																@endif
																<hr>
																<a class="dropdown-item" href="#" data-toggle="modal" data-target="#eliminarEmpresa_{{$empresa->id}}">
																	Eliminar
																</a>
															</div>
														</div>
													</div>
												</td>
											</tr>
											<!-- Controlar -->
											<div class="modal fade" id="concederControl_{{$empresa->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog" role="document">
													<div class="modal-content text-left">
														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLabel">Control: {{$empresa->nombre}}</h5>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<form action="{{url('root/empresa/controlar', $empresa->id)}}" class="form-horizontal" method="post">
																@csrf
																<div class="form-group">
																	<label for="">Codigo</label>
																	<input type="text" class="form-control" name="codigo" placeholder="Codigo de control">
																</div>
																<hr>
																<div class="row">
																	<div class="col">
																		<button type="button" class="btn btn-secondary" data-dismiss="modal">
																			<i class="fa fa-chevron-left"></i>
																			Atras
																		</button>
																	</div>
																	<div class="col text-right">
																		<button class="btn btn-primary">
																			<i class="fa fa-paper-plane"></i>
																			Enviar
																		</button>
																	</div>
																</div>  
															</form>  
														</div>
													</div>
												</div>
											</div>

											<!-- Editar -->
											<div class="modal fade" id="editarEmpresa_{{$empresa->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content text-left">
														<div class="modal-header">
															<h5 class="modal-title" id="exampleModalLabel">Editar</h5>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<form action="{{url('root/empresa', $empresa->id)}}" class="form-horizontal" method="post">
																@csrf
																@method('PATCH')
																<div class="row">
																	<div class="form-group col-12 col-md-6">
																		<label for="">Nombre</label>
																		<input type="text" name="nombre" id="nombre" class="form-control" value="{{$empresa->nombre}}" required="">
																	</div>
																	<div class="form-group col-12 col-md-6">
																		<label for="">RUT</label>
																		<input type="text" name="rut" id="rut" class="form-control" value="{{$empresa->RUT}}">
																	</div>
																	<div class="form-group col-12 col-md-6">
																		<label for="">URL</label>
																		<input type="text" name="URL" id="URL" class="form-control" value="{{$empresa->URL}}">
																	</div>
																	<div class="form-group col-12 col-md-6">
																		<label for="">URL1</label>
																		<input type="text" name="URL1" id="URL1" class="form-control" value="{{$empresa->URL1}}">
																	</div>
																	<div class="form-group col-12 col-md-6">
																		<label for="">URL2</label>
																		<input type="text" name="URL2" id="URL2" class="form-control" value="{{$empresa->URL2}}">
																	</div>
																	<div class="form-group col-12 col-md-6">
																		<label for="">URL3</label>
																		<input type="text" name="URL3" id="URL3" class="form-control" value="{{$empresa->URL3}}">
																	</div>
																	<div class="form-group col-12 col-md-6">
																		<label for="">Estado</label>
																		<select name="estado" id="" class="form-control">
																			@if($empresa->estado == "pendiente")
																			<option value="pendiente" selected="">Pendiente</option>
																			<option value="completo">Completo</option>
																			<option value="creando">Creando</option>
																			<option value="deshabilitado">Deshabilitado</option>
																			@elseif($empresa->estado == "completo")
																			<option value="pendiente">Pendiente</option>
																			<option value="completo" selected="">Completo</option>
																			<option value="creando">Creando</option>
																			<option value="deshabilitado">Deshabilitado</option>
																			@elseif($empresa->estado == "creando")
																			<option value="pendiente" >Pendiente</option>
																			<option value="completo">Completo</option>
																			<option value="creando" selected="">Creando</option>
																			<option value="deshabilitado">Deshabilitado</option>
																			@elseif($empresa->estado == "deshabilitado")
																			<option value="pendiente" >Pendiente</option>
																			<option value="completo">Completo</option>
																			<option value="creando">Creando</option>
																			<option value="deshabilitado" selected="">Deshabilitado</option>
																			@endif
																		</select>
																	</div>
																	<div class="form-group col-12 col-md-6">
																		<label for="">Expira</label>
																		<input type="date" name="expira" id="expira" class="form-control" value="@if($empresa->expira != null){{$empresa->expira->format('Y-m-d')}}@endif">
																	</div>

																</div>
																<hr>
																<div class="row">
																	<div class="col">
																		<button type="button" class="btn btn-secondary" data-dismiss="modal">
																			<i class="fa fa-chevron-left"></i>
																			Atras
																		</button>
																	</div>
																	<div class="col text-right">
																		<button class="btn btn-primary">
																			<i class="fa fa-save"></i>
																			Guardar
																		</button>
																	</div>
																</div>  
															</form>  
														</div>
													</div>
												</div>
											</div>


											<!-- Eliminar -->
											<div class="modal fade deleteModal" id="eliminarEmpresa_{{$empresa->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
												<div class="modal-dialog modal-dialog-centered" role="document">
													<div class="modal-content">
														<div class="modal-header bg-gradient-danger">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body text-center">
															<i class="fa fa-exclamation-triangle fa-4x text-secondary"></i>
															<br>
															<br>
															<h4>¿Desea eliminar la empresa?</h4>
															<hr>
															<div class="row">
																<div class="col">
																	<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
																		NO
																	</button>
																</div>
																<div class="col">
																	<form action="{{ url('root/empresa', $empresa->id)}} }}" method="POST">
																		@csrf
																		<input type='hidden' name='_method' value='DELETE'>
																		<button class="btn btn-danger btn-block">
																			SI
																		</button>
																	</form>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											@endforeach
										</tbody>
									</table>
								</div>
							</div>
							<div class="row text-center">
								{{$empresas->links()}}
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection