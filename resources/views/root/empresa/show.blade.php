@extends('layouts.root', ['menu_activo' => 'empresa'])

@section('contenido')
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{$empresa->nombre}}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
              <li class="breadcrumb-item"><a href="{{url('root/empresa')}}">Empresas</a></li>
              <li class="breadcrumb-item active">#{{$empresa->id}}</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
    	
      <div class="container-fluid">

        <!-- Main row -->
          <div class="row">
            @include('root.empresa.partes.registros')            
            @include('root.empresa.partes.ventas')
            @include('root.empresa.partes.usuarios')
            @include('root.empresa.partes.plugins')
            @include('root.empresa.partes.temas')
          </div>
      </div>
    </section>
  </div>
@endsection

@section('scripts')
	<script>
		$(document).ready(function()
		{
			$('.btn_contraseña').click(function()
			{
				$('.contraseña').val(Math.random().toString(36).substring(2));
			});
		});
	</script>
@endsection