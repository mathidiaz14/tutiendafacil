<div class="col-12">
	<div class="card collapsed-card">
		<div class="card-header border-1">
			<h3 class="card-title">
				Usuarios ({{count($empresa->usuarios)}})
			</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
					<i class="fas fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="table table-responsive">
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Email</th>
								<th>CI</th>
								<th>Editar</th>
								<th>Eliminar</th>
							</tr>
							@foreach($empresa->usuarios as $usuario)
							<tr>
								<td>{{$usuario->id}}</td>
								<td>{{$usuario->nombre}}</td>
								<td>{{$usuario->email}}</td>
								<td>{{$usuario->ci}}</td>
								<td>

									<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editarUsuario_{{$usuario->id}}">
										<i class="fa fa-edit"></i>
									</button>

									<!-- Modal -->
									<div class="modal fade" id="editarUsuario_{{$usuario->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content text-left">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<form action="{{url('root/usuario', $usuario->id)}}" class="form-horizontal" method="post">
														@csrf
														@method('PATCH')
														<div class="form-group">
															<label for="">Nombre</label>
															<input type="text" name="nombre" id="nombre" class="form-control" value="{{$usuario->nombre}}" required="">
														</div>
														<div class="form-group">
															<label for="">Email</label>
															<input type="email" name="email" id="email" class="form-control" value="{{$usuario->email}}">
														</div>
														<div class="form-group">
															<label for="">CI</label>
															<input type="text" name="ci" id="ci" class="form-control" value="{{$usuario->ci}}">
														</div>
														<div class="form-group">
															<label for="">Contraseña</label>
															<div class="row">
																<div class="col-10">
																	<input type="text" name="contrasena" id="contraseña" class="contraseña form-control">
																</div>
																<div class="col-2">
																	<a class="btn btn-info btn_contraseña btn-block">
																		<i class="fa fa-dice"></i>
																	</a>
																</div>
															</div>
														</div>
														<hr>
														<div class="row">
															<div class="col">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">
																	<i class="fa fa-chevron-left"></i>
																	Atras
																</button>
															</div>
															<div class="col text-right">
																<button class="btn btn-primary">
																	<i class="fa fa-save"></i>
																	Guardar
																</button>
															</div>
														</div>  
													</form>  
												</div>
											</div>
										</div>
									</div>
								</td>
								<td>
									@include('ayuda.eliminar', ['id' => $usuario->id, 'ruta' => url('root/usuario', $usuario->id)])
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>