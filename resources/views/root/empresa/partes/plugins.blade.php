<div class="col-12">
	<div class="card collapsed-card">
		<div class="card-header border-1">
			<h3 class="card-title">
				Plugins instalados ({{count($empresa->plugins)}})
			</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
					<i class="fas fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			@if(count($empresa->plugins) == 0)
			@include('ayuda.sin_registros')
			@else
			<div class="row">
				<div class="table table-responsive">
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Nombre</th>
								<th>Estado</th>
								<th>Fecha instalado</th>
							</tr>
							@foreach($empresa->plugins as $plugin)
							<tr>
								<td>{{$plugin->nombre}}</td>
								<td>{{$plugin->pivot->estado}}</td>
								<td>{{$plugin->pivot->created_at != null ? $plugin->pivot->created_at->format('d/m/Y') : ""}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>  
				</div>
			</div>
			@endif
		</div>
		<div class="card-footer">
			<div class="row">
				<div class="col-12 text-right">
					<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#agregar_plugin">
						<i class="fa fa-plus mr-2"></i>
						Agregar
					</button>

					<div class="modal fade" id="agregar_plugin" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
						<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">
										Agregar Plugin
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body text-center">
									<div class="table table-responsive">
										<table class="table table-striped">
											<tbody>
												<tr>
													<th>Imagen</th>
													<th>Nombre</th>
													<th>Tipo</th>
													<th></th>
												</tr>
												@foreach(App\Models\Plugin::all() as $plugin)
													<tr>
														<td><img src="{{asset($plugin->imagen)}}" alt="" width="100px"></td>
														<td>{{$plugin->nombre}}</td>
														<td>{{$plugin->tipo}}</td>
														<td><a href="" class="btn btn-info">Instalar</a></td>
													</tr>
												@endforeach
											</tbody>
										</table>
									</div>		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
</div>