<div class="col-12">
	<div class="card collapsed-card">
		<div class="card-header border-1">
			<h3 class="card-title">
				Registros ({{$registros->total()}})
			</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
					<i class="fas fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="table table-responsive">
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>Fecha</th>
								<th>Estado</th>
								<th>Usuario</th>
								<th>Objeto</th>
								<th>Accion</th>
								<th>Mensaje</th>
							</tr>
							@foreach($registros as $registro)
							<tr>
								<td>
									{{$registro->created_at->format('d/m/Y H:i')}}
								</td>
								<td>
									@include('ayuda.log_estado', ['estado' => $registro->estado])
								</td>
								<td>
									@if($registro->usuario_id == "root")
										Sistema
									@else
										<a href="{{url('root/usuario', $registro->usuario_id)}}">
											{{$registro->usuario->nombre}}
										</a>
									@endif
								</td>
								<td>
									{{$registro->objeto}}
								</td>
								<td>
									{{$registro->accion}}
								</td>
								<td>
									{{$registro->mensaje}}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>  
				</div>
			</div>
			@include('ayuda.links', ['link' => $registros])
		</div>
	</div>
</div>