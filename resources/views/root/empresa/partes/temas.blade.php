<div class="col-12">
	<div class="card collapsed-card">
		<div class="card-header border-1">
			<h3 class="card-title">
				Temas instalados ({{count($empresa->temas)}})
			</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
					<i class="fas fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			@if(count($empresa->temas) == 0)
				@include('ayuda.sin_registros')
			@else
				<div class="row">
					<div class="table table-responsive">
						<table class="table table-striped">
							<tbody>
								<tr>
									<th>Imagen</th>
									<th>Nombre</th>
									<th>Estado</th>
									<th>Fecha instalado</th>
								</tr>
								@foreach($empresa->temas as $tema)
								<tr>
									<td>
										<img src="{{asset('empresas/'.$empresa->id.'/'.$tema->pivot->carpeta.'/screenshot.png')}}" alt="" width="100px">
									</td>
									<td>{{$tema->nombre}}</td>
									<td>{{$tema->pivot->estado}}</td>
									<td>{{$tema->pivot->created_at != null ? $tema->pivot->created_at->format('d/m/Y') : ""}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>  
					</div>
				</div>
			@endif
		</div>
	</div>
</div>