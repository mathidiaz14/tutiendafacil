<div class="col-12">
	<div class="card collapsed-card">
		<div class="card-header border-1">
			<h3 class="card-title">
				Ventas ({{$ventas->total()}})
			</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-secondary btn-sm" data-card-widget="collapse" title="Collapse">
					<i class="fas fa-plus"></i>
				</button>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="table table-responsive">
					<table class="table table-striped">
						<tbody>
							<tr>
								<th>#</th>
								<th>Estado</th>
								<th>Fecha</th>
								<th>Codigo</th>
								<th>Ver</th>
							</tr>
							@foreach($ventas->sortByDesc('created_at') as $venta)
							<tr>
								<td>{{$venta->id}}</td>
								<td>@include('ayuda.venta_estado')</td>
								<td>{{$venta->created_at->format('d/m/Y')}}</td>
								<td>{{$venta->codigo}}</td>
								<td>
									<a href="{{url('root/venta', $venta->id)}}" class="btn btn-success">
										<i class="fa fa-eye"></i>
									</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>	
				</div>
				@include('ayuda.links', ['link' => $ventas])
			</div>
		</div>
	</div>
</div>