@extends('layouts.root', ['menu_activo' => 'empresa'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Nueva empresa</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{url('root/empresa')}}">Empresas</a></li>
            <li class="breadcrumb-item active">Nueva empresas</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    
    <div class="container-fluid">

      <!-- Main row -->
      <div class="row">
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-12">
          <!-- Map card -->
          <div class="card">
            <div class="card-body">
              <form action="{{url('root/empresa')}}" method="post" class="form-horizontal">
                @csrf
                <div class="row">
                  <div class="col-12">
                    <h5 class="alert alert-info"><b>Datos de la empresa</b></h5>
                  </div>
                  <div class="form-group col-12 col-md-6">
                    <label for="">Nombre de la empresa</label>
                    <input type="text" name="nombre" class="form-control" placeholder="Nombre del negocio" required="">
                  </div>

                  <div class="form-group col-12 col-md-6">
                    <label for="">URL</label>
                    <div class="row">
                      <div class="col-10 url_col">
                        <input type="text" class="form-control url" name="url" placeholder="Dirección URL de la empresa" required="">
                        <span class="text-danger url_denegada" style="display:none;"><b>URL no disponible</b></span>
                      </div>
                      <div class="col-2">
                        <a class="btn btn-info btn-block text-white btn_comprobar">
                          <i class="fa fa-check"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-12">
                    <h5 class="alert alert-info"><b>Datos del usuario</b></h5>
                  </div>
                  <div class="form-group col-12 col-md-6">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" name="usuario_nombre" placeholder="Nombre de usuario" required="">
                  </div>
                  <div class="form-group col-12 col-md-6">
                    <label for="">Email</label>
                    <input type="email" class="form-control" name="usuario_email" placeholder="Email de usuario" required="">
                  </div>
                  <div class="form-group col-12 col-md-6">
                    <label for="">¿Colocar una contraseña manualmente?</label>
                    <br>
                    <small>En caso contrario se enviara un enlace para que el usuario coloque una.</small>

                    @include('ayuda.switch', ['nombre' => 'usuario_contrasena_manual', 'estado' => null])
                  </div>
                  <div class="form-group col-12 col-md-6">
                    <label for="">Contraseña</label>
                    <input type="password" class="form-control" id="usuario_contrasena" name="usuario_contrasena" placeholder="Nueva contraseña" disabled="">
                  </div>
                </div>
                <hr>
                <div class="form-group text-right">
                  <button class="btn btn-info">
                    <i class="fa fa-save"></i>
                    Guardar
                  </button>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    </div>
  </section>
</div>
@endsection

@section('scripts')
<script>
  $(document).ready(function()
  {
    $('#url').keyup(function()
    {
      $('#url').val($('#url').val().replace(/\s/g, "-").replace(/[^ a-z0-9]+/ig,"").toLowerCase());
    });

    $('#usuario_contrasena_manual').on('change', function()
    {
      if($('#usuario_contrasena').is(':disabled'))
      {
        $('#usuario_contrasena').prop('disabled', false);
        $('#usuario_recontrasena').prop('disabled', false);
      }else
      {
        $('#usuario_contrasena').prop('disabled', true);
        $('#usuario_recontrasena').prop('disabled', true);
      }

    });

    $('.btn_comprobar').click(function()
    {
      $.get("{{url('registrar/comprobar')}}/3"+$('.url').val(), function(respuesta)
      {
        if(respuesta == "si")
        {
          $('.url').removeClass('is-invalid');
          $('.url').addClass('is-valid');
          $('.url_denegada').hide();
        }
        else
        {
          $('.url').removeClass('is-valid');
          $('.url').addClass('is-invalid'); 
          $('.url_denegada').fadeIn();
        }
      });
    });
  });
</script>
@endsection