@extends('layouts.root', ['menu_activo' => 'ssl'])

@section('css')

@endsection

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Certificados SSL</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
						<li class="breadcrumb-item active">Certificados SSL</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->
	<!-- Main content -->
	<section class="content">
		
		<div class="container-fluid">

			<!-- Main row -->
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header border-1">
							<div class="row">
								<div class="col">
									<h3 class="card-title">
										Certificados SSL
									</h3>
								</div>
								<div class="col text-right">
									<button type="button" class="btn btn-info" data-toggle="modal" data-target="#agregarURL">
										<i class="fa fa-plus"></i>
										Agregar URL
									</button>

									<!-- Modal -->
									<div class="modal fade" id="agregarURL" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header bg-gradient-info">
													<div class="col-10 text-left">
														<i class="fa fa-plus"></i>
														Agregar URL
													</div>
													<div class="col-2">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
												</div>
												<div class="modal-body text-left">
													<form action="{{url('root/ssl')}}" method="post" class="form-horizontal">
														@csrf
														<div class="form-group">
															<label for="">URL</label>
															<input type="text" class="form-control" id="url" name="url" placeholder="URL">
														</div>
														<hr>
														<div class="form-group text-right">
															<button class="btn btn-info">
																<i class="fa fa-save"></i>
																Guardar
															</button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col">
									@if($urls->count() == 0)
										@include('ayuda.sin_registros')
									@else
										<div class="table table-responsive">
											<table class="table table-striped">
												<tbody>
													<tr>
														<th>Estado</th>
														<th>URL</th>
														<th>Vence</th>
														<th>Días restantes</th>
														<th>Eliminar</th>
													</tr>
													@foreach($urls as $url)
														<tr>
															<th>
																@php
																	$dias = control_ssl($url->url);
																@endphp
																
																@if(!$dias)
																	<span class="badge badge-danger">
																		Vencido
																	</span>
																@elseif($dias < 15)
																	<span class="badge badge-warning">
																		Por vencer
																	</span>
																@else
																	<span class="badge badge-success">
																		Correcto
																	</span>
																@endif
															</th>
															<th>
																{{$url->url}}
															</th>
															<th>
																	{{\Carbon\Carbon::now()->addDays($dias)->format('d/m/Y')}}
															</th>
															<th>
																{{$dias}}
															</th>
															<th>
																@include('ayuda.eliminar', ['id' => $url->id, 'ruta' => url('root/ssl', $url->id)])
															</th>
														</tr>
													@endforeach
												</tbody>
											</table>  
										</div>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
<script>
	$('#agregarURL').on('shown.bs.modal', function () {
	  	$('#url').trigger('focus');
	});
</script>
@endsection
