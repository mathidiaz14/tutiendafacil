<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-item ">
            <a href="{{ url('/admin') }}"
                class="nav-link @if ($menu_activo == 'inicio') active @endif">
                <i class="nav-icon fas fa-chevron-right"></i>
                <p>
                    Inicio
                </p>
            </a>
        </li>
        <li class="nav-item ">
            <a href="{{ url('root/empresa') }}"
                class="nav-link @if ($menu_activo == 'empresa') active @endif">
                <i class="nav-icon fas fa-chevron-right"></i>
                <p>
                    Empresas
                </p>
            </a>
        </li>
        <li class="nav-item ">
            <a href="{{ url('root/usuario') }}"
                class="nav-link @if ($menu_activo == 'usuario') active @endif">
                <i class="nav-icon fas fa-chevron-right"></i>
                <p>
                    Usuarios
                </p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ url('root/venta') }}"
                class="nav-link @if ($menu_activo == 'venta') active @endif">
                <i class="nav-icon fas fa-chevron-right"></i>
                <p>
                    Ventas
                </p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ url('root/codigo') }}"
                class="nav-link @if ($menu_activo == 'codigo') active @endif">
                <i class="nav-icon fas fa-chevron-right"></i>
                <p>
                    Codigos descuento
                </p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ url('root/administrar') }}"
                class="nav-link @if ($menu_activo == 'administrar') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Administrar
                </p>
            </a>
        </li>


        <li class="nav-item ">
            <a href="{{ url('root/email') }}"
                class="nav-link @if ($menu_activo == 'email') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Emails
                </p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ url('root/ssl') }}"
                class="nav-link @if ($menu_activo == 'ssl') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Control SSL
                </p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ url('root/comandos') }}"
                class="nav-link @if ($menu_activo == 'comandos') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Comandos
                </p>
            </a>
        </li>
        
        <li class="nav-item ">
            <a href="{{ url('root/cron') }}"
                class="nav-link @if ($menu_activo == 'cron') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Registros CRON
                </p>
            </a>
        </li>


        <li class="nav-item ">
          <a href="{{ url('root/respaldo') }}"
              class="nav-link @if ($menu_activo == 'respaldo') active @endif">
              <i class="nav-icon fa fa-chevron-right"></i>
              <p>
                  Respaldos (AWS)
              </p>
          </a>
      </li>

        <li class="nav-item ">
            <a href="{{ url('root/error') }}"
                class="nav-link @if ($menu_activo == 'error') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Errores
                </p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ url('root/ayuda') }}"
                class="nav-link @if ($menu_activo == 'ayuda') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Ayuda
                </p>
            </a>
        </li>

        <li class="nav-item ">
            <a href="{{ url('root/tema') }}"
                class="nav-link @if ($menu_activo == 'tema') active @endif">
                <i class="nav-icon fas fa-chevron-right"></i>
                <p>
                    Temas
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ url('root/log') }}"
                class="nav-link @if ($menu_activo == 'log') active @endif">
                <i class="nav-icon fa fa-chevron-right"></i>
                <p>
                    Log
                </p>
            </a>
        </li>
    </ul>
</nav>