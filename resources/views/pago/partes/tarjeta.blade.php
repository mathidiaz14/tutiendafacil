<div class="card">
	<div class="card-header" id="headingOne">
		<h2 class="mb-0">
			<button class="btn btn-link btn-block text-left text-secondary collapsed" type="button" data-toggle="collapse" data-target="#collapseTarjeta" aria-expanded="true" aria-controls="collapseTarjeta">
				<i class="fa fa-credit-card mr-3"></i>
				Credito / Debito
			</button>
		</h2>
	</div>

	<div id="collapseTarjeta" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
		<div class="card-body">
			<form id="form-checkout" class="form-horizontal">
				<progress value="0" class="progress-bar" style="display:none;">Cargando...</progress>
				<div class="row">
					<div class="form-group col-12 col-md-6">
						<label for="">Numero de tarjeta</label>
						<input class="form-control number_card" type="text" name="cardNumber" id="form-checkout__cardNumber" maxlength="16" required autofocus />
					</div>
					<div class="form-group col-6 col-md-3">
						<label for="">Vencimiento</label>
						<input class="form-control" type="text" name="cardExpirationDate" id="form-checkout__cardExpirationDate" maxlength="5" required />
					</div>
					<div class="form-group col-6 col-md-3">
						<label for="">CCV</label>
						<input class="form-control" type="text" name="securityCode" id="form-checkout__securityCode" maxlength="3" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required />
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="">Titular de la tarjeta</label>
						<input class="form-control name_card" type="text" name="cardholderName" id="form-checkout__cardholderName" required/>
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="">Cuotas</label>
						<select class="form-control" name="installments" id="form-checkout__installments" required></select>
					</div>

					<input class="form-control" type="text" name="identificationNumber" id="form-checkout__identificationNumber" value="{{$venta->cliente->documento}}" required style="display:none;" />
					<select class="form-control issuer" name="issuer" id="form-checkout__issuer" required style="display:none;"></select>
					<input class="form-control" type="email" name="cardholderEmail" id="form-checkout__cardholderEmail" value="{{$venta->cliente->email}}" required style="display:none;" />
					<select class="form-control" name="identificationType" id="form-checkout__identificationType" required style="display:none;"></select>
					
					<!-- Visto solo en todos menos sm-->
					<div class="form-group col-12 text-right  d-sm-none d-md-block">
						<button type="submit" id="form-checkout__submit" class="btn btn-secondary">
							<i class="fa fa-lock mr-3"></i>
							Pagar
						</button>
					</div>

					<!-- Visto solo en sm-->
					<div class="form-group col-12 d-none d-sm-block d-md-none">
						<button type="submit" id="form-checkout__submit" class="btn btn-secondary btn-block py-3">
							<i class="fa fa-lock mr-3"></i>
							Pagar
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="https://www.mercadopago.com/v2/security.js" view="card"></script>
<script src="https://sdk.mercadopago.com/js/v2"></script>
<script>
	$('.number_card').bind('keyup paste', function(){
		this.value = this.value.replace(/[^0-9]/g, '');
	});

	const mp = new MercadoPago('{{$venta->empresa->mercado_pago->public_key}}');
	const cardForm = mp.cardForm({
		amount: "{{(int)$venta->precio - (int)$venta->descuento}}",
		autoMount: true,
		form: {
			id: "form-checkout",
			cardholderName: {
				id: "form-checkout__cardholderName",
				placeholder: "Juan Perez",
			},
			cardholderEmail: {
				id: "form-checkout__cardholderEmail",
				placeholder: "juan@gmail.com",
			},
			cardNumber: {
				id: "form-checkout__cardNumber",
				placeholder: "0000 0000 0000 0000",
			},
			cardExpirationDate: {
				id: "form-checkout__cardExpirationDate",
				placeholder: "MM/YY",
			},
			securityCode: {
				id: "form-checkout__securityCode",
				placeholder: "CCV",
			},
			installments: {
				id: "form-checkout__installments",
				placeholder: "Cuotas",
			},
			identificationType: {
				id: "form-checkout__identificationType",
				placeholder: "Tipo de documento",
			},
			identificationNumber: {
				id: "form-checkout__identificationNumber",
				placeholder: "12345678",
			},
			issuer: {
				id: "form-checkout__issuer",
				placeholder: "Banco emisor",
			},
		},
		callbacks: {
			onFormMounted: error => {
				if (error) return console.warn("Form Mounted handling error: ", error);
			},
			onSubmit: event => {
				$('.cargando').fadeIn();
				event.preventDefault();

				const {
					cardholderName,
					paymentMethodId: payment_method_id,
					issuerId: issuer_id,
					cardholderEmail: email,
					amount,
					token,
					installments,
					identificationNumber,
					identificationType,
				} = cardForm.getCardFormData();

				$.ajax({
					type:'POST',
					url:"{{url('checkout/pago/tarjeta')}}",
					headers: {
						"Content-Type": "application/json",
					},
					data:JSON.stringify({
						_token:"{{csrf_token()}}",
						token,
						issuer_id,
						payment_method_id,
						cardholderName,
						transaction_amount: Number(amount),
						installments: Number(installments),
						description: "{{$venta->codigo}}",
						payer: {
							email,
							identification: {
								type: identificationType,
								number: identificationNumber,
							},
						},
					}),
					success: function (result) 
					{
						if(result.status == "approved")
						{
							window.location.replace("{{url('eticket', $venta->codigo)}}");
						}
						else
						{
							$('.alert').html(result.status_detail);
							$('.alert').fadeIn();

							$('input').val("");
							$("select option:selected").prop("selected", false);
						}

						$('.cargando').fadeOut();
					},
				});
			},
			onFetching: (resource) => {
				
			}
		},
	});
</script>