<div class="card">
	<div class="card-header" id="headingThree">
		<h2 class="mb-0">
			<button class="btn btn-link btn-block text-left text-secondary collapsed" type="button" data-toggle="collapse" data-target="#collapseEfectivo" aria-expanded="false" aria-controls="collapseEfectivo">
				<i class="fa fa-money mr-3"></i>
				Pago en efectivo
			</button>
		</h2>
	</div>
	<div id="collapseEfectivo" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					<div class="alert alert-secondary">
						<p>Realizara el pago al momento de la entrega del producto.</p>
					</div>
				</div>
				<div class="col-12">
					<form action="{{url('checkout/pago/efectivo')}}" class="form-horizontal" method="post">
						@csrf
						<input type="hidden" name="id" value="{{$venta->id}}">
						<div class="form-group text-right">
							<button class="btn btn-secondary">
								<i class="fa fa-paper-plane mr-3"></i>
								Enviar
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>