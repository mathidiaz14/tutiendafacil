<div class="card">
	<div class="card-header" id="headingThree">
		<h2 class="mb-0">
			<button class="btn btn-link btn-block text-left text-secondary collapsed" type="button" data-toggle="collapse" data-target="#collapseTransferencia" aria-expanded="false" aria-controls="collapseTransferencia">
				<i class="fa fa-university mr-3"></i>
				Transferencia bancaria
			</button>
		</h2>
	</div>
	<div id="collapseTransferencia" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					<div class="alert alert-secondary">
						<b>Datos para transferir:</b>
						<hr>
						{!! $venta->empresa->pago_transferencia->datos !!}
					</div>
				</div>
				<div class="col-12">
					<form action="{{url('checkout/pago/transferencia')}}" method="post" class="form-horizontal" enctype="multipart/form-data">
						@csrf
						<input type="hidden" name="id" value="{{$venta->id}}">
						<div class="row">
							<div class="col-12">
								<div class="alert alert-secondary">
									<b>Comprobante de transferencia:</b>
									<hr>
									<div class="form-group">
										<input type="file" name="archivo" class="form-control-file" required>
									</div>
								</div>
							</div>
							<div class="col-12 text-right">
								<button class="btn btn-secondary">
									<i class="fa fa-paper-plane mr-3"></i>
									Enviar
								</button>
							</div>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</div>
</div>