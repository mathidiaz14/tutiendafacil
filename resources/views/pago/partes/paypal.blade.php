<div class="card">
	<div class="card-header" id="headingTwo">
		<h2 class="mb-0">
			<button class="btn btn-link btn-block text-left text-secondary collapsed" type="button" data-toggle="collapse" data-target="#collapsePaypal" aria-expanded="false" aria-controls="collapsePaypal">
				<i class="fa fa-paypal mr-3"></i>
				PayPal
			</button>
		</h2>
	</div>
	<div id="collapsePaypal" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
		<div class="card-body">
			PayPal
		</div>
	</div>
</div>