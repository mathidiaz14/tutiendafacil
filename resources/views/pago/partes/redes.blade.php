<div class="card">
	<div class="card-header" id="headingThree">
		<h2 class="mb-0">
			<button class="btn btn-link btn-block text-left text-secondary collapsed" type="button" data-toggle="collapse" data-target="#CollapseRedes" aria-expanded="false" aria-controls="CollapseRedes">
				<i class="fa fa-home mr-3"></i>
				Redes de cobranza
			</button>
		</h2>
	</div>
	<div id="CollapseRedes" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
		<div class="card-body">
			<form id="form-checkout" action="{{url('checkout/pago/redes')}}" method="post" class="form-horizontal">
				@csrf
				<div class="row">
					<div class="form-group col-12 col-md-6">
						<label for="name">Nombre</label>
						<input id="form-checkout__payerFirstName" name="name" type="text" class="form-control" placeholder="Nombre" required>
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="lastname">Apellido</label>
						<input id="form-checkout__payerLastName" name="lastname" type="text" class="form-control" placeholder="Apellido" required>
					</div>
					<div class="form-group hide">
						<label for="identificationType">Tipo</label>
						<select id="form-checkout__identificationTypeRedes" name="identificationType" type="text" class="form-control" required>
						</select>
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="identificationNumber">CI</label>
						<input id="form-checkout__identificationNumber" name="identificationNumber" type="text" class="form-control" placeholder="Número de documento" required>
					</div>
					<div class="form-group col-12 col-md-6">
						<label for="identificationType">Red de cobranza</label>
						<select id="" name="payment_method_id" type="text" class="form-control" required>
							<option value="abitab">Abitab</option>
							<option value="redpagos">Redpagos</option>
						</select>
					</div>
					<div class="col-12 text-right">
						<input id="form-checkout__email" name="email" type="text" class="form-control" value="{{$venta->cliente->email}}" required style="opacity:0;">
						<input type="hidden" name="transactionAmount" id="transactionAmount" value="{{(int)$venta->precio - (int)$venta->descuento}}">
						<input type="hidden" name="description" id="description" value="{{$venta->codigo}}">
						<button type="submit" class="btn btn-secondary">
							<i class="fa fa-paper-plane mr-3"></i>
							Enviar
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="https://sdk.mercadopago.com/js/v2"></script>
	<script>
		const mp_redes = new MercadoPago('{{$venta->empresa->mercado_pago->public_key}}');


		function createSelectOptions(elem, options, labelsAndKeys = { label : "name", value : "id"}){
		   const {label, value} = labelsAndKeys;

		   elem.options.length = 0;

		   const tempOptions = document.createDocumentFragment();

		   options.forEach( option => {
		       const optValue = option[value];
		       const optLabel = option[label];

		       const opt = document.createElement('option');
		       opt.value = optValue;
		       opt.textContent = optLabel;

		       tempOptions.appendChild(opt);
		   });

		   elem.appendChild(tempOptions);
		}

		// Get Identification Types
		(async function getIdentificationTypes () {
		   try {
		       const identificationTypes = await mp_redes.getIdentificationTypes();
		       const docTypeElement = document.getElementById('form-checkout__identificationTypeRedes');

		       createSelectOptions(docTypeElement, identificationTypes)
		   }catch(e) {
		       return console.error('Error getting identificationTypes: ', e);
		   }
		})()

	</script>