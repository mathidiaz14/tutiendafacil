@extends('layouts.compra')

@section('contenido')
	
	<div class="row bg-dark text-white p-3">
		<div class="col-12 text-center mt-2">
			<h5>Metodo de entrega</h5>
		</div>
	</div>
	<div class="row mt-3 mb-2">
		<div class="col-12">
			<form action="{{url('checkout/entrega', $venta->codigo)}}" method="post" class="form-horizontal">
				@csrf
				<div class="form-group">
					@if(($venta->empresa->configuracion->envio == null) and ($venta->empresa->configuracion->retiro == null)) 
						<p class="alert alert-danger">La empresa no tiene ninguna forma de entrega configurada, por lo cual no podemos continuar con la compra. Puede comunicarse con la empresa a través del correo <b>{{$venta->empresa->configuracion->email_admin}}</b>. <br>Disculpe la molestia.</p>
					@endif

					<div class="row mt-5">
						@if($venta->empresa->configuracion->envio == "on")
							<div class="col text-center">
								<input class="hide" type="radio" name="entrega" id="envio" value="envio" @if($venta->entrega == "envio") checked="" @endif >
							  	<label class="btn btn-dark px-5 py-3 boton_label" id="metodo_envio" for="envio">
							  		<i class="fa fa-truck fa-2x"></i>
							  		<p>Envío a domicilio</p>
							  	</label>
							</div>
						@endif

						@if($venta->empresa->configuracion->retiro == "on")
							<div class="col text-center">
							  	<input class="hide" type="radio" name="entrega" id="retiro" value="retiro" @if($venta->entrega == "retiro") checked="" @endif>
								<label class="btn btn-dark px-5 py-3 boton_label" id="metodo_retiro" for="retiro">
									<i class="fa fa-home fa-2x"></i>
									<p>Retiro en local</p>
								</label>
							</div>
						@endif
					</div>
				</div>
				
				@if($venta->empresa->configuracion->envio == "on")
					<div class="envio m-3" style="@if($venta->entrega == 'envio') display: block; @else display: none; @endif">
						<div class="form-group">
							<label for="">Ciudad</label>
							<input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ingrese la ciudad" value="{{$venta->cliente->ciudad}}">
						</div>
						<div class="row">
							<div class="col-8">
								<div class="form-group">
									<label for="">Dirección</label>
									<input type="text" class="form-control" name="direccion" id="direccion" placeholder="Ingrese la dirección" value="{{$venta->cliente->direccion}}">
								</div>
							</div>
							<div class="col-4">
								<div class="form-group">
									<label for="">Apartamento</label>
									<input type="text" class="form-control" name="apartamento" id="apartamento" placeholder="Apartamento" value="{{$venta->cliente->apartamento}}">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="">Observación</label>
							<input type="text" class="form-control" name="observacion" id="observacion" placeholder="Ingrese la observación" value="{{$venta->cliente_observacion}}">
						</div>
					</div>
				@endif

				@if($venta->empresa->configuracion->retiro == "on")
					<div class="retiro m-3" style="@if($venta->entrega == 'retiro') display: block; @else display: none; @endif">
						@if($venta->empresa->locales->count() == 0)
							<p class="alert alert-danger">No hay ningun local para retirar el producto</p>
						@endif
						@foreach($venta->empresa->locales as $local)
							<div class="form-check">
							  	<input class="form-check-input" type="radio" name="local" id="local_{{$local->id}}" value="{{$local->id}}" @if($loop->first) checked="" @endif>
							  	<label class="form-check-label" for="local_{{$local->id}}">
							  		<p>
							  			{{$local->nombre}}<br>
							  			<small>{{$local->localidad}}</small>
							  		</p>
							  	</label>
							</div>
						@endforeach
					</div>
				@endif
				<hr>
				<div class="row">
					<div class="col">
						<a href="{{url('checkout/datos', $venta->codigo)}}" class="btn btn-secondary px-5">
							<i class="fa fa-angle-left mr-3"></i>
							Atras
						</a>
					</div>
					<div class="col text-right">
						<button class="btn btn-dark btn_pago px-5" disabled>
							Ir a pago
							<i class="fa fa-angle-right ml-3"></i>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$(document).ready(function()
		{
			$('#envio').click(function()
			{
				$('#metodo_envio').addClass('activo');
				$('#metodo_retiro').removeClass('activo');

				$('.retiro').hide();
				$('.envio').fadeIn();
				
				$('#ciudad').attr('required', true);
				$('#direccion').attr('required', true);

				$('.btn_pago').attr('disabled', false);
			});

			$('#retiro').click(function()
			{
				$('#metodo_envio').removeClass('activo');
				$('#metodo_retiro').addClass('activo');

				$('.envio').hide();
				$('.retiro').fadeIn();

				$('#ciudad').attr('required', false);
				$('#direccion').attr('required', false);
				$('.btn_pago').attr('disabled', false);
			})
			
			$('.pasos').removeClass('activo');
			$('.pasos.envio').addClass('activo');
		});
	</script>
@endsection