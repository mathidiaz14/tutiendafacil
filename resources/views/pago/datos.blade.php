@extends('layouts.compra')

@section('contenido')
	<div class="row bg-dark text-white p-3">
		<div class="col-12 text-center mt-2">
			<h5>Datos de facturación</h5>
		</div>
	</div>
	<div class="row mt-3 mb-2 p-3">
		<div class="col-12">
			<form action="{{url('checkout/datos', $venta->codigo)}}" method="post" class="form-horizontal">
				@csrf
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="form-group">
							<label for="">Nombre *</label>
							<input type="text" class="form-control" name="name" placeholder="Ingresa tu nombre" required="" value="{{$venta->cliente->nombre}}">
						</div>
					</div>
					<div class="col-12 col-md-6">
						<div class="form-group">
							<label for="">Apellido *</label>
							<input type="text" class="form-control" name="surname" placeholder="Ingresa tu apellido" required="" value="{{$venta->cliente->apellido}}">
						</div>
					</div>
				</div>	
				<div class="row">
					<div class="col-12 col-md-6">
						<div class="form-group">
							<label for="">Teléfono *</label>
							<input type="phone" class="form-control" name="telefono" placeholder="Ingresa un teléfono" value="{{$venta->cliente->telefono}}" required>
						</div>
					</div>
					<div class="col-4 col-md-2">
						<div class="form-group">
							<label for="">Tipo</label>
							<select name="documento_tipo" id="" class="form-control">
								<option value="ci">CI</option>
								<option value="otro">Otro</option>
							</select>
						</div>
					</div>
					<div class="col-8 col-md-4">
						<div class="form-group">
							<label for="">Documento *</label>
							<input type="text" class="form-control" name="documento" placeholder="Documento" value="{{$venta->cliente->documento}}" required>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<label for="">¿Tienes un cupón?</label>
							<input type="text" name="cupon" class="form-control" placeholder="Ingresa aquí el cupón">
						</div>
					</div>
				</div>					
				<hr>
				<div class="row">
					<div class="col text-right">
						<button class="btn btn-dark px-5 mt-2">
							Ir a entrega
							<i class="fa fa-angle-right ml-3"></i>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$('.pasos').removeClass('activo');
		$('.pasos.datos').addClass('activo');
	</script>
@endsection