@extends('layouts.compra')

@section('contenido')
	<div class="row bg-dark text-white p-3">
		<div class="col-12 text-center mt-2">
			<h5>¡Gracias por tu compra!</h5>
		</div>
	</div>
	<div class="row mt-3 mb-2">
		<div class="col-12" id="info">
			<div class="table table-responsive">
				<table class="table table-striped">
					<tr>
						<td>
							Numero de orden
						</td>
						<td>
							<b>{{$venta->codigo}}</b>
						</td>
					</tr>
					<tr>
						<td>
							Nombre del comprador 
						</td>
						<td>
							<b>{{$venta->cliente->nombre}}</b>
						</td>
					</tr>
					<tr>
						<td>
							Email del comprador 
						</td>
						<td>
							<b>{{$venta->cliente->email}}</b>
						</td>
					</tr>
					@if($venta->cliente->telefono != null)
						<tr>
							<td>
								Telefono del comprador 	
							</td>
							<td>
								<b>{{$venta->cliente->telefono}}</b>
							</td>
						</tr>
					@endif

					<tr>
						<td>
							Forma de entrega
						</td>
						<td>
							@if($venta->entrega == "retiro")
								<b>Retiro</b>
							@else
								<b>Entrega</b>
							@endif
						</td>
					</tr>
					<tr>
						@if($venta->entrega == "retiro")
							<td>
								Retiro en 
							</td>
							<td>
								<b>{{$venta->local->nombre}}</b>
							</td>
						@else
							<td>
								Entrega en 
							</td>
							<td>
								<b>{{$venta->cliente->direccion}} {{$venta->cliente->apartamento}}</b>
							</td>
						@endif
					</tr>

					@if($venta->cliente_observacion != null)
					<tr>
						<td>
							Observación
						</td>
						<td>
							<b>{{$venta->cliente_observacion}}</b>	
						</td>
					</tr>
					@endif

					<tr>
						<td>
							Metodo de pago
						</td>
						<td>
							@include('ayuda.pago_metodo')
						</td>
					</tr>
					<tr>
						<td>
							Estado
						</td>
						<td>
							@include('ayuda.venta_estado')
						</td>
					</tr>
				</table>
			</div>
			
		</div>
		<div class="col">
			<hr>
			<div class="row pt-3">
				<div class="col-12 col-md-6 mt-2">
					<a class="btn btn-secondary btn-block btn_print">
						<i class="fa fa-print mr-3"></i>
						Imprimir comprobante
					</a>
				</div>
				<div class="col-12 col-md-6 mt-2">
					<a href="http://{{$venta->empresa->URL}}" class="btn btn-dark btn-block">
						<i class="fa fa-home mr-3"></i>
						Volver a {{$venta->empresa->nombre}}
					</a>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$(document).ready(function()
		{
			$('.btn_print').click(function()
			{
				var contenido= document.getElementById('info').innerHTML;
				var contenidoOriginal= document.body.innerHTML;
				document.body.innerHTML = contenido;
				window.print();
				document.body.innerHTML = contenidoOriginal;
			});
		});
	</script>	
@endsection
