@extends('layouts.compra')

@section('contenido')
	<div class="row bg-dark text-white p-3">
		<div class="col-12 text-center mt-2">
			<h5>Ingrese su email para completar la compra</h5>
		</div>
	</div>
	<div class="row mt-5 mb-2">
		<div class="col-12">
			<form action="{{url('checkout/login', $venta->codigo)}}" method="post" class="form-horizontal">
				@csrf
				<div class="row">
					<div class="col-12">
						<div class="form-group">
							<input type="email" class="form-control" name="email" placeholder="Ingresa un email aquí..." required="">
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col">
						
					</div>
					<div class="col text-right">
						<button class="btn btn-dark">
							Continuar
							<i class="fa fa-angle-right ml-3"></i>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$('.pasos').removeClass('activo');
		$('.pasos.login').addClass('activo');
	</script>
@endsection
