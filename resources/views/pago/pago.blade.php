@extends('layouts.compra')

@section('contenido')
	@include('ayuda.cargando')
	<div class="row bg-dark text-white p-3">
		<div class="col-12 text-center mt-2">
			<h5>Metodo de pago</h5>
		</div>
	</div>
	<div class="row mt-3 mb-2">
		<div class="col-12 mt-3">
			<div class="accordion" id="accordionExample">

				@if((isset($venta->empresa->mercado_pago)) and ($venta->empresa->mercado_pago->estado == "conectado"))
					@include('pago.partes.tarjeta')
					
					@include('pago.partes.redes')
				@endif

				@if((isset($venta->empresa->pago_transferencia)) and ($venta->empresa->pago_transferencia->estado == "conectado"))
					@include('pago.partes.transferencia')
				@endif

				@if((isset($venta->empresa->pago_efectivo)) and ($venta->empresa->pago_efectivo->estado == "conectado"))
					@include('pago.partes.efectivo')
				@endif

				@if(((empty($venta->empresa->mercado_pago)) or ($venta->empresa->mercado_pago->estado == "desconectado")) and ((empty($venta->empresa->pago_transferencia)) or ($venta->empresa->pago_transferencia->estado == "desconectado")) and ((empty($venta->empresa->pago_efectivo)) or ($venta->empresa->pago_efectivo->estado == "desconectado")))
					<div class="alert alert-danger">
						<p>
							La empresa no tiene ningun medio de pago habilitado, comuniquese con la empresa para que podamos solucionarlo. Gracias por tu tiempo.
						</p>
						<small>
							<a href="https://{{empresa()->URL}}">
								Volver a {{empresa()->nombre}}
							</a>
						</small>
					</div>
				@endif
				

			</div>
		</div>
		<div class="col-12 mt-3">
			<a href="{{url('checkout/entrega', $venta->codigo)}}" class="btn btn-secondary">
				<i class="fa fa-angle-left mr-3"></i>
				Atras
			</a>
		</div>
	</div>
@endsection

@section('js')
	<script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>
	<script src="https://sdk.mercadopago.com/js/v2"></script>
	<script>
		$('.pasos').removeClass('activo');
		$('.pasos.pago').addClass('activo');
	</script>
@endsection