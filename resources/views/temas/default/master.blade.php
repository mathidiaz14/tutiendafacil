<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="description" content>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


	<title>{{i('titulo')}}</title>

	<link rel="icon" href="{{logo_url()}}">

	<link rel="stylesheet" href="{{myasset('assets/css/core-style.css')}}">
	<link rel="stylesheet" href="{{myasset('assets/style.css')}}">

</head>
<body>

	<header class="header_area">
		<div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">

			<nav class="classy-navbar" id="essenceNav">

				<a class="nav-brand" href="{{url('/')}}">
					{{i('titulo')}}
				</a>

				<div class="classy-navbar-toggler">
					<span class="navbarToggler"><span></span><span></span><span></span></span>
				</div>

				<div class="classy-menu">

					<div class="classycloseIcon">
						<div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
					</div>

					<div class="classynav">
						<ul>
							<li><a href="#">Categorias</a>
								<ul class="dropdown">
									@foreach(categorias() as $categoria)
										<li><a href="{{url($categoria->url)}}">{{$categoria->titulo}}</a></li>
									@endforeach
								</ul>
							</li>
							<li><a href="#">Paginas</a>
								<ul class="dropdown">
									@foreach(ver_menus() as $menu)
										<li><a href="{{url($menu->url)}}">{{$menu->titulo}}</a></li>
									@endforeach
								</ul>
							</li>
							<li><a href="">Blog</a></li>
							<li><a href="{{url('contacto')}}">Contacto</a></li>
						</ul>
					</div>

				</div>
			</nav>

			<div class="header-meta d-flex clearfix justify-content-end">

				<div class="search-area">
					<form action="{{url('productos')}}" method="get">
						<input type="search" name="busqueda" id="headerSearch" placeholder="Busca aquí...">
						<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
					</form>
				</div>

				<div class="favourite-area">
					<a href="{{url('perfil/deseos')}}"><img src="{{myasset('assets/img/core-img/heart.svg')}}" alt></a>
				</div>

				<div class="user-login-info">
					<a href="{{url('perfil')}}"><img src="{{myasset('assets/img/core-img/user.svg')}}" alt></a>
				</div>

				<div class="cart-area">
					<a href="#" id="essenceCartBtn"><img src="{{myasset('assets/img/core-img/bag.svg')}}" alt> 
						<span>{{cantidad_productos_carrito() == 0 ? "" : cantidad_productos_carrito()}}</span>
					</a>
				</div>
			</div>
		</div>
	</header>


	<div class="cart-bg-overlay"></div>
	<div class="right-side-cart-area">

		<div class="cart-button">
			<a href="#" id="rightSideCart"><img src="{{myasset('assets/img/core-img/bag.svg')}}" alt> 
				<span>
					{{cantidad_productos_carrito() == 0 ? "" : cantidad_productos_carrito()}}
				</span>
			</a>
		</div>
		<div class="cart-content d-flex">
			@if(Session::get('carrito') != null)
				<div class="cart-list">
					@foreach(Session::ger('carrito') as $producto)
						<div class="single-cart-item">
							<a href="#" class="product-image">
								<img src="{{producto_url_imagen($producto->id)}}" class="cart-thumb" alt>

								<div class="cart-item-desc">
									<span class="product-remove"><i class="fa fa-close" aria-hidden="true"></i></span>
									<span class="badge">{{$producto->titulo}}</span>
									<p class="size">Talle: S</p>
									<p class="color">Color: Rojo</p>
									<p class="price">$ {{producto_precio($producto->id)}}</p>
								</div>
							</a>
						</div>
					@endforeach
				</div>
				<div class="cart-amount-summary">
					<h2>Resumen</h2>
					<ul class="summary-table">
						<li><span>Subtotal:</span> <span>$ {{productos_precio_total($productos)}}</span></li>
						<li><span>Envio:</span> <span>Gratis</span></li>
						<li><span>Descuento:</span> <span>-15%</span></li>
						<li><span>Total:</span> <span>$ {{productos_descontar_porcentaje($productos, 15)}}</span></li>
					</ul>
					<div class="checkout-btn mt-100">
						<a href="checkout.html" class="btn essence-btn">Ir a pagar</a>
					</div>
				</div>
			@else
				<div class="cart-amount-summary">
					<h2>Aun no hay ningun producto en el carrito</h2>
				</div>
			@endif

		</div>
	</div>

	@yield('contenido')

	<footer class="footer_area clearfix">
		<div class="container">
			<div class="row">

				<div class="col-12 col-md-6">
					<div class="single_widget_area d-flex mb-30">

						<div class="footer-logo mr-50">
							<a href="#"><img src="{{myasset('assets/img/core-img/logo2.png')}}" alt></a>
						</div>

						<div class="footer_menu">
							<ul>
								<li><a href="shop.html">Shop</a></li>
								<li><a href="blog.html">Blog</a></li>
								<li><a href="contact.html">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="single_widget_area mb-30">
						<ul class="footer_widget_menu">
							<li><a href="#">Order Status</a></li>
							<li><a href="#">Payment Options</a></li>
							<li><a href="#">Shipping and Delivery</a></li>
							<li><a href="#">Guides</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms of Use</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row align-items-end">

				<div class="col-12 col-md-6">
					<div class="single_widget_area">
						<div class="footer_heading mb-30">
							<h6>Subscribe</h6>
						</div>
						<div class="subscribtion_form">
							<form action="#" method="post">
								<input type="email" name="mail" class="mail" placeholder="Your email here">
								<button type="submit" class="submit"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
							</form>
						</div>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="single_widget_area">
						<div class="footer_social_area">
							<a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							<a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
							<a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							<a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
							<a href="#" data-toggle="tooltip" data-placement="top" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				@include('ayuda.tema.footer')
			</div>
		</div>
	</footer>


	<script src="{{myasset('assets/js/jquery/jquery-2.2.4.min.js')}}"></script>

	<script src="{{myasset('assets/js/popper.min.js')}}"></script>

	<script src="{{myasset('assets/js/bootstrap.min.js')}}"></script>

	<script src="{{myasset('assets/js/plugins.js')}}"></script>

	<script src="{{myasset('assets/js/classy-nav.min.js')}}"></script>

	<script src="{{myasset('assets/js/active.js')}}"></script>

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>

</body>
</html>