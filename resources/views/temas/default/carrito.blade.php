@extends(ttf_extends('master'))

@section('contenido')
    <main>
        <div class="container" style="margin-top: 150px;">
            <div class="row my-5">
                <div class="col-12 text-center">
                	<h2>Carrito de compras</h2>
                </div>
				@if($productos != null)
            		@include('ayuda.tema.carrito', ['productos' => $productos])
                @else
					<div class="col-12 my-4 text-center">
						<hr>
						<p>No hay ningun producto en el carrito.</p>
					</div>
				@endif
            </div>
        </div>
    </main>
@endsection
