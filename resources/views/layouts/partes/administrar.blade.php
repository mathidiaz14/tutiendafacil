<li class="nav-item 
@if(isset($menu_superior) and ($menu_superior == 'administrar')) 
menu-open 
@endif">
<a href="#" class="nav-link">
  <i class="nav-icon fas fa-tools"></i>
  <p>
    Administrar
    <i class="fas fa-angle-left right"></i>
  </p>
</a>
<ul class="nav nav-treeview">
  <li class="nav-item pl-2 ">
    <a href="{{url('admin/configuracion')}}" class="nav-link @if($menu_activo == 'configuracion') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Configuración
      </p>
    </a>
  </li>
  <li class="nav-item pl-2 ">
    <a href="{{url('admin/plugin')}}" class="nav-link @if($menu_activo == 'plugin') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Plugins
      </p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/usuario')}}" class="nav-link @if($menu_activo == 'usuario') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Usuarios
      </p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/newsletter')}}" class="nav-link @if($menu_activo == 'newsletter') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Suscriptores
      </p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/error')}}" class="nav-link @if($menu_activo == 'error') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Incidencias
      </p>
    </a>
  </li>
  
  <li class="nav-item pl-2">
    <a href="{{url('admin/error/soporte')}}" class="nav-link @if($menu_activo == 'soporte') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Soporte
      </p>
    </a>
  </li>
  
  <li class="nav-item pl-2">
    <a href="{{url('admin/notificacion')}}" class="nav-link @if($menu_activo == 'notificacion') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Notificaciones
      </p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/mensaje')}}" class="nav-link @if($menu_activo == 'mensaje') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Mensajes
      </p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/visita')}}" class="nav-link @if($menu_activo == 'visitas') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Estadisticas
      </p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/visual')}}" class="nav-link @if($menu_activo == 'visual') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>
        Visual
      </p>
    </a>
  </li>
</ul>
</li>