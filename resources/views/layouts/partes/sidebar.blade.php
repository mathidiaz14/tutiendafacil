<!-- Main Sidebar Container -->
  <aside class="main-sidebar elevation-4 sidebar-dark-info">
    <!-- Brand Logo -->
    <a href="@if(Auth::user()->empresa->estado == 'completo') http://{{Auth::user()->empresa->URL}} @else # @endif" class="brand-link text-center" target="_blank">
        <img src="{{asset('img/favicon.png')}}" class="brand-image">
        <span class="brand-text font-weight-light titulo-pagina">TuTiendaFacil</span>
        <span class="titulo-pagina-uy">.uy</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <span class="d-block text-white">
            Hola
          </span>
          <b class="text-white">
            {{Auth::user()->nombre}}
          </b>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column pb-5" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item ">
            <a href="{{url('/admin')}}" class="nav-link @if($menu_activo == 'inicio') active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Inicio
              </p>
            </a>
          </li>
          
          @if(empty(visual('tienda_menu')) or (visual('tienda_menu')->valor == "on"))
            @include('admin.menu')
          @endif

          @include('layouts.partes.apariencia')
          
          @include('layouts.partes.administrar')

          <li class="">
            <a class="">
              <hr style="margin-top: 1rem; margin-bottom: 1rem; border: 0; border-top: 1px solid rgb(255 255 255 / 20%);">
            </a>
          </li>

          @foreach(empresa()->plugins as $plugin)
            @if($plugin->pivot->estado == "activo")
              @if(empty(visual($plugin->nombre.'_menu')) or (visual($plugin->nombre.'_menu')->valor == "on"))
                @include('Plugins.'.$plugin->carpeta.".menu")
              @endif
            @endif
          @endforeach

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>