<li class="nav-item 
@if(isset($menu_superior) and ($menu_superior == 'apariencia')) 
menu-open 
@endif">
<a href="" class="nav-link">
  <i class="nav-icon fas fa-palette"></i>
  <p>
    Apariencia
    <i class="fas fa-angle-left right"></i>
  </p>
</a>
<ul class="nav nav-treeview">
  <li class="nav-item pl-2">
    <a href="@if(Auth::user()->empresa->estado == 'completo') http://{{Auth::user()->empresa->URL}} @else # @endif" target="_blank" class="nav-link">
      <i class="far fa-circle nav-icon"></i>
      <p>Ver la web</p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/pagina')}}" class="nav-link @if($menu_activo == 'pagina') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>Paginas</p>
    </a>
  </li>
  
  <li class="nav-item pl-2">
    <a href="{{url('admin/menu')}}" class="nav-link @if($menu_activo == 'menu') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>Menús</p>
    </a>
  </li>
  <li class="nav-item pl-2">
    <a href="{{url('admin/web')}}" class="nav-link @if($menu_activo == 'personalizar') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>Personalizar web</p>
    </a>
  </li>
  <!--
  <li class="nav-item pl-2">
    <a href="{{url('admin/tema')}}" class="nav-link @if($menu_activo == 'temas') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>Temas</p>
    </a>
  </li>
  -->
  <li class="nav-item pl-2">
    <a href="{{url('admin/web/codigo')}}" class="nav-link @if($menu_activo == 'codigo') active @endif">
      <i class="far fa-circle nav-icon"></i>
      <p>Editar codigo</p>
    </a>
  </li>
</ul>
</li>