<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{$venta->empresa->nombre}}</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{asset('css/compra.css')}}">
	<link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	@yield('css')
</head>
<body>
	<header class="shadow">
		<nav class="nav bg-white p-3 pt-4">
			<div class="col-12 col-md-4 text-center">
				<a href="http://{{$venta->empresa->URL}}" style="text-decoration:none;">
					<h3 class="text-secondary">{{$venta->empresa->nombre}}</h3>
				</a>
			</div>
			<div class="col-12 col-md-8 pt-2">
				<div class="row">
					<div class="col pasos login">
						<h6 class="text-dark">
							<i class="fa fa-user mr-2"></i> 
							Email
						</h6>
					</div>
					<div class="col pasos datos">
						<h6 class="text-dark">
							<i class="fa fa-address-card mr-2"></i> 
							Datos
						</h6>
					</div>
					<div class="col pasos envio">
						<h6 class="text-dark">
							<i class="fa fa-truck mr-2"></i> 
							Envío
						</h6>
					</div>
					<div class="col pasos pago">
						<h6 class="text-dark">
							<i class="fa fa-credit-card mr-2"></i> 
							Finalizar
						</h6>
					</div>
				</div>
			</div>
		</nav>
	</header>	
	<section class="container">
		@include('ayuda.alerta_login')
		<div class="row">
			<div class="col-12 col-md-4 detalle mb-5">
				<div class="row bg-dark text-white p-3">
					<div class="col-12 text-center mt-2">
						<h5>Detalles del pedido</h5>
					</div>
				</div>
				<div class="row mt-3">
					@if($venta->productos != null)
						<div class="col-12 mb-2">
							<hr style="border-top: 1px solid rgba(0,0,0,.1);">
						</div>

						<div class="col-12">
							@foreach($venta->productos as $producto)
								<div class="row mb-2 p-1">
									<div class="col-2">
										@if(isset($producto->pivot->variante_id))
							    			<img 	src="{{asset(producto_variante($producto->pivot->variante_id)->imagen)}}" alt="" width="50px" 
							    					onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'"
							    					style="	-webkit-box-shadow: 	1px 1px 2px 0px rgba(0,0,0,0.75);
															-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															object-fit: cover;
														    object-position: center;
														    height: 50px;"	>
							    		@else
							    			<img 	src="{{producto_url_imagen($producto->id)}}" alt="" width="50px" 
							    					onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'"
							    					style="	-webkit-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															object-fit: cover;
														    object-position: center;
														    height: 50px;"	>
							    		@endif
									</div>
									<div class="col-7">
										<small>{{$producto->nombre}} x {{$producto->pivot->cantidad}}</small>
										@if($producto->pivot->variante_id != null)
											<br>
											<small>
												@foreach(producto_variante($producto->pivot->variante_id)->valores as $valor)
													<h2 id      = "valor_{{$valor->id}}"
						                                class   = "badge badge-dark mr-2" 
						                                attr-id = "{{$valor->id}}"
						                                style   = "background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%);
						                                            min-width: 4em;
						                                            opacity: .8;"
						                            >
						                                {{$valor->nombre != null ? $valor->nombre : "-"}}
						                            </h2>
												@endforeach
											</small>	
										@endif
									</div>
									<div class="col-3">
										<small><b>$ {{$producto->pivot->precio}}</b></small>
									</div>
								</div>
							@endforeach

							<div class="row">
								<div class="col-12">
									<hr style="border-top: 1px solid rgba(0,0,0,.1);">
								</div>
							</div>
							
							@if($venta->descuento != null)
								<div class="row">
									<div class="col-8 text-right">
										<p>Cupón:</p>
									</div>
									<div class="col-4 text-right">
										<p><b>$ -{{$venta->descuento}}</b></p>
									</div>
									<div class="col-12">
										<hr style="border-top: 1px solid rgba(0,0,0,.1);">
									</div>
								</div>
							@endif

							@if($venta->estado == "comenzado")
								<div class="row">
									<div class="col-12 text-center mt-2">
										<small>
											<a href="{{url('checkout/cancelar', $venta->id)}}" class="btn btn-outline-danger">
												<i class="fa fa-times"></i>
												Cancelar compra
											</a>	
										</small>
									</div>
								</div>
							@endif
						</div>	
					@else
						<div class="col-12">
							<p>No hay productos en el carrito de compras</p>
						</div>
					@endif
				</div>
			</div>
			<div class="col-12 col-md-1">
				
			</div>
			<div class="col-12 col-md-7 info mb-5">
				@yield('contenido')
			</div>
		</div>
	</section>
	<footer class="bg-white p-3 mt-3">
		<div class="container text-dark" style="opacity: .75;">
			<div class="row">
				<div class="col text-left">
					@if($venta->estado == "comenzado")
						<b class="contador_tiempo"></b> <br>	
						<small>10 Min para finalizar la compra</small>
					@endif
				</div>
				<div class="col text-right mt-3">
					<h5>
						Total: 
						<b class="ml-2">$ {{(int)$venta->precio - (int)$venta->descuento}}</b>
					</h5>
				</div>
			</div>
		</div>
	</footer>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
  	<script src="{{asset('js/csrf.js')}}"></script>
  	<script>
  		$(document).ready(function()
  		{
  			$(".contador_tiempo").timer(
			{
				countdown: true,
				duration:'10m',
				callback: function(){
					window.location.href = "https://{{$venta->empresa->URL}}";
				},
			});
  		});
  	</script>
	@yield('js')
</body>
</html>