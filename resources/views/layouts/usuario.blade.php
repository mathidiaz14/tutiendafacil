<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{empresa()->nombre}}</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<link rel="stylesheet" href="{{asset('dashboard/plugins/fontawesome-free/css/all.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/compra.css')}}">
	<link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	@yield('css')
</head>
<body>
	<header>
		<nav class="nav bg-white p-3">
			<div class="col-12 col-md-4 text-center">
				<a href="http://{{empresa()->URL}}" style="text-decoration:none;">
					<h3 class="text-dark">{{empresa()->nombre}}</h3>
				</a>
			</div>
			<div class="col-12 col-md-8 pt-2">
				@if(Session::has('cliente'))
					<div class="row">
						<div class="col pasos compras">
							<a href="{{url('perfil')}}">
								<b class="text-dark">
									<i class="fa fa-shopping-cart"></i> Mis Compras
								</b>
							</a>
						</div>
						<div class="col pasos datos">
							<a href="{{url('perfil/datos')}}">
								<b class="text-dark">
									<i class="fa fa-address-card"></i> Mis datos
								</b>
							</a>
						</div>
						<div class="col pasos deseos">
							<a href="{{url('perfil/deseos')}}">
								<b class="text-dark">
									<i class="fa fa-heart"></i> Lista de deseos
								</b>
							</a>
						</div>
					</div>
				@endif
			</div>
		</nav>
	</header>	
	@yield('contenido')
	
	@if(Session::get('cliente') != null)
		<footer class="bg-white p-3">
			<div class="container-fluid">
				<div class="row">
					<div class="col text-right">
						<a href="{{url('perfil/logout')}}" class="btn btn-secondary px-5">
							<i class="fa fa-sign-out"></i>
							Salir
						</a>
					</div>
				</div>
			</div>
		</footer>
	@endif

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  	<script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
  	<script src="{{asset('js/csrf.js')}}"></script>
  	
	@yield('js')
</body>
</html>