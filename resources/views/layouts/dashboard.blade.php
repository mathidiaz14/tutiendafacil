<!DOCTYPE html>
<html lang="en">
<head>
	<title>TuTiendaFacil.uy</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="{{asset('dashboard/plugins/fontawesome-free/css/all.min.css')}}">
	<link rel="stylesheet" href="{{asset('dashboard/dist/css/adminlte.css')}}">
	<link rel="stylesheet" href="{{asset('dashboard/plugins/summernote/summernote-bs4.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" href="{{asset('dashboard/dist/css/toastr.min.css')}}">
	
	<link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
	
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<script src="{{asset('dashboard/plugins/jquery/jquery.min.js')}}"></script>

    <script src="{{ asset('dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <script>
		var $ = jQuery.noConflict();
	</script>
	
	@yield('css')

</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
		@include('ayuda.alerta')
		
		@include('ayuda.cargando')

		@include('layouts.partes.nav')

		@include('layouts.partes.sidebar')

		@yield('contenido')

		@include('layouts.partes.footer')
		
	</div>

	<aside class="control-sidebar control-sidebar-dark pt-2">
		<div class="col-12">
			<div class="card ">
				<div class="card-header">
					<p class="text-dark">
						Titulo
					</p>
				</div>
				<div class="card-body ">
					<p>Contenido</p>
				</div>
			</div>
		</div>
  	</aside>
	<!-- ./wrapper -->
	
	<script src="{{asset('dashboard/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
	<script src="{{asset('dashboard/plugins/chart.js/Chart.min.js')}}"></script>
	<script src="{{asset('dashboard/plugins/moment/moment.min.js')}}"></script>
	<script src="{{asset('dashboard/plugins/daterangepicker/daterangepicker.js')}}"></script>
	<script src="{{asset('dashboard/plugins/summernote/summernote-bs4.min.js')}}"></script>
	<script src="{{asset('dashboard/dist/js/adminlte.js')}}"></script>
	<script src="{{asset('js/csrf.js')}}"></script>
	
	@yield('scripts')

	<!-- Matomo -->
	<script>
		var _paq = window._paq = window._paq || [];
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function() {
			var u="//matomo.mathiasdiaz.uy/";
			_paq.push(['setTrackerUrl', u+'matomo.php']);
			_paq.push(['setSiteId', '1']);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
			g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
		})();
	</script>
	<!-- End Matomo Code -->
</body>
</html>
