<li class="nav-item 
  @if(isset($menu_superior) and ($menu_superior == 'inmobiliaria')) 
    menu-open 
  @endif">
  <a href="" class="nav-link">
    <i class="fa fa-building nav-icon"></i>
    <p>
      Inmobiliaria
      <i class="fas fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item  pl-2">
      <a href="{{url('admin/inmobiliaria')}}" class="nav-link @if($menu_activo == 'inmobiliaria_propiedades') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Propiedades</p>
      </a>
    </li>
  </ul>
</li>