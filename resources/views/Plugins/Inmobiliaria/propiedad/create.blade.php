@extends('layouts.dashboard', ['menu_activo' => 'inmobiliaria', 'menu_superior' => 'inmobiliaria'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-6">
					<h1 class="m-0 text-dark">Crear propiedad</h1>
				</div><!-- /.col -->
				<div class="col-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item">Inmobiliaria</li>
                        <li class="breadcrumb-item active">Crear propiedad</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<section class="content">
		<div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5>
                                Detalles
                            </h5>
                        </div>
                        <div class="col text-right">
                            <a href="{{url('admin/inmobiliaria')}}" class="btn btn-secondary">
                                <i class="fa fa-chevron-left"></i>
                                Atras
                            </a>
                        </div>
                    </div>
                </div>
                <form action="{{url('admin/inmobiliaria/propiedad')}}" class="form-horizontal" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="">Titulo</label>
                                    <input type="text" class="form-control" name="titulo" required placeholder="Titulo" require>
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="">Precio</label>
                                    <input type="text" class="form-control" name="precio" required placeholder="Precio" require>
                                </div>
                            </div>

                            <div class="col-12">
                                <textarea name="descripcion" id="editor" rows="10" class="summernote form-control" required></textarea>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Tipo</label>
                                <select name="tipo" id="" class="form-control">
                                    <option value="apartamento">Apto</option>
                                    <option value="casa">Casa</option>
                                    <option value="terreno">Terreno</option>
                                    <option value="oficina">Oficina</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Estado</label>
                                <select name="estado" id="" class="form-control">
                                    <option value="activo">Activo</option>
                                    <option value="inactivo">Inactivo</option>
                                    <option value="vendido">Vendido</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Dirección</label>
                                <input type="text" class="form-control" name="direccion" placeholder="Dirección" required>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Ciudad</label>
                                <input type="text" class="form-control" name="ciudad" placeholder="Ciudad" required>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Localidad</label>
                                <input type="text" class="form-control" name="localidad" placeholder="Localidad">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Codigo postal</label>
                                <input type="text" class="form-control" name="codigo_postal" placeholder="Codigo Postal">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Dormitorios</label>
                                <input type="text" class="form-control" name="dormitorios" placeholder="Dormitorios">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Baños</label>
                                <input type="text" class="form-control" name="banos" placeholder="Baños">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Area</label>
                                <input type="text" class="form-control" name="area" placeholder="Area m2">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Garaje</label>
                                @include('ayuda.switch', ['nombre' => "garaje", 'estado' => "off"])
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Marcar como destacado?</label>
                                @include('ayuda.switch', ['nombre' => "destacado", 'estado' => "off"])
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">En venta?</label>
                                @include('ayuda.switch', ['nombre' => "venta", 'estado' => "on"])
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col text-right">
                                <button class="btn btn-info">
                                    <i class="fa fa-save mr-2"></i>
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
			</div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5>Caracteristicas</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center text-secondary">
                            <i class="fas fa-info-circle fa-3x"></i>
                            <br><br>
                            <p>Debe hacer click en guardar antes de poder completar este campo</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5>Imagenes</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center text-secondary">
                            <i class="fas fa-info-circle fa-3x"></i>
                            <br><br>
                            <p>Debe hacer click en guardar antes de poder completar este campo</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5>Amenities</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center text-secondary">
                            <i class="fas fa-info-circle fa-3x"></i>
                            <br><br>
                            <p>Debe hacer click en guardar antes de poder completar este campo</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5>Planos</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center text-secondary">
                            <i class="fas fa-info-circle fa-3x"></i>
                            <br><br>
                            <p>Debe hacer click en guardar antes de poder completar este campo</p>
                        </div>
                    </div>
                </div>
            </div>
			
		</div>
	</section>	
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('.summernote').summernote({
                placeholder: "Escribe aquí una descripción",
                height: 200,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                }
            });
        });
    </script>
@endsection