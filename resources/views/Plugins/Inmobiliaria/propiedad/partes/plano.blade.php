 <div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h5>Planos</h5>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col text-center text-secondary">
                @if($propiedad->planos->count() == 0)
                    @include('ayuda.sin_registros')
                @else
                
                @endif
            </div>                                
        </div>
    </div>
</div>