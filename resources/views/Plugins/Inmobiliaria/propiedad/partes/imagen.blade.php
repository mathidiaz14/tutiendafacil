<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h5>Imagenes</h5>
            </div>
            <div class="col text-right">
                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#boton_agregar_imagen">
                    <i class="fa fa-plus"></i>
                    Agregar
                </button>

                <!-- Modal -->
                <div class="modal fade text-left" id="boton_agregar_imagen" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-gradient-secondary">
                                <div class="col-10">
                                    <h5>Agregar imagenes</h5>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col">
                                        <form action="{{url('admin/inmobiliaria/multimedia')}}" method="post" class="dropzone" id="cargarImagenPropiedad" enctype="multipart/form-data">
                                            @csrf
                                            <input type="hidden" name="propiedad" id="propiedad" value="{{$propiedad->id}}">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col text-center text-secondary">
                <div id="imagenes">
                    @include('Plugins.Inmobiliaria.propiedad.partes.imagenes')
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js"></script>
<script>
    $('.dropzone').dropzone(
    { 
        dictDefaultMessage: "Haz click aquí o arrastra las imágenes para cargarlas",
        maxFilesize: 30, 
        acceptedFiles: ".jpeg,.jpg,.png,.gif, .mp4",
        success: function () 
        {
            $('#imagenes').load("{{url('admin/inmobiliaria/multimedia')}}/{{$propiedad->id}}"); 
        }
    });

</script>
