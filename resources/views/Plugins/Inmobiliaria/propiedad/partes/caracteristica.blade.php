<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col">
                <h5>Caracteristicas</h5>
            </div>
            <div class="col text-right">
                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#boton_agregar_caracteristica">
                    <i class="fa fa-plus"></i>
                    Agregar
                </button>

                <!-- Modal -->
                <div class="modal fade text-left" id="boton_agregar_caracteristica" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header bg-gradient-secondary">
                                <div class="col-10">
                                    <h5>Agregar caracteristicas</h5>
                                </div>
                                <div class="col-2">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="">Nueva caractersitica</label>
                                        <div class="table table-responsive">
                                            <table class="table table-striped">
                                                <form action="{{url('admin/inmobiliaria/caracteristica')}}" method="post">
                                                    @csrf
                                                    <input type="hidden" name="propiedad" value="{{$propiedad->id}}">
                                                    <tr>
                                                        <td>
                                                            <input type="text" class="form-control" name="nombre" placeholder="Nombre" required="">
                                                        </td>
                                                        <td>
                                                            <input type="text" class="form-control" name="valor" placeholder="Valor" required="">
                                                        </td>
                                                        <td>
                                                            <button class="btn btn-info btn-block">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </form>
                                            </table>
                                        </div>
                                        <hr>
                                    </div>
                                    <div class="col-12">
                                        <label for="">Caractersiticas existentes</label>
                                        <div class="table table-responsive">
                                            <table class="table table-striped">
                                                @foreach(empresa()->inmobiliaria_caracteristicas as $caracteristica)

                                                    @if($propiedad->caracteristicas->where('id', $caracteristica->id)->first() == null)
                                                        <form action="{{url('admin/inmobiliaria/caracteristica/attach')}}" method="post">
                                                            @csrf
                                                            <input type="hidden" name="propiedad" value="{{$propiedad->id}}">
                                                            <input type="hidden" name="caracteristica" value="{{$caracteristica->id}}">

                                                            <tr>
                                                                <td>{{$caracteristica->nombre}}</td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="valor" placeholder="Valor" required="">
                                                                </td>
                                                                <td>
                                                                    <button class="btn btn-info btn-block text-white" onclick="function(){$('.cargando').fadeIn()}">
                                                                        <i class="fa fa-plus"></i>
                                                                    </button>
                                                                </td>
                                                            </tr>   
                                                        </form>
                                                    @endif
                                                @endforeach
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <form action="{{url('admin/inmobiliaria/caracteristica', $propiedad->id)}}" method="post">
        @csrf
        @method('PATCH')
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    @if($propiedad->caracteristicas->count() == 0)
                        @include('ayuda.sin_registros')
                    @else
                        <div class="table table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Titulo</th>
                                    <th>Valor</th>
                                    <th></th>
                                </tr>
                                @foreach($propiedad->caracteristicas as $caracteristica)
                                    <tr id="caracteristica_{{$caracteristica->pivot->id}}">
                                        <td>{{$caracteristica->nombre}}</td>
                                        <td>
                                            <input type="text" class="form-control" value="{{$caracteristica->pivot->valor}}" name="caracteristica_valor_{{$caracteristica->id}}">
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-block btn_eliminar_caracteristica" attr-id="{{$caracteristica->id}}">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        @if($propiedad->caracteristicas->count() > 0)
            <div class="card-footer">
                <div class="row">
                    <div class="col text-right">
                        <button class="btn btn-info">
                            <i class="fa fa-save"></i>
                            Guardar
                        </button>
                    </div>
                </div>
            </div>
        @endif
    </form>
</div>