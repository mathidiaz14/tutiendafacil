@if($propiedad->imagenes->count() == 0)
	@include('ayuda.sin_registros')
@else
	<div class="row" id="sortable"> 	
		@foreach($propiedad->imagenes->sortBy('numero') as $imagen)
			<div class="col-2 mt-2">	
				@php
					$explode = explode('.', $imagen->url);
				@endphp
				@if($explode[1] != 'mp4')
					<img src="{{asset($imagen->url)}}" alt="" width="100%" attr-id="{{$imagen->id}}" class="imagenes-propiedad">
				@else
					<video width="100%" src="{{asset($imagen->url)}}" attr-id="{{$imagen->id}}" class="imagenes-propiedad"></video>
					<i class="fa fa-play" style="    position: absolute;top: 35%;left: 35%;background: #ffffffeb;padding: 10%;border-radius: 20%;"></i>
				@endif
				<form action="{{url('admin/inmobiliaria/multimedia', $imagen->id)}}" method="POST">
					@csrf
					<input type='hidden' name='_method' value='DELETE'>
					<button class="btn btn-danger" style="position: absolute; top:5px; right: 10px;">
						<i class="fa fa-trash"></i>
					</button>
				</form>
			</div>
		@endforeach
	</div>		
@endif

<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<script>
	$( "#sortable" ).sortable({
		opacity: 0.5,
		stop: function()
		{
			var array = [];

			$('.imagenes-propiedad').each(function(index, elemento)
			{
				array.push([$(this).attr('attr-id'), index]);
			})

			$.ajax({
	            url: "{{url('admin/inmobiliaria/multimedia', $propiedad->id)}}",
				type: 'POST',
	            dataType: "JSON",
	            data: {
	                "_token": $('meta[name="csrf-token"]').attr('content'),
	                "_method": "PATCH",
	                "array": array,
	            }
	        });
		}
	});

	
</script>