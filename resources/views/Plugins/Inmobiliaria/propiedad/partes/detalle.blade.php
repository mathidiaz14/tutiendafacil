<div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h5>
                                Detalles
                            </h5>
                        </div>
                        <div class="col text-right">
                            <a href="{{url('admin/inmobiliaria')}}" class="btn btn-secondary">
                                <i class="fa fa-chevron-left"></i>
                                Atras
                            </a>
                        </div>
                    </div>
                </div>
                <form action="{{url('admin/inmobiliaria/propiedad', $propiedad->id)}}" class="form-horizontal" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="">Titulo</label>
                                    <input type="text" class="form-control" name="titulo" required placeholder="Titulo" value="{{$propiedad->titulo}}">
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="">Precio</label>
                                    <input type="text" class="form-control" name="precio" required placeholder="Precio" value="{{$propiedad->precio}}">
                                </div>
                            </div>

                            <div class="col-12">
                                <textarea name="descripcion" id="editor" rows="10" class="summernote form-control">{{$propiedad->descripcion}}</textarea>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Tipo</label>
                                <select name="tipo" id="" class="form-control">
                                    @if($propiedad->tipo == "apartamento")
                                        <option value="apartamento" selected>Apto</option>
                                    @else
                                        <option value="apartamento">Apto</option>
                                    @endif

                                    @if($propiedad->tipo == "casa")
                                        <option value="casa" selected>Casa</option>
                                    @else
                                        <option value="casa">Casa</option>
                                    @endif

                                    @if($propiedad->tipo == "terreno")
                                        <option value="terreno" selected>Terreno</option>
                                    @else
                                        <option value="terreno">Terreno</option>
                                    @endif

                                    @if($propiedad->tipo == "oficina")
                                        <option value="oficina" selected>Oficina</option>
                                    @else
                                        <option value="oficina">Oficina</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Estado</label>
                                <select name="estado" id="" class="form-control">
                                    @if($propiedad->estado == "activo")
                                        <option value="activo" selected>Activo</option>
                                    @else
                                        <option value="activo">Activo</option>
                                    @endif

                                    @if($propiedad->estado == "inactivo")
                                        <option value="inactivo" selected>Inactivo</option>
                                    @else
                                        <option value="inactivo">Inactivo</option>
                                    @endif

                                    @if($propiedad->estado == "vendido")
                                        <option value="vendido" selected>Vendido</option>
                                    @else
                                        <option value="vendido">Vendido</option>
                                    @endif

                                    @if($propiedad->estado == "alquilado")
                                        <option value="alquilado" selected>Alquilado</option>
                                    @else
                                        <option value="alquilado">Alquilado</option>
                                    @endif
                                </select>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Dirección</label>
                                <input type="text" class="form-control" name="direccion" value="{{$propiedad->direccion}}" placeholder="Dirección" required>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Ciudad</label>
                                <input type="text" class="form-control" name="ciudad" value="{{$propiedad->ciudad}}" placeholder="Ciudad" required>
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Localidad</label>
                                <input type="text" class="form-control" name="localidad" value="{{$propiedad->localidad}}" placeholder="Localidad">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Codigo postal</label>
                                <input type="text" class="form-control" name="codigo_postal" value="{{$propiedad->codigo_postal}}" placeholder="Codigo Postal">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Dormitorios</label>
                                <input type="text" class="form-control" name="dormitorios" value="{{$propiedad->dormitorios}}" placeholder="Dormitorios">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Baños</label>
                                <input type="text" class="form-control" name="banos" value="{{$propiedad->baños}}" placeholder="Baños">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Area</label>
                                <input type="text" class="form-control" name="area" value="{{$propiedad->area}}" placeholder="Area m2">
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Garaje</label>
                                @include('ayuda.switch', ['nombre' => "garaje", 'estado' => $propiedad->garaje])
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">Marcar como destacado?</label>
                                @include('ayuda.switch', ['nombre' => "destacado", 'estado' => $propiedad->destacado])
                            </div>
                            <div class="col-12 col-md-6 col-lg-4 mt-2">
                                <label for="">En venta?</label>
                                @include('ayuda.switch', ['nombre' => "venta", 'estado' => $propiedad->venta])
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col text-right">
                                <button class="btn btn-info">
                                    <i class="fa fa-save mr-2"></i>
                                    Guardar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
			</div>