@extends('layouts.dashboard', ['menu_activo' => 'inmobiliaria', 'menu_superior' => 'inmobiliaria'])

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
@endsection

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-6">
					<h1 class="m-0 text-dark">Editar propiedad</h1>
				</div><!-- /.col -->
				<div class="col-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item">Inmobiliaria</li>
                        <li class="breadcrumb-item active">Editar propiedad</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<section class="content">
		<div class="container-fluid">
            
            @include('Plugins.Inmobiliaria.propiedad.partes.detalle')

            @include('Plugins.Inmobiliaria.propiedad.partes.caracteristica')

            @include('Plugins.Inmobiliaria.propiedad.partes.imagen')
			
		</div>
	</section>	
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function()
        {
            $('.summernote').summernote({
                placeholder: "Escribe aquí una descripción",
                height: 200,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                }
            });

            $('.btn_eliminar_caracteristica').click(function()
            {
                var id = $(this).attr('attr-id');

                $.ajax({
                    url: "{{url('admin/inmobiliaria/caracteristica/detach')}}",
                    type: 'POST',
                    dataType: "JSON",
                    data: {
                        "_token": $('meta[name="csrf-token"]').attr('content'),
                        "propiedad": '{{$propiedad->id}}',
                        "caracteristica": id,
                    }
                }).done(function(){
                    location.reload();
                });
            });
        });
    </script>
@endsection