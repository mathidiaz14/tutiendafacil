@extends('layouts.dashboard', ['menu_activo' => 'inmobiliaria_propiedades', 'menu_superior' => 'inmobiliaria'])

@section('contenido')

<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item active">Inmobiliaria</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">

					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col">
									<h5 class="card-title">
										Propiedades
									</h5>
								</div>
                                <div class="col text-right">
                                    <a href="{{url('admin/inmobiliaria/propiedad/create')}}" class="btn btn-info">
                                        <i class="fa fa-plus mr-2"></i>
                                        Agregar propiedad
                                    </a>
                                </div>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
                            	<div class="col-12">
									@if($propiedades->count() == 0)
		                                @include("ayuda.sin_registros")
		                            @else
	                            		<div class="table table-responsive">
	                            			<table class="table table-stripped">
	                            				<tr>
	                            					<th>#</th>
	                            					<th>Titulo</th>
	                            					<th>Tipo</th>
	                            					<th>Precio</th>
	                            					<th>Dirección</th>
	                            					<th>Estado</th>
	                            					<th></th>
	                            					<th></th>
	                            				</tr>
	                            				@foreach($propiedades as $propiedad)
	                            					<tr>
	                            						<td>{{$propiedad->id}}</td>
	                            						<td>{{$propiedad->titulo}}</td>
	                            						<td>{{$propiedad->tipo}}</td>
	                            						<td>{{$propiedad->precio}}</td>
	                            						<td>{{$propiedad->direccion}}</td>
	                            						<td>{{$propiedad->estado}}</td>
	                            						<td>
	                            							<a href="{{route('propiedad.edit', $propiedad->id)}}" class="btn btn-primary">
	                            								<i class="fa fa-edit"></i>
	                            							</a>
	                            						</td>
	                            						<td>
	                            							@include('ayuda.eliminar', ['id' => $propiedad->id, 'ruta' => url('admin/inmobiliaria/propiedad', $propiedad->id)])
	                            						</td>
	                            					</tr>
	                            				@endforeach
	                            			</table>
	                            		</div>
	                            	@endif
                            	</div>			
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
<script>
	
</script>
@endsection