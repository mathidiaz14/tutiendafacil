<li class="nav-item 
  @if(isset($menu_superior) and ($menu_superior == 'mercadolibre')) 
    menu-open 
  @endif">
  <a href="" class="nav-link">
    <i class="fas fa-circle nav-icon"></i>
    <p>
      MercadoLibre
      <i class="fas fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item  pl-2">
      <a href="{{url('admin/mercadolibre')}}" class="nav-link @if($menu_activo == 'mercadolibre_resumen') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Resumen</p>
      </a>
    </li>
    <li class="nav-item  pl-2">
      <a href="{{url('admin/mercadolibre/producto')}}" class="nav-link @if($menu_activo == 'mercadolibre_producto') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Productos</p>
      </a>
    </li>
    <li class="nav-item  pl-2">
      <a href="{{url('admin/mercadolibre/venta')}}" class="nav-link @if($menu_activo == 'mercadolibre_venta') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Ventas</p>
      </a>
    </li>
    <li class="nav-item  pl-2">
      <a href="{{url('admin/mercadolibre/consultas')}}" class="nav-link @if($menu_activo == 'mercadolibre_venta') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Consultas</p>
      </a>
    </li>
    <li class="nav-item  pl-2">
      <a href="{{url('admin/mercadolibre/configuracion')}}" class="nav-link @if($menu_activo == 'mercadolibre_configuracion') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Configuración</p>
      </a>
    </li>
  </ul>
</li>