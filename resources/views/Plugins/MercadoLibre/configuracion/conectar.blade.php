@extends('layouts.dashboard', ['menu_activo' => 'mercadolibre_configuracion', 'menu_superior' => 'mercadolibre'])

@section('contenido')

<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item">MercadoLibre</li>
						<li class="breadcrumb-item active">Conectar</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">

					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col">
									<h5 class="card-title">
										Conectar con MercadoLibre
									</h5>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-12 text-center">
									<img src="{{asset('img/mercadolibre.png')}}" alt="" width="400px" class="my-4">
									<br><br>
									<h3>¡Es necesario conectar tu cuenta con la de MercadoLibre para comenzar! </h3>
									<br><br>
									<a href="https://auth.mercadolibre.com.uy/authorization?
											client_id={{env('ML_CLIENT_ID')}}
											&response_type=code
											&platform_id=ml
											&state={{Auth::user()->empresa_id}}
											&redirect_uri={{url('mercadolibre/conexion')}}" class="btn-lg btn-info px-5">
										<i class="fa fa-plug"></i>
										Conectar
									</a>    
									<br><br>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
<script>
	
</script>
@endsection