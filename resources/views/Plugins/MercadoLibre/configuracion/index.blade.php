@extends('layouts.dashboard', ['menu_activo' => 'mercadolibre_configuracion', 'menu_superior' => 'mercadolibre'])

@section('contenido')

<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item">MercadoLibre</li>
						<li class="breadcrumb-item active">Configuración</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">

					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col">
									<h5 class="card-title">
										Configuración
									</h5>
								</div>
								<div class="col text-right">
									<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deconectarMercadolibre">
										<i class="fa fa-times"></i>
										<i class="fa fa-plug"></i>
										Desconectar
									</button>

									<!-- Modal -->
									<div class="modal fade deleteModal" id="deconectarMercadolibre" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
										<div class="modal-dialog modal-dialog-centered" role="document">
									  		<div class="modal-content">
												<div class="modal-header bg-gradient-danger">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
									    		</div>
									    		<div class="modal-body text-center">
									    			<p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
									      			<h4>¿Desea desconectar su cuenta de MercadoLibre?</h4>
									      			<br>
									            <hr>
									      			<div class="row">
									      				<div class="col">
								    						<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
								    							NO
								    						</button>
									      				</div>
									      				<div class="col">
									      					<a href="{{url('admin/mercadopago/desconectar')}}" class="btn btn-danger btn-block">
																SI
															</a>
									      				</div>
									      			</div>
									    		</div>
									  		</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-md-4">
									@include('ayuda.boton_modal', [
																	'id' 				=> 'importar',
																	'color' 			=> 'dark btn-block', 
																	'icono' 			=> '<i class="fa fa-download mr-3"></i>',
																	'icono_grande'		=> '<i class="fa fa-exclamation-circle fa-4x text-secondary"></i>',
																	'texto_boton' 		=> 'Importar productos desde MercadoLibre a TTF',
																	'texto_principal' 	=> '¿Desea importar los productos?',
																	'ruta' 				=> url("admin/mercadolibre/configuracion/importar"),
																])
								</div>
								<div class="col-12 col-md-4">
									@include('ayuda.boton_modal', [
																	'id' 				=> 'publicar',
																	'color' 			=> 'dark btn-block', 
																	'icono' 			=> '<i class="fa fa-upload mr-3"></i>',
																	'icono_grande'		=> '<i class="fa fa-exclamation-circle fa-4x text-secondary"></i>',
																	'texto_boton' 		=> 'Publicar productos desde TTF a MercadoLibre',
																	'texto_principal' 	=> '¿Desea publicar los productos?',
																	'ruta' 				=> url("admin/mercadolibre/configuracion/publicar"),
																])
								</div>
								<div class="col-12 col-md-4">
									@include('ayuda.boton_modal', [
																	'id' 				=> 'asociar',
																	'color' 			=> 'dark btn-block', 
																	'icono' 			=> '<i class="fa fa-sync mr-3"></i>',
																	'icono_grande'		=> '<i class="fa fa-exclamation-circle fa-4x text-secondary"></i>',
																	'texto_boton' 		=> 'Asociar productos de TTF y MercadoLibre',
																	'texto_principal' 	=> '¿Desea asociar los productos?',
																	'ruta' 				=> url("admin/mercadolibre/configuracion/asociar"),
																])
								</div>
							</div>	
						</div>
					</div>
				</section>

				<section class="col-12">
					<div class="card">
						<div class="card-header">
							<h5 class="card-title">
								Datos del usuario
							</h5>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-12 col-md-6">
								<p>ID: <b>{{$usuario_ml['id']}}</b></p>
								<p>Nombre: <b>{{$usuario_ml['first_name']}}</b></p>
								<p>Apellido: <b>{{$usuario_ml['last_name']}}</b></p>
								<p>Email: <b>{{$usuario_ml['email']}}</b></p>
								<p>Teléfono: <b>{{$usuario_ml['phone']['number']}}</b></p>
								<p>Dirección: <b>{{$usuario_ml['address']['address']}}</b></p>
								<p>Nickname: <b><a href="{{$usuario_ml['permalink']}}" target="_blank">{{$usuario_ml['nickname']}}</a></b></p>
							</div>
							<div class="col-12 col-md-6">
								<p>Tipo de usuario: <b>{{$usuario_ml['user_type']}}</b></p>
								<p>Fecha de registro: <b>{{\Carbon\Carbon::create($usuario_ml['registration_date'])->format('d/m/Y H:i')}}</b></p>
								<p>Pais: <b>{{$usuario_ml['country_id']}}</b></p>
								<p>Experiencia de vendedor: <b>{{$usuario_ml['seller_experience']}}</b></p>
								<p>Reputación de vendedor: <b>{{$usuario_ml['seller_reputation']['level_id']}}</b></p>
								<p>Cantidad de publicaciones: <b>{{$cantidad['paging']['total']}}</b></p>
							</div>		
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
<script>
	
</script>
@endsection

