@extends('layouts.dashboard', ['menu_activo' => 'mercadolibre_producto', 'menu_superior' => 'mercadolibre'])

@section('contenido')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Productos MercadoLibre</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="{{url('admin')}}">Inicio</a></li>
						<li class="breadcrumb-item">MercadoLibre</li>
						<li class="breadcrumb-item active">Productos</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	
	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<section class="col-lg-12">

					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col">
									<h3 class="card-title">
										Productos <small>({{empresa()->productos->where('mlu_id', '!=', 'null')->count()}})</small>
									</h3>
								</div>
							</div>
						</div>
						<div class="card-body">
							@include('admin.producto.secciones.lista_productos', ['productos' => $productos])
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</div>
@endsection

@section('scripts')
<script>
	
</script>
@endsection