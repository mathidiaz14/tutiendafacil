<div class="row">
	<div class="col-12 col-md-6">
		<section class="col-lg-12 connectedSortable">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">
						<i class="fas fa-pen"></i>
						Crear borrador
					</h3>
				</div>
				<div class="card-body">
					<form action="{{url('admin/blog/entrada')}}" class="form-horizontal" method="post">
						@csrf
						<input type="hidden" name="url" id="url">
						<input type="hidden" name="estado" value="borrador">
						<div class="form-group">
							<label for="">
								Titulo
							</label>
							<input type="text" class="form-control" name="titulo" id="titulo" placeholder="Ingrese titulo">
						</div>
						<div class="form-group">
							<label for="">
								Contenido
							</label>
							<textarea name="contenido" id="" cols="30" rows="5" class="form-control" placeholder="Ingrese el contenido"></textarea>
						</div>
						<hr>
						<div class="form-group text-right">
							<button class="btn btn-info">
								<i class="fa fa-save"></i>
								Guardar
							</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
	<div class="col-12 col-md-6">
		<section class="col-lg-12 connectedSortable">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title">
						<i class="fas fa-pen"></i>
						Ultimas entradas
					</h5>
				</div>
				<div class="card-body">
					@php
						$entradas = empresa()->blogEntradas->sortByDesc('created_at')->take(5);
					@endphp

					@if($entradas->count() == 0)
						@include('ayuda.sin_registros')
					@else
					<div class="table table-responsive">
						<table class="table table-striped">
							<tbody>
								<tr>
									<th>Nombre</th>
									<th>Estado</th>
									<th>Comentarios</th>
									<th>Fecha creación</th>
								</tr>
								@foreach($entradas as $entrada)
								<tr>
									<td>
										<a href="{{url('admin/blog/entrada', $entrada->id)}}/edit">
											{{$entrada->titulo}}
										</a>
									</td>
									<td>{{$entrada->estado}}</td>
									<td>
										@if(blog_comentarios($entrada))
											{{$entrada->comentarios->count()}}
										@else
											Desactivados
										@endif
									</td>
									<td>{{$entrada->created_at->format('d/m/Y H:i')}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					@endif
				</div>
			</div>
		</section>
	</div>
</div>

<script>
	$('#titulo').keyup(function()
	{
		$('#url').val($(this).val().toLowerCase().replace(/\s/g, "-").replace(/[^ a-z0-9áéíóúüñ]+/ig,"-"));
	});
</script>