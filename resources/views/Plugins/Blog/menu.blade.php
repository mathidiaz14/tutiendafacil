<li class="nav-item 
  @if(isset($menu_superior) and ($menu_superior == 'blog')) 
    menu-open 
  @endif">
  <a href="" class="nav-link">
    <i class="fas fa-newspaper nav-icon"></i>
    <p>
      Blog
      <i class="fas fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item  pl-2">
      <a href="{{url('admin/blog/entrada')}}" class="nav-link @if($menu_activo == 'blog_entrada') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Entradas</p>
      </a>
    </li>
    <li class="nav-item  pl-2">
      <a href="{{url('admin/blog/comentario')}}" class="nav-link @if($menu_activo == 'blog_comentario') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Comentarios</p>
      </a>
    </li>
    <li class="nav-item  pl-2">
      <a href="{{url('admin/blog/categoria')}}" class="nav-link @if($menu_activo == 'blog_categoria') active @endif">
        <i class="far fa-circle nav-icon"></i>
        <p>Categorias</p>
      </a>
    </li>
  </ul>
</li>