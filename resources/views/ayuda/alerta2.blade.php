<style>
	.ttf_notificacion_alert
	{
		position: fixed;
	    color: white;
	    z-index: 3000;
	    text-align: center;
	    bottom: 30px;
	    display: none;
	}

	.ttf_notificacion_alert .body
	{
	    height: 100px;
        border-radius: 5px;
        box-shadow: 2px 2px 7px -3px black;
	}

	.ttf_notificacion_alert .content
	{
		padding-top: 35px;
	}

	.ttf_notificacion_alert .icon
	{
		font-size: 40px;
		padding-top: 20px;
		background: rgba(0,0,0,.2);
		height: 100%;
	}

	.ttf_notificacion_alert .exito
	{
		background: #2ECC40db;
	}

	.ttf_notificacion_alert .error
	{
		background: #f24343db;
	}

	.ttf_notificacion_alert .warning
	{
		background: #eae356db;
	}

	.ttf_notificacion_alert .info
	{
		background: #3a86eadb;
	}

	.ttf_notificacion_alert .close
	{
		display: none;
		font-size: 25px;
		position: absolute;
		color: white;
		top: 5px;
		right: 10px;
	}
</style>

<div class="ttf_notificacion_alert col-12 col-md-6 offset-md-3">
    <div class="row body">
        <div class="col-2 icon"></div>
        <div class="col-9 content"></div>
        <div class="col-1">
            <a class="close">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>

<script>
	var timeOutId = "";

        $('.ttf_notificacion_alert .close').click(function()
        {
            $('.ttf_notificacion_alert').fadeOut();
        });

        $('.ttf_notificacion_alert').hover(function()
        {
            clearTimeout(timeOutId);
            $('.ttf_notificacion_alert .close').fadeIn();
        });

        $('.ttf_notificacion_alert').on('mouseleave', function()
        {
            timeOutId = setTimeout(function(){$('.ttf_notificacion_alert').fadeOut();}, 3000);
            $('.ttf_notificacion_alert .close').hide();
        });

        function ttf_notificacion($content, $type) 
        {
            $('.ttf_notificacion_alert .content').html("<p>"+$content+"</p>");

            if($type == "exito")
            {
                $('.ttf_notificacion_alert .body').removeClass('error');
                $('.ttf_notificacion_alert .body').removeClass('warning');
                $('.ttf_notificacion_alert .body').removeClass('info');
                $('.ttf_notificacion_alert .body').addClass('exito');

                $('.ttf_notificacion_alert .icon').html('<i class="fa fa-check"></i>');
            }

            if($type == "warning")
            {
                $('.ttf_notificacion_alert .body').removeClass('error');
                $('.ttf_notificacion_alert .body').removeClass('exito');
                $('.ttf_notificacion_alert .body').removeClass('info');
                $('.ttf_notificacion_alert .body').addClass('warning');

                $('.ttf_notificacion_alert .icon').html('<i class="fa fa-triangle-exclamation"></i>');
            }

            if($type == "error")
            {
                $('.ttf_notificacion_alert .body').removeClass('exito');
                $('.ttf_notificacion_alert .body').removeClass('warning');
                $('.ttf_notificacion_alert .body').removeClass('info');
                $('.ttf_notificacion_alert .body').addClass('error');

                $('.ttf_notificacion_alert .icon').html('<i class="fa fa-times"></i>');
            }

            if($type == "info")
            {
                $('.ttf_notificacion_alert .body').removeClass('exito');
                $('.ttf_notificacion_alert .body').removeClass('warning');
                $('.ttf_notificacion_alert .body').removeClass('error');
                $('.ttf_notificacion_alert .body').addClass('info');

                $('.ttf_notificacion_alert .icon').html('<i class="fa fa-exclamation"></i>');
            }

            $('.ttf_notificacion_alert').fadeIn(1000);

            timeOutId = setTimeout(function(){$('.ttf_notificacion_alert').fadeOut();}, 6000);
        };

        @if(session('error'))
            ttf_notificacion("{{session('error')}}", "error");
            {{session()->forget('error')}}
        @endif

        @if(session('exito'))
            ttf_notificacion("{{session('exito')}}", "exito");
            {{session()->forget('exito')}}
        @endif

        @if(session('warning'))
            ttf_notificacion("{{session('warning')}}", "warning");
            {{session()->forget('warning')}}
        @endif

        @if(session('info'))
            ttf_notificacion("{{session('info')}}", "info");
            {{session()->forget('info')}}
        @endif
</script>