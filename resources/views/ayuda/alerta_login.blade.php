@if(session('error'))
    <div class="alert alert-danger" role="alert">
        {{session('error')}}
    </div>
    {{session()->forget('error')}}
@endif

@if(session('exito'))
    <div class="alert alert-success" role="alert">
        {{session('exito')}}
    </div>
    {{session()->forget('exito')}}
@endif