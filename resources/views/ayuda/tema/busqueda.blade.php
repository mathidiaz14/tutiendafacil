<!-- 
	Llamado de busqueda
	********************

	include('ayuda.tema.busqueda')
-->

<style>
	.busqueda
	{
		position: fixed;
	    width: 100%;
	    padding: 0 25%;
	    top:50%;
	    z-index: 2;  
	    display: none;
	}

	.btn_busqueda
	{
		cursor: pointer;
	}

	.busqueda_fondo
	{
		position: fixed;
	    width: 100%;
	    height: 100%;
	    top:0;
	    z-index: 1;   
	    background: #000000;
	    opacity: .8;
	    display: none;
	}

	.busqueda input
	{
		height: 60px;
	    color: black;
	    border: solid 3px white;
	}

	.busqueda .input-group-text
	{
		background: white;
	}

	.btn_cerrar_busqueda
	{
		margin-top: 100px;
		color: white!important;
	}

	.btn_cerrar_busqueda a
	{
		text-decoration: none;
	}
</style>

<div class='busqueda' style="display: none;">
	<form action="{{url('productos')}}" method='post' class='form-horizontal'>
		<div class='form-group'>
	      	<div class='input-group'>
		        <input type='text' class='form-control' id='busqueda_input' name='busqueda' placeholder='¿Qué deseas buscar?'>
	      	</div>
		</div>
	</form>
	<div class='col text-center btn_cerrar_busqueda'>
		<a style='color:white!important;'>
			<i class='fa fa-times fa-2x'></i> CERRAR
		</a>
	</div>
</div>

<div class='busqueda_fondo'></div>

<script>
	$(document).ready(function()
	{
		$('.btn_busqueda').click(function()
		{
			$('.busqueda').fadeIn();
			$('.busqueda_fondo').fadeIn();
			$('#busqueda_input').focus();
		});

		$('.btn_cerrar_busqueda').click(function()
		{
			$('.busqueda').fadeOut();
			$('.busqueda_fondo').fadeOut();
		});

		$('.busqueda_fondo').click(function()
		{
			$('.busqueda').fadeOut();
			$('.busqueda_fondo').fadeOut();
		});			
	});
</script>