<link rel="stylesheet" href="{{asset('css/baguetteBox.css')}}">
<link rel="stylesheet" href="{{asset('css/gallery-grid.css')}}">

<div class="tz-gallery">
	<div class="row">
		<div class="col-3">
			<div class="row">
				@foreach($producto->imagenes as $imagen)
					@if(!$loop->first)
						<div class="col-6 p-1">
							@if($imagen->url_ml == null)
				                <a class="lightbox" href="{{asset($imagen->url)}}">
				                	<img src="{{asset($imagen->url)}}" alt="{{asset($imagen->alternativo)}}" width="100%" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'" style="height: 60px;object-fit: cover;object-position: center;">
				                </a>
				            @else
				            	<a class="lightbox" href="{{asset($imagen->url_ml)}}">
				                	<img src="{{asset($imagen->url_ml)}}" alt="{{asset($imagen->alternativo)}}" width="100%" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'" style="height: 60px;object-fit: cover;object-position: center;">
				                </a>
				            @endif
						</div>
					@endif
				@endforeach	
			</div>
		</div>
		<div class="col-9">
			<div class="row">
				<div class="col-12">
					@if($producto->imagenes->first() == null)
						<a id="imagen_principal" class="lightbox" href='{{asset("img/default.jpg")}}' attr-href='{{asset("img/default.jpg")}}'>
		                	<img src='{{asset("img/default.jpg")}}' attr-url='{{asset("img/default.jpg")}}' width="100%" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'" style="height: 100%;object-fit: cover;object-position: center;">
		                </a>
					@else
		                @if($imagen->url_ml == null)
		                	<a id="imagen_principal" class="lightbox" href="{{asset($producto->imagenes->first()->url)}}" attr-href="{{asset($producto->imagenes->first()->url)}}">
			                	<img src="{{asset($producto->imagenes->first()->url)}}" attr-url="{{asset($producto->imagenes->first()->url)}}" alt="{{asset($producto->imagenes->first()->alternativo)}}" width="100%" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'" style="height: 100%;object-fit: cover;object-position: center;">
			                </a>
		                @else
		                	<a id="imagen_principal" class="lightbox" href="{{asset($producto->imagenes->first()->url_ml)}}" attr-href="{{asset($producto->imagenes->first()->url_ml)}}">
			                	<img src="{{asset($producto->imagenes->first()->url_ml)}}" attr-url="{{asset($producto->imagenes->first()->url_ml)}}" alt="{{asset($producto->imagenes->first()->alternativo)}}" width="100%" onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'" style="height: 100%;object-fit: cover;object-position: center;">
			                </a>
		                @endif
					@endif
				</div>
			</div>
		</div>
	</div>	
</div>

<script src="{{asset('js/baguetteBox.js')}}"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>