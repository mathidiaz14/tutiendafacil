@if($pagina->tipo == "contacto")

	@php
		$a      = rand(1,9);
        $b      = rand(1,9);
        $signo  = rand(0,2);
        
        if($signo == 0)
        {
            $resultado = $a + $b;
            $operacion = $a." + ".$b." = ?";
        }
        elseif($signo == 1)
        {
            $resultado = $a - $b;
            $operacion = $a." - ".$b." = ?";
        }
        else{
            $resultado = $a * $b;
            $operacion = $a." * ".$b." = ?";
        }

        Session(['resultado' => $resultado, 'operacion' => $operacion]);
	@endphp

	{!! RecaptchaV3::initJs() !!}

	
	
	@if ($errors->has('g-recaptcha-response'))
		<div class="alert alert-danger" role="alert">
			{{ $errors->first('g-recaptcha-response') }}  
		</div>
	@elseif ($errors->has('nombre'))
		<div class="alert alert-danger" role="alert">
			{{ $errors->first('nombre') }}  
		</div>
	@elseif ($errors->has('email'))
		<div class="alert alert-danger" role="alert">
			{{ $errors->first('email') }}  
		</div>
	@elseif ($errors->has('mensaje'))
		<div class="alert alert-danger" role="alert">
			{{ $errors->first('mensaje') }}  
		</div>
	@endif

	<form action="{{url('contacto_empresa')}}" method="post" class="form-horizontal col-12">
	    @csrf
	    {!! RecaptchaV3::field('register') !!}
	    <input type="checkbox" name="faxonly" id="faxonly" style="display:none;">
	    <div class="form-group">
	        <label for="" class="form-label">Nombre</label>
	        <input type="text" class="form-control" name="nombre" required="" value="{{old('nombre')}}" placeholder="Nombre">
	    </div>
	    <div class="form-group">
	        <label for="" class="form-label">Email</label>
	        <input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email">
	    </div>
	    <div class="form-group">
	        <label for="" class="form-label">Mensaje</label>
	        <textarea name="contenido" id="" cols="30" rows="5" class="form-control" placeholder="Mensaje">{{old('contenido')}}</textarea>
	    </div>
	    <div class="form-group text-left">
          <label for="">
            <b>{{Session('operacion')}}</b>
          </label>
          <input type="text" class="form-control" name="math" placeholder="Su respuesta" required>
        </div>
	    <div class="form-group text-right">
	        <button class="btn btn-primary">
	            <i class="fa fa-send"></i>
	            Enviar
	        </button>
	    </div>
	</form>
@endif