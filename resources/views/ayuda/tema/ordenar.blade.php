<select id="orden" class="form-control">
	@switch($orden)
		@case('asc')
			<option value="asc" selected="">Ascendente</option>
			<option value="desc">Descendente</option>
			<option value="men">Menor precio</option>
			<option value="may">Mayor precio</option>
        @break
		@case('desc')
			<option value="asc">Ascendente</option>
			<option value="desc" selected="">Descendente</option>
			<option value="men">Menor precio</option>
			<option value="may">Mayor precio</option>
		@break
		@case('men')
			<option value="asc">Ascendente</option>
			<option value="desc">Descendente</option>
			<option value="men" selected="">Menor precio</option>
			<option value="may">Mayor precio</option>
		@break
		@case('may')
			<option value="asc">Ascendente</option>
			<option value="desc">Descendente</option>
			<option value="men">Menor precio</option>
			<option value="may" selected="">Mayor precio</option>
		@break
		@default
			<option value="asc">Ascendente</option>
			<option value="desc">Descendente</option>
			<option value="men">Menor precio</option>
			<option value="may">Mayor precio</option>
		@break
	@endswitch
	
</select>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
	$('#orden').on('change', function() 
	{
		@if($busqueda != null)
			$(location).attr('href','{{url("productos")}}?busqueda={{$busqueda}}&orden='+this.value);
		@else
			$(location).attr('href','{{url("productos")}}?orden='+this.value);
		@endif
	});
</script>