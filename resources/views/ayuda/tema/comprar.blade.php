@if(($producto->variantes->count() == 0) and ($producto->cantidad == 0))
    <div class="alert alert-danger">
        <p>El producto no tiene stock disponible</p>
    </div>
@elseif(($producto->variantes->count() > 0) and ($producto->variantes->where('cantidad', '>', '0')->count() == 0))
    <div class="alert alert-danger">
        <p>El producto no tiene stock disponible</p>
    </div>
@else
    <div class="col-12">
        <input type="hidden" name="producto" id="producto" value="{{$producto->id}}" >
        <input type="hidden" name="variante" id="variante">

        @php
            $variantes      = $producto->variantes;
            $valores        = collect();

            foreach($variantes->where('cantidad', '>', '0') as $variante)
                $valores->push($variante->valores[0]->id);

            $valores = $valores->unique();

            $i = 0;
        @endphp

        @if($variantes->count() > 0)
            @foreach($producto->atributos as $atributo)
                <div class="row">
                    <div class="col-3">
                        <small class="mr-2">
                            {{$atributo->nombre}}:
                        </small>
                    </div>
                    <div class="col-9">
                        @foreach($atributo->valores as $valor)
                            <h2 id      = "valor_{{$valor->id}}"
                                class   = "badge badge-dark btn_valor mr-2" 
                                attr-id = "{{$valor->id}}"
                                style   = "background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%); 
                                            min-width: 4em;
                                            cursor: pointer;
                                            @if(($i > 0) or (!$valores->contains($valor->id)))
                                                cursor: not-allowed;
                                                opacity: .5;
                                            @endif
                                        "
                                @if(($i == 0) and ($valores->contains($valor->id)))
                                    attr-inicial    = "true"
                                    onClick         = "btn_valor('{{$valor->id}}')"
                                @endif
                            >
                                {{$valor->nombre != null ? $valor->nombre : "-"}}
                            </h2>
                        @endforeach
                    </div>
                </div>
                @php $i ++; @endphp
            @endforeach
            <div class="row">
                <div class="col-6">
                    <small id="cantidad_text">
                        Cantidad: --
                    </small>
                </div>
                <div class="col-6 text-right">
                    <small id="limpiar_variantes" style="cursor: pointer;">
                        <i class="fa fa-undo"></i> 
                        Limpiar
                    </small>
                </div>
            </div>
        @endif
        

        <div class="row mt-4">
            <div class="col-4 col-lg-2 mt-2">
                <a class="btn btn-primary btn-block btn_menos">
                    <i class="fa fa-minus"></i>
                </a>
            </div>
            <div class="col-4 col-lg-2 mt-2">
                <input type="number" class="form-control text-center" name="cantidad" id="cantidad" readonly value="1" min="1" 
                @if($producto->variantes->count() > 0)
                    disabled
                @else
                    @if($producto->cantidad != null)
                        max="{{$producto->cantidad}}"
                    @endif
                @endif
                >
            </div>    
            <div class="col-4 col-lg-2 mt-2">
                <a class="btn btn-primary btn-block btn_mas">
                    <i class="fa fa-plus"></i>
                </a>
            </div>
            <div class="col-12 col-lg-6 mt-2">
                <button class="btn btn-primary btn-block btn_agregar_carrito" 
                    @if($producto->variantes->count() > 0)
                        disabled
                    @endif
                >
                    <i class="fa fa-shopping-bag"></i>
                    Agregar al carrito
                </button>
            </div>
        </div>       
    </div>

    <!-- Modal -->
    <div class="modal fade" id="mensaje_compra" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" style="border-radius: 0px!important;">
                <div class="modal-body text-center">
                    <div class="row">
                        <div class="col-12 text-right">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="col-12 mb-4">
                            <i class="fa fa-check-circle fa-3x text-success"></i>
                            <br><br>
                            <h4>Articulo añadido a tu compra</h4>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                        <div class="col-6">
                            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                                Seguir comprando
                            </button>
                        </div>
                        <div class="col-6">
                            <a href="{{url('carrito')}}" class="btn btn-dark btn-block text-white">
                                Ir al carrito
                            </a>   
                        </div>
                    </div>      
                </div>
            </div>
        </div>
    </div>

    <script>
        function btn_valor(data)
        {
            $.get("{{url('variante')}}/"+data, function(result)
            {
                if(result[0] == "valores")
                {
                    $(".btn_valor").attr('onClick', '');

                    $.each(result[1], function(index, elemento)
                    {
                        $('#valor_'+elemento).css('opacity', 1);
                        $('#valor_'+elemento).css('cursor', 'pointer');
                        $('#valor_'+elemento).attr('onClick', 'btn_valor("'+data+'-'+elemento+'")');       
                    });

                }else if(result[0] == "variante")
                {
                    $(".btn_valor").attr('onClick', '');

                    var cantidad    = result[1][0].cantidad;
                    var precio      = result[1][0].precio;
                    var variante    = result[1][0].id;
                    var imagen      = result[1][0].imagen;
                    
                    $('#cantidad_text').html('Cantidad: '+cantidad);

                    if(imagen != null)
                    {
                        $('#imagen_principal').attr('href', '{{asset('')}}/'+imagen);
                        $('#imagen_principal > img').attr('src', '{{asset('')}}/'+imagen);
                    }

                    if(result[1][0].precio != null)
                        $('.precio').html('<b>$'+precio+'</b>');

                    $('#cantidad').prop('max', cantidad);
                    
                    $('.btn_agregar_carrito').prop('disabled', false);
                    $('.btn_mas').removeClass('disabled');
                    $('.btn_menos').removeClass('disabled');
                    $('.btn_comprar_ahora').removeClass('disabled');

                    $('#variante').val(variante);
                }else
                {
                    alert('No hay stock de esta variante');
                }
            })
        }

        function limpiar_variantes()
        {
            $('.precio').html('<b>${{$producto->precio}}</b>');

            $('#cantidad').prop('max', 1);
            $('#cantidad').prop('readonly', true);
            $('#cantidad').val(1);

            $('.btn_agregar_carrito').prop('disabled', true);
            $('.btn_mas').addClass('disabled');
            $('.btn_menos').addClass('disabled');
            $('.btn_comprar_ahora').addClass('disabled');

            $('#variante').val("");

            $('#imagen_principal').attr('href', $('#imagen_principal').attr('attr-href'));
            $('#imagen_principal > img').attr('src', $('#imagen_principal > img').attr('attr-url'));

            $('.btn_valor').each(function(index, elemento)
            {
                var id = $(this).attr('attr-id');

                if($(this).attr('attr-inicial') == "true")
                {
                    $(this).attr('onClick', 'btn_valor('+id+')');
                    $(this).css('opacity', 1);
                    $(this).css('cursor', 'pointer');
                }
                else
                {
                    $(this).css('opacity', .5);
                    $(this).css('cursor', 'not-allowed');
                }
            });
        }

        $('#limpiar_variantes').click(function()
        {
            limpiar_variantes();
        });

        $('.btn_mas').click(function()
        {
            if(parseFloat($('#cantidad').val()) + 1 <= $('#cantidad').attr('max'))
                $('#cantidad').val(parseFloat($('#cantidad').val()) + 1);
        });

        $('.btn_menos').click(function()
        {
            if(parseFloat($('#cantidad').val()) > 1)
                $('#cantidad').val(parseFloat($('#cantidad').val()) - 1);
        });

        $('.btn_agregar_carrito').click(function()
        {
            $.ajax({
                url: "{{url('agregar_carrito')}}",
                dataType: "JSON",
                method: "POST",
                data: {
                    "_token"            : $('meta[name="csrf-token"]').attr('content'),
                    'producto'          : $('#producto').val(),
                    'variante'          : $('#variante').val(),
                    'cantidad'          : $('#cantidad').val(),
                }
            }).done(function()
            {
                $('#mensaje_compra').modal('toggle');

                if($('.numero_productos_carrito').is(':visible'))
                {
                    var cantidad    = parseFloat($('.numero_productos_carrito').html());
                    var productos   = parseFloat($('#cantidad').val());

                    $('.numero_productos_carrito').html(cantidad + productos);
                }else
                {
                    $('.btn_carrito').html("<i class='fa fa-shopping-cart'></i><span class='numero_productos_carrito'>1</span>");
                }

                limpiar_variantes();
            });
        });

    </script>
@endif