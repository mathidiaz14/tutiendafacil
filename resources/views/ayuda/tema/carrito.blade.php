<!-- 
	Llamado del carrito
	********************

	include('ayuda.tema.carrito', ['productos' => $productos])
-->

<div class="col-12 my-4">
	<div class="table table-responsive">
		<table class="table">
			<tbody>
				@foreach ($productos as $producto)
				    <tr>
				    	<td>
				    		@if(isset($producto['variante']) and ($producto['variante']->imagen != null))
				    			<img 	src="{{asset($producto['variante']->imagen)}}" alt="" width="100" 
				    					style="	-webkit-box-shadow: 	1px 1px 2px 0px rgba(0,0,0,0.75);
												-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												object-fit: cover;
											    object-position: center;
											    height: 90%;"	>
				    		@else
				    			<img 	src="{{producto_url_imagen($producto['producto']->id)}}" alt="" width="100" 
				    					style="	-webkit-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
												object-fit: cover;
											    object-position: center;
											    height: 90%;"	>
				    		@endif
				    	</td>
				    	<td class="align-middle">
							<a href="{{producto_url($producto['producto']->id)}}">	
								{{$producto['producto']->nombre}}
								@if($producto['variante'] != null)
									<br>
									<small>
										@foreach($producto['variante']->valores as $valor)
											<h2 id      = "valor_{{$valor->id}}"
				                                class   = "badge badge-dark mr-2" 
				                                attr-id = "{{$valor->id}}"
				                                style   = "background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%);
				                                            min-width: 4em;"
				                            >
				                                {{$valor->nombre != null ? $valor->nombre : "-"}}
				                            </h2>
										@endforeach
									</small>
								@endif
							</a>
				    	</td>
				    	<td class="align-middle">
				    		<b>x {{$producto['cantidad']}}</b>
				    	</td>
				    	<td class="align-middle">
				    		@if(isset($producto['variante']) and ($producto['variante']->precio != null))
				    			<b>${{$producto['variante']->precio * $producto['cantidad']}}</b>
				    		@else
			    				<b>${{producto_precio($producto['producto']->id) * $producto['cantidad']}}</b>
				    		@endif
				    	</td>
				    	<td class="align-middle">
				    		<a href="{{eliminar_producto_carrito($producto['id'])}}" class="btn text-danger">
								<i class="fa fa-trash"></i>
								Eliminar
							</a>
				    	</td>
				    </tr>
				@endforeach
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td><p>Total</p></td>
					<td><p><b>${{productos_precio_total($productos)}}</b></p></td>
					<td></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
<div class="col-6">
	<a href="{{url('vaciar_carrito')}}" class="text-danger">
		<i class="fa fa-trash"></i>
		Vaciar carrito
	</a>
</div>
<div class="col-6 text-right">
	<a href="{{comprar()}}" class="btn btn-primary">
		Continuar con el pago
	</a>
</div>