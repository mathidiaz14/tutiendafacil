<style>
    .cargando
    {
        position: fixed;
        top: 0px;
        left: 0px;
        z-index: 2050;
        width: 100%;
        height: 100%;
        background: #0000009e;
        text-align: center;
    }

    // Ring
    .lds-ring {
      display: inline-block;
      position: relative;
      width: 64px;
      height: 64px;
    }
    .lds-ring div {
      box-sizing: border-box;
      display: block;
      position: absolute;
      width: 51px;
      height: 51px;
      margin: 6px;
      border: 6px solid #00ff9e;
      border-radius: 50%;
      animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
      border-color: #00ff9e transparent transparent transparent;
    }
    .lds-ring div:nth-child(1) {
      animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
      animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
      animation-delay: -0.15s;
    }
    @keyframes lds-ring {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }
</style>

<div class="cargando" style="display: none;">
    <div class="lds-ring">
        <div></div><div></div><div></div><div></div>
    </div>
</div>  

<script>
   
    $('.lds-ring').css("position","absolute");
    $('.lds-ring').css("top", Math.max(0, (($(window).height() - $($('.lds-ring')).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    $('.lds-ring').css("left", Math.max(0, (($(window).width() - $($('.lds-ring')).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
</script>