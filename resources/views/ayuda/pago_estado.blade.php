@switch($venta->pago_estado)
	@case("pendiente")
		<span class="badge badge-danger" style="min-width:15em;">
			Pendiente
		</span>
	@break

	@case("aprobado")
		<span class="badge badge-success" style="min-width:15em;">
			Confirmado
		</span>
	@break

	@case("devuelto")
		<span class="badge badge-warning" style="min-width:15em;">
			Devolución realizada
		</span>
	@break
@endswitch