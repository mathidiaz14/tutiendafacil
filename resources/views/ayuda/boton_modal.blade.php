<!-- Button trigger modal -->
<button type="button" class="btn btn-{{$color}}" data-toggle="modal" data-target="#boton_{{$id}}">
	{!!$icono!!}
	{{$texto_boton}}
</button>

<!-- Modal -->
<div class="modal fade deleteModal" id="boton_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-gradient-{{$color}}">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-center">
				{!!$icono_grande!!}
				<br>
				<br>
				<h4 class="text-secondary">{{$texto_principal}}</h4>
				<br>
				<hr>
				<div class="row">
					<div class="col">
						<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
							NO
						</button>
					</div>
					<div class="col">
						<form action="{{ $ruta }}" method="POST">
							@csrf
							<button class="btn btn-{{$color}} btn-block">
								SI
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
