@switch($venta->pago_metodo)
	@case("tarjeta")
		<span class="badge badge-info" style="min-width:15em;">
			Credito / Debito
		</span>
	@break

	@case("redes")
		<span class="badge badge-secondary" style="min-width:15em;">
			Redes de cobranza
		</span>
	@break

	@case("transferencia")
		<span class="badge badge-dark" style="min-width:15em;">
			Transferencia bancaria
		</span>
	@break

	@case("efectivo")
		<span class="badge badge-primary" style="min-width:15em;">
			Efectivo
		</span>
	@break
@endswitch