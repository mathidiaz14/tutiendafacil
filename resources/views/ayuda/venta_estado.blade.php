@switch($venta->estado)
	@case("comenzado")
		<span class="badge badge-secondary" style="min-width:15em;">
			Comenzado
		</span>
	@break

	@case("en_preparacion")
		<span class="badge badge-info" style="min-width:15em;">
			Preparando producto
		</span>
	@break

	@case("entrega")
		<span class="badge badge-dark" style="min-width:15em;">
			Listo para entregar
		</span>
	@break

	@case("entregado")
		<span class="badge badge-success" style="min-width:15em;">
			Pedido entregado
		</span>
	@break

	@case("cancelado")
		<span class="badge badge-danger" style="min-width:15em;">
			Pedido cancelado
		</span>
	@break

	@case("devuelto")
		<span class="badge badge-warning" style="min-width:15em;">
			Devolución realizada
		</span>
	@break

	@case("pago_aprobado")
		<span class="badge badge-success" style="min-width:15em;">
			Pago confirmado
		</span>
	@break
@endswitch