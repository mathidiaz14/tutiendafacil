<div class="position-fixed bottom-0 right-0 p-3" style="z-index: 5; right: 0; bottom: 0;">
  <div class="ttf_notificacion toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="5000">
    <div class="toast-body">
        <div class="row">
            <div class="col-11">
                <b class="ttf_icono"></b>
                <b class="ttf_titulo"></b>
            </div>
            <div class="col-1">
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="col-12 ttf_contenido"></div>
      </div>
    </div>
  </div>
</div>

<script>
	function ttf_notificacion(titulo, contenido, tipo) 
    {
        $('.ttf_notificacion .ttf_contenido').html(contenido);
        $('.ttf_notificacion .ttf_titulo').html(titulo);

        if(tipo == "exito")
        {
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-danger');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-warning');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-info');
            $('.ttf_notificacion .toast-body').addClass('bg-gradient-success');

            $('.ttf_notificacion .ttf_icono').html('<i class="fa fa-check-circle mr-2"></i> ')
        }

        if(tipo == "warning")
        {
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-danger');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-success');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-info');
            $('.ttf_notificacion .toast-body').addClass('bg-gradient-warning');

            $('.ttf_notificacion .ttf_icono').html('<i class="fa fa-triangle-exclamation mr-2"></i>')
        }

        if(tipo == "error")
        {
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-success');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-warning');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-info');
            $('.ttf_notificacion .toast-body').addClass('bg-gradient-danger');

            $('.ttf_notificacion .ttf_icono').html('<i class="fa fa-times-circle mr-2"></i> ')
        }

        if(tipo == "info")
        {
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-success');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-warning');
            $('.ttf_notificacion .toast-body').removeClass('bg-gradient-danger');
            $('.ttf_notificacion .toast-body').addClass('bg-gradient-info');

            $('.ttf_notificacion .ttf_icono').html('<i class="fa fa-exclamation-circle mr-2"></i>')
        }


		$('.ttf_notificacion').toast('show');
    };

    @if(session('error'))
        ttf_notificacion("Error", "{{session('error')}}", "error");
        {{session()->forget('error')}}
    @endif

    @if(session('exito'))
        ttf_notificacion("Exito", "{{session('exito')}}", "exito");
        {{session()->forget('exito')}}
    @endif

    @if(session('warning'))
        ttf_notificacion("Alvertencia", "{{session('warning')}}", "warning");
        {{session()->forget('warning')}}
    @endif

    @if(session('info'))
        ttf_notificacion("Info", "{{session('info')}}", "info");
        {{session()->forget('info')}}
    @endif

</script>