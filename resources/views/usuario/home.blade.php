@extends("layouts.usuario")

@section('contenido')
	<section class="container mb-5">
		@include('ayuda.alerta_login')
		<div class="row">
			<div class="col-12 detalle mb-5">
				<div class="row bg-dark text-white p-3">
					<div class="col-12 text-center">
						<h5>Mis compras</h5>
					</div>
				</div>
				<div class="row mt-4">
					<div class="col-12">
						@if($cliente->ventas->count() == 0)
							@include('ayuda.sin_registros')
						@else
							<div class="table table-responsive">
								<table class="table table-striped">
									<tr>
										<th>#</th>
										<th>Estado</th>
										<th>Entrega</th>
										<th>Fecha</th>
										<th>Precio</th>
										<th></th>
									</tr>
									@foreach($cliente->ventas as $venta)
										<tr>
											<td>{{$venta->codigo}}</td>
											<td>@include('ayuda.venta_estado')</td>
											<td>@include('ayuda.metodo_envio')</td>
											<td>{{$venta->created_at->format('d/m/Y H:i')}}</td>
											<td>${{$venta->precio - $venta->descuento}}</td>
											<td>
												<a href="{{url('perfil/venta', $venta->id)}}" class="text-dark">
													<i class="fa fa-eye"></i>
													Detalles
												</a>
											</td>
										</tr>
									@endforeach
								</table>
							</div>	
						@endif
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	<script>
		$(".pasos").removeClass('activo');
		$(".pasos.compras").addClass('activo');
	</script>
@endsection