@extends("layouts.usuario")

@section('contenido')
	<section class="container">
		<div class="row">
			<div class="col-12 detalle mb-5">
				<div class="row bg-dark text-white p-3">
					<div class="col-12 text-center">
						<h5>Mis datos</h5>
					</div>
				</div>
				<div class="row mt-4">
					<div class="col-12 col-md-10 offset-md-1">
						@include('ayuda.alerta_login')
						<form action="{{url('perfil/datos')}}" method="post" class="form-horizontal">
							@csrf
							<input type="hidden" name="cliente" value="{{$cliente->id}}">
							<div class="row">
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="">Nombre</label>
										<input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{$cliente->nombre}}">
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="">Apellido</label>
										<input type="text" class="form-control" name="apellido" placeholder="Apellido" value="{{$cliente->apellido}}">
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="">Email</label>
										<input type="email" class="form-control" value="{{$cliente->email}}" disabled>
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="">Documento</label>
										<input type="text" class="form-control" value="{{$cliente->documento}}" name="documento" placeholder="Documento">
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="">Teléfono</label>
										<input type="text" class="form-control" value="{{$cliente->telefono}}" name="telefono" placeholder="Teléfono">
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="">Fecha de nacimiento</label>
										<input type="date" class="form-control" value="{{$cliente->fecha_nacimiento}}" name="fecha_nacimiento" placeholder="Fecha Nacimiento">
									</div>
								</div>
								<div class="col-12 col-md-6">
									<div class="form-group">
										<label for="">Ciudad</label>
										<input type="text" class="form-control" value="{{$cliente->ciudad}}" name="ciudad" placeholder="Ciudad">
									</div>
								</div>
								<div class="col-6 col-md-3">
									<div class="form-group">
										<label for="">Dirección</label>
										<input type="text" class="form-control" value="{{$cliente->direccion}}" name="direccion" placeholder="Dirección">
									</div>
								</div>
								<div class="col-6 col-md-3">
									<div class="form-group">
										<label for="">Apartamento</label>
										<input type="text" class="form-control" value="{{$cliente->apartamento}}" name="apartamento" placeholder="Apartamento">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<hr>
								</div>
								<div class="col-12">
									<div class="form-group">
										<label for="">Cambiar PIN</label>
										<input type="text" class="form-control" name="pin" placeholder="Cambio de PIN">
									</div>
								</div>
								<div class="col-12">
									<hr>
								</div>
								<div class="col-12">
									<div class="form-group text-right">
										<button class="btn btn-dark px-4">
											<i class="fa fa-save"></i>
											Guardar
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	<script>
		$(".pasos").removeClass('activo');
		$(".pasos.datos").addClass('activo');
	</script>
@endsection