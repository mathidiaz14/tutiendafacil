@extends("layouts.usuario")

@section('contenido')	
	<section class="container">
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 detalle mb-5">
				<div class="row bg-dark text-white p-3">
					<div class="col-12 text-center">
						<h5>Iniciar sesión</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-12 p-5">
						@include('ayuda.alerta_login')
						<form action="{{url('perfil')}}" method="post" class="form-horizontal">
							@csrf
							<input type="hidden" name="empresa" value="{{empresa()->id}}">
							<div class="form-group">
								<label for="">Email</label>
								<input type="email" class="form-control" name="email" value="{{old('email')}}" placeholder="Email" required autofocus>
							</div>
							<div class="form-group">
								<label for="">PIN</label>
								<input type="password" class="form-control" name="pin" placeholder="PIN" required>
							</div>
							<div class="form-group mt-5">
								<button class="btn btn-dark btn-block p-3">
									Ingresar
								</button>
							</div>
							<div class="form-group text-right">
								<a class="text-dark" href="{{url('perfil/reenviar/pin')}}">
									Reenviar PIN
								</a>
								<br>
								<a class="text-dark" href="{{url('perfil/nuevo')}}">
									Crear cuenta
								</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection