@extends("layouts.usuario")

@section('contenido')
	<section class="container">
		@include('ayuda.alerta_login')
		<div class="row">
			<div class="col-12 detalle mb-5">
				<div class="row bg-dark text-white p-3">
					<div class="col-12 text-center">
						<h5>Lista de deseos</h5>
					</div>
				</div>
				<div class="row mt-4">
					@if(count($cliente->deseos) == 0)
						<div class="col-12 text-center">
							@include('ayuda.sin_registros')
						</div>
					@else
						<div class="col-12">
							<div class="table table-responsive">
								<table class="table table-striped">
									@foreach($cliente->deseos as $producto)
										<tr>
											<td>
												<img 	src="{{producto_url_imagen($producto->id)}}" alt="" width="100px" 
							    					onerror="this.onerror=null; this.src='{{asset("img/default.jpg")}}'"
							    					style="	-webkit-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
															object-fit: cover;
														    object-position: center;
														    height: 100px;"	>
											</td>
											<td class="align-middle">
												<b>{{$producto->nombre}}</b>
											</td>
											<td class="align-middle">
												<b>${{producto_precio($producto->id)}}</b>
												@if($producto->precio_promocion != null)
													<small style="text-decoration: line-through;">${{$producto->precio}}</small>
												@endif
											</td>
											<td class="align-middle">
												<form action="{{url('perfil/eliminar/deseo')}}" method="post">
													@csrf
													<input type="hidden" name="producto" value="{{$producto->id}}">
													<button class="btn btn-danger">
														<i class="fa fa-trash"></i>
														Eliminar
													</button>
												</form>
											</td>
										</tr>
									@endforeach
								</table>
							</div>	
						</div>
					@endif
				</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
	<script>
		$(".pasos").removeClass('activo');
		$(".pasos.deseos").addClass('activo');
	</script>
@endsection