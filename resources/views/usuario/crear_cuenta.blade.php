@extends("layouts.usuario")

@section('contenido')
	<section class="container">
		<div class="row">
			<div class="col-12 col-md-8 col-lg-6 offset-md-2 offset-lg-3 detalle mb-5">
				<div class="row bg-dark text-white p-3">
					<div class="col-12 text-center">
						<h5>Nueva cuenta</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-12 p-5">
						@include('ayuda.alerta_login')
						<form action="{{url('perfil/nuevo')}}" method="post" class="form-horizontal">
							@csrf
							<input type="hidden" name="empresa" value="{{empresa()->id}}">
							<div class="form-group">
								<label for="">Email</label>
								<input type="email" class="form-control" name="email" placeholder="Email" autofocus required>
							</div>
							<div class="form-group mt-5">
								<button class="btn btn-dark btn-block p-3">
									Enviar
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection