@extends("layouts.usuario")

@section('contenido')
<section class="container mb-5">
	@include('ayuda.alerta_login')
	<div class="row">
		<div class="col-12 detalle mb-5">
			<div class="row bg-dark text-white p-3">
				<div class="col-3">
					<a href="{{url('perfil')}}" class="btn btn-light">
						<i class="fa fa-chevron-left"></i>
						Atras
					</a>
				</div>
				<div class="col-6 text-center">
					<h5>#{{$venta->codigo}}</h5>
				</div>
			</div>
			<div class="row mt-4">
				<div class="col-12 col-md-10 offset-md-1">
					<p><b>Datos</b></p>
					<div class="row">
						<div class="col-4">
							Fecha:
						</div>
						<div class="col-8">
							<b>{{$venta->created_at->format('d/m/Y - H:i')}}</b>
						</div>
					</div>
					<div class="row">
						<div class="col-4">
							Estado:
						</div>
						<div class="col-8">
							@include('ayuda.venta_estado')
						</div>
					</div>
					<div class="row">
						<div class="col-4">
							Metodo Pago:
						</div>
						<div class="col-8">
							@include('ayuda.pago_metodo')
						</div>
					</div>
					<div class="row">
						<div class="col-4">
							Estado Pago:
						</div>
						<div class="col-8">
							@include('ayuda.pago_estado')
						</div>
					</div>

					@if($venta->mp_id != null)
					<div class="row">
						<div class="col-4">
							Codigo MercadoPago:
						</div>
						<div class="col-8">
							<b>{{$venta->mp_id}}</b>
						</div>
					</div>
					@endif

					<div class="row">
						<div class="col-4">
							Entrega:
						</div>
						<div class="col-8">
							@include('ayuda.metodo_envio')
						</div>
					</div>
					@if($venta->observacion != null)
					<div class="row">
						<div class="col-4">
							Observación:
						</div>
						<div class="col-8">
							<b>{{$venta->observacion}}</b>		
						</div>
					</div>
					@endif
				</div>	

				<div class="col-12 col-md-10 offset-md-1">
					<br>
					<div class="row">
						<div class="col-4">
							Nombre:
						</div>
						<div class="col-8">
							<b>{{$venta->cliente->nombre}}</b>
						</div>
					</div>
					<div class="row">
						<div class="col-4">
							Apellido:
						</div>
						<div class="col-8">
							<b>{{$venta->cliente->apellido}}</b>
						</div>
					</div>
					<div class="row">
						<div class="col-4">
							Email:
						</div>
						<div class="col-8">
							<b>{{$venta->cliente->email}}</b>
						</div>
					</div>

					@if($venta->cliente->telefono != null)
					<div class="row">
						<div class="col-4">
							Telefono:			
						</div>
						<div class="col-8">
							<b>{{$venta->cliente->telefono}}</b>	
						</div>
					</div>
					@endif

					@if($venta->cliente->ciudad != null)
					<div class="row">
						<div class="col-4">
							Ciudad:			
						</div>
						<div class="col-8">
							<b>{{$venta->cliente->ciudad}}</b>
						</div>
					</div>
					@endif

					@if($venta->cliente->direccion != null)
					<div class="row">
						<div class="col-4">
							Dirección:			
						</div>
						<div class="col-8">
							<b>{{$venta->cliente->direccion}}</b>
						</div>
					</div>
					@endif

					@if($venta->cliente->apartamento != null)
					<div class="row">
						<div class="col-4">
							Apartamento:			
						</div>
						<div class="col-8">
							<b>{{$venta->cliente->apartamento}}</b>
						</div>
					</div>
					@endif


					@if($venta->cliente_observacion != null)
					<div class="row">
						<div class="col-4">
							Observación:		
						</div>
						<div class="col-8">
							<b>{{$venta->cliente_observacion}}</b>
						</div>
					</div>
					@endif
				</div>

				<div class="col-12">
					<hr>
				</div>

				<div class="col-12 col-md-10 offset-md-1">
					<p><b>Productos</b></p>
					<div class="table table-responsive">
						<table class="table table-striped">
							<tbody>
								@foreach($venta->productos as $producto)
								<tr>
									<td>
										@if(isset($producto->pivot->variante_id))
										<img 	src="{{asset(producto_variante($producto->pivot->variante_id)->imagen)}}" alt="" width="50px" 
										style="	-webkit-box-shadow: 	1px 1px 2px 0px rgba(0,0,0,0.75);
										-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
										box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
										object-fit: cover;
										object-position: center;
										height: 50px;"	>
										@else
										<img 	src="{{producto_url_imagen($producto->id)}}" alt="" width="50px" 
										style="	-webkit-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
										-moz-box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
										box-shadow: 1px 1px 2px 0px rgba(0,0,0,0.75);
										object-fit: cover;
										object-position: center;
										height: 50px;"	>
										@endif
									</td>
									<td>
										<b>{{$producto->nombre}}</b>
										@if($producto->pivot->variante_id != null)
										<br>
										<small>
											@foreach(producto_variante($producto->pivot->variante_id)->valores as $valor)
											<h2 id      = "valor_{{$valor->id}}"
												class   = "badge badge-dark mr-2" 
												attr-id = "{{$valor->id}}"
												style   = "background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%);
												min-width: 4em;
												opacity: .8;"
												>
												{{$valor->nombre != null ? $valor->nombre : "-"}}
											</h2>
											@endforeach
										</small>	
										@endif
									</td>
									<td>x {{$producto->pivot->cantidad}}</td>
									<td>${{$producto->pivot->precio}}</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								@if($venta->descuento != null)
								<tr>
									<td></td>
									<td></td>
									<td>Descuento:</td>
									<td>$ -{{$venta->descuento}}</td>
								</tr>
								@endif
								<tr>
									<td></td>
									<td></td>
									<td>Total:</td>
									<td><b>${{$venta->precio - $venta->descuento}}</b></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('js')
<script>
	$(".pasos").removeClass('activo');
	$(".pasos.compras").addClass('activo');
</script>
@endsection