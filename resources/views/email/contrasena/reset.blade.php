@extends('email.master')

@section('contenido')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0; border-bottom: solid 1px #eeeeee;" align="center">
				{{$contenido['titulo']}}
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<br><br>
				<p>Haga click en el siguiente botón para poder resetear su contraseña.</p>
				<br><br>
				<a href="{{$contenido['url']}}" class="btn btn-primary" style="color:white;">
					Resetear contraseña
				</a>
				<br><br><br>
				<hr>
				<br>
				<small>
					Si no puedes acceder haciendo click en el boton, copia y pega el siguiente link en el navegador
					<br>
					<br>
					{{$contenido['url']}}
				</small>
				
			</td>
		</tr>
	</table>	
@endsection
