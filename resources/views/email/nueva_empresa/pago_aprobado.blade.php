@extends('email.master')

@section('contenido')
	
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0; border-bottom: solid 1px #eeeeee;" align="center">
				Se aprobo el pago de su suscripción
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 50px 0;" align="left">
				<br>
				<p>Hola, <b>{{$contenido['nombre']}}</b>:
					<br><br>
					El pago de tu suscripción ya fue aprobado, puedes ingresar <a href="{{url('/admin')}}">Aquí</a> y disfrutar de nuestra plataforma.
					<br><br>
					Te recomendamos ingresar a <a href="{{url('ayuda')}}" target="_blank">tutiendafacil.uy/ayuda</a> para aprender a utilizar la plataforma, de todas formas estamos a las órdenes para contestar tus dudas.
					<br><br>
					Saludos cordiales.
					<br>
					-El equipo de TuTiendaFacil.uy
				</p>
			</td>
		</tr>
	</table>
@endsection
