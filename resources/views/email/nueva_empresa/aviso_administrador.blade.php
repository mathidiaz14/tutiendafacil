@extends('email.master')

@section('contenido')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;" align="center">
				{{$contenido['titulo']}}
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696;" align="center">
				<table style="width: 80%">
					<tr>
						<td style="text-align: center;">Empresa:</td>
						<td style="text-align: center;"><b>{{$contenido['empresa']->nombre}}</b></td>
					</tr>
					<tr>
						<td style="text-align: center;">Email:</td>
						<td style="text-align: center;"><b>{{$contenido['empresa']->configuracion->email_admin}}</b></td>
					</tr>
					<tr>
						<td style="text-align: center;">Plan:</td>
						<td style="text-align: center;"><b>{{$contenido['empresa']->plan}}</b></td>
					</tr>
					<tr>
						<td style="text-align: center;">Pago:</td>
						<td style="text-align: center;"><b>{{$contenido['empresa']->pago}}</b></td>
					</tr>
					<tr>
						<td style="text-align: center;">URL:</td>
						<td style="text-align: center;"><b>{{$contenido['empresa']->URL}}</b></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
@endsection
