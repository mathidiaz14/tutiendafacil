@extends('email.master')

@section('contenido')
	
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0; border-bottom: solid 1px #eeeeee;" align="center">
				Se rechazo el pago de su suscripción
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 50px 0;" align="left">
				<br>
				<p>Hola, <b>{{$contenido['nombre']}}</b>:
					<br><br>
					El pago de tu suscripción fue rechazado, recomendamos pruebes otra forma de pago.
					<br><br>
					Si tienes algun problema comunicate con nuestro equipo de soporte.
					<br><br>
					Saludos cordiales.
					<br>
					-El equipo de TuTiendaFacil.uy
				</p>
			</td>
		</tr>
	</table>
@endsection
