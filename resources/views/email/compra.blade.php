@extends('email.master')

@section('contenido')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0;" align="center">
				{{$contenido['titulo']}}
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; border-top: solid 1px #eeeeee;">
				<br>
				<table style="width:100%;">
					<tr>
						<td>
							Numero de orden
						</td>
						<td>
							<b>#{{$contenido['venta']->codigo}}</b>
						</td>
					</tr>
					<tr>
						<td>
							Nombre 
						</td>
						<td>
							<b>{{$contenido['venta']->cliente->nombre}}</b>
						</td>
					</tr>
					<tr>
						<td>
							Email 
						</td>
						<td>
							<b>{{$contenido['venta']->cliente->email}}</b>
						</td>
					</tr>
					@if($contenido['venta']->cliente->telefono != null)
						<tr>
							<td>
								Telefono 	
							</td>
							<td>
								<b>{{$contenido['venta']->cliente->telefono}}</b>
							</td>
						</tr>
					@endif
					<tr>
						<td>
							Forma de entrega
						</td>
						<td>
							@if($contenido['venta']->entrega == "retiro")
								<b>Retiro</b>
							@else
								<b>Entrega</b>
							@endif
						</td>
					</tr>
					<tr>
						@if($contenido['venta']->entrega == "retiro")
							<td>
								Retiro en 
							</td>
							<td>
								<b>{{$contenido['venta']->local->nombre}}</b>
							</td>
						@else
							<td>
								Entrega en 
							</td>
							<td>
								<b>{{$contenido['venta']->cliente->direccion}} {{$contenido['venta']->cliente->apartamento}}</b>
							</td>
						@endif
					</tr>

					@if($contenido['venta']->cliente_observacion != null)
						<tr>
							<td>
								Observación
							</td>
							<td>
								<b>{{$contenido['venta']->cliente_observacion}}</b>	
							</td>
						</tr>
					@endif

					<tr>
						<td>
							Estado
						</td>
						<td>
							<b>@include('ayuda.venta_estado', ['venta' => $contenido['venta']])</b>
						</td>
					</tr>
				</table>

				<h3 style="margin:40px 0;">Productos:</h3>
				
				<table style="width:100%;">
					<tbody>
						@foreach($contenido['venta']->productos as $producto)
						    <tr>
						    	<td style="padding-right: 20px;">
						    		<p>
						    			<b>{{$producto->nombre}}</b>
						    			@if($producto->pivot->variante_id != null)
											<br>
											<small>Variante:</small>
											@foreach(producto_variante($producto->pivot->variante_id)->valores as $valor)
												<small style="background: linear-gradient(45deg, {{$valor->color1}} 50%, {{$valor->color2}} 50%); padding: 0 .5em!important; display: inline-block;border-radius: 5px;">
													<b>{{$valor->nombre != null ? $valor->nombre : "-"}}</b>
												</small>	
											@endforeach
										@endif
						    		</p>
						    	</td>
						    	<td>
						    		x {{$producto->pivot->cantidad}}
						    	</td>
						    	<td class="align-middle">
									<p>
										<b>$ {{$producto->pivot->precio}}</b>
									</p>
						    	</td>
						    </tr>
						@endforeach
					</tbody>
					<tfoot>
						@if($contenido['venta']->descuento != null)
						<tr>
							<td></td>
							<td><p><b>Desceunto</b></p></td>
							<td><p><b>$ {{$contenido['venta']->descuento}}</b></p></td>
						</tr>
						@endif
						<tr>
							<td><p><b></b></p></td>
							<td><p><b>Total</b></p></td>
							<td><p><b>$ {{$contenido['venta']->precio - $contenido['venta']->descuento}}</b></p></td>
						</tr>
					</tfoot>
	    		</table>
			</td>
		</tr>
	</table>

	<p style="margin:50px; color:#969696;">
		Puedes ver el comprobante en linea puede ingresar <a href="{{url('eticket',$contenido['venta']->codigo)}}">Aquí</a>
	</p>
@endsection