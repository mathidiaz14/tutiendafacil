@extends('email.master')

@section('contenido')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0; border-bottom: solid 1px #eeeeee;" align="center">
				{{$contenido['titulo']}}
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 90px 0;" align="center">
				<br>
				<p>Respuesta del administrador del sistio</p>
				<br>
				<table style="width:100%; margin-top:50px;">
					<tr>
						<td>
							Error:
						</td>
						<td>
							<a href="{{url('root/error', $contenido['mensaje']->error->id)}}">
								#{{$contenido['mensaje']->error->id}}
							</a>
						</td>
					</tr>
					<tr>
						<td>
							Mensaje:
						</td>
						<td>
							{{$contenido['mensaje']->mensaje}}
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
@endsection