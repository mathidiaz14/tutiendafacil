@extends('email.master')

@section('contenido')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0; border-bottom: solid 1px #eeeeee;" align="center">
				{{$contenido['titulo']}}
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 90px 0;" align="center">
				<table style="width:100%; margin-top:50px;">
					<tr>
						<td>
							Error:
						</td>
						<td>
							<a href="{{url('root/error', $contenido['error']->id)}}">
								#{{$contenido['error']->id}}
							</a>
						</td>
					</tr>
					<tr>
						<td>
							Usuario:
						</td>
						<td>
							{{$contenido['error']->usuario->nombre}}
						</td>
					</tr>
					<tr>
						<td>
							Mensaje:
						</td>
						<td>
							{{$contenido['error']->mensaje}}
						</td>
					</tr>
				</table>
				<a href="{{url('root/error', $contenido['error']->id)}}">
					Ver el error
				</a>
			</td>
		</tr>
	</table>
@endsection