@extends('email.master')

@section('contenido')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0;" align="center">
				{{$contenido['titulo']}}
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 90px 0; border-top: solid 1px #eeeeee;" align="center">
				<br>
				<table>
					<tbody>
						<tr>
							<td>Nombre:</td>
							<td>
								<b>{{$contenido['nombre']}}</b>
							</td>
						</tr>
						<tr>
							<td>Email:</td>
							<td>
								<b>{{$contenido['email']}}</b>
							</td>
						</tr>
						<tr>
							<td>Mensaje:</td>
							<td>
								<b>{{$contenido['mensaje']}}</b>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</table>
	<!-- /// Hero subheader -->
@endsection