@extends('email.master')

@section('contenido')
	<!-- / Hero subheader -->
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0; border-bottom: solid 1px #eeeeee;" align="center">
				Finaliza tu compra
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; padding: 0 0px 30px 0;" align="center">
				<br><br>
				<p>Recuerda que tienes una compra sin finalizar en la pagina {{$contenido['pagina']}}, puedes retomarla haciendo click en el siguiente enlace</p>
				<br><br>
				<a href="{{$contenido['url']}}">Finalizar compra</a>
			</td>
		</tr>
	</table>
	<!-- /// Hero subheader -->
@endsection