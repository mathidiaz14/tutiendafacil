@extends('email.master')

@section('contenido')
	<table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
		<tr>
			<td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 20px 0 15px 0;" align="center">
				Finalizo la carga de las publicaciones en MercadoLibre
			</td>
		</tr>

		<tr>
			<td class="hero-subheader__content" style=" line-height: 27px; color: #969696; border-top: solid 1px #eeeeee;" align="center">
				<br>
				Se publicaron {{$contenido['publicaciones']}} productos
				<br>
			</td>
		</tr>
	</table>
@endsection