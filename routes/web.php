<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ControladorPrincipal;
use App\Http\Controllers\ControladorConfiguracion;
use App\Http\Controllers\ControladorUsuario;
use App\Http\Controllers\ControladorPagina;
use App\Http\Controllers\ControladorNotificacion;
use App\Http\Controllers\ControladorRegistroYPlanes;
use App\Http\Controllers\ControladorLanding;
use App\Http\Controllers\ControladorMensaje;
use App\Http\Controllers\ControladorNewsletter;
use App\Http\Controllers\ControladorTema;
use App\Http\Controllers\ControladorPlugin;
use App\Http\Controllers\ControladorError;
use App\Http\Controllers\ControladorVisita;
use App\Http\Controllers\ControladorMenu;
use App\Http\Controllers\ControladorStock;
use App\Http\Controllers\ControladorCliente;
use App\Http\Controllers\ControladorLocal;
use App\Http\Controllers\ControladorCupon;
use App\Http\Controllers\ControladorCategoria;
use App\Http\Controllers\ControladorProducto;
use App\Http\Controllers\ControladorAtributo;
use App\Http\Controllers\ControladorAtributoValor;
use App\Http\Controllers\ControladorVariante;
use App\Http\Controllers\ControladorVenta;
use App\Http\Controllers\ControladorProveedor;
use App\Http\Controllers\ControladorMultimedia;
use App\Http\Controllers\ControladorPago;
use App\Http\Controllers\ControladorMercadoPago;
use App\Http\Controllers\ControladorMetodoPago;
use App\Http\Controllers\ControladorConfiguracionVisual;
use App\Http\Controllers\ControladorPos;

use App\Http\Controllers\Root\RootControladorEmpresas;
use App\Http\Controllers\Root\RootControladorUsuarios;
use App\Http\Controllers\Root\RootControladorVentas;
use App\Http\Controllers\Root\RootControladorCodigos;
use App\Http\Controllers\Root\RootControladorMensajesError;
use App\Http\Controllers\Root\RootControladorError;
use App\Http\Controllers\Root\RootControladorGit;
use App\Http\Controllers\Root\RootControladorAdministrar;
use App\Http\Controllers\Root\RootControladorAyudaCategoria;
use App\Http\Controllers\Root\RootControladorAyudaEntrada;
use App\Http\Controllers\Root\RootControladorLog;
use App\Http\Controllers\Root\RootControladorTemas;
use App\Http\Controllers\Root\RootControladorCron;
use App\Http\Controllers\Root\RootControladorRespaldo;
use App\Http\Controllers\Root\RootControladorTest;
use App\Http\Controllers\Root\RootControladorSSL;
use App\Http\Controllers\Root\RootControladorComandos;
use App\Http\Controllers\Root\RootControladorComandoSchedule;
use App\Http\Controllers\Root\RootControladorEmail;

use App\Http\Controllers\Plugins\Blog\BlogControladorEntrada;
use App\Http\Controllers\Plugins\Blog\BlogControladorComentario;
use App\Http\Controllers\Plugins\Blog\BlogControladorCategoria;
use App\Http\Controllers\Plugins\Blog\BlogControladorConfiguracion;
use App\Http\Controllers\Plugins\Blog\BlogControladorPrincipal;

use App\Http\Controllers\Plugins\MercadoLibre\MercadoLibreControladorPrincipal;
use App\Http\Controllers\Plugins\MercadoLibre\MercadoLibreControladorProducto;
use App\Http\Controllers\Plugins\MercadoLibre\MercadoLibreControladorConfiguracion;

use App\Http\Controllers\Plugins\Inmobiliaria\InmobiliariaControladorPrincipal;
use App\Http\Controllers\Plugins\Inmobiliaria\InmobiliariaControladorPropiedad;
use App\Http\Controllers\Plugins\Inmobiliaria\InmobiliariaControladorCaracteristica;
use App\Http\Controllers\Plugins\Inmobiliaria\InmobiliariaControladorComodidad;
use App\Http\Controllers\Plugins\Inmobiliaria\InmobiliariaControladorMultimedia;
use App\Http\Controllers\Plugins\Inmobiliaria\InmobiliariaControladorPlano;
use App\Http\Controllers\Plugins\Inmobiliaria\InmobiliariaControladorContacto;

use App\Http\Controllers\Ayuda\ControladorAyudaPrincipal;

Route::get('/', [ControladorPrincipal::class ,'index']);

Route::get('recargar-csrf', function(){
return csrf_token();
});

/****************************************************************/
/* Controladores para registro de empresas y elección de planes */
/****************************************************************/

Route::post('login', 							[ControladorRegistroYPlanes::class ,'login']);
Route::get('registrar', 						[ControladorRegistroYPlanes::class ,'ver_registrarse']);
Route::get('registrarse', 						[ControladorRegistroYPlanes::class ,'ver_registrarse']);
Route::get('empezar', 							[ControladorRegistroYPlanes::class ,'ver_registrarse']);
Route::post('registrarse', 						[ControladorRegistroYPlanes::class ,'registrarse']);
Route::get('envio_contrasena',					[ControladorRegistroYPlanes::class ,'ver_reseteo_contrasena_envio']);
Route::post('envio_contrasena',					[ControladorRegistroYPlanes::class ,'reseteo_contrasena_envio']);
Route::get('resetear/contrasena/{id}',			[ControladorRegistroYPlanes::class ,'ver_reseteo_contrasena']);
Route::post('resetear/contrasena',				[ControladorRegistroYPlanes::class ,'reseteo_contrasena']);
Route::get('registrar/expiro',					[ControladorRegistroYPlanes::class ,'ver_expiro'])->middleware('auth');


/**************************************************/
/* Controladores de administrador de cada empresa */
/**************************************************/

Route::get('/admin', 											[ControladorPrincipal::class ,'dashboard'])->middleware('admin');
Route::resource('admin/visita',									ControladorVisita::class)->middleware('admin');
Route::resource('admin/newsletter', 							ControladorNewsletter::class)->middleware('admin');
Route::resource('admin/usuario',                                ControladorUsuario::class)->middleware('admin');

Route::resource('admin/menu',                                   ControladorMenu::class)->middleware('admin');
Route::post('admin/menu/mover',                                 [ControladorMenu::class, 'mover'])->middleware('admin');

Route::resource('admin/pagina',                                 ControladorPagina::class)->middleware('admin');
Route::get('admin/pagina/eliminar/imagen/{id}',                 [ControladorPagina::class ,'eliminar_imagen'])->middleware('admin');

Route::get('admin/perfil',                                      [ControladorUsuario::class ,'ver_perfil'])->middleware('admin');
Route::post('admin/perfil',                                     [ControladorUsuario::class ,'perfil'])->middleware('admin');

Route::resource('admin/configuracion',                          ControladorConfiguracion::class)->middleware('admin');
Route::get('admin/configuracion/eliminar/logo',                 [ControladorConfiguracion::class ,'eliminar_logo'])->middleware('admin');

Route::get('admin/generar/codigo/',                             [ControladorPrincipal::class ,'generar_codigo'])->middleware('admin');

Route::get('admin/error/soporte',                               [ControladorError::class , 'soporte'])->middleware('admin');
Route::resource('admin/error',                                  ControladorError::class)->middleware('admin');
Route::post('admin/error/responder/{id}',                       [ControladorError::class ,'responder'])->middleware('admin');
Route::get('admin/error/captura/{ID}',                          [ControladorError::class , 'captura'])->middleware('admin');
Route::get('admin/error/adjunto/{ID}',                          [ControladorError::class , 'adjunto'])->middleware('admin');

Route::resource('admin/notificacion',                           ControladorNotificacion::class)->middleware('admin');
Route::get('admin/notificacion/cambiar/pendiente',              [ControladorNotificacion::class, 'pendiente'])->middleware('admin');
Route::get('admin/cargar/notificacion/',                        [ControladorNotificacion::class, 'cargar_notificacion']);

Route::resource('admin/mensaje',                                ControladorMensaje::class)->middleware('admin');
Route::get('admin/mensaje/cambiar/pendiente',                   [ControladorMensaje::class, 'pendiente'])->middleware('admin');
Route::get('admin/cargar/mensaje/',                             [ControladorMensaje::class, 'cargar_mensaje']);

Route::resource('admin/web',                                    ControladorLanding::class)->middleware('admin');
Route::resource('admin/web',                                    ControladorLanding::class)->middleware('admin');
Route::post('admin/web/codigo',                                 [ControladorLanding::class , 'codigo'])->middleware('admin');
Route::post('admin/web/guardar',                                [ControladorLanding::class , 'guardar'])->middleware('admin');

Route::resource('admin/tema',                                   ControladorTema::class)->middleware('admin');
Route::get('admin/tema/instalar/{id}',                          [ControladorTema::class , 'instalar'])->middleware('admin');
Route::get('admin/tema/activar/{id}',                           [ControladorTema::class , 'activar'])->middleware('admin');
Route::get('admin/tema/descargar/{id}',                         [ControladorTema::class , 'descargar'])->middleware('admin');
Route::get('admin/tema/resetear/actual',                        [ControladorTema::class , 'reinstalar'])->middleware('admin');
Route::get('admin/tema/pago/exitoso',                           [ControladorTema::class , 'pago_exitoso'])->middleware('admin');
Route::get('admin/tema/pago/pendiente',                         [ControladorTema::class , 'pago_pendiente'])->middleware('admin');
Route::get('admin/tema/pago/fallido',                           [ControladorTema::class , 'pago_fallido'])->middleware('admin');

Route::resource('admin/plugin',                                 ControladorPlugin::class)->middleware('admin');
Route::post('admin/plugin/pago/{id}',                           [ControladorPlugin::class , 'pago'])->middleware('admin');
Route::get('admin/plugin/instalar/{id}',                        [ControladorPlugin::class , 'instalar'])->middleware('admin');
Route::get('admin/plugin/activar/{id}',                         [ControladorPlugin::class , 'activar'])->middleware('admin');
Route::get('admin/plugin/desactivar/{id}',                      [ControladorPlugin::class , 'desactivar'])->middleware('admin');

Route::resource('admin/visual',                                 ControladorConfiguracionVisual::class)->middleware('admin');

/*************************/
/* Controlador de tienda */
/*************************/

Route::resource('admin/proveedor', 								ControladorProveedor::class)->middleware('admin');
Route::resource('admin/stock', 									ControladorStock::class)->middleware('admin');
Route::resource('admin/cliente', 								ControladorCliente::class)->middleware('admin');
Route::resource('admin/local', 									ControladorLocal::class)->middleware('admin');
Route::post('admin/local/guardar',                              [ControladorLocal::class, 'guardar'])->middleware('admin');

Route::resource('admin/atributo',                               ControladorAtributo::class)->middleware('admin');
Route::resource('admin/atributoValor',                          ControladorAtributoValor::class)->middleware('admin');
Route::resource('admin/variante',                               ControladorVariante::class)->middleware('admin');
Route::resource('admin/producto', 								ControladorProducto::class)->middleware('admin');
Route::get('admin/producto/{ID}/edit/{SECCION}',                [ControladorProducto::class, 'edit'])->middleware('admin');
Route::get('admin/producto/estado/{ID}',                        [ControladorProducto::class, 'cambiar_estado'])->middleware('admin');

Route::resource('admin/multimedia',                             ControladorMultimedia::class)->middleware('admin');
Route::post('admin/multimedia/mover/imagen',                    [ControladorMultimedia::class, 'mover'])->middleware('admin');

Route::resource('admin/categoria', 								ControladorCategoria::class)->middleware('admin');
Route::get('admin/categoria/borrar/imagen/{ID}',				[ControladorCategoria::class ,'borrar_imagen'])->middleware('admin');
Route::get('admin/categoria/estado/{ID}',                       [ControladorCategoria::class ,'cambiar_estado'])->middleware('admin');
Route::post('admin/categoria/mover',                            [ControladorCategoria::class ,'mover'])->middleware('admin');

Route::resource('admin/venta', 					                ControladorVenta::class)->middleware('admin');
Route::get('admin/venta/entregar/{id}',				            [ControladorVenta::class ,'entregar'])->middleware('admin');
Route::get('admin/venta/devolver/{id}',				            [ControladorVenta::class ,'devolver'])->middleware('admin');
Route::get('admin/venta/cancelar/{id}',                         [ControladorVenta::class ,'cancelar'])->middleware('admin');
Route::get('admin/venta/pago/{id}',                             [ControladorVenta::class ,'pago'])->middleware('admin');
Route::get('admin/venta/entrega/{id}',                          [ControladorVenta::class ,'entrega'])->middleware('admin');
Route::get('admin/venta/transferencia/{id}',                    [ControladorVenta::class ,'descargar_transferencia'])->middleware('admin');

Route::resource('admin/cupon',                                  ControladorCupon::class)->middleware('admin');
Route::get('admin/cupon/cambiar/{id}',                          [ControladorCupon::class ,'cambiar'])->middleware('admin');

Route::get('admin/pos/buscar/{ID}',                             [ControladorPOS::class, 'buscar'])->middleware('admin');
Route::post('admin/pos/cliente',                                [ControladorPOS::class, 'buscar_cliente'])->middleware('admin');
Route::get('admin/pos/carrito',                                 [ControladorPOS::class, 'carrito'])->middleware('admin');
Route::resource('admin/pos', 									ControladorPOS::class)->middleware('admin');

Route::get('admin/metodoPago',                                  [ControladorMetodoPago::class , 'index'])->middleware('admin');
Route::get('admin/metodoPago/set/transferencia',                [ControladorMetodoPago::class , 'set_transferencia'])->middleware('admin');
Route::post('admin/metodoPago/save/transferencia',              [ControladorMetodoPago::class , 'save_transferencia'])->middleware('admin');

Route::get('admin/metodoPago/set/efectivo',                     [ControladorMetodoPago::class , 'set_efectivo'])->middleware('admin');

Route::get('admin/mercadopago/desconectar',                     [ControladorMercadoPago::class ,'desconexion'])->middleware('admin');

Route::get('mercadopago/conexion',                              [ControladorMercadoPago::class, 'conexion']);
Route::get('mercadopago/renovar/{ID}',                          [ControladorMercadoPago::class, 'renovar']);
Route::get('mercadopago/webhooks',                              [ControladorMercadoPago::class, 'webhooks']);
Route::get('mercadopago/ipn',                                   [ControladorMercadoPago::class, 'ipn']);
Route::post('mercadopago/webhooks/{ID}',                        [ControladorMercadoPago::class, 'webhooks_id']);


/*************************/
/* Controladores de venta*/
/*************************/

Route::get('checkout/{ID}', 									[ControladorPago::class , 'ver_checkout']);
Route::post('checkout/login/{ID}',                              [ControladorPago::class , 'checkout_login']);
Route::get('checkout/datos/{ID}',                               [ControladorPago::class , 'ver_checkout_datos']);
Route::post('checkout/datos/{ID}', 								[ControladorPago::class , 'checkout_datos']);
Route::get('checkout/entrega/{ID}',                             [ControladorPago::class , 'ver_checkout_entrega']);
Route::post('checkout/entrega/{ID}',                            [ControladorPago::class , 'checkout_entrega']);

Route::get('checkout/pago/{ID}',                                [ControladorPago::class , 'ver_checkout_pago']);
Route::post('checkout/pago/tarjeta', 							[ControladorPago::class , 'checkout_pago_tarjeta']);
Route::post('checkout/pago/redes',                              [ControladorPago::class , 'checkout_pago_redes']);
Route::post('checkout/pago/transferencia',                      [ControladorPago::class , 'checkout_pago_transferencia']);
Route::post('checkout/pago/efectivo',                           [ControladorPago::class , 'checkout_pago_efectivo']);

Route::get('checkout/cancelar/{ID}',                            [ControladorPago::class , 'checkout_cancelar']);

/***********************/
/* Controlador de Blog */
/***********************/

Route::name('blog')->resource('admin/blog/entrada',                     BlogControladorEntrada::class)->middleware(['admin', 'blog']);
Route::name('blog')->resource('admin/blog/comentario',                  BlogControladorComentario::class)->middleware(['admin', 'blog']);
Route::name('blog')->resource('admin/blog/categoria',                   BlogControladorCategoria::class)->middleware(['admin', 'blog']);

/*******************************/
/* Controlador de MercadoLibre */
/*******************************/

Route::get('admin/mercadolibre/configuracion',                          [MercadoLibreControladorConfiguracion::class, 'index'])->middleware(['admin', 'mercadolibre']);
Route::get('admin/mercadolibre/conectar',                               [MercadoLibreControladorConfiguracion::class, 'conectar'])->middleware(['admin', 'mercadolibre']);
Route::get('admin/mercadolibre/desconectar',                            [MercadoLibreControladorConfiguracion::class, 'desconectar'])->middleware(['admin', 'mercadolibre']);

Route::post('admin/mercadolibre/configuracion/importar',                [MercadoLibreControladorConfiguracion::class, 'importar'])->middleware(['admin', 'mercadolibre']);
Route::post('admin/mercadolibre/configuracion/exportar',                [MercadoLibreControladorConfiguracion::class, 'exportar'])->middleware(['admin', 'mercadolibre']);
Route::post('admin/mercadolibre/configuracion/asociar',                 [MercadoLibreControladorConfiguracion::class, 'asociar'])->middleware(['admin', 'mercadolibre']);

Route::get('admin/mercadolibre/configuracion/asociar/categoria',        [MercadoLibreControladorConfiguracion::class, 'get_asociar_categoria'])->middleware(['admin', 'mercadolibre']);
Route::post('admin/mercadolibre/configuracion/asociar/categoria',       [MercadoLibreControladorConfiguracion::class, 'set_asociar_categoria'])->middleware(['admin', 'mercadolibre']);

Route::name('mercadolibre.producto')
->resource('admin/mercadolibre/producto',                               MercadoLibreControladorProducto::class)->middleware(['admin', 'mercadolibre']);

Route::name('mercadolibre')
->resource('admin/mercadolibre',                                        MercadoLibreControladorPrincipal::class)->middleware(['admin', 'mercadolibre']);

Route::get('admin/mercadolibre/conexion',                               [MercadoLibreControladorPrincipal::class, 'conexion'])->middleware('admin');
Route::get('admin/mercadolibre/desconectar',                            [MercadoLibreControladorPrincipal::class ,'desconexion'])->middleware('admin');
Route::get('admin/mercadolibre/webhooks',                               [MercadoLibreControladorPrincipal::class, 'webhooks']);
Route::post('admin/mercadolibre/webhooks/{ID}',                         [MercadoLibreControladorPrincipal::class, 'webhooks_id']);
Route::get('admin/mercadolibre/ipn',                                    [MercadoLibreControladorPrincipal::class, 'ipn']);

/***********************/
/* Controlador de Inmobiliaria */
/***********************/

Route::get('admin/inmobiliaria',                                        [InmobiliariaControladorPrincipal::class, 'index'])->middleware(['admin', 'inmobiliaria']);
Route::resource('admin/inmobiliaria/propiedad',                         InmobiliariaControladorPropiedad::class)->middleware(['admin', 'inmobiliaria']);
Route::resource('admin/inmobiliaria/comodidad',                         InmobiliariaControladorComodidad::class)->middleware(['admin', 'inmobiliaria']);
Route::resource('admin/inmobiliaria/multimedia',                        InmobiliariaControladorMultimedia::class)->middleware(['admin', 'inmobiliaria']);
Route::resource('admin/inmobiliaria/plano',                             InmobiliariaControladorPlano::class)->middleware(['admin', 'inmobiliaria']);
Route::resource('admin/inmobiliaria/contacto',                    		InmobiliariaControladorContacto::class)->middleware(['admin', 'inmobiliaria']);
Route::resource('admin/inmobiliaria/caracteristica',                    InmobiliariaControladorCaracteristica::class)->middleware(['admin', 'inmobiliaria']);
Route::post('admin/inmobiliaria/caracteristica/detach', 				[InmobiliariaControladorCaracteristica::class, 'detach'])->middleware(['admin', 'inmobiliaria']);
Route::post('admin/inmobiliaria/caracteristica/attach', 				[InmobiliariaControladorCaracteristica::class, 'attach'])->middleware(['admin', 'inmobiliaria']);

/**************************/
/* Controladores de Ayuda */
/**************************/

Route::get('ayuda', 												[ControladorAyudaPrincipal::class, 'index']);
Route::get('ayuda/buscar', 											[ControladorAyudaPrincipal::class, 'buscar']);
Route::get('ayuda/entrada/{id}', 									[ControladorAyudaPrincipal::class, 'entrada']);
Route::get('ayuda/categoria/{id}',									[ControladorAyudaPrincipal::class, 'categoria']);


/*************************/
/* Controladores de ROOT */
/*************************/

Route::get('/root',                                             [ControladorPrincipal::class ,'dashboard'])->middleware('root');

Route::name('root')->resource('root/usuario', 		        	RootControladorUsuarios::class)->middleware('root');
Route::name('root')->resource('root/venta', 					RootControladorVentas::class)->middleware('root');
Route::name('root')->resource('root/codigo', 					RootControladorCodigos::class)->middleware('root');
Route::name('root')->resource('root/log',                       RootControladorLog::class)->middleware('root');
Route::name('root')->resource('root/cron',                      RootControladorCron::class)->middleware('root');
Route::name('root')->resource('root/ssl',                       RootControladorSSL::class)->middleware('root');
Route::name('root')->resource('root/respaldo',                  RootControladorRespaldo::class)->middleware('root');
Route::name('root')->resource('root/ayuda',                     RootControladorAyudaCategoria::class)->middleware('root');
Route::name('root')->resource('root/ayuda/entrada', 			RootControladorAyudaEntrada::class)->middleware('root');
Route::name('root')->resource('root/comandos',                  RootControladorComandos::class)->middleware('root');

Route::get('root/comandos/bloquear/{ID}',                       [RootControladorComandos::class, 'bloquear'])->middleware('root');
Route::get('root/comandos/ejecutar/{ID}',                       [RootControladorComandos::class, 'ejecutar'])->middleware('root');

Route::name('root')->resource('root/comandosSchedules',         RootControladorComandoSchedule::class)->middleware('root');
Route::get('root/comandosSchedules/cambiar/{ID}',               [RootControladorComandoSchedule::class, 'cambiar'])->middleware('root');

Route::resource('root/empresa', 								RootControladorEmpresas::class)->middleware('root');
Route::post('root/empresa/controlar/{ID}',                      [RootControladorEmpresas::class , 'control'])->middleware('root');
Route::get('root/empresa/deshabilitar/{ID}',                    [RootControladorEmpresas::class , 'deshabilitar'])->middleware('root');

Route::name('root')->resource('root/tema',                      RootControladorTemas::class)->middleware('root');
Route::name('root.mensaje.')->resource('root/mensaje/error',    RootControladorMensajesError::class)->middleware('root');

Route::name('root')->resource('root/error',                     RootControladorError::class)->middleware('root');
Route::get('root/error/captura/{ID}',							[RootControladorError::class , 'captura'])->middleware('root');
Route::get('root/error/tomar/{ID}',								[RootControladorError::class , 'tomar'])->middleware('root');
Route::get('root/error/resolver/{ID}',							[RootControladorError::class , 'resolver'])->middleware('root');
Route::get('root/error/reabrir/{ID}',							[RootControladorError::class , 'reabrir'])->middleware('root');

Route::get('root/perfil', 										[RootControladorUsuarios::class , 'get_perfil'])->middleware('root');
Route::post('root/perfil', 										[RootControladorUsuarios::class , 'set_perfil'])->middleware('root');

Route::get('root/administrar', 									[RootControladorAdministrar::class , 'index'])->middleware('root');
Route::get('root/administrar/actualizar',                       [RootControladorAdministrar::class , 'actualizar'])->middleware('root');
Route::post('root/administrar/email', 							[RootControladorAdministrar::class , 'email'])->middleware('root');
Route::post('root/administrar/opcion',                          [RootControladorAdministrar::class , 'set_opcion'])->middleware('root');

Route::name('root')->resource('root/email',                     RootControladorEmail::class)->middleware('root');
Route::get('root/email/reenviar/{ID}',                          [RootControladorEmail::class, 'reenviar_email'])->middleware('root');

/************************************************/
/* Controladores de las landing de las empresas */
/************************************************/

Route::get('perfil',                                            [ControladorPrincipal::class , 'get_perfil']);
Route::post('perfil',                                           [ControladorPrincipal::class , 'set_perfil']);
Route::post('perfil/silencioso',                                [ControladorPrincipal::class , 'set_perfil_silencioso']);
Route::get('perfil/reenviar/pin',                               [ControladorPrincipal::class , 'get_reenviar_pin']);
Route::post('perfil/reenviar/pin',                              [ControladorPrincipal::class , 'set_reenviar_pin']);
Route::get('perfil/nuevo',                                      [ControladorPrincipal::class , 'get_crear_cuenta']);
Route::post('perfil/nuevo',                                     [ControladorPrincipal::class , 'set_crear_cuenta']);
Route::get('perfil/venta/{ID}',                                 [ControladorPrincipal::class , 'get_compra']);
Route::get('perfil/datos',                                      [ControladorPrincipal::class , 'get_datos']);
Route::post('perfil/datos',                                     [ControladorPrincipal::class , 'set_datos']);
Route::get('perfil/deseos',                                     [ControladorPrincipal::class , 'get_deseos']);
Route::post('perfil/deseos',                                    [ControladorPrincipal::class , 'set_deseos']);
Route::post('perfil/eliminar/deseo',                            [ControladorPrincipal::class , 'eliminar_deseos']);

Route::get('perfil/logout',                                     [ControladorPrincipal::class , 'perfil_logout']);

Route::get('categoria/{ID}', 									[ControladorPrincipal::class , 'categoria']);
Route::get('productos', 										[ControladorPrincipal::class , 'productos']);
Route::get('producto/{ID}',                                     [ControladorPrincipal::class , 'producto']);
Route::get('variante/{ID}',                                     [ControladorPrincipal::class , 'variante']);
Route::get('carrito', 											[ControladorPrincipal::class , 'carrito']);
Route::get('pagina/{ID}', 										[ControladorPrincipal::class , 'pagina']);
Route::get('comprar', 											[ControladorPrincipal::class , 'comprar']);
Route::post('comprar_ahora/{ID}', 								[ControladorPrincipal::class , 'comprar_ahora']);
Route::post('agregar_carrito', 									[ControladorPrincipal::class , 'agregar_carrito']);
Route::get('eliminar_carrito/{ID}', 							[ControladorPrincipal::class , 'eliminar_carrito']);
Route::get('vaciar_carrito',		 							[ControladorPrincipal::class , 'vaciar_carrito']);
Route::post('contacto', 										[ControladorPrincipal::class , 'contacto']);
Route::post('contacto_empresa', 								[ControladorPrincipal::class , 'contacto_empresa']);
Route::post('visita', 											[ControladorPrincipal::class , 'visita']);
Route::post('newsletter', 										[ControladorPrincipal::class , 'newsletter']);
Route::get('eticket/{ID}', 										[ControladorPrincipal::class , 'eticket']);

Route::get('blog', 												[BlogControladorPrincipal::class , 'index'])->middleware('blog');
Route::get('blog/categoria/{ID}',           					[BlogControladorPrincipal::class , 'categoria'])->middleware('blog');
Route::post('blog/comentario/{ID}',         					[BlogControladorPrincipal::class , 'comentario'])->middleware('blog');
Route::get('blog/{ID}',                     					[BlogControladorPrincipal::class , 'entrada'])->middleware('blog');
Route::post('blog/buscar',                  					[BlogControladorPrincipal::class , 'buscar'])->middleware('blog');

Route::get('inmobiliaria/propiedades/{TIPO?}',					[InmobiliariaControladorPrincipal::class, 'ver_propiedades'])->middleware('inmobiliaria');
Route::get('inmobiliaria/propiedad/{ID}',	        			[InmobiliariaControladorPrincipal::class, 'ver_propiedad'])->middleware('inmobiliaria');
Route::post('inmobiliaria/buscar',			        			[InmobiliariaControladorPrincipal::class, 'buscar_propiedades'])->middleware('inmobiliaria');
Route::post('inmobiliaria/contacto',	            			[InmobiliariaControladorPrincipal::class, 'contacto'])->middleware('inmobiliaria');

Route::get('{ID}', 						            			[ControladorPrincipal::class , 'url']);
Route::get('{ID}/{ID2}',                                        [ControladorPrincipal::class , 'url']);
Route::get('{ID}/{ID2}/{ID3}',                                  [ControladorPrincipal::class , 'url']);